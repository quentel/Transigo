#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                       vw_fiches_signaletiques.py                           #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée un fichier texte par pays, regroupant toutes les données dont nous disposons
# L'ensemble est formaté à la va-vite, et n'a pas d'autres fonctions que de nous permettre de
# vérifier la pertinence des informations dont nous disposons

# 1. IMPORTATION DES MODULES NECESSAIRES
from __future__ import division
import sqlite3, os, math
from time import strftime, strptime
from docx import Document
from docx.shared import Pt, Cm
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH
from functions import *


log("")
log("==============================================")
log("Lancement du script vw_fiches_signaletiques.py")
log("==============================================")

# 2. FONCTIONS UTILES

# 2.1 Recherche dans une table dédiée à la donnée

def connectTable(table,prefixe,pays):
	cur = conn.cursor()						
	cur.execute("SELECT * from "+table+" where "+prefixe+"_ISO_3166_1_alpha_2 = ?", (pays,))
	return cur

# 3. SCRIPT

path = "output/fiches_signaletiques/"

# Si le répertoire n'existe pas, créons-le
fs_dir = os.path.dirname(path)
if not os.path.exists(fs_dir):
	log("Création du répertoire destiné à accueillir nos fiches signalétiques : output/fiches_signaletiques/")
	os.makedirs(fs_dir)

for pays in listePays():
	
	# 3.1. Récupération et formatage des données
	
	# 3.1.1. Superficie
	area_str = ""
	area = getArea(pays)
	area = iAmAnInteger(area) 
	area_s = formatInt(area)
	if area_s == "0":
		area_s = " N/A"
	area_str = " {} km2 (Banque mondiale)".format(area_s)
	
	# 3.1.2. Population
	population_str = ""	
	year = getPopLastYear(pays,"Banque mondiale")
	population = getPop(pays,year,"Banque mondiale")
	population = iAmAnInteger(population)
	population_s = formatInt(population)
	if population_s == "0":
		population_s = " N/A"
	population_str = " {} habitants ({}, {})".format(population_s, "Banque mondiale", year)
	
	# 3.1.3. Densité
	density_str = ""
	try:		
		density = population/area
		density = iAmAFloat(density)
		density_s = formatFloat(density)
	except:
		density_s = " N/A"
	density_str =" {} hab./km2 (pop./superficie)".format(density_s)
	
	# 3.1.4. Croissance démographique annuelle	
	population_growth_str = ""	
	year = getDataLastYear("npg","nations_population_growths",pays)
	population_growth = getDataValue("npg","nations_population_growths",pays,year)	
	population_growth = iAmAFloat(population_growth)
	year = year.replace("/","-")
	population_growth_s = formatFloat(population_growth)
	if population_growth_s == "0,00":		
		population_growth_s = " N/A"
	population_growth_str = " {} % par an ({}, {})".format(population_growth_s, "PNUD", year)
	
	# 3.1.5. Taux de fécondité
	fertility_str = ""
	year = getDataLastYear("nfer","nations_fertility",pays)		
	fertility_list = getDataValueSource("nfer","nations_fertility",pays, year)
	fertility = fertility_list[0]
	source = fertility_list[1]
	fertility = iAmAFloat(fertility)
	if fertility > 1.999:
		enfant_s = "enfants"
	else:
		enfant_s = "enfant"
	fertility_s = formatFloat(fertility)
	if fertility_s == "0,00":		
		fertility_str = " N/A"
	fertility_str = " {} {} par femme (Banque mondiale, {})".format(fertility_s, enfant_s, year)
	
	# 3.1.6. Espérance de vie à la naissance
	life_expectancy_str = ""	
	year = getDataLastYear("nle","nations_life_expectancies",pays)
	life_expectancy = getDataValue("nle","nations_life_expectancies", pays, year)
	life_expectancy = iAmAFloat(life_expectancy)
	life_expectancy_s = formatFloat(life_expectancy)
	if life_expectancy_s == "0,00":		
		life_expectancy_str = " N/A"	
	life_expectancy_str = " {} ans ({}, {})".format(life_expectancy_s, "PNUD", year)	
	
	# 3.1.7. Taux d'alphabétisation
	literacy_rate_str = ""
	years = getDataLastYear("nlr","nations_literacy_rates",pays)
	literacy_rate = getDataValue("nlr","nations_literacy_rates", pays, years)
	literacy_rate = iAmAFloat(literacy_rate)
	literacy_rate_s = formatFloat(literacy_rate)
	years = years.encode("utf-8")
	years = years.replace("–","-")	
	if literacy_rate_s == "0,00":		
		literacy_rate_str = " N/A"
	else:	
		literacy_rate_str = " {} % ({}, {})".format(literacy_rate_s, "PNUD", years)
	
	# 3.1.8. Religions
	
	#3.1.34 Religions
	
	rels = getReligions(pays)
	rels_str = ""
	# En résultat, j'ai une liste de listes
	for rel in rels:
		label = rel[0]
		pop = rel[1]
		year = rel[2]
		total = getReligionsTotal(pays,year)
		try:
			rel_pc = (pop/total)*100	
			rel_pc_s = iAmAFloat(rel_pc)
			rel_pc_s = formatFloat(rel_pc)
		except:
			rel_pc_s = "N/A"	
		# On n'affiche pas le total		
		if label == "Total":
			rel_str = ""
		# On n'affiche pas les langues pratiquées par moins de 1% de la population		
		elif rel_pc < 1:
			rel_str = "" 
		else:
			label = label.encode("utf-8")
			rel_str = " {} ({} %, UNSD, {})".format(label,rel_pc_s,year)
			rels_str = rels_str+","+rel_str
	# Supprimer la virgule initiale, i.e. le premier caractère	
	if rels_str.startswith(", "): rels_str = rels_str[1:]
	if rels_str == "": rels_str = " N/A"
	rels_str = unicode(rels_str, "utf_8")	

	# 3.1.9. Indice de développement humain
	HDI_str = ""
	year = getDataLastYear("nh","nations_hdi",pays)
	HDI_value = getDataValue("nh","nations_hdi", pays,year)
	HDI_rank = getHDIRank(pays, year)
	HDI_value = iAmAFloat(HDI_value)
	HDI_rank = iAmAnInteger(HDI_rank)
	HDI_value_s = formatFloat(HDI_value)
	HDI_rank_s = formatInt(HDI_rank)
	HDI_str = " {} ({}e rang mondial {}, {})".format(HDI_value_s, HDI_rank_s, "PNUD", year)
	
	# 3.1.10. Monnaie
	monnaie_str = ""
	code_monnaie = getCurrencyCode(pays)
	label_monnaie = getCurrencyLabel(pays)
	label_monnaie = label_monnaie.encode("utf-8")
	taux = getLastXChangeRateValue(code_monnaie)
	taux = iAmAFloat(taux)
	if taux > 1.999:
		euro = "euros"
	else:
		euro = "euro"
	# Nous voulons 3 décimales, et non 2, après la virgule
	taux_s = format('%.3f' % taux).replace('.', ',')
	validite = getLastXChangeRateDate(code_monnaie)
	if validite != "N/A":
		validite_obj = strptime(validite, "%Y-%m-%d %H:%M:%S")
		validite_str = strftime("%d-%m-%Y", validite_obj)	
	if code_monnaie == "XXX":
		monnaie_str = " N/A"
	else:
		monnaie_str = " {}. 1 {} = {} {} au {} (taux de chancellerie)".format(label_monnaie, code_monnaie, taux_s, euro, validite_str)
	monnaie_str = unicode(monnaie_str, "utf_8")
	
	# 3.1.11. PIB
	PIB_str = ""	
	year = getDataLastYear("ng","nations_gdp",pays)
	PIBetSource = getDataValueSource("ng","nations_gdp",pays,year)
	PIB = PIBetSource[0]
	source = PIBetSource[1]
	PIB = iAmAnInteger(PIB)
	PIB_s = formatInt(PIB)
	if PIB_s == "0":
		PIB_str = " N/A"
	else:
		PIB_str = " {} USD ({}, {})".format(PIB_s, year, source)
		
	# 3.1.12. PIB par habitant
	PIBpc_str = ""
	year = getDataLastYear("ngc","nations_gdp_per_capita",pays)
	PIBpcetSource = getDataValueSource("ngc","nations_gdp_per_capita",pays, year)
	PIBpc = PIBpcetSource[0]
	source = PIBetSource[1]
	PIBpc = iAmAFloat(PIBpc)
	PIBpc_s = formatFloat(PIBpc)
	if PIBpc_s == "0,00":
		PIBpc_str = " N/A"
	else:
		PIBpc_str = " {} USD ({}, {})".format(PIBpc_s, year, source)
	
	# 3.1.13. Taux de pauvreté
	# Là, les données sont équitablement partagées entre deux sources. Citons l'autre quand l'une fait défaut.	
	
	poverty_str = ""	
	povertyAndYear = getDataValueYear("npov","nations_poverty",pays, unicode("PNUD - Indice multidimensionnel"))
	poverty = povertyAndYear[0]
	year = povertyAndYear[1]
	poverty = iAmAFloat(poverty)
	poverty_s = formatFloat(poverty)
	year = iAmAnInteger(year)
	if poverty_s == "0,00":
		povertyAndYear2 = getDataValueYear("npov","nations_poverty",pays, unicode("Banque mondiale"))
		poverty2 = povertyAndYear2[0]
		year2 = povertyAndYear2[1]
		poverty2 = iAmAFloat(poverty2)		
		poverty2_s = formatFloat(poverty2)
		if poverty2_s == "0,00":
			poverty_str = " N/A"
		else:
			poverty_str = " {} % ({}, {})".format(poverty2_s, year2, "Banque mondiale")	
	else:
		poverty_str = " {} % ({}, {})".format(poverty_s, year, "PNUD - Indice multidimmensionnel")	

	# 3.1.14. Taux de croissance
	PIBgrowth_str = ""	
	year = getDataLastYear("ngg","nations_gdp_growth",pays)
	PIBgrowthetSource = getDataValueSource("ngg","nations_gdp_growth",pays, year)
	PIBgrowth = PIBgrowthetSource[0]
	source = PIBgrowthetSource[1]
	PIBgrowth = iAmAFloat(PIBgrowth)
	PIBgrowth_s = formatFloat(PIBgrowth)
	if PIBgrowth_s == "0,00":
		PIBgrowth_str = " N/A"
	else:
		PIBgrowth_str = " {} % ({}, {})".format(PIBgrowth_s, year, source)	
	
	# 3.1.15. Taux de chômage
	unemployment_str = ""	
	year = getDataLastYear("nu","nations_unemployment",pays)
	unemploymentetSource = getDataValueSource("nu","nations_unemployment",pays, year)
	unemployment = unemploymentetSource[0]
	source = unemploymentetSource[1]
	unemployment = iAmAFloat(unemployment)
	unemployment_s = formatFloat(unemployment)
	if unemployment_s == "0,00":
		unemployment_str = " N/A"
	else:
		unemployment_str = " {} % ({}, {})".format(unemployment_s, year, source)	
	
	# 3.1.16. Déficit budgétaire
	deficit_str = ""	
	year = getDataLastYear("gd","general_gov_deficit",pays)
	deficitetSource = getDataValueSource("gd","general_gov_deficit",pays, year)
	deficit = deficitetSource[0]
	source = deficitetSource[1]
	deficit = iAmAFloat(deficit)
	deficit_s = formatFloat(deficit)
	if deficit_s == "0,00":
		deficit_str = " N/A"
	else:
		deficit_str = " {} % du PIB ({}, {})".format(deficit_s, year, source)
	
	# 3.1.17. Dette publique (dette de l'Etat nette, en % du PIB)
	debt_str = ""	
	year = getDataLastYear("gg","general_gov_net_debt",pays)
	detteetSource = getDataValueSource("gg","general_gov_net_debt",pays, year)
	debt = detteetSource[0]
	source = detteetSource[1]
	debt = iAmAFloat(debt)
	debt_s = formatFloat(debt)
	if debt_s == "0,00":
		debt_str = " N/A"
	else:
		debt_str = " {} % du PIB ({}, {})".format(debt_s, year, source)

	# 3.1.18. Taux d'inflation
	inflation_str = ""
	year = getDataLastYear("nif","nations_inflations",pays)
	inflationetSource = getDataValueSource("nif","nations_inflations",pays, year)
	inflation = inflationetSource[0]
	source = inflationetSource[1]
	inflation = iAmAFloat(inflation)
	inflation_s = formatFloat(inflation)
	if inflation_s == "0,00":
		inflation_str = " N/A"
	else:
		inflation_str = " {} % ({}, {})".format(inflation_s, year, source)
		
	# 3.1.19. Part des principaux secteurs d'activité dans le PIB
	agriculture_str = ""
	industry_str = ""
	services_str = ""	
	year_agri = getDataLastYear("nav","nations_agriculture_value_added",pays)
	agricultureetSource = getDataValueSource("nav","nations_agriculture_value_added",pays, year)
	agriculture = agricultureetSource[0]
	source_agri = agricultureetSource[1]
	agriculture = iAmAFloat(agriculture)
	agriculture_s = formatFloat(agriculture)
	if agriculture_s == "0,00":
		agriculture_str = " agriculture : N/A ,"
	else:
		agriculture_str = " agriculture : {} % ({}, {}), ".format(agriculture_s, year, source_agri)	
	year_indus = getDataLastYear("niv","nations_industry_value_added",pays)
	industryetSource = getDataValueSource("niv","nations_industry_value_added",pays, year)
	industry = industryetSource[0]
	source_indus = agricultureetSource[1]
	industry = iAmAFloat(industry)
	industry_s = formatFloat(industry)
	if industry_s == "0,00":
		industry_str = " industrie : N/A ,"
	else:
		industry_str = " industrie : {} % ({}, {}), ".format(industry_s, year_indus, source_indus)		
	year_serv = getDataLastYear("nsv","nations_services_value_added",pays)
	servicesetSource = getDataValueSource("nsv","nations_services_value_added",pays, year)
	services = servicesetSource[0]
	source_serv = servicesetSource[1]
	services = iAmAFloat(services)
	services_s = formatFloat(services)
	if services_s == "0,00":
		services_str = " services : N/A"
	else:
		services_str = " services : {} % ({}, {})".format(services_s, year_serv, source_serv)
	trio_str = agriculture_str+industry_str+services_str	
	
	# 3.1.20. Importations
	vimportations_str = ""	
	year = getDataLastYear("nim","nations_imports",pays)
	vimportations_list = getDataValueSource("nim","nations_imports",pays, year)
	vimportations = vimportations_list[0]
	vimportations_source = vimportations_list[1]
	vimportations = iAmAnInteger(vimportations)
	vimportations_s = formatInt(vimportations)
	if vimportations_s == "0":
		vimportations_s = " N/A"
	vimportations_str = " {} USD, {}, {}".format(vimportations_s, vimportations_source, year)
		
	# 3.1.21. Exportations
	vexportations_all_str = ""	
	year =  getDataLastYear("nex","nations_exports",pays)
	vexportations_list = getDataValueSource("nex","nations_exports",pays, year)
	vexportations_all = vexportations_list[0]
	vexportations_all_source = vexportations_list[1]
	vexportations_all = iAmAnInteger(vexportations_all)
	vexportations_all_s = formatInt(vexportations_all)
	if vexportations_all_s == "0":
		vexportations_all_s = " N/A"
	vexportations_all_str = " {} USD, {}, {}".format(vexportations_all_s, vexportations_all_source, year)
	
	# 3.1.22. Importations en France de produits du pays
	# On sort l'année courante et l'année précédente
	vimportationsFR_str = ""
	year = getDataLastYear("ki","kiosque_imports",pays)
	allyears = getAllYears("ki","kiosque_imports",pays)
	vimportations = getDataValue("ki","kiosque_imports",pays,year)
	vimportations_i = iAmAnInteger(vimportations)
	vimportations_s = formatInt(vimportations_i)
	# Pour l'année n-2
	donneesdisponibles = len(allyears)
	if donneesdisponibles > 1:
		anneemoinsdeux = allyears[donneesdisponibles-2]	
		vimportationsmoinsdeux = getDataValue("ki", "kiosque_imports", pays,anneemoinsdeux)
		vimportationsmoinsdeux_i = iAmAnInteger(vimportationsmoinsdeux)
		vimportationsmoinsdeux_s = formatInt(vimportationsmoinsdeux_i)
	else:
		vimportationsmoinsdeux_s = "N/A"
	if vimportations_s == "0":
		vimportations_s = " N/A"
	if vimportationsmoinsdeux_s == "0":
		vimportationsmoinsdeux_s = "N/A"
	vimportationsFR_str = " {} euros (DGT, {}), {} euros (DGT, {})".format(vimportations_s, year,vimportationsmoinsdeux_s,anneemoinsdeux)
	
	# 3.1.23. Exportations de produits français vers ce pays
	vexportations_str = ""	
	year = getDataLastYear("ke","kiosque_exports",pays)
	allyears = getAllYears("ke","kiosque_exports",pays)
	vexportations = getDataValue("ke","kiosque_exports",pays,year)
	vexportations_i = iAmAnInteger(vexportations)
	vexportations_s = formatInt(vexportations_i)
	# Pour l'année n-2
	donneesdisponibles = len(allyears)
	if donneesdisponibles > 1:
		anneemoinsdeux = allyears[donneesdisponibles-2]	
		vexportationsmoinsdeux = getDataValue("ke", "kiosque_exports", pays,anneemoinsdeux)
		vexportationsmoinsdeux_i = iAmAnInteger(vexportationsmoinsdeux)
		vexportationsmoinsdeux_s = formatInt(vexportationsmoinsdeux_i)
	else:
		vexportationsmoinsdeux_s = "N/A"
	if vexportations_s == "0":
		vexportations_s = " N/A"
	if vexportationsmoinsdeux_s == "0":
		vexportationsmoinsdeux_s = "N/A"
	vexportationsFR_str = " {} euros (DGT, {}), {} euros (DGT, {})".format(vexportations_s, year,vexportationsmoinsdeux_s,anneemoinsdeux)

	# 3.1.24 Solde commercial
	soldeFR_str = ""
	if vexportations_i == "0" or vimportations_i == "0":
		solde_s = " N/A"
	else:	
		solde = vexportations_i-vimportations_i
		solde_s = formatInt(solde)
	if vexportationsmoinsdeux_i == "0" or vimportationsmoinsdeux_i == "0":	
		soldemoinsdeux_s = " N/A"	
	else:	
		soldemoinsdeux = vexportationsmoinsdeux_i-vimportationsmoinsdeux_i
		soldemoinsdeux_s = formatInt(soldemoinsdeux)
	soldeFR_str = " {} euros ({}), {} euros ({})".format(solde_s,year,soldemoinsdeux_s,anneemoinsdeux)	
		
	# 3.1.25 Nom officiel
	off_str = ""	
	off_name = getOfficial(pays)
	off_name = off_name.encode("utf-8")
	off_str = " {} (Wikipedia)".format(off_name)
	off_str = unicode(off_str, "utf_8")
	
	# 3.1.26 Dirigeants
	
	chef1_str = ""
	chef1_str_titre = ""
	chef1 = getLeaders_1(pays)
	nom_du_chef1 = chef1[0]
	try:
		nom_du_chef1_str = nom_du_chef1.encode("utf-8")
	except:
		nom_du_chef1_str = ""
	role_du_chef1 = chef1[1]
	try:
		role_du_chef1 = role_du_chef1.encode("utf-8")
	except:
		role_du_chef1 = "N/A"
	if nom_du_chef1 == "N/A":
		chef1_str = ""
	if role_du_chef1 == "N/A":
		role1_str = ""
	else:
		role1_str = role_du_chef1	
	chef1_str_titre = " {} :".format(role1_str)
	chef1_str = " {} (Wikipedia)".format(nom_du_chef1_str)	
	chef1_str_titre = unicode(chef1_str_titre, "utf_8")
	chef1_str = unicode(chef1_str, "utf_8")
	
	chef2_str = ""
	chef2_str_titre = ""	
	chef2 = getLeaders_2(pays)
	nom_du_chef2 = chef2[0]
	try:
		nom_du_chef2_str = nom_du_chef2.encode("utf-8")
	except:
		nom_du_chef2_str = ""
	role_du_chef2 = chef2[1]
	try:
		role_du_chef2 = role_du_chef2.encode("utf-8")
	except:
		role_du_chef2 = "N/A"
	if nom_du_chef2 == "N/A":
		chef2_str = ""
	if role_du_chef2 == "N/A":
		role2_str = ""
	else:
		role2_str = role_du_chef2	
	chef2_str_titre = " {} :".format(role2_str)
	chef2_str = " {} (Wikipedia)".format(nom_du_chef2_str)	
	chef2_str_titre = unicode(chef2_str_titre, "utf_8")
	chef2_str = unicode(chef2_str, "utf_8")
	
	chef3_str = ""	
	chef3_str_titre = ""
	role3_str = ""		
	chef3 = getLeaders_3(pays)
	nom_du_chef3 = chef3[0]
	try:
		nom_du_chef3_str = nom_du_chef3.encode("utf-8")
	except:
		nom_du_chef3_str = ""
	role_du_chef3 = chef3[1]
	try:
		role_du_chef3 = role_du_chef3.encode("utf-8")
	except:
		role_du_chef3 = "N/A"
	if nom_du_chef3 == "N/A":
		chef3_str = ""
	if role_du_chef3 == "N/A":
		role3_str = ""
	else:
		role3_str = role_du_chef3	
	chef3_str_titre = " {} :".format(role3_str)
	chef3_str = " {} (Wikipedia)".format(nom_du_chef3_str)	
	chef3_str_titre = unicode(chef3_str_titre, "utf_8")
	chef3_str = unicode(chef3_str, "utf_8")
			
	# 3.1.27 Cinq principaux fournisseurs
	founisseurs_str = ""	
	liste_Fournisseurs = getBestProviders(pays)
	# Commençons par trier le résultat du moins important au plus important
	liste_Fournisseurs.sort(key=lambda fournisseur: fournisseur[1])
	try:
		fournisseur_5 = liste_Fournisseurs[0]
	except:
		fournisseur_5 = "N/A"
	try:	
		fournisseur_4 = liste_Fournisseurs[1]
	except:
		fournisseur_4 = "N/A"
	try:
		fournisseur_3 = liste_Fournisseurs[2]
	except:
		fournisseur_3 = "N/A"
	try:	
		fournisseur_2 = liste_Fournisseurs[3]
	except:
		fournisseur_2 = "N/A"
	try:	
		fournisseur_1 = liste_Fournisseurs[4]
	except:
		fournisseur_1 = "N/A"
	# 1er
	if fournisseur_1 != "N/A":	
		code_pays_1 = fournisseur_1[0]
		nom_pays_1 = deAlpha2versLibelleFR(code_pays_1)
		pdm_pays_1 = fournisseur_1[1]
		annee_pays_1 = fournisseur_1[2]
		source_pays_1 = fournisseur_1[3]
		pdm_pays_1 = iAmAFloat(pdm_pays_1)
		pdm_pays_1 = formatFloat(pdm_pays_1)
		nom_pays_1 = nom_pays_1.encode("utf-8")
		pdm_pays_1 = pdm_pays_1.encode("utf-8")
		source_pays_1 = source_pays_1.encode("utf-8")	
		f1_str = "1er - {} : {} % ({}, {})".format(nom_pays_1, pdm_pays_1, source_pays_1,annee_pays_1) 
		f1_str = unicode(f1_str, "utf_8")	
	else:
		f1_str = ""	
	# 2ème	
	if fournisseur_2 != "N/A":
		code_pays_2 = fournisseur_2[0]
		nom_pays_2 = deAlpha2versLibelleFR(code_pays_2)
		pdm_pays_2 = fournisseur_2[1]
		annee_pays_2 = fournisseur_2[2]
		source_pays_2 = fournisseur_2[3]
		pdm_pays_2 = iAmAFloat(pdm_pays_2)
		pdm_pays_2 = formatFloat(pdm_pays_2)
		nom_pays_2 = nom_pays_2.encode("utf-8")
		pdm_pays_2 = pdm_pays_2.encode("utf-8")
		source_pays_2 = source_pays_2.encode("utf-8")
		f2_str = "2ème - {} : {} % ({}, {})".format(nom_pays_2, pdm_pays_2, source_pays_2, annee_pays_2) 
		f2_str = unicode(f2_str, "utf_8")
	else:
		f2_str = ""	
	# 3ème	
	if fournisseur_3 != "N/A":
		code_pays_3 = fournisseur_3[0]
		nom_pays_3 = deAlpha2versLibelleFR(code_pays_3)
		pdm_pays_3 = fournisseur_3[1]
		annee_pays_3 = fournisseur_3[2]
		source_pays_3 = fournisseur_3[3]
		pdm_pays_3 = iAmAFloat(pdm_pays_3)
		pdm_pays_3 = formatFloat(pdm_pays_3)
		nom_pays_3 = nom_pays_3.encode("utf-8")
		pdm_pays_3 = pdm_pays_3.encode("utf-8")
		source_pays_3 = source_pays_3.encode("utf-8")
		f3_str = "3ème - {} : {} % ({}, {})".format(nom_pays_3, pdm_pays_3, source_pays_3, annee_pays_3) 
		f3_str = unicode(f3_str, "utf_8")	
	else:
		f3_str = ""
	# 4ème	
	if fournisseur_4 != "N/A":	
		code_pays_4 = fournisseur_4[0]
		nom_pays_4 = deAlpha2versLibelleFR(code_pays_4)
		pdm_pays_4 = fournisseur_4[1]
		annee_pays_4 = fournisseur_4[2]
		source_pays_4 = fournisseur_4[3]
		pdm_pays_4 = iAmAFloat(pdm_pays_4)
		pdm_pays_4 = formatFloat(pdm_pays_4)
		nom_pays_4 = nom_pays_4.encode("utf-8")
		pdm_pays_4 = pdm_pays_4.encode("utf-8")
		source_pays_4 = source_pays_4.encode("utf-8")
		f4_str = "4ème - {} : {} % ({}, {})".format(nom_pays_4,pdm_pays_4, source_pays_4,annee_pays_4) 
		f4_str = unicode(f4_str, "utf_8")	
	else:
		f4_str = ""	
	# 5ème
	if fournisseur_5 != "N/A":
		code_pays_5 = fournisseur_5[0]
		nom_pays_5 = deAlpha2versLibelleFR(code_pays_5)
		pdm_pays_5 = fournisseur_5[1]
		annee_pays_5 = fournisseur_5[2]
		source_pays_5 = fournisseur_5[3]
		pdm_pays_5 = iAmAFloat(pdm_pays_5)
		pdm_pays_5 = formatFloat(pdm_pays_5)
		nom_pays_5 = nom_pays_5.encode("utf-8")
		pdm_pays_5 = pdm_pays_5.encode("utf-8")
		source_pays_5 = source_pays_5.encode("utf-8")
		f5_str = "5ème - {} : {} % ({}, {})".format(nom_pays_5, pdm_pays_5, source_pays_5, annee_pays_5) 
		f5_str = unicode(f5_str, "utf_8")
	else:
		f5_str = ""
	fournisseurs_str = f1_str+" ; "+f2_str+" ; "+f3_str+" ; "+f4_str+" ; "+f5_str
		
	# 3.1.28 Cinq principaux clients
	clients_str = ""			
	liste_Clients = getBestClients(pays)
	# Commençons par trier le résultat du moins important au plus important
	liste_Clients.sort(key=lambda client: client[1])
	try:
		client_5 = liste_Clients[0]
	except:
		client_5 = "N/A"
	try:	
		client_4 = liste_Clients[1]
	except:
		client_4 = "N/A"
	try:
		client_3 = liste_Clients[2]
	except:
		client_3 = "N/A"
	try:	
		client_2 = liste_Clients[3]
	except:
		client_2 = "N/A"
	try:	
		client_1 = liste_Clients[4]
	except:
		client_1 = "N/A"
	# 1er
	if client_1 != "N/A":	
		code_pays_1 = client_1[0]
		nom_pays_1 = deAlpha2versLibelleFR(code_pays_1)
		pdm_pays_1 = client_1[1]
		annee_pays_1 = client_1[2]
		source_pays_1 = client_1[3]
		pdm_pays_1 = iAmAFloat(pdm_pays_1)
		pdm_pays_1 = formatFloat(pdm_pays_1)
		nom_pays_1 = nom_pays_1.encode("utf-8")
		pdm_pays_1 = pdm_pays_1.encode("utf-8")
		source_pays_1 = source_pays_1.encode("utf-8")	
		c1_str = "1er - {} : {} % ({}, {})".format(nom_pays_1, pdm_pays_1, source_pays_1,annee_pays_1) 
		c1_str = unicode(c1_str, "utf_8")	
	else:
		c1_str = ""	
	# 2ème	
	if client_2 != "N/A":
		code_pays_2 = client_2[0]
		nom_pays_2 = deAlpha2versLibelleFR(code_pays_2)
		pdm_pays_2 = client_2[1]
		annee_pays_2 = client_2[2]
		source_pays_2 = client_2[3]
		pdm_pays_2 = iAmAFloat(pdm_pays_2)
		pdm_pays_2 = formatFloat(pdm_pays_2)
		nom_pays_2 = nom_pays_2.encode("utf-8")
		pdm_pays_2 = pdm_pays_2.encode("utf-8")
		source_pays_2 = source_pays_2.encode("utf-8")
		c2_str = "2ème - {} : {} % ({}, {})".format(nom_pays_2, pdm_pays_2, source_pays_2, annee_pays_2) 
		c2_str = unicode(c2_str, "utf_8")
	else:
		c2_str = ""	
	# 3ème	
	if client_3 != "N/A":
		code_pays_3 = client_3[0]
		nom_pays_3 = deAlpha2versLibelleFR(code_pays_3)
		pdm_pays_3 = client_3[1]
		annee_pays_3 = client_3[2]
		source_pays_3 = client_3[3]
		pdm_pays_3 = iAmAFloat(pdm_pays_3)
		pdm_pays_3 = formatFloat(pdm_pays_3)
		nom_pays_3 = nom_pays_3.encode("utf-8")
		pdm_pays_3 = pdm_pays_3.encode("utf-8")
		source_pays_3 = source_pays_3.encode("utf-8")
		c3_str = "3ème - {} : {} % ({}, {})".format(nom_pays_3, pdm_pays_3, source_pays_3, annee_pays_3) 
		c3_str = unicode(c3_str, "utf_8")	
	else:
		c3_str = ""
	# 4ème	
	if client_4 != "N/A":	
		code_pays_4 = client_4[0]
		nom_pays_4 = deAlpha2versLibelleFR(code_pays_4)
		pdm_pays_4 = client_4[1]
		annee_pays_4 = client_4[2]
		source_pays_4 = client_4[3]
		pdm_pays_4 = iAmAFloat(pdm_pays_4)
		nom_pays_4 = nom_pays_4.encode("utf-8")
		pdm_pays_4 = formatFloat(pdm_pays_4)
		pdm_pays_4 = pdm_pays_4.encode("utf-8")
		source_pays_4 = source_pays_4.encode("utf-8")
		c4_str = "4ème - {} : {} % ({}, {})".format(nom_pays_4,pdm_pays_4, source_pays_4,annee_pays_4) 
		c4_str = unicode(c4_str, "utf_8")	
	else:
		c4_str = ""	
	# 5ème
	if client_5 != "N/A":
		code_pays_5 = client_5[0]
		nom_pays_5 = deAlpha2versLibelleFR(code_pays_5)
		pdm_pays_5 = client_5[1]
		annee_pays_5 = client_5[2]
		source_pays_5 = client_5[3]
		pdm_pays_5 = iAmAFloat(pdm_pays_5)
		pdm_pays_5 = formatFloat(pdm_pays_5)
		nom_pays_5 = nom_pays_5.encode("utf-8")
		pdm_pays_5 = pdm_pays_5.encode("utf-8")
		source_pays_5 = source_pays_5.encode("utf-8")
		c5_str = "5ème - {} : {} % ({}, {})".format(nom_pays_5, pdm_pays_5, source_pays_5, annee_pays_5) 
		c5_str = unicode(c5_str, "utf_8")
	else:
		c5_str = ""
	clients_str = c1_str+" ; "+c2_str+" ; "+c3_str+" ; "+c4_str+" ; "+c5_str

	# 3.1.29 Nature du régime
	state_form_str = ""	
	state_form = getStateForm(pays)
	state_form = state_form.encode("utf-8")
	state_form_str = " {}  (Wikipedia)".format(state_form)
	state_form_str = unicode(state_form_str, "utf_8")	
	
	# 3.1.30 Capitale
	capitale_str = ""	
	capitale = getCapitale(pays)
	capitale_name = capitale[0]
	capitale_pop = capitale[1]
	capitale_year = capitale[2]
	capitale_name_s = capitale_name.encode("utf-8")
	capitale_year_s = iAmAnInteger(capitale_year)
	capitale_pop = iAmAnInteger(capitale_pop)
	capitale_pop_s = formatInt(capitale_pop)
	if capitale_name == "N/A":
		capitale_str = " N/A"
	else :
		capitale_str = " {} ({} hab., {}, {})".format(capitale_name_s,capitale_pop_s, "UNSD", capitale_year_s)	
	capitale_str = unicode(capitale_str, "utf_8")
	
	# 3.1.31 Langue(s) pratiquée(s)		
	langs = getLanguages(pays)
	langs_str = ""
	# En résultat, j'ai une liste de listes
	for lang in langs:
		label_en = lang[0]
		label = deLangueENversLangueFR(label_en)
		pop = lang[1]
		year = lang[2]
		total = getLanguagesTotal(pays,year)
		try:
			lang_pc = (pop/total)*100	
			lang_pc_s = iAmAFloat(lang_pc)
			lang_pc_s = formatFloat(lang_pc)
		except:
			lang_pc_s = "N/A"	
		
		# On n'affiche pas le total		
		if label in (u"Total"):
			lang_str = ""		
		# On n'affiche pas les langues pratiquées par moins de 1% de la population		
		elif lang_pc < 1:
			lang_str = "" 
		else:
			label = label.encode("utf-8")
			lang_str = " {} ({} %, UNSD, {})".format(label,lang_pc_s,year)
			langs_str = langs_str+","+lang_str
	# Supprimer le " ," initial, i.e. les deux premiers caractères	
	if langs_str.startswith(", "): langs_str = langs_str[1:]
	if langs_str == "": langs_str = " N/A"
	langs_str = unicode(langs_str, "utf_8")	
	
# 3.1.32 Langue(s) officielle(s)		
	langsOff = getOfficialLanguages(pays)
	langsOff = langsOff.encode("utf-8")
	langsOff_str = ""
	if langsOff == "":
		langsOff_str = " N/A"
	else:
		langsOff_str = " {} (Wikipedia)".format(langsOff)
	langsOff_str = unicode(langsOff, "utf-8")
	
	# 3.1.33 Villes principales
	mainTown1_name = unicode()
	mainTown2_name = unicode()
	mainTown3_name = unicode()
	mainTown4_name = unicode()
	mainTown5_name = unicode()
	mainTown1_pop = int()
	mainTown2_pop = int()
	mainTown3_pop = int()
	mainTown4_pop = int()
	mainTown5_pop = int()
	mainTown1_type = str()
	mainTown2_type = str()
	mainTown3_type = str()
	mainTown4_type = str()
	mainTown5_type = str()
	mainTown1_year = str()
	mainTown2_year = str()
	mainTown3_year = str()
	mainTown4_year = str()
	mainTown5_year = str()
	townsList = getTowns(pays)
	try:
		mainTown1 = townsList[0]
		mainTown1_name = mainTown1[0]
		mainTown1_pop = mainTown1[1]
		mainTown1_type = mainTown1[2]
		mainTown1_year = mainTown1[3]
		mainTown1_name_s = mainTown1_name.encode('utf-8')
		mainTown1_pop = iAmAnInteger(mainTown1_pop)
		mainTown1_pop_s = formatInt(mainTown1_pop)
		mainTown1_str = " {} ({} hab., UNSD, {})".format(mainTown1_name_s,mainTown1_pop_s, mainTown1_year)
	except:
		mainTown1_str = ""
		mainTown1_name_s = "N/A"
	try:
		mainTown2 = townsList[1]
		mainTown2_name = mainTown2[0]
		mainTown2_pop = mainTown2[1]
		mainTown2_type = mainTown2[2]
		mainTown2_year = mainTown2[3]
		mainTown2_name_s = mainTown2_name.encode('utf-8')
		mainTown2_pop = iAmAnInteger(mainTown2_pop)
		mainTown2_pop_s = formatInt(mainTown2_pop)
		mainTown2_str = ", {} ({} hab., UNSD, {})".format(mainTown2_name_s,mainTown2_pop_s, mainTown2_year)
	except:
		mainTown2_str = ""
		mainTown2_name_s = "N/A"
	try:	
		mainTown3 = townsList[2]
		mainTown3_name = mainTown3[0]
		mainTown3_pop = mainTown3[1]
		mainTown3_type = mainTown3[2]
		mainTown3_year = mainTown3[3]
		mainTown3_name_s = mainTown3_name.encode('utf-8')
		mainTown3_pop = iAmAnInteger(mainTown3_pop)
		mainTown3_pop_s = formatInt(mainTown3_pop)
		mainTown3_str = ", {} ({} hab., UNSD, {})".format(mainTown3_name_s,mainTown3_pop_s, mainTown3_year)	
	except:
		mainTown3_str = ""
		mainTown3_name_s = "N/A"
	try:
		mainTown4 = townsList[3]
		mainTown4_name = mainTown4[0]
		mainTown4_pop = mainTown4[1]
		mainTown4_type = mainTown4[2]
		mainTown4_year = mainTown4[3]
		mainTown4_name_s = mainTown4_name.encode('utf-8')
		mainTown4_pop = iAmAnInteger(mainTown4_pop)
		mainTown4_pop_s = formatInt(mainTown4_pop)
		mainTown4_str = ", {} ({} hab., UNSD, {})".format(mainTown4_name_s,mainTown4_pop_s, mainTown4_year)	
	except:
		mainTown4_str = ""
		mainTown4_name_s = "N/A"
	try:
		mainTown5 = townsList[4]
		mainTown5_name = mainTown5[0]
		mainTown5_pop = mainTown5[1]
		mainTown5_type = mainTown5[2]
		mainTown5_year = mainTown5[3]
		mainTown5_name_s = mainTown5_name.encode('utf-8')
		mainTown5_pop = iAmAnInteger(mainTown5_pop)
		mainTown5_pop_s = formatInt(mainTown5_pop)
		mainTown5_str = ", {} ({} hab., UNSD, {})".format(mainTown5_name_s,mainTown5_pop_s, mainTown5_year)
	except:
		mainTown5_str = ""
		mainTown5_name_s = "N/A"	
	mainTowns_str = str()	
	if mainTown1_name_s != "N/A":
		mainTowns_str = mainTown1_str
		if mainTown2_name_s != "N/A":
			mainTowns_str = mainTowns_str+mainTown2_str
			if mainTown3_name_s != "N/A":
				mainTowns_str = mainTowns_str+mainTown3_str
				if mainTown4_name_s != "N/A":
					mainTowns_str = mainTowns_str+mainTown4_str
					if mainTown5_name_s != "N/A":
						mainTowns_str = mainTowns_str+mainTown5_str
					else:
						pass					
				else:
					pass
			else:
				pass
		else:
			pass
	else:
		mainTowns_str = " N/A"	

	mainTowns_str = unicode(mainTowns_str, "utf_8")	
	
	# 3.1.34 Agglomérations principales
	
	mainAgglo1_name = unicode()
	mainAgglo2_name = unicode()
	mainAgglo3_name = unicode()
	mainAgglo4_name = unicode()
	mainAgglo5_name = unicode()
	mainAgglo1_pop = int()
	mainAgglo2_pop = int()
	mainAgglo3_pop = int()
	mainAgglo4_pop = int()
	mainAgglo5_pop = int()
	mainAgglo1_type = str()
	mainAgglo2_type = str()
	mainAgglo3_type = str()
	mainAgglo4_type = str()
	mainAgglo5_type = str()
	mainAgglo1_year = str()
	mainAgglo2_year = str()
	mainAgglo3_year = str()
	mainAgglo4_year = str()
	mainAgglo5_year = str()
	agglosList = getAgglomerations(pays)
	try:
		mainAgglo1 = agglosList[0]
		mainAgglo1_name = mainAgglo1[0]
		mainAgglo1_pop = mainAgglo1[1]
		mainAgglo1_type = mainAgglo1[2]
		mainAgglo1_year = mainAgglo1[3]
		mainAgglo1_name_s = mainAgglo1_name.encode('utf-8')
		mainAgglo1_pop = iAmAnInteger(mainAgglo1_pop)
		mainAgglo1_pop_s = formatInt(mainAgglo1_pop)
		mainAgglo1_str = " {} ({} hab., UNSD, {})".format(mainAgglo1_name_s,mainAgglo1_pop_s, mainAgglo1_year)
	except:
		mainAgglo1_str = ""
		mainAgglo1_name_s = "N/A"
	try:
		mainAgglo2 = agglosList[1]
		mainAgglo2_name = mainAgglo2[0]
		mainAgglo2_pop = mainAgglo2[1]
		mainAgglo2_type = mainAgglo2[2]
		mainAgglo2_year = mainAgglo2[3]
		mainAgglo2_name_s = mainAgglo2_name.encode('utf-8')
		mainAgglo2_pop = iAmAnInteger(mainAgglo2_pop)
		mainAgglo2_pop_s = formatInt(mainAgglo2_pop)
		mainAgglo2_str = ", {} ({} hab., UNSD, {})".format(mainAgglo2_name_s,mainAgglo2_pop_s, mainAgglo2_year)
	except:
		mainAgglo2_str = ""
		mainAgglo2_name_s = "N/A"
	try:	
		mainAgglo3 = agglosList[2]
		mainAgglo3_name = mainAgglo3[0]
		mainAgglo3_pop = mainAgglo3[1]
		mainAgglo3_type = mainAgglo3[2]
		mainAgglo3_year = mainAgglo3[3]
		mainAgglo3_name_s = mainAgglo3_name.encode('utf-8')
		mainAgglo3_pop = iAmAnInteger(mainAgglo3_pop)
		mainAgglo3_pop_s = formatInt(mainAgglo3_pop)
		mainAgglo3_str = ", {} ({} hab., UNSD, {})".format(mainAgglo3_name_s,mainAgglo3_pop_s, mainAgglo3_year)	
	except:
		mainAgglo3_str = ""
		mainAgglo3_name_s = "N/A"
	try:
		mainAgglo4 = agglosList[3]
		mainAgglo4_name = mainAgglo4[0]
		mainAgglo4_pop = mainAgglo4[1]
		mainAgglo4_type = mainAgglo4[2]
		mainAgglo4_year = mainAgglo4[3]
		mainAgglo4_name_s = mainAgglo4_name.encode('utf-8')
		mainAgglo4_pop = iAmAnInteger(mainAgglo4_pop)
		mainAgglo4_pop_s = formatInt(mainAgglo4_pop)
		mainAgglo4_str = ", {} ({} hab., UNSD, {})".format(mainAgglo4_name_s,mainAgglo4_pop_s, mainAgglo4_year)	
	except:
		mainAgglo4_str = ""
		mainAgglo4_name_s = "N/A"
	try:
		mainAgglo5 = agglosList[4]
		mainAgglo5_name = mainAgglo5[0]
		mainAgglo5_pop = mainAgglo5[1]
		mainAgglo5_type = mainAgglo5[2]
		mainAgglo5_year = mainAgglo5[3]
		mainAgglo5_name_s = mainAgglo5_name.encode('utf-8')
		mainAgglo5_pop = iAmAnInteger(mainAgglo5_pop)
		mainAgglo5_pop_s = formatInt(mainAgglo5_pop)
		mainAgglo5_str = ", {} ({} hab., UNSD, {})".format(mainAgglo5_name_s,mainAgglo5_pop_s, mainAgglo5_year)
	except:
		mainAgglo5_str = ""
		mainAgglo5_name_s = "N/A"	
	mainAgglos_str = str()	
	if mainAgglo1_name_s != "N/A":
		mainAgglos_str = mainAgglo1_str
		if mainAgglo2_name_s != "N/A":
			mainAgglos_str = mainAgglos_str+mainAgglo2_str
			if mainAgglo3_name_s != "N/A":
				mainAgglos_str = mainAgglos_str+mainAgglo3_str
				if mainAgglo4_name_s != "N/A":
					mainAgglos_str = mainAgglos_str+mainAgglo4_str
					if mainAgglo5_name_s != "N/A":
						mainAgglos_str = mainAgglos_str+mainAgglo5_str
					else:
						pass					
				else:
					pass
			else:
				pass
		else:
			pass
	else:
		mainAgglos_str = " N/A"	

	mainAgglos_str = unicode(mainAgglos_str, "utf_8")	
	
	# 3.2. Construction de la fiche
	
	# Le nom de mon fichier sera de la forme "code pays ISO 2 lettres_test.txt"	
	file_name = pays+"_fs.docx"
	complete_file_name = os.path.join(path,file_name)
	
	# Je crée mon fichier
	fiche_signaletique = Document()
	
	# Nous voulons un document en A4 et des marges étroites
	sections = fiche_signaletique.sections
	for section in sections:	
		section.page_height = Cm(29.7)
		section.page_width = Cm(21)
		section.top_margin = Cm(1)
		section.bottom_margin = Cm(1)
		section.left_margin = Cm(1)
		section.right_margin = Cm(1)
	
	# Création des styles
	styles = fiche_signaletique.styles
	#style.base_style = styles['Normal']
	style1 = styles['Normal']
	font1 = style1.font	
	font1.name = 'FreeSerif'
	font1.size = Pt(10)	
	style2 = styles.add_style('Titre', WD_STYLE_TYPE.PARAGRAPH)
	font2 = style2.font
	font2.size = Pt(18)
	font2.bold = True
	font2.name = 'FreeSerif'	
	style3 = styles.add_style('Titraille 1', WD_STYLE_TYPE.PARAGRAPH)
	font3 = style3.font
	font3.bold = True
	font3.name = 'FreeSerif'		
	
	# Je recupère le nom du pays en Français
	nom_pays = deAlpha2versLibelleFR(pays)

	# Je donne un titre à mon document
	title = fiche_signaletique.add_paragraph(nom_pays, style='Titre')
	title.alignment = WD_ALIGN_PARAGRAPH.CENTER
	
	# 1er paragraphe : données politiques
	ligne1 = unicode("Nom officiel :", "utf-8")
	ligne2 = unicode("Nature du régime :", "utf-8")
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne1).bold = True
	list.add_run(off_str)	
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne2).bold = True
	list.add_run(state_form_str)
	if chef1_str != "" and chef1_str != " N/A (Wikipedia)":
		list = fiche_signaletique.add_paragraph("", style='List Bullet')
		list.add_run(chef1_str_titre).bold = True
		list.add_run(chef1_str)
	if chef2_str != "" and chef2_str != " N/A (Wikipedia)":
		list = fiche_signaletique.add_paragraph("", style='List Bullet')
		list.add_run(chef2_str_titre).bold = True
		list.add_run(chef2_str)
	if chef3_str != "" and chef3_str != " N/A (Wikipedia)":
		list = fiche_signaletique.add_paragraph("", style='List Bullet')
		list.add_run(chef3_str_titre).bold = True
		list.add_run(chef3_str)
		
	# 2ème paragraphe : données géographiques
	ligne0 = unicode("Données géographiques", "utf-8")
	ligne1 = unicode("Superficie :", "utf-8")
	ligne2 = unicode("Capitale :", "utf-8")
	ligne3 = unicode("Villes principales :", "utf-8")
	ligne4 = unicode("Agglomérations principales :", "utf-8")
	ligne5 = unicode("Langue(s) officielle(s) : ", "utf-8")	
	ligne6 = unicode("Langue(s) pratiquée(s) :", "utf-8")
	fiche_signaletique.add_paragraph(ligne0, style='Titraille 1')
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne1).bold = True
	list.add_run(area_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne2).bold = True
	list.add_run(capitale_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne3).bold = True
	list.add_run(mainTowns_str)	
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne4).bold = True
	list.add_run(mainAgglos_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne5).bold = True
	list.add_run(langsOff_str)	
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne6).bold = True
	list.add_run(langs_str)
	
	# 3ème paragraphe : données démographiques
	ligne0 = unicode("Données démographiques", "utf-8")
	ligne1 = unicode("Population :", "utf-8")
	ligne2 = unicode("Densité :", "utf-8")
	ligne3 = unicode("Croissance démographique :", "utf-8")
	ligne4 = unicode("Taux de fécondité :", "utf-8")
	ligne5 = unicode("Espérance de vie à la naissance :", "utf-8")
	ligne6 = unicode("Taux d'alphabétisation :", "utf-8")
	ligne7 = unicode("Religions :", "utf-8")
	ligne8 = unicode("Indice de développement humain :", "utf-8")
	fiche_signaletique.add_paragraph(ligne0, style='Titraille 1')
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne1).bold = True
	list.add_run(population_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne2).bold = True
	list.add_run(density_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne3).bold = True
	list.add_run(population_growth_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne4).bold = True
	list.add_run(fertility_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne5).bold = True
	list.add_run(life_expectancy_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne6).bold = True
	list.add_run(literacy_rate_str)	
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne7).bold = True
	list.add_run(rels_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne8).bold = True
	list.add_run(HDI_str)
	
	# 4ème paragraphe : données économiques
	ligne0 = unicode("Données économiques", "utf-8")
	ligne1 = unicode("Monnaie :", "utf-8")
	ligne2 = unicode("PIB :", "utf-8")
	ligne3 = unicode("PIB par habitant :", "utf-8")
	ligne4 = unicode("Taux de pauvreté :", "utf-8")
	ligne5 = unicode("Taux de croissance :", "utf-8")
	ligne6 = unicode("Taux de chômage :", "utf-8")
	ligne7 = unicode("Déficit budgétaire :", "utf-8")
	ligne8 = unicode("Dette publique :", "utf-8")
	ligne9 = unicode("Taux d'inflation :", "utf-8")
	ligne10 = unicode("Part des principaux secteurs d'activité dans le PIB :", "utf-8")
	ligne11 = unicode("Importations :", "utf-8")
	ligne12 = unicode("Exportations :", "utf-8")
	ligne13 = unicode("Principaux fournisseurs :", "utf-8")
	ligne14 = unicode("Principaux clients :", "utf-8")
	ligne15 = unicode("Importations en France de produits provenant de ce pays :", "utf-8")
	ligne16 = unicode("Exportations françaises vers ce pays :", "utf-8")
	ligne17 = unicode("Solde commercial de la France :", "utf-8")
	fiche_signaletique.add_paragraph(ligne0, style='Titraille 1')
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne1).bold = True
	list.add_run(monnaie_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne2).bold = True
	list.add_run(PIB_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne3).bold = True
	list.add_run(PIBpc_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne4).bold = True
	list.add_run(poverty_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne5).bold = True
	list.add_run(PIBgrowth_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne6).bold = True
	list.add_run(unemployment_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne7).bold = True
	list.add_run(deficit_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne8).bold = True
	list.add_run(debt_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne9).bold = True
	list.add_run(inflation_str)	
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne10).bold = True
	list.add_run(trio_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne11).bold = True
	list.add_run(vimportations_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne12).bold = True
	list.add_run(vexportations_all_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne13).bold = True
	list.add_run(fournisseurs_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne14).bold = True
	list.add_run(clients_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne15).bold = True
	list.add_run(vimportationsFR_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne16).bold = True
	list.add_run(vexportationsFR_str)
	list = fiche_signaletique.add_paragraph("", style='List Bullet')
	list.add_run(ligne17).bold = True
	list.add_run(soldeFR_str)
	# J'enregistre les changements		
	fiche_signaletique.save(complete_file_name)