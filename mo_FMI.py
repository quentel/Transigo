#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                mo_FMI.py                                   #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe les données qui nous intéressent depuis le dernier World Economic Outlook
# (téléchargé grâce au script dl_FMI.py) vers notre base de données (créée avec mo_SQL.py)

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, collections, datetime, os
from functions import log, deAlpha3versAlpha2, listePays

log("")
log("=============================")
log("Lancement du script mo_FMI.py")
log("=============================")

# 2. PARSING DES DONNEES DU FMI
# N.B. : l'exploitation de ces données varie d'un type de données l'autre de 4 à 7 facteurs. Il n'est donc pas très utile
# de créer une fonction unique qui couvrirait tous les cas possibles...

path_fmi  = "datas/fmi/last_WEO.xls"
# En quelle année sommes-nous ?
year_obj = datetime.datetime.now()
# Nous voulons que cette année soit un nombre entier, afin de pouvoir la manipuler arithmétiquement
year_int = int(datetime.datetime.strftime(year_obj,'%Y'))	


# 2.1 PIB AU PRIX COURANT EN DOLLARS (5 DERNIERES ANNEES)
mon_fichier_fmi = open(path_fmi, "rb")
reader_fmi = csv.reader(mon_fichier_fmi, delimiter='\t')
rownum_fmi = 0
compteur = 0
log("Parsing et injection du PIB")			
for row in reader_fmi:
	try:
		# Dans le jargon du FMI, NGDPD correspond au PIB au prix courant en dollars. On ne cherche donc que les lignes portant ce code à la 3ème colonne
		if row[2] == "NGDPD":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche commence il y a six ans			
			i = this_year-5			
			while i < this_year:
			# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					mon_PIB = row[i]
					# On supprime les séparateurs de milliers à l'américaine, qui empêchent une bonne transformation au type float()
					mon_PIB = mon_PIB.replace(",", "")
					# Et le tout est un nombre, pas une chaîne de caractères					
					mon_PIB = float(mon_PIB)
					# Le PIB en USD est toujours donné en milliards. Je le stocke comme tel.
					mon_PIB = mon_PIB*1000000000 
					# Le tout est un entier
					mon_PIB = int(mon_PIB)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					try:
						# Je stocke le tout dans la table nations_gdp
						conn = sqlite3.connect("datas/bases/general.db")
						conn.execute("INSERT INTO nations_gdp (ng_values, ng_sources, ng_years, ng_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(mon_PIB,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass	
	  			# On passe à l'année suivante
				i = i+1
	except Exception, e:
		log("***Erreur sur le parsing du PIB : %s" % e)
		pass
	rownum_fmi = rownum_fmi+1
log("%s PIB entrés dans la base de données" % compteur)
mon_fichier_fmi.close()

# 2.2 SOLDE DE LA BALANCE DES PRIX COURANTS EN % DU PIB (DERNIERE DONNEE DISPONIBLE)
mon_fichier_fmi2 = open(path_fmi, "rb")
reader_fmi2 = csv.reader(mon_fichier_fmi2, delimiter='\t')
rownum_fmi2 = 0
compteur = 0
log("Parsing et injection de la balance des comptes courants")			
for row in reader_fmi2:
	try:
		# Dans le jargon du FMI, BCA_NGDPD correspond au solde de la balance des comptes courants. On ne cherche donc que les lignes portant ce code à la 3ème colonne
		if row[2] == "BCA_NGDPD":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche s'arrête au maximum il y a dix ans			
			borne = this_year-11
			# La recherche commence l'année dernière			
			i = this_year-1
			# Créons un indicateur booléen pour arrêter la recherche quand une donnée est trouvée
			is_Found = False			
			while i > borne and is_Found == False:
				# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					ma_Balance = row[i]
					# C'est un nombre, pas une chaîne de caractères					
					ma_Balance = float(ma_Balance)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					is_Found = True
					try:
						# Je stocke le tout dans la table nations_account_balances
						conn = sqlite3.connect("datas/bases/general.db")
						conn.execute("INSERT INTO nations_account_balances (nca_values, nca_sources, nca_years, nca_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(ma_Balance,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass
				# On passe à l'année suivante
				i = i-1
	except Exception, e:
		log("***Erreur sur le parsing de la balance des comptes courants : %s" % e)
		pass
	rownum_fmi2 = rownum_fmi2+1
log("%s soldes de la balance des comptes courants entrés dans la base de données" % compteur)
mon_fichier_fmi2.close()

# 2.3 PIB PAR HABITANT, EN DOLLARS (5 DERNIERES ANNEES)
mon_fichier_fmi3 = open(path_fmi, "rb")
reader_fmi3 = csv.reader(mon_fichier_fmi3, delimiter='\t')
rownum_fmi3 = 0
compteur = 0
log("Parsing et injection du PIB par tête")
for row in reader_fmi3:
	try:
		# Dans le jargon du FMI, NGDPDPC correspond au PIB/Habitant en dollars. On ne cherche donc que les lignes portant ce code à la 3ème colonne
		if row[2] == "NGDPDPC":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche commence il y a six ans			
			i = this_year-5					
			while i < this_year:
				# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					mon_PIB_par_tete = row[i]
					# On supprime les séparateurs de milliers à l'américaine, qui empêche une bonne transformation au type float()
					mon_PIB_par_tete = mon_PIB_par_tete.replace(",", "")
					# C'est un nombre, pas une chaîne de caractères
					mon_PIB_par_tete = float(mon_PIB_par_tete)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					try:
						# Je stocke le tout dans la table nations_gdp_per_capita
						conn = sqlite3.connect("datas//bases/general.db")
						conn.execute("INSERT INTO nations_gdp_per_capita (ngc_values, ngc_sources, ngc_years, ngc_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(mon_PIB_par_tete,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass
				# On passe à l'année suivante
				i = i+1
	except Exception, e:
		log("***Erreur sur le parsing du PIB par habitant  : %s" % e)
		pass
	rownum_fmi3 = rownum_fmi3+1
log("%s PIB par habitant entrés dans la base de données" % compteur)
mon_fichier_fmi3.close()

# 2.4 POPULATION (DERNIERE DONNEE DISPONIBLE)
mon_fichier_fmi4 = open(path_fmi, "rb")
reader_fmi4 = csv.reader(mon_fichier_fmi4, delimiter='\t')
rownum_fmi4 = 0
compteur = 0
log("Parsing et injection de la population")
for row in reader_fmi4:
	try:
		# Dans le jargon du FMI, LP correspond à la population. On ne cherche donc que les lignes portant ce code à la 3ème colonne
		if row[2] == "LP":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche s'arrête au maximum il y a dix ans			
			borne = this_year-11
			# La recherche commence l'année dernière			
			i = this_year-1
			# Créons un indicateur booléen pour arrêter la recherche quand une donnée est trouvée
			is_Found = False			
			while i > borne and is_Found == False:
				# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					ma_Population = row[i]
					# On supprime les séparateurs de milliers à l'américaine, qui empêche une bonne transformation au type float()
					ma_Population = ma_Population.replace(",", "")
					# C'est un nombre, pas une chaîne de caractères
					ma_Population = float(ma_Population)
					# Ce chiffre est exprimé en million
					ma_Population = ma_Population*1000000
					# Et le résultat est un entier
					ma_Population = int(ma_Population)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					is_Found = True
					try:
						# Je stocke le tout dans la table nations_populations
						conn = sqlite3.connect("datas//bases/general.db")
						conn.execute("INSERT INTO nations_populations (np_values, np_sources, np_years, np_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(ma_Population,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass
				# On passe à l'année suivante
				i = i-1
	except Exception, e:
		log("***Erreur sur le parsing de la population : %s" % e)
		pass
	rownum_fmi4 = rownum_fmi4+1
log("%s populations entrées dans la base de données" % compteur)
mon_fichier_fmi4.close()

# 2.5 TAUX DE CHOMAGE (DERNIERE DONNEE DISPONIBLE)
mon_fichier_fmi5 = open(path_fmi, "rb")
reader_fmi5 = csv.reader(mon_fichier_fmi5, delimiter='\t')
rownum_fmi5 = 0
compteur = 0
log("Parsing et injection du taux de chômage")
for row in reader_fmi5:
	try:
		# Dans le jargon du FMI, LUR correspond au taux de chômage. On ne cherche donc que les lignes portant ce code à la 3ème colonne	
		if row[2] == "LUR":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche s'arrête au maximum il y a dix ans			
			borne = this_year-11
			# La recherche commence l'année dernière			
			i = this_year-1
			# Créons un indicateur booléen pour arrêter la recherche quand une donnée est trouvée
			is_Found = False			
			while i > borne and is_Found == False:				# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					mon_Taux = row[i]
					# On supprime les séparateurs de milliers à l'américaine, qui empêche une bonne transformation au type float()
					mon_Taux = mon_Taux.replace(",", "")
					# C'est un nombre, pas une chaîne de caractères
					mon_Taux = float(mon_Taux)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					is_Found = True
					try:
						# Je stocke le tout dans la table nations_unemployment
						conn = sqlite3.connect("datas/bases/general.db")
						conn.execute("INSERT INTO nations_unemployment (nu_values, nu_sources, nu_years, nu_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(mon_Taux,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass
				# On passe à l'année suivante
				i = i-1
	except Exception, e:
		log("***Erreur sur le parsing du taux de chômage : %s" % e)
		pass
	rownum_fmi5 = rownum_fmi5+1
log("%s taux de chômage entrés dans la base de données" % compteur)
mon_fichier_fmi5.close()

# 2.6 TAUX D'INFLATION (DERNIERE DONNEE DISPONIBLE)
mon_fichier_fmi6 = open(path_fmi, "rb")
reader_fmi6 = csv.reader(mon_fichier_fmi6, delimiter='\t')
rownum_fmi6 = 0
compteur = 0
log("Parsing et injection du taux d'inflation")
for row in reader_fmi6:
	try:
		# Dans le jargon du FMI, PCPIEPCH correspond au taux d'inflation exprimé en % et à la fin de la période considérée. On ne cherche donc que les lignes portant ce code à la 3ème colonne	
		if row[2] == "PCPIEPCH":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche s'arrête au maximum il y a dix ans			
			borne = this_year-11
			# La recherche commence l'année dernière			
			i = this_year-1
			# Créons un indicateur booléen pour arrêter la recherche quand une donnée est trouvée
			is_Found = False			
			while i > borne and is_Found == False:				# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					mon_Taux = row[i]
					# On supprime les séparateurs de milliers à l'américaine, qui empêche une bonne transformation au type float()
					mon_Taux = mon_Taux.replace(",", "")
					# C'est un nombre, pas une chaîne de caractères
					mon_Taux = float(mon_Taux)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					is_Found = True
					try:
						# Je stocke le tout dans la table nations_unemployment
						conn = sqlite3.connect("datas/bases/general.db")
						conn.execute("INSERT INTO nations_inflations (nif_values, nif_sources, nif_years, nif_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(mon_Taux,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass
				# On passe à l'année suivante
				i = i-1
	except Exception, e:
		log("***Erreur sur le parsing du taux d'inflation : %s" % e)
		pass
	rownum_fmi6 = rownum_fmi6+1
log("%s taux de chômage entrés dans la base de données" % compteur)
mon_fichier_fmi6.close()

# 2.7 DETTE PUBLIQUE (NETTE) EN % DU PIB (DERNIERE DONNEE DISPONIBLE)
mon_fichier_fmi7 = open(path_fmi, "rb")
reader_fmi7 = csv.reader(mon_fichier_fmi7, delimiter='\t')
rownum_fmi7 = 0
compteur = 0
log("Parsing et injection du taux d'inflation")
for row in reader_fmi7:
	try:
		# Dans le jargon du FMI, GGXWDN_NGDP correspond à la dette de l'Etat, nette, exprimée en % du PIB. cherche donc que les lignes portant ce code à la 3ème colonne	
		if row[2] == "GGXWDN_NGDP":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche s'arrête au maximum il y a dix ans			
			borne = this_year-11
			# La recherche commence l'année dernière			
			i = this_year-1
			# Créons un indicateur booléen pour arrêter la recherche quand une donnée est trouvée
			is_Found = False			
			while i > borne and is_Found == False:				# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					mon_Taux = row[i]
					# On supprime les séparateurs de milliers à l'américaine, qui empêche une bonne transformation au type float()
					mon_Taux = mon_Taux.replace(",", "")
					# C'est un nombre, pas une chaîne de caractères
					mon_Taux = float(mon_Taux)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					is_Found = True
					try:
						# Je stocke le tout dans la table general_gov_net_debt
						conn = sqlite3.connect("datas/bases/general.db")
						conn.execute("INSERT INTO general_gov_net_debt (gg_values, gg_sources, gg_years, gg_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(mon_Taux,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass
				# On passe à l'année suivante
				i = i-1
	except Exception, e:
		log("***Erreur sur le parsing de la dette publique nette : %s" % e)
		pass
	rownum_fmi7 = rownum_fmi7+1
log("%s taux d'endettement public nets entrés dans la base de données" % compteur)
mon_fichier_fmi7.close()

# 2.8 DEFICIT BUDGETAIRE EN % DU PIB (DERNIERE DONNEE DISPONIBLE)

mon_fichier_fmi8 = open(path_fmi, "rb")
reader_fmi8 = csv.reader(mon_fichier_fmi8, delimiter='\t')
rownum_fmi8 = 0
compteur = 0
log("Parsing et injection du déficit budgétaire")
for row in reader_fmi8:
	try:
		# Dans le jargon du FMI, GGXCNL_NGDP correspond à la dette de l'Etat, nette, exprimée en % du PIB. cherche donc que les lignes portant ce code à la 3ème colonne	
		if row[2] == "GGXCNL_NGDP":
			# On détermine les bornes temporelles de notre recherche			
			this_year = year_int-1971
			# La recherche s'arrête au maximum il y a dix ans			
			borne = this_year-11
			# La recherche commence l'année dernière			
			i = this_year-1
			# Créons un indicateur booléen pour arrêter la recherche quand une donnée est trouvée
			is_Found = False			
			while i > borne and is_Found == False:				# On ne s'embarrasse pas des années pour lesquelles nous n'avons pas de données
				if row[i] != "n/a" and row[i] != "":
					mon_Taux = row[i]
					# On supprime les séparateurs de milliers à l'américaine, qui empêche une bonne transformation au type float()
					mon_Taux = mon_Taux.replace(",", "")
					# C'est un nombre, pas une chaîne de caractères
					mon_Taux = float(mon_Taux)
					mon_annee = i+1971
					mon_pays = deAlpha3versAlpha2(row[1])
					is_Found = True
					try:
						# Je stocke le tout dans la table general_gov_net_debt
						conn = sqlite3.connect("datas/bases/general.db")
						conn.execute("INSERT INTO general_gov_deficit (gd_values, gd_sources, gd_years, gd_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(mon_Taux,unicode("FMI"),mon_annee,mon_pays))
						conn.commit()
						conn.close()
						compteur = compteur+1
					except:
						pass
				# On passe à l'année suivante
				i = i-1
	except Exception, e:
		log("***Erreur sur le parsing du déficit public : %s" % e)
		pass
	rownum_fmi8 = rownum_fmi8+1
log("%s déficits publics entrés dans la base de données" % compteur)
mon_fichier_fmi8.close()

# 3. SUPPRESSION DU FICHIER AINSI EXPLOITE
os.remove(path_fmi)
log("Supression de la copie au format Excel du WEO ainsi exploitée")