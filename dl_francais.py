#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                            dl_francais.py                                  #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge les fichiers ZIP mis à disposition par le ministère des Affaires étrangères et du Développement
# international relatif aux français établis hors de France. Il trouve ces fichiers via le site Opendata du gouvernement français 
# (data.gouv.fr/fr/datasets/donnees-brutes-pour-creation-de-statistiques-sur-les-visas-delivres-par-la-france-de-2006-a-2016/), télécharge cette page pour y chercher les liens pertinents
# télécharge le fichiers .zip qui nous intéresse, ouvre ce conteneur, récupère en son sein les données elles-mêmes, enregistre
# le tout dans un répertoire "visas" et supprime tout le reste de notre disque dur.

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, shutil, urllib2, re, libarchive
from functions import log

log("")
log("==================================")
log("Lancement du script dl_francais.py")
log("==================================")

# 2. SCRIPT

# L'identifiant du jeu de données qui nous intéresse
id_dataset = "582dc787c751df5bb4c0bb7e"

# L'URL qui en découle
URL = "https://www.data.gouv.fr/api/1/datasets/"+id_dataset+"/full/"
# Création du répertoire temporaire destiné à accueillir nos fichiers avant décompression
dl_dir = "datas/francais"
# Si le répertoire n'existe pas, créons-le
if not os.path.exists(dl_dir):
	log("Création du répertoire destiné à accueillir nos fichiers")
	os.makedirs(dl_dir)

# 2.1. Téléchargeons

# On ouvre le fichier contenant le menu
response = urllib2.urlopen(URL)
# On stocke son contenu dans la variable "menu"
menu = response.read()
# Nous rechercherons des URL annoncés par le tag... "url" ! Les lignes se terminent par une accolade fermante. Nous récupérons tout ce qui se trouve entre les deux
pattern = re.compile('url(.*?)}')
# Le résultat est stocké dans la liste "results"
results = pattern.findall(menu)
# Il nous reste à ouvrir notre liste de résultat
for result in results:
	link = result[4:-1]
	# Trouvons le nom du fichier lui-même, après le dernier slash
	index = link.rfind("/")
	# Nous avons besoin du nom du fichier, pour le réenregistrer
	dl_name = link[(index+1):]
	# Si ce fichier se termine par .7z, c'est celui que nous cherchons
	if dl_name.find(".7z") > -1:
		# Nous allons l'enregistrer dans le répertoire tmp, sous son appelation diplomatique							
		dl_zip = dl_dir+"/"+dl_name
		try:
			with open(dl_zip,'w') as f:
				f.write(urllib2.urlopen(link).read())
				f.close()
			log("Téléchargement du fichier "+dl_zip)
		except Exception, e:	
			log("===Echec du téléchargement d'un fichier .7z : %s" % e)
			pass	

# 2.2 Décompactons

# Récupérons le chemin actuel
normal_dir = os.getcwd()
# Listons les fichiers dans le répertoire qui nous intéresse
liste_fichiers = os.listdir(dl_dir)

for fichier in liste_fichiers:
	# Positionnons nous provisoirement dans le répertoire où sont nos fichiers
	os.chdir(dl_dir)
	# Décompactons si nécessaire
	if fichier.find(".7z") > -1:
		libarchive.extract_file(fichier)
		# Supprimons le fichier compacté
		os.remove(fichier)	
	# Revenons à notre répertoire normal
	os.chdir(normal_dir)				
	log("Décompression du fichier "+fichier)

# 2.3. Vérifions s'il reste des fichiers à décompacter (c'est toujours possible)

# Listons les fichiers dans le répertoire qui nous intéresse
liste_fichiers2 = os.listdir(dl_dir)

for fichier2 in liste_fichiers2:
	# Positionnons nous provisoirement dans le répertoire où sont nos fichiers
	os.chdir(dl_dir)
	# Décompactons si nécessaire
	if fichier2.find(".7z") > -1:
		libarchive.extract_file(fichier2)
		# Supprimons le fichier compacté
		os.remove(fichier2)	
	# Revenons à notre répertoire normal
	os.chdir(normal_dir)				
	log("Décompression du fichier "+fichier2)

