#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                mo_BM.py                                    #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe les données qui nous intéressent depuis l'API de la Banque mondiale
# Cf. http://data.worldbank.org/indicator?display=

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, urllib2, datetime
from BeautifulSoup import BeautifulSoup
from functions import log, deAlpha3versAlpha2, listePays

log("")
log("============================")
log("Lancement du script mo_BM.py")
log("============================")

# 1. FONCTIONS

# 1.1. Test de la présence de la donnée dans un flux déjà parsé 

def dateDansRetour(flux_xml):
	try:
		resultat = flux_xml.find("wb:value")
		# Le flux XML de la Banque est parfois mal formaté, et l'output ne peut alors pas être converti en texte. Il faut gérer cela...
		if resultat is None:
			resultat = "1"
			return resultat
		else:
			resultat = resultat.text
			return resultat
	except Exception, e:
		log("***Erreur lors de la vérification de la présence de la donnée : %s" % e)

# 1.2. Parsing du fichier XML accessible par l'URL et le code de la donnée recherchée

def fctXML(worldbank_base_url,annee,code_donnee):
    my_url = worldbank_base_url+code_donnee+"?date="+str(annee)
    try:
    	my_data = urllib2.urlopen(my_url)
    	my_xml = BeautifulSoup(my_data)
    	return my_xml
    except Exception, e:
		log("***Erreur sur le parsing d'un fichier XML : %s" % e)
		pass
    
# 1.3 Détermination de la catégorie banque mondiale, sur la base du RNB

def fctCategory(RNB):
	if RNB < 1046:
		cat = "Bas revenu"
	elif RNB > 1045 and RNB < 4126:
		cat = "Revenu moyen bas"
	elif RNB > 4125 and RNB < 12736:
		cat = "Revenu moyen haut"
	else:
		cat = "Haut revenu"
	return cat
	
# 1.4 Initialisation de la date

# En quelle année sommes-nous ?
year_obj = datetime.datetime.now()
# Nous voulons que cette année soit un nombre entier, afin de pouvoir la manipuler arithmétiquement
year_int = int(datetime.datetime.strftime(year_obj,'%Y'))	

# 1.5 Récupération et stockage de la donnée, sur les cinq dernières années

def fctData5years(fctVar,table,prefixe,code_donnee):
	log("Lancement de la fonction fctData5years pour l'alimentation de la table %s" % table)
	compteur = 0
	for pays in listePays():
		# Pour l'URL de base de la banque mondiale, on veut le code ISO 3166-1 du pays en minuscules
		str_pays = pays.encode('utf8')
		log("5 dernières données - Début du traitement du pays %s" % str_pays)
		pays_min = pays.lower()
		# On construit la base de l'URL
		worldbank_base_url = "http://api.worldbank.org/countries/"+pays_min+"/indicators/"
		# Nous essayons d'ouvrir toutes les pages entre il y a six ans (soit cinq ans avant la dernière donnée pertinente) et l'année dernière
		year = year_int-5
		while year < year_int:
			verif = fctVar(worldbank_base_url,year,code_donnee)
	 		if dateDansRetour(verif) == "1" or None:
 	 			pass
 			else:
 				try:
 					ma_Donnee_xml = fctVar(worldbank_base_url,year,code_donnee)
 					annee = ma_Donnee_xml.find("wb:date").string
 					annee = int(annee)
 					ma_Donnee = ma_Donnee_xml.find("wb:value").string
 					if ma_Donnee == None:
 						pass
 					else:
 						ma_Donnee = float(ma_Donnee)
 						# Je stocke le tout dans la table pertinente
 						try:
 							conn = sqlite3.connect("datas/bases/general.db")
							conn.execute("INSERT INTO "+table+" ("+prefixe+"_values, "+prefixe+"_sources, "+prefixe+"_years, "+prefixe+"_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(ma_Donnee,unicode("Banque mondiale"),annee,pays))
							conn.commit()
							conn.close()
							compteur = compteur+1
 						except Exception, e:
 							log("***Erreur dans la saisie en base de données : %s" %e)
 							pass
 				except Exception, e:
					log("***Erreur sur le parsing : %s" % e)
					pass
			year = year+1	
	log("%s données parsées et injectées dans la base" % compteur)

# 1.6 Récupération et stockage de la dernière donnée disponible

def fctData1year(fctVar,table,prefixe,code_donnee):
	log("Lancement de la fonction fctData1year pour l'alimentation de la table %s" % table)
	compteur = 0
	for pays in listePays():
		# Pour l'URL de base de la banque mondiale, on veut le code ISO 3166-1 du pays en minuscules
		str_pays = pays.encode('utf8')
		log("Dernière donnée disponible - Début du traitement du pays %s" % str_pays)
		pays_min = pays.lower()
		# On construit la base de l'URL
		worldbank_base_url = "http://api.worldbank.org/countries/"+pays_min+"/indicators/"
		year = year_int-1
		# On ne cherche que sur les 10 dernières années	
		borne = year_int-11
		reussite = False
		while year > borne and reussite is False:
			if dateDansRetour(fctVar(worldbank_base_url,year,code_donnee)) == "1" or None:
				year = year-1
 				pass
 			else:
 				try:
 					ma_Donnee_xml = fctVar(worldbank_base_url,year,code_donnee)
 					annee = ma_Donnee_xml.find("wb:date").string
 					annee = int(annee)
 					ma_Donnee = ma_Donnee_xml.find("wb:value").string
 					if ma_Donnee == None:
 						year = year-1
 						pass
 					else:
 						ma_Donnee = float(ma_Donnee)
 						# On stocke le résultat dans la table ad hoc
 						try:
 							conn = sqlite3.connect("datas/bases/general.db")
							conn.execute("INSERT INTO "+table+" ("+prefixe+"_values, "+prefixe+"_sources, "+prefixe+"_years, "+prefixe+"_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(ma_Donnee,unicode("Banque mondiale"),annee,pays))
							conn.commit()
							conn.close()
 							compteur = compteur+1
 							reussite = True
 						except Exception, e:
 							log("***Erreur dans la saisie en base de données : %s" %e)
 							pass
 				except Exception, e:
					log("***Erreur sur le parsing : %s" % e)
					pass
			year = year-1	
	log("%s données parsées et injectées dans la base" % compteur)
	
# 1.7 Récupération et stockage de la dernière donnée disponible dans une table qui existe peut être déjà

def fctData1yearCond(fctVar,table,prefixe,rangee,code_donnee):
	log("Lancement de la fonction fctData1yearCond pour l'alimentation de la table %s" % table)
	compteur = 0
	for pays in listePays():
		# Pour l'URL de base de la banque mondiale, on veut le code ISO 3166-1 du pays en minuscules
		str_pays = pays.encode('utf8')
		log("Dernière donnée disponible - Début du traitement du pays %s" % str_pays)
		pays_min = pays.lower()
		# On construit la base de l'URL
		worldbank_base_url = "http://api.worldbank.org/countries/"+pays_min+"/indicators/"
		year = year_int-1
		# On ne cherche que sur les 10 dernières années	
		borne = year_int-11
		reussite = False
		while year > borne and reussite is False:
			if dateDansRetour(fctVar(worldbank_base_url,year,code_donnee)) == "1" or None:
				year = year-1
 				pass
 			else:
 				try:
 					ma_Donnee_xml = fctVar(worldbank_base_url,year,code_donnee)
 					annee = ma_Donnee_xml.find("wb:date").string
 					annee = int(annee)
 					ma_Donnee = ma_Donnee_xml.find("wb:value").string
 					if ma_Donnee == None:
 						year = year-1
 						pass
 					else:
 						ma_Donnee = float(ma_Donnee)
						# On stocke le tout dans la table ad hoc
 						try:
 							conn = sqlite3.connect("datas/bases/general.db")
							# On vérifie d'abord si ce pays existe déjà dans la table à un autre titre 						 						
							cur = conn.cursor()						
							cur.execute("SELECT * from "+table+" where "+prefixe+"_ISO_3166_1_alpha_2 = ?", (pays,))		
 							if len(cur.fetchall()) == 0:
								# Si le pays n'existe pas déjà dans la table, on le crée							
								conn.execute("INSERT INTO "+table+" ("+rangee+", "+prefixe+"_ISO_3166_1_alpha_2) VALUES (?, ?)",(ma_Donnee,pays))
								conn.commit()
								conn.close()
 							else:
 								# Si le pays existe déjà dans la table, on se contente de l'updater
								conn.execute("UPDATE "+table+" set "+rangee+" = ? where "+prefixe+"_ISO_3166_1_alpha_2 = ?", (ma_Donnee,pays)) 
								conn.commit()
								conn.close()
 							compteur = compteur+1
 							reussite = True
 						except Exception, e:
 							log("***Erreur dans la saisie en base de données : %s" %e)
 							pass
 				except Exception, e:
					log("***Erreur sur le parsing : %s" % e)
					pass
			year = year-1	
	log("%s données parsées et injectées dans la base" % compteur)

	
# 2. SCRIPT

# 2.1 PIB
log("Parsing et injection du PIB")
fctData5years(fctXML,"nations_gdp","ng","NY.GDP.MKTP.CD")

# 2.2 PIB par tête
log("Parsing et injection du PIB par tête")
fctData5years(fctXML,"nations_gdp_per_capita","ngc","NY.GDP.PCAP.CD")

# 2.3 Croissance du PIB
log("Parsing et injection de la croissance du PIB")
fctData5years(fctXML,"nations_gdp_growth","ngg","NY.GDP.MKTP.KD.ZG")

# 2.4 Population
log("Parsing et injection de la population")
fctData1year(fctXML,"nations_populations","np","SP.POP.TOTL")

# 2.5 Taux de chômage
log("Parsing et injection du taux de chômage")
fctData1year(fctXML,"nations_unemployment","nu","9.0.Unemp.All")

# 2.6 Balance des comptes courants
log("Parsing et injection de la balance des comptes courants")
fctData1year(fctXML,"nations_account_balances","nca","BN.CAB.XOKA.GD.ZS")

# 2.7 Inflation
log("Parsing et injection de l'inflation")
fctData1year(fctXML,"nations_inflations","nif","FP.CPI.TOTL.ZG")

# 2.8 Part de l'agriculture dans le PIB
log("Parsing et injection de la part de la valeur ajoutée de l'agriculture dans le PIB")
fctData1year(fctXML,"nations_agriculture_value_added","nav","NV.AGR.TOTL.ZS")

# 2.9 Part de l'industrie dans le PIB
log("Parsing et injection de la part de la valeur ajoutée de l'industrie dans le PIB")
fctData1year(fctXML,"nations_industry_value_added","niv","NV.IND.TOTL.ZS")

# 2.10 Part de l'industrie dans le PIB
log("Parsing et injection de la part de la valeur ajoutée des services dans le PIB")
fctData1year(fctXML,"nations_services_value_added","nsv","NV.SRV.TETC.ZS")

# 2.11 Investissements directs étrangers
log("Parsing et injection des flux d'investissements directs étrangers dans le pays")
fctData1year(fctXML,"nations_fdi","nfdi","BX.KLT.DINV.CD.WD")

# 2.12 Superficie
log("Parsing et injection de la superficie")
fctData1yearCond(fctXML,"nations_datas","nd","nd_area","AG.LND.TOTL.K2") 

# 2.13 Taux de pauvreté
log("Parsing et injection du taux de pauvreté")
fctData1year(fctXML,"nations_poverty","npov","SI.POV.NAHC")

# 2.14 Taux de fécondité
log("Parsing et injection du taux de fécondité")
fctData1year(fctXML,"nations_fertility","nfer","SP.DYN.TFRT.IN")

# 2.9 Un cas particulier : la catégorie Banque mondiale (sur la base du RNB) 		

compteur_categorie = 0
log("Parsing et injection de la catégorie Banque mondiale")
for pays in listePays():
	# Pour l'URL de base de la banque mondiale, on veut le code ISO 3166-1 du pays en minuscules
	str_pays = pays.encode('utf8')
	log("Catégorie BM - début du traitement du pays %s" % str_pays)
	pays_min = pays.lower()
	# On construit la base de l'URL
	worldbank_base_url = "http://api.worldbank.org/countries/"+pays_min+"/indicators/"
	year = year_int-1
	borne = year_int-11
	reussite_categorie = False	
	while year > borne and reussite_categorie is False:	
 		if dateDansRetour(fctXML(worldbank_base_url,year,"NY.GNP.PCAP.CD")) == "1" or None:
 			year = year-1
 			pass
 		else:
 			try:
 				GNI_xml = fctXML(worldbank_base_url,year,"NY.GNP.PCAP.CD")
 				annee = GNI_xml.find("wb:date").string
 				annee = int(annee)
 				GNI = GNI_xml.find("wb:value").string
 				if GNI == None:
 					year = year-1
 					pass
 				else:
 					GNI = float(GNI)
 					category = fctCategory(GNI)
  					# On stocke le tout dans la table nations_data
					try:
 						conn = sqlite3.connect("datas/bases/general.db")
						# On vérifie d'abord si ce pays existe déjà dans la table à un autre titre 						 						
						cur = conn.cursor()
						cur.execute("SELECT * from nations_datas where nd_ISO_3166_1_alpha_2 = ?", (pays,)	)
						if len(cur.fetchall()) == 0:
 							conn.execute("INSERT INTO nations_datas (nd_worldbank_category, nd_ISO_3166_1_alpha_2) VALUES (?, ?)",(category, pays))
							conn.commit()
							conn.close()
 						else:
							# Si le pays existe déjà dans la table, on se contente de l'updater
							conn.execute("UPDATE nations_datas set nd_worldbank_category = ? where nd_ISO_3166_1_alpha_2 = ?",(category, pays)) 
							conn.commit()
							conn.close() 							
 						compteur_categorie = compteur_categorie+1
 						reussite_categorie = True
 					except Exception, e:
 						log("***Erreur dans la saisie en base de données de la categorie : %s" %e)
 			except Exception, e:
				log("***Erreur sur le parsing de la categorie : %s" % e)
				pass
		year = year-1
log("%s catégories Banque mondiale parsées et injectées dans la base de données" % compteur_categorie)