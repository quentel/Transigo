#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                           mo_UNSD_cities.py                                #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_general.py) les données qui
# nous intéressent depuis les dumps du système de statistiques des Nations Unies,
# que nous avons téléchargés avec dl_UNSD_cities.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, os
from functions import log, deLibelleENversAlpha2UNSD, deVilleENversVilleFR, listeVillesEN

log("")
log("=====================================")
log("Lancement du script mo_UNSD_cities.py")
log("=====================================")

# 2. SCRIPT

compteur = 0

# On a un seul fichier dans le dossier, mais son nom varie
path_UNSD  = "datas/unsd/cities/"

liste_fichiers_UNSD = os.listdir(path_UNSD)

for fichier in liste_fichiers_UNSD:
	
	log("Traitement du fichier %s." % fichier)		
	# On ouvre le fichier	
	UNSD_path = path_UNSD+fichier
	mon_fichier_UNSD = open(UNSD_path, "rb")
	reader_UNSD = csv.reader(mon_fichier_UNSD, delimiter=';')
	
	liste = list()	
	compteur = 0
	# Et on le lit ligne à ligne
	for row in reader_UNSD:
		try:		
			if row[3] == "Both Sexes":
				country_name = row[0]
				year = row[1]
				city_name = row[4]
				city_type = row[5]
				pop = row[9]
				country_code = deLibelleENversAlpha2UNSD(country_name)
				pop = int(pop)
				year = int(year)
				if city_name.isupper() is True:
					city_name = city_name.lower()
					city_name = city_name.capitalize()
					is_capital_city = True
				else:
					is_capital_city = False
				if unicode(city_name) in listeVillesEN():
					city_name = deVilleENversVilleFR(city_name)
				# Voyons si cette ville existe déjà dans la base de données
				conn = sqlite3.connect("datas/bases/general.db")
 				cur = conn.cursor()						
				cur.execute("SELECT mt_years from countries_main_towns where mt_ISO_3166_1_alpha_2 = ? and mt_towns = ? and mt_capital_cities = ? and mt_types = ?", (country_code, unicode(city_name), is_capital_city, unicode(city_type)))		
				presence = len(cur.fetchall())
				if presence == 0:
					# Si cette ville n'existe pas dans la base de données, on l'insère
					conn.execute("INSERT INTO countries_main_towns (mt_towns, mt_populations, mt_years, mt_types, mt_capital_cities, mt_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?, ?, ?)",(unicode(city_name), pop, year, unicode(city_type), is_capital_city, country_code))						
					conn.commit()
					compteur = compteur+1
				else:
					# Si cette ville existe dans la base de données, voyons quelle est la fraicheur de ses informations				
					for row in cur:
						old_year = row[0]
						old_year = int(old_year)
						if year > old_year:
							print "Plus récent"
							conn.execute("UPDATE countries_main_towns set mt_populations = ?, mt_years = ? where mt_ISO_3166_1_alpha_2 = ? and mt_towns = ? and mt_capital_cities = ? and mt_types = ?", (pop, year, country_code, unicode(city_name), is_capital_city, unicode(city_type))) 
							conn.commit()				

		except:
			pass
		
	mon_fichier_UNSD.close()
		
	# Et on supprime le fichier traité
	path_to_rm = path_UNSD+fichier
	os.remove(path_to_rm)
	
log("%s villes et capitales entrées dans notre base" % compteur)