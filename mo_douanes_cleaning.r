
##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                          mo_douanes_cleaning.r                             #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script traite le fichiers téléchargés avec dl_douanes.py pour les transformer
# en un set de données par année, et en regroupant toutes les catégories de produits 
# importés ou exportés par mois et par pays partenaire.

# Récupérons le chemin de notre fichier, passé en argument au moment du lancement du script par Python
myArgs <- commandArgs(trailingOnly = FALSE)
myPath <- paste(myArgs[7], "/datas/douanes/", sep = "", collapse = NULL)
# Fixons un nouveau répertoire de travail
setwd(myPath)
# Récupérons la liste des fichiers que nous avons téléchargés
files_list <- list.files()
# Classons cette liste du fichier le plus ancien au plus récent
files_list <- sort(files_list, decreasing = TRUE)
# Initialisation de la dataframe pour les importations que nous allons remplir
importations <- data.frame()
# Initialisation de la dataframe pour les exportations que nous allons remplir
exportations <- data.frame()

# On ouvre nos fichiers un par un
for (file in files_list) 
  {
  # Initialisation de la dataframe pour les importations que nous allons remplir
#  importations <- data.frame()
  # Initialisation de la dataframe pour les exportations que nous allons remplir
# exportations <- data.frame()
  # Initialisation de la dataframe qui contiendra les informations de ce fichier
  dataset <- data.frame()
  # Lecture du fichier
  dataset <- read.csv(file, header=FALSE, sep=";")
  # Nommage des colonnes, afin de les rendres plus faciles à manipuler
  colnames(dataset) <- c("Operation","Month","Year","NC8","Partner","Value","Weight","USUP")
  # Formatage de la colonne "Value" en format numérique, pour contourner le mélange de points et de virgules, d'espaces et de guillemets d'un fichier l'autre
  dataset$Value <- as.numeric(gsub(",", ".", as.character(dataset$Value)))
  # On peut virer les colonnes "Poids" et "Unités supplémentaires", dont nous n'avons pas l'usage.
  dataset$Weight <- NULL
  dataset$USUP <- NULL
  # On peut convertir les colonnes mois et année en une seule colonne date
  dataset$Date <- c(paste("01",sprintf("%02d", dataset$Month), dataset$Year, sep = "", collapse = NULL))
  dataset$Date <- as.Date(dataset$Date, format = "%d%m%Y")
  # Du coup, on peut virer la colonne "Mois", dont nous n'avons pas l'usage.
  dataset$Month <- NULL
  # On cherche l'année minimum et l'année maxium de chaque set de données
  min_date = min(dataset$Date)
  min_year <- format(min_date, "%Y")
  max_date = max(dataset$Date)
  max_year <- format (max_date, "%Y")
  # On traite séparément les importations et les exportations
  first_line <- head(dataset,1)
  if (first_line$Operation == "I")
    {
    # Si le fichier correspond à une seule année d'opérations, c'est simple
    if (min_year == max_year)
      {
      # On peut maintenant compiler, sur chaque mois, toutes les ventes à un partenaire dans une catégorie de produits
      dataset <- aggregate(dataset$Value, list(partner=dataset$Partner, nc8=dataset$NC8, date=dataset$Date, year=dataset$Year), sum)
      file_name <- paste("importations_", min_year, ".csv", sep = "", collapse = NULL)
      write.csv(dataset, file_name)  
      }# Fin du traitement des fichiers sur une seule année
    # Si non, on créee une dataset globale, dont on écrasera ultérieurement toutes les entrées depuis la date à laquelle commence notre nouvelle dataframe
    else
      {
      # Si notre dataframe "importations" est vide, c'est que nous sommes sur l'avant dernier fichier, à reprendre tel quel
      if (nrow(importations) == 0)
        {
        importations <- dataset
        }# Fin du traitement de l'avant dernier fichier
      # Si ce n'est pas le cas, il faut retirer de notre ancienne dataset tout ce qui concerne la période couverte par son successeur
      else
        {
        # On cherche la date à laquelle commence notre nouvelle dataframe
        min_date <- min(dataset$Date)
        # Je supprime de ma dataframe globale tout ce qui est postérieur au début de la nouvelle dataframe
        importations <- importations[!(importations$Date>=min_date), ]
        # J'ajoute ma nouvelle dataset à ma dataset globale
        importations <- rbind(importations,dataset)
        # On peut maintenant compiler, sur chaque mois, toutes les ventes à un partenaire dans une catégorie de produits
        importations <- aggregate(importations$Value, list(partner=importations$Partner, nc8=importations$NC8, date=importations$Date, year=importations$Year), sum)
        for (y in unique(importations$year))
          {
          annual_subset <- importations[(importations$year == y), ]
          file_name <- paste("importations_", y, ".csv", sep = "", collapse = NULL)
          write.csv(annual_subset, file_name)
          }
        }# Fin du traitement du dernier fichier
      }# Fin du traitement des fichiers sur plusieurs années
    }# Fin du traitement des importations
    #Traitement des exportations
    else
    {
      # Si le fichier correspond à une seule année d'opérations, c'est simple
      if (min_year == max_year)
      {
      # On peut compiler, sur chaque mois, toutes les ventes à un partenaire dans une catégorie de produits
      dataset <- aggregate(dataset$Value, list(partner=dataset$Partner, nc8=dataset$NC8, date=dataset$Date, year=dataset$Year), sum)
      file_name <- paste("exportations_", min_year, ".csv", sep = "", collapse = NULL)
      write.csv(dataset, file_name)  
      }# Fin du traitement des fichiers sur une seule année
      # Si non, on créee une dataset globale, dont on écrasera ultérieurement toutes les entrées depuis la date à laquelle commence notre nouvelle dataframe
      else
      {
      # Si notre dataframe "importations" est vide, c'est que nous sommes sur l'avant dernier fichier, à reprendre tel quel
      if (nrow(exportations) == 0)
      {
        exportations <- dataset
      }# Fin du traitement de l'avant dernier fichier
      # Si ce n'est pas le cas, il faut retirer de notre ancienne dataset tout ce qui concerne la période couverte par son successeur
      else
      {
        # On cherche la date à laquelle commence notre nouvelle dataframe
        min_date <- min(dataset$Date)
        # Je supprime de ma dataframe globale tout ce qui est postérieur au début de la nouvelle dataframe
        exportations <- exportations[!(exportations$Date>=min_date), ]
        # J'ajoute ma nouvelle dataset à ma dataset globale
        exportations <- rbind(exportations,dataset)
        # On peut maintenant compiler, sur chaque mois, toutes les ventes à un partenaire dans une catégorie de produits
        exportations <- aggregate(exportations$Value, list(partner=exportations$Partner, nc8=exportations$NC8, date=exportations$Date, year=exportations$Year), sum)
        for (y in unique(exportations$year))
        {
          annual_subset <- exportations[(exportations$year == y), ]
          file_name <- paste("exportations_", y, ".csv", sep = "", collapse = NULL)
          write.csv(annual_subset, file_name)
        }
      }# Fin du traitement du dernier fichier
    }# Fin du traitement des fichiers sur plusieurs années
  }# Fin du traitement des exportations
  # Et on supprime le fichier traité
  file.remove(file)
}# Fin du traitement de tous les fichiers
