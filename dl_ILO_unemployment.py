#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                         dl_ILO_unemployment.py                             #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge le fichier relatif aux taux de chômage selon l'OIT
# compilés par la division des statistiques de l'ONU

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, urllib2, zipfile
from functions import log

log("")
log("==========================================")
log("Lancement du script dl_ILO_unemployment.py")
log("==========================================")

# L'URL du premier fichier à télécharger est fixe :
# http://data.un.org/Handlers/DownloadHandler.ashx?DataFilter=tableCode:3A&DataMartId=LABORSTA&Format=scsv&c=2,3,5,7,9,11,13,14&s=country_nameEnglish:asc,yr:desc,sexCode:asc

# 2. SCRIPT

# Nom et chemin du fichier que nous allons créer
dl_path = "datas/ilo/unemployment/"
dl_file = dl_path+"unemployment_ilo.zip"

# Si le répertoire n'existe pas, créons-le
dl_dir = os.path.dirname(dl_path)
if not os.path.exists(dl_path):
	log("Création du répertoire destiné à accueillir notre fichier : datas/ilo/unemployment/")
	os.makedirs(dl_dir)

url =	"http://data.un.org/Handlers/DownloadHandler.ashx?DataFilter=tableCode:3A&DataMartId=LABORSTA&Format=scsv&c=2,3,5,7,9,11,13,14&s=country_nameEnglish:asc,yr:desc,sexCode:asc"
try:	
	log("Tentative de téléchargement sur l'URL :"+url)
	req = urllib2.Request(url)
	with open(dl_file,'w') as f:
		f.write(urllib2.urlopen(req).read())
	f.close()
	log("Téléchargement des données sur le chômage de l'OIT terminé !")
	try:
		with zipfile.ZipFile(dl_file, "r") as z:
			z.extractall(dl_path)
		log("Décompression du fichier "+dl_file)
		try:
			os.remove(dl_file)
		except:
			log("***Echec de la supression du fichier "+dl_file)
			pass
	except:
		log("***Echec de la décompression du fichier "+dl_file)
		pass
except:
	log("***Echec du téléchargement des données sur le chômage de l'OIT !")
	pass