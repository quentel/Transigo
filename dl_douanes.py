#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                             dl_douanes.py                                  #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge les fichiers ZIP mis à disposition par les douanes françaises sur leur site internet
# relatifs aux statistiques == nationales == du commerce extérieur. Il trouve ces fichiers via le site Opendata du gouvernement français 
# (data.gouv.fr/fr/datasets/statistiques-nationales-du-commerce-exterieur/), télécharge cette page pour y chercher les liens pertinents
# télécharge les fichiers .zip qui nous intéresse, ouvre ces conteneurs, récupère en leur sein les données elles-mêmes, enregistre
# le tout dans un répertoire "douane" et supprime tout le reste de notre disque dur.

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, shutil, urllib2, re, zipfile
from functions import log

log("")
log("=================================")
log("Lancement du script dl_douanes.py")
log("=================================")

# 2. SCRIPT

# L'identifiant du jeu de données qui nous intéresse
id_dataset = "5369a0b4a3a729239d206418"

# L'URL qui en découle
URL = "https://www.data.gouv.fr/api/1/datasets/"+id_dataset+"/full/"

# Création du répertoire temporaire destiné à accueillir nos fichiers avant décompression
dl_dir = "tmp"
# Si le répertoire n'existe pas, créons-le
if not os.path.exists(dl_dir):
	log("Création du répertoire temporaire destiné à accueillir nos fichiers provisoirres")
	os.makedirs(dl_dir)

# On ouvre le fichier contenant le menu
response = urllib2.urlopen(URL)
# On stocke son contenu dans la variable "menu"
menu = response.read()
# Nous rechercherons des URL annoncés par le tag... "url" ! Les lignes se terminent par une accolade fermante. Nous récupérons tout ce qui se trouve entre les deux
pattern = re.compile('url(.*?)}')
# Le résultat est stocké dans la liste "results"
results = pattern.findall(menu)
# Créons un compteur
count = 1
# Il nous reste à ouvrir notre liste de résultat
for result in results:
	# Le premier lien n'est jamais signifiant : il renvoie vers la liste de liens elle-même
	if count > 1:	
		link = result[4:-1]
		# Trouvons le nom du fichier lui-même, après le dernier slash
		index = link.rfind("/")
		# Nous avons besoin du nom du fichier, pour le réenregistrer
		dl_name = link[(index+1):]		
		# Nous allons l'enregistrer dans le répertoire tmp, sous son appelation douanière							
		dl_zip = dl_dir+"/"+dl_name
		try:
			with open(dl_zip,'w') as f:
				f.write(urllib2.urlopen(link).read())
				f.close()
			log("Téléchargement du fichier "+dl_zip)
			# Nous décompactons le dossier concerné
			try:
				path_unzipped = "decompresse/"+str(count)+"/"									
				# Si le répertoire n'existe pas, on le crée
				if not os.path.exists(path_unzipped):
					os.makedirs(path_unzipped)
				with zipfile.ZipFile(dl_zip, "r") as z:
					z.extractall(path_unzipped)
					log("Décompression du fichier "+dl_zip)
				# Ces zip contiennent soit un répertoire, soit un fichier. Vérifions-le
				try:
					mon_chemin = path_unzipped
					contenu = os.listdir(mon_chemin)
					# S'il y a un seul élément dans le chemin, c'est que c'est soit un fichier isolé, soit un répertoire
				#	if len(contenu) == 1:
				#		print "il y a un seul élément dans le chemin"
					for fichier in contenu:
						# Si leur nom contient un .txt, c'est un fichier
						if fichier.find(".txt") > -1:
							# On retrouve son chemin
							original_file = path_unzipped+fichier
							# On s'apprête à le copier dans notre répertoire définitif, avec son nom
							copy_name = dl_name.replace("zip","txt")
							copy_name = str(count)+"-"+copy_name
							copy_file = "datas/douanes/"+copy_name
							# Si le répertoire n'existe pas, créons-le
							copy_dir = os.path.dirname(copy_file)
							if not os.path.exists(copy_dir):
								os.makedirs(copy_dir)
							# Et on copie le fichier dans le bon répertoire
							try:
								# On copie le bon fichier dans le bon répertoire													
								shutil.copyfile(original_file,copy_file)
								# Et on supprime le fichier original pour éviter les conflits ultérieurs
								os.remove(original_file)
							except Exception, e:
								log("===Echec de la copie du fichier %s" % e)
								pass
						# Dans le cas contraire, c'est un répertoire
						else:
							# On regarde ce qu'il y a dans ce répertoire
							mon_sous_chemin = mon_chemin+fichier+"/"
							sous_contenu = os.listdir(mon_sous_chemin)
							try:
								for sous_fichier in sous_contenu:
									# Si son nom contient NC8PAYS, c'est le fichier qui nous intéresse
									if sous_fichier.find("NC8PAYS")>-1:
										original_file = "decompresse/"+str(count)+"/"+fichier+"/"+sous_fichier
										copy_name = dl_name.replace("zip","txt")
										copy_name = str(count)+"-"+copy_name
										copy_file = "datas/douanes/"+copy_name
										# Si le répertoire n'existe pas, créons-le
										copy_dir = os.path.dirname(copy_file)
										if not os.path.exists(copy_dir):
											os.makedirs(copy_dir)
											log("Création du répertoire datas/douanes/")
										# Et on copie le fichier dans le bon répertoire
										try:
											shutil.copyfile(original_file,copy_file)
											# Et on supprime le fichier original pour éviter les conflits ultérieurs
											os.remove(original_file)
										except:
											log("===Echec de la copie du fichier "+original_file+" vers "+copy_file)
											pass														
							except Exception, e:
								log("===Echec du traitement d'un répertoire décompacté : %s" % e)													
								pass
				except:
					pass									
			except Exception, e:
				log("=== Erreur lors de la décompression du fichier. Il est possible que le module Unzip ne supporte pas ce format : %s" % e)
				pass							
		except Exception, e:
			log("===Echec du téléchargement d'un fichier .zip : %s" % e)
			pass														
	count = count+1
# 
if os.path.exists("/tmp"):
	shutil.rmtree('tmp')
if os.path.exists("decompresse"):
	shutil.rmtree('decompresse')
