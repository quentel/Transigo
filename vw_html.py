#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                       		vw_html.py                                     #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée un fichier HTML (non complet : seulement le <BODY> qui nous intéresse)
# par pays, reprenant les données dont nous disposons

# 1. IMPORTATION DES MODULES NECESSAIRES
from __future__ import division
import sqlite3, os, math, time, datetime
from time import strftime, strptime
from functions import *


log("")
log("==============================")
log("Lancement du script vw_html.py")
log("==============================")

# 2. FONCTIONS UTILES

# 2.1. Transformation d'une chaîne au format HTML
def reformatString(my_Str):
	my_Str = unicode(my_Str)
	my_Str = my_Str.encode("ascii", "xmlcharrefreplace")
	return my_Str

# 2. Styles
# Nous construisons ici des morceaux de fichiers HTML, à insérer entre des balises <body> et </body>.
# Nous n'avons donc pas accès au <head> et ne pouvons pas faire référence à un fichier CSS externe.
# Nous devons donc préciser nos règles de style en mode inline... Alors autant le faire proprement

# Style des cellules d'en-tête
def setTH(my_code):
	my_result = "<td bgcolor = \"#939597\" style=\"font-size: 80%;padding:3px;border-collapse:separate;border:3px solid;border-color:#939597;\">"+my_code+"</td>\n"
	return my_result

def setTHCenter(my_code):
	my_result = "<td bgcolor = \"#939597\" style=\"font-size: 80%;padding:3px;text-align:center;border-collapse:separate;border:3px solid;border-color:#939597;\">"+my_code+"</td>\n"
	return my_result


# Style des cellules de données
def setTD(my_code):
	my_result = "<td style=\"font-size: 80%;padding:3px;border-collapse:separate;border:3px solid;border-color:#939597;\">"+my_code+"</td>\n"
	return my_result

def setTDRight(my_code):
	my_result = "<td style=\"font-size: 70%;padding:3px;text-align:right;border-collapse:separate;border:3px solid;border-color:#939597;\">"+my_code+"</td>\n"
	return my_result
	

# 3. SCRIPT

path = "output/html/"

# Si le répertoire n'existe pas, créons-le
dir = os.path.dirname(path)
if not os.path.exists(dir):
	log("Création du répertoire destiné à accueillir nos fichiers html : output/html/")
	os.makedirs(dir)

for pays in listePays():
	
	# 3.1. Récupération et formatage des données
	
	# As an aperitzer : date de création (et donc de mise-à-jour) du fichier
	aujourdhui = datetime.date.today()
	date_MAJ = aujourdhui.strftime('%d %b %Y')
	date_MAJ_str = "Fiche mise-&agrave;-jour le "+date_MAJ
	
	# 3.1.1. Nom
	nom_pays = deAlpha2versLibelleFR(pays)
	nom_pays_str = reformatString(nom_pays)
		
	# 3.1.2 Nom officiel
	off_name = getOfficial(pays)
	if off_name == "N/A":
		off_str = "<b>Aucune donn&eacute;es</b>"
	else:
		off_name = reformatString(off_name)
		off_str = "<b>{}</b> (Wikipedia)".format(off_name)	
	
	# 3.1.3 Nature du régime
	state_form_str = ""	
	state_form = getStateForm(pays)
	if state_form == "N/A":
		state_form_str = "<b>Aucune donn&eacute;es</b>"
	else:	
		state_form = reformatString(state_form)
		state_form_str = "<b>{}</b> (Wikipedia)".format(state_form)

	# 3.1.4 Langue(s) officielle(s)		
	langsOff = getOfficialLanguages(pays)
	langsOff = reformatString(langsOff)
	if langsOff == "":
		langsOff_str = ""
	else:
		langsOff_str = "<b>{}</b> (Wikipedia)\n".format(langsOff)
	
	# 3.1.5 Dirigeants
	chef1_str = ""
	role1_str = ""
	chef1_str_titre = ""
	chef1 = getLeaders_1(pays)
	nom_du_chef1 = reformatString(chef1[0])
	role_du_chef1 = reformatString(chef1[1])
	if (nom_du_chef1 == "N/A") and (role_du_chef1 == "N/A"):
		chef1_str = ""
	else:	
		chef1_str = "<tr>\n"+setTH(role_du_chef1)+setTD("<b>"+nom_du_chef1+"</b> (Wikipedia)")+"</tr>\n"
	
	chef2_str = ""
	role2_str = ""
	chef2_str_titre = ""
	chef2 = getLeaders_2(pays)
	nom_du_chef2 = reformatString(chef2[0])
	role_du_chef2 = reformatString(chef2[1])
	if (nom_du_chef2 == "N/A") and (role_du_chef2 == "N/A"):
		chef2_str = ""
	else:
		chef2_str = "<tr>\n"+setTH(role_du_chef2)+setTD("<b>"+nom_du_chef2+"</b> (Wikipedia)")+"</tr>\n"			
	chef3_str = ""
	role3_str = ""
	chef3_str_titre = ""
	chef3 = getLeaders_3(pays)
	nom_du_chef3 = reformatString(chef3[0])
	role_du_chef3 = reformatString(chef3[1])
	if (nom_du_chef3 == "N/A") and (role_du_chef3 == "N/A"):
		chef3_str = ""
	else:
		chef3_str = "<tr>\n"+setTH(role_du_chef3)+setTD("<b>"+nom_du_chef3+"</b> (Wikipedia)")+"</tr>\n"
	
	# 3.1.6 Capitale
	capitale_str = ""	
	capitale = getCapitale(pays)
	capitale_name = capitale[0]
	capitale_pop = capitale[1]
	capitale_year = capitale[2]
	capitale_name_s = reformatString(capitale_name)
	capitale_year_s = iAmAnInteger(capitale_year)
	capitale_year_s = str(capitale_year_s)
	capitale_pop = iAmAnInteger(capitale_pop)
	capitale_pop_s = formatInt(capitale_pop)
	if (capitale_pop_s=="00") or (capitale_pop_s=="00"):
		capitale_pop_s = ""
		capitale_year_s = ""
	else:
	 	capitale_pop_s = capitale_pop_s+" hab., "
	 	capitale_year_s = ", "+capitale_year_s
	if capitale_name == "N/A":
		capitale_str = "<b>Aucune donn&eacute;e</b>"
	else :
		capitale_str = "<b>{}</b> ({}{}{})".format(capitale_name_s,capitale_pop_s, "UNSD", capitale_year_s)
		
	# 3.1.7 Villes principales
	count_towns = 0
	townsList = list()
	town_name = ""
	town_pop = 0
	town_type = ""
	town_year = 0
	town_str = ""
	towns_str = ""
	mainTowns_str = ""
	townsList = getTowns(pays)
	if len(townsList) == 0:
		mainTowns_label = "Villes principales"
		mainTowns_str = setTD("<b>Aucune donn&eacute;e</b>")+"</tr>\n"
	elif 	len(townsList) == 1:
		mainTowns_label = "Ville principale"
		for town in townsList:
			town_name = reformatString(town[0])
			town_pop = formatInt(iAmAnInteger(town[1]))
			town_type = town[2]
			town_year = town[3]
			town_str = "<b>{}</b> ({} hab., UNSD, {})".format(town_name,town_pop, town_year)
			town_str = setTD(town_str)+"</tr>\n"
			mainTowns_str = town_str
	elif len(townsList) > 1:
		mainTowns_label = "Villes principales"
		count_towns = 1
		for town in townsList:
			town_name = reformatString(town[0])
			town_pop = formatInt(iAmAnInteger(town[1]))
			town_type = town[2]
			town_year = town[3]
			town_str = "{}. <b>{}</b> ({} hab., UNSD, {})".format(count_towns,town_name,town_pop, town_year)
			town_str = setTD(town_str)+"</tr>\n"
			if count_towns > 1:
				town_str = "<tr><td>&nbsp;</td>"+town_str				
			# On n'affiche que les 10 premières données
			if count_towns > 10:
				town_str = ""
			count_towns = count_towns+1		
			mainTowns_str = mainTowns_str+town_str
			
	# 3.1.8 Agglomérations principales
	count_agglos = 0
	agglosList = list()
	agglo_name = ""
	agglo_pop = 0
	agglo_type = ""
	agglo_year = 0
	agglo_str = ""
	agglos_str = ""
	mainAgglos_str = ""
	agglosList = getAgglomerations(pays)
	if len(agglosList) == 0:
		mainAgglos_label = "Agglom&eacute;rations principales"
		mainAgglos_str = setTD("<b>Aucune donn&eacute;e</b>")+"</tr>\n"
	elif 	len(agglosList) == 1:
		mainAgglos_label = "Agglom&eacute;ration principale"
		for agglo in agglosList:
			agglo_name = reformatString(agglo[0])
			agglo_pop = formatInt(iAmAnInteger(agglo[1]))
			agglo_type = agglo[2]
			agglo_year = agglo[3]
			agglo_str = "<b>{}</b> ({} hab., UNSD, {})".format(agglo_name, agglo_pop, agglo_year)
			agglo_str = setTD(agglo_str)+"</tr>\n"
			mainAgglos_str = agglo_str
	elif len(agglosList) > 1:
		mainAgglos_label = "Agglom&eacute;rations principales"
		count_agglos = 1
		for agglo in agglosList:
			agglo_name = reformatString(agglo[0])
			agglo_pop = formatInt(iAmAnInteger(agglo[1]))
			agglo_type = agglo[2]
			agglo_year = agglo[3]
			agglo_str = "{}. <b>{}</b> ({} hab., UNSD, {})".format(count_agglos,agglo_name,agglo_pop, agglo_year)
			agglo_str = setTD(agglo_str)+"</tr>\n"
			if count_agglos > 1:
				agglo_str = "<tr><td>&nbsp;</td>"+agglo_str				
			# On n'affiche que les 10 premières données
			if count_agglos > 10:
				agglo_str = ""
			count_agglos = count_agglos+1		
			mainAgglos_str = mainAgglos_str+agglo_str
			
	# 3.1.9. Superficie
	area_str = ""
	area = getArea(pays)
	area = iAmAnInteger(area) 
	area_s = formatInt(area)
	if area_s == "0":
		area_str = "<b>Aucune donn&eaute;e</b>"
	else:
		area_str = "<b>{} km2</b> (Banque mondiale)".format(area_s)
	
	# 3.1.10. Population
	population_str = ""	
	year = getPopLastYear(pays,"Banque mondiale")
	population = getPop(pays,year,"Banque mondiale")
	population = iAmAnInteger(population)
	population_s = formatInt(population)
	if (population_s == "0") or (population_s == "N/A") or (population_s == ""):
		population_str = "<b>Aucune donn&eaute;e</b>"
	else:
		population_str = "<b>{} habitants</b> ({}, {})".format(population_s, "Banque mondiale", year)
	
	# 3.1.11. Densité
	density_str = ""
	try:		
		density = population/area
		density = iAmAFloat(density)
		density_s = formatFloat(density)
		density_str ="<b>{} hab./km2</b> (pop./superficie)".format(density_s)
	except:
		density_str = "<b>Donn&eaute;e insuffisantes</b>"
	
	# 3.1.12. Croissance démographique annuelle	
	population_growth_str = ""	
	year = getDataLastYear("npg","nations_population_growths",pays)
	population_growth = getDataValue("npg","nations_population_growths",pays,year)	
	population_growth = iAmAFloat(population_growth)
	year = year.replace("/","-")
	population_growth_s = formatFloat(population_growth)
	if population_growth_s == "0,00":		
		population_growth_str = "<b>Aucune donn&eacute;e</b>"
	else:	
		population_growth_str = "<b>{} %</b> par an ({}, {})".format(population_growth_s, "PNUD", year)
	
	# 3.1.13. Taux de fécondité
	fertility_str = ""
	year = getDataLastYear("nfer","nations_fertility",pays)		
	fertility_list = getDataValueSource("nfer","nations_fertility",pays, year)
	fertility = fertility_list[0]
	source = fertility_list[1]
	fertility = iAmAFloat(fertility)
	if fertility > 1.999:
		enfant_s = "enfants"
	else:
		enfant_s = "enfant"
	fertility_s = formatFloat(fertility)
	if fertility_s == "0,00":		
		fertility_str = "<b>Aucune donn&eacute;e</b>"
	else:	
		fertility_str = "<b>{} {} par femme</b> (Banque mondiale, {})".format(fertility_s, enfant_s, year)
	
	# 3.1.14. Espérance de vie à la naissance
	life_expectancy_str = ""	
	year = getDataLastYear("nle","nations_life_expectancies",pays)
	life_expectancy = getDataValue("nle","nations_life_expectancies", pays, year)
	life_expectancy = iAmAFloat(life_expectancy)
	life_expectancy_s = formatFloat(life_expectancy)
	if life_expectancy_s == "0,00":		
		life_expectancy_str = "<b>Aucune donn&eacute;e</b>"	
	else:
		life_expectancy_str = "<b>{} ans</b> ({}, {})".format(life_expectancy_s, "PNUD", year)	
	
	# 3.1.15. Taux d'alphabétisation
	literacy_rate_str = ""
	years = getDataLastYear("nlr","nations_literacy_rates",pays)
	literacy_rate = getDataValue("nlr","nations_literacy_rates", pays, years)
	literacy_rate = iAmAFloat(literacy_rate)
	literacy_rate_s = formatFloat(literacy_rate)
	years = years.encode("utf-8")
	years = years.replace("–","-")	
	if literacy_rate_s == "0,00":		
		literacy_rate_str = "<b>Aucune donn&eacute;e</b>"
	else:	
		literacy_rate_str = "<b>{} %</b> ({}, {})".format(literacy_rate_s, "PNUD", years)	

	# 3.1.16. Indice de développement humain
	year = getDataLastYear("nh","nations_hdi",pays)
	HDI_value = getDataValue("nh","nations_hdi", pays,year)
	HDI_rank = getHDIRank(pays, year)
	HDI_value = iAmAFloat(HDI_value)
	HDI_rank = iAmAnInteger(HDI_rank)
	HDI_value_s = formatFloat(HDI_value)
	HDI_rank_s = formatInt(HDI_rank)
	if HDI_rank_s == 1:
		cardinal = "er"
	else:
		cardinal = "&egrave;me"
	if (HDI_value_s == "0,00") or (HDI_value_s == ""):		
		HDI_str = "<b>Aucune donn&eacute;e</b>"
	else:
		HDI_str = "<b>{}</b>, soit le <b>{}{} rang mondial</b> ({}, {})".format(HDI_value_s, HDI_rank_s, cardinal, "PNUD", year)
	
	# 3.1.17 Langue(s) pratiquée(s)		
	count_langs = 0
	langs = list()
	langs_label = ""
	label = ""
	pop = 0
	pop_s = ""
	year = 0
	total = 0
	lang_str = ""
	langs_str = ""
	langs = getLanguages(pays)
	if len(langs) == 0:
		langs_label = "Langues pratiqu&eacute;es"
		langs_str = setTD("<b>Aucune donn&eacute;e</b>")+"</tr>\n"
	elif 	len(langs) == 1:
		langs_label = "Langue pratiqu&eacute;e"
		for lang in langs:
			label = reformatString(deLangueENversLangueFR(lang[0]))
			pop = lang[1]
			pop_s = formatInt(iAmAnInteger(pop))
			year = lang[2]
			total = getLanguagesTotal(pays,year)
			try:
				lang_pc = (pop/total)*100	
				lang_pc_s = formatFloat(iAmAFloat(lang_pc))
			except:
				lang_pc_s = "N/A"	
			# On n'affiche pas le total		
			if label in (u"Total"):
				lang_str = ""		 
			else:
				lang_str = " <b>{}</b> ({} locuteurs, soit {} %, UNSD, {})".format(label,pop_s,lang_pc_s,year)
				lang_str = setTD(lang_str)+"</tr>\n"				
				langs_str = lang_str
	elif len(langs) > 1:
		langs_label = "Langues pratiqu&eacute;es"
		count_langs = 1
		for lang in langs:
			label = reformatString(deLangueENversLangueFR(lang[0]))
			pop = lang[1]
			pop_s = formatInt(iAmAnInteger(pop))
			year = lang[2]
			total = getLanguagesTotal(pays,year)
			try:
				lang_pc = (pop/total)*100	
				lang_pc_s = formatFloat(iAmAFloat(lang_pc))
			except:
				lang_pc_s = "N/A"	
			# On n'affiche pas le total		
			if label in (u"Total"):
				lang_str = ""
				count_langs = 0		 
			else:
				lang_str = " <b>{}</b> ({} locuteurs, soit {} %, UNSD, {})".format(label,pop_s,lang_pc_s,year)
				lang_str = setTD(lang_str)+"</tr>\n"				
			if count_langs > 1:
				lang_str = "<tr><td>&nbsp;</td>"+lang_str
			# On n'affiche que les 10 premières données
			if count_langs > 10:
				lang_str = ""
			count_langs = count_langs+1		
			langs_str = langs_str+lang_str
	
	# 3.1.18 Religion(s)		
	count_rels = 0
	rels = list()
	rels_label = ""
	label = ""
	pop = 0
	pop_s = ""
	year = 0
	total = 0
	rel_str = ""
	rels_str = ""
	rels = getReligions(pays)
	if len(rels) == 0:
		rels_label = "Religions"
		rels_str = setTD("<b>Aucune donn&eacute;e</b>")+"</tr>\n"
	elif 	len(rels) == 1:
		rels_label = "Religion"
		for rel in rels:
			label = reformatString(rel[0])
			pop = rel[1]
			pop_s = formatInt(iAmAnInteger(pop))
			year = rel[2]
			total = getReligionsTotal(pays,year)
			try:
				rel_pc = (pop/total)*100	
				rel_pc_s = formatFloat(iAmAFloat(rel_pc))
			except:
				rel_pc_s = "N/A"	
			# On n'affiche pas le total		
			if label in (u"Total"):
				rel_str = ""		 
			else:
				rel_str = "<b>{}</b> ({} personnes, soit {} %, UNSD, {})".format(label,pop_s,rel_pc_s,year)
				rel_str = setTD(rel_str)+"</tr>\n"
				rels_str = rel_str
	elif len(rels) > 1:
		rels_label = "Religions"
		count_rels = 1
		for rel in rels:
			label = reformatString(rel[0])
			pop = rel[1]
			pop_s = formatInt(iAmAnInteger(pop))
			year = rel[2]
			total = getReligionsTotal(pays,year)
			try:
				rel_pc = (pop/total)*100	
				rel_pc_s = formatFloat(iAmAFloat(rel_pc))
			except:
				rel_pc_s = "N/A"	
			# On n'affiche pas le total		
			if label in (u"Total"):
				rel_str = ""
				count_rels = 0		 
			else:
				rel_str = "<b>{}</b> ({} personnes, soit {} %, UNSD, {})\n".format(label,pop_s,rel_pc_s,year)
				rel_str = setTD(rel_str)+"</tr>\n"				
			if count_rels > 1:
				rel_str = "<tr><td>&nbsp;</td>"+rel_str				
			# On n'affiche que les 10 premières données
			if count_rels > 10:
				rel_str = ""
			count_rels = count_rels+1		
			rels_str = rels_str+rel_str

	# 3.1.19. Monnaie
	monnaie_str = ""
	code_monnaie = getCurrencyCode(pays)
	label_monnaie = getCurrencyLabel(pays)
	label_monnaie = reformatString(label_monnaie)
	taux = getLastXChangeRateValue(code_monnaie)
	taux = iAmAFloat(taux)
	if taux > 1.999:
		euro = "euros"
	else:
		euro = "euro"
	# Nous voulons 3 décimales, et non 2, après la virgule
	taux_s = format('%.3f' % taux).replace('.', ',')
	validite = getLastXChangeRateDate(code_monnaie)
	if validite != "N/A":
		validite_obj = strptime(validite, "%Y-%m-%d %H:%M:%S")
		validite_str = strftime("%d-%m-%Y", validite_obj)	
	if code_monnaie == "XXX":
		monnaie_str = "Aucune donn&eacute;e"
	else:
		monnaie_str = "<b>{}</b>. 1 {} = {} {} au {} (taux de chancellerie)".format(label_monnaie, code_monnaie, taux_s, euro, validite_str)
	
	# 3.1.11. Tableaux des données macroéconomiques
	# Nous prendrons en abscisse les années disponibles pour le PIB en USD courants du FMI (la BM, elle, a toujours un an de retard). 	
	years = getAllYearsSourced("ng","nations_gdp",pays,"FMI")
	if len(years) == 0:
		PIBs_BM_str = ""
	else:
		# Nos en-têtes
		macroeco_table_headers = "<td>&nbsp;</td>\n"
		for year in years:
			year = str(year)
			header = setTHCenter(year)
			macroeco_table_headers = macroeco_table_headers+header
		# Nos chiffres		
		PIBs_BM_str = setTH("PIB<br />(Banque mondiale, USD courants)")
		PIBs_FMI_str = setTH("PIB<br />(FMI, USD courants)")		
		PIBs_BM_pc_str = setTH("PIB par habitant<br />(Banque mondiale, USD)")
		PIBs_FMI_pc_str = setTH("PIB par habitant<br />(FMI, USD)")
		PIBgrowth_str = setTH("Taux de croissance du PIB<br />(Banque mondiale)")

		for year in years:
			PIB_BM = getDataValueSourced("ng","nations_gdp",pays,year,"Banque mondiale")		
			PIB_FMI = getDataValueSourced("ng","nations_gdp",pays,year,"FMI")		
			PIB_BM_pc = getDataValueSourced("ngc","nations_gdp_per_capita",pays,year,"Banque mondiale")	
			PIB_FMI_pc = getDataValueSourced("ngc","nations_gdp_per_capita",pays,year,"FMI")
			PIBgrowth = getDataValueSourced("ngg","nations_gdp_growth",pays, year, unicode("Banque mondiale"))
			PIB_BM = formatInt(iAmAnInteger(PIB_BM))
			PIB_FMI = formatInt(iAmAnInteger(PIB_FMI))
			PIB_BM_pc = formatFloat(iAmAFloat(PIB_BM_pc))
			PIB_FMI_pc = formatFloat(iAmAFloat(PIB_FMI_pc))
			PIBgrowth = formatFloat(iAmAFloat(PIBgrowth))
			if (PIB_BM == "0") or (PIB_BM =="") or (PIB_BM =="00"):
				PIBs_BM_str = PIBs_BM_str = PIBs_BM_str+setTDRight("N/A")
			else:
				PIB_BM_str = "{}".format(PIB_BM)
				PIB_BM_str = setTDRight(PIB_BM_str)
				PIBs_BM_str = PIBs_BM_str+PIB_BM_str
			if (PIB_FMI == "0") or (PIB_FMI =="") or (PIB_FMI =="00"):
				PIBs_FMI_str = PIBs_FMI_str+setTDRight("N/A")
			else:
				PIB_FMI_str = "{}".format(PIB_FMI)
				PIB_FMI_str = setTDRight(PIB_FMI_str)
				PIBs_FMI_str = PIBs_FMI_str+PIB_FMI_str
			if (PIB_BM_pc == "0") or (PIB_BM_pc =="") or (PIB_BM_pc =="00"):
				PIBs_BM_pc_str = PIBs_BM_pc_str+setTDRight("N/A")
			else:
				PIB_BM_pc_str = "{}".format(PIB_BM_pc)
				PIB_BM_pc_str = setTDRight(PIB_BM_pc_str)
				PIBs_BM_pc_str = PIBs_BM_pc_str+PIB_BM_pc_str
			if (PIB_FMI_pc == "0,00") or (PIB_FMI_pc =="")  or (PIB_FMI_pc =="00"):
				PIBs_FMI_pc_str = PIBs_FMI_pc_str+setTDRight("N/A")
			else:
				PIB_FMI_pc_str = "{}".format(PIB_FMI_pc)
				PIB_FMI_pc_str = setTDRight(PIB_FMI_pc_str)				
				PIBs_FMI_pc_str = PIBs_FMI_pc_str+PIB_FMI_pc_str
			if (PIBgrowth == "0,00") or (PIBgrowth =="") or (PIBgrowth =="00"):
				PIBgrowth_str = PIBgrowth_str+setTDRight("N/A")
			else:
				PIBgrowth_1_str = "{} %".format(PIBgrowth)
				PIBgrowth_1_str = setTDRight(PIBgrowth_1_str)
				PIBgrowth_str = PIBgrowth_str+PIBgrowth_1_str

	# 3.1.16. Catégorie Banque mondiale
	cat_str = ""
	cat = getCategory(pays)
	cat = reformatString(cat) 
	if cat == "":
		cat_str = "N/A"
	else:
		cat_str = "<b>{}</b>".format(cat)


	# 3.1.17. Taux de chômage selon le FMI
	year = getDataLastYearSourced("nu","nations_unemployment",pays, unicode("FMI"))
	unemployment_FMI = getDataValueSourced("nu","nations_unemployment",pays, year, unicode("FMI"))
	unemployment_FMI = formatFloat(iAmAFloat(unemployment_FMI))	
	if (unemployment_FMI == "0,00") or (unemployment_FMI ==""):

		unemployment_FMI_str = "Aucune donn&eacute;e"
	else:
		unemployment_FMI_str = "<b>{} %</b> ({})".format(unemployment_FMI, year)
	
	# 3.1.18. Taux de chômage selon la Banque mondiale
	year = getDataLastYearSourced("nu","nations_unemployment",pays, unicode("Banque mondiale"))
	unemployment_BM = getDataValueSourced("nu","nations_unemployment",pays, year, unicode("Banque mondiale"))
	unemployment_BM = formatFloat(iAmAFloat(unemployment_BM))	
	if (unemployment_BM == "0,00") or (unemployment_BM ==""):
		unemployment_BM_str = "Aucune donn&eacute;e"
	else:
		unemployment_BM_str = "<b>{} %</b> ({})".format(unemployment_BM, year)

	# 3.1.19. Taux de chômage selon l'OIT
	year = getDataLastYearSourced("nu","nations_unemployment",pays, unicode("OIT"))
	unemployment_ILO = getDataValueSourced("nu","nations_unemployment",pays, year, unicode("OIT"))
	unemployment_ILO = formatFloat(iAmAFloat(unemployment_ILO))	
	if (unemployment_ILO == "0,00") or (unemployment_ILO ==""):
		unemployment_ILO_str = "Aucune donn&eacute;e"
	else:
		unemployment_ILO_str = "<b>{} %</b> ({})".format(unemployment_ILO, year)

	# 3.1.20. Taux de pauvreté selon le PNUD
	year = getDataLastYearSourced("npov", "nations_poverty", pays, unicode("PNUD - Indice multidimensionnel"))
	poverty_UNDP = getDataValueSourced("npov", "nations_poverty", pays, year, unicode("PNUD - Indice multidimensionnel"))
	poverty_UNDP = formatFloat(iAmAFloat(poverty_UNDP))
	year = year.replace("/"," &grave; ")
	year = year.replace("-"," &grave; ")
	year = reformatString(year)	
	if (poverty_UNDP == "0,00") or (poverty_UNDP ==""):
		poverty_UNDP_str = "Aucune donn&eacute;e"
	else:
		poverty_UNDP_str = "<b>{} %</b> ({})".format(poverty_UNDP, year)

	# 3.1.21. Taux de pauvreté selon la Banque mondiale
	year = getDataLastYearSourced("npov", "nations_poverty", pays, unicode("Banque mondiale"))
	poverty_BM = getDataValueSourced("npov", "nations_poverty", pays, year, unicode("Banque mondiale"))
	poverty_BM = formatFloat(iAmAFloat(poverty_BM))	
	if (poverty_BM == "0,00") or (poverty_BM ==""):
		poverty_BM_str = "Aucune donn&eacute;e"
	else:
		poverty_BM_str = "<b>{} %</b> ({})".format(poverty_BM, year)
	
	# 3.1.22. Déficit budgétaire
	# Nous n'avons que la dernière année dans notre BDD	
	year = getDataLastYear("gd","general_gov_deficit",pays)
	deficitetSource = getDataValueSource("gd","general_gov_deficit",pays, year)
	deficit = deficitetSource[0]
	source = deficitetSource[1]
	deficit = iAmAFloat(deficit)
	deficit_s = formatFloat(deficit)
	if deficit_s == "0,00":
		deficit_str = "Aucune donn&eacute;e"
	else:
		deficit_str = "<b>{} % du PIB</b> ({}, {})".format(deficit_s, year, source)
	
	# 3.1.23. Dette publique (dette de l'Etat nette, en % du PIB)
	# Nous n'avons que la dernière année dans notre BDD	
	year = getDataLastYear("gg","general_gov_net_debt",pays)
	detteetSource = getDataValueSource("gg","general_gov_net_debt",pays, year)
	debt = detteetSource[0]
	source = detteetSource[1]
	debt = iAmAFloat(debt)
	debt_s = formatFloat(debt)
	if debt_s == "0,00":
		debt_str = "Aucune donn&eacute;e"
	else:
		debt_str = "<b>{} % du PIB</b> ({}, {})".format(debt_s, year, source)

	# 3.1.24. Taux d'inflation selon la Banque mondiale
	year = getDataLastYearSourced("nif","nations_inflations",pays, unicode("Banque mondiale"))
	inflation_BM = getDataValueSourced("nif","nations_inflations",pays, year, unicode("Banque mondiale"))
	inflation_BM = formatFloat(iAmAFloat(inflation_BM))	
	if (inflation_BM == "0,00") or (inflation_BM ==""):
		inflation_BM_str = "Aucune donn&eacute;e"
	else:
		inflation_BM_str = "<b>{} %</b> ({})".format(inflation_BM, year)

	# 3.1.25. Taux d'inflation selon le FMI
	year = getDataLastYearSourced("nif","nations_inflations",pays, unicode("FMI"))
	inflation_FMI = getDataValueSourced("nif","nations_inflations",pays, year, unicode("FMI"))
	inflation_FMI = formatFloat(iAmAFloat(inflation_FMI))	
	if (inflation_FMI == "0,00") or (inflation_FMI ==""):
		inflation_FMI_str = "Aucune donn&eacute;e"
	else:
		inflation_FMI_str = "<b>{} %</b> ({})".format(inflation_FMI, year)
			
	# 3.1.19. Part des principaux secteurs d'activité dans le PIB
	year_agri = getDataLastYear("nav","nations_agriculture_value_added",pays)
	agricultureetSource = getDataValueSource("nav","nations_agriculture_value_added",pays, year)
	agriculture = agricultureetSource[0]
	source_agri = agricultureetSource[1]
	agriculture = iAmAFloat(agriculture)
	agriculture_s = formatFloat(agriculture)
	if (agriculture_s == "0,00") or (agriculture_s == ""):
		agriculture_str = "Agriculture : aucune donn&eacute;e"
	else:
		agriculture_str = "Agriculture : <b>{} %</b> ({}, {})".format(agriculture_s, year, source_agri)	
	
	year_indus = getDataLastYear("niv","nations_industry_value_added",pays)
	industryetSource = getDataValueSource("niv","nations_industry_value_added",pays, year)
	industry = industryetSource[0]
	source_indus = agricultureetSource[1]
	industry = iAmAFloat(industry)
	industry_s = formatFloat(industry)
	if (industry_s == "0,00") or (industry_s == ""):
		industry_str = "Industrie : aucune donn&eacute;e"
	else:
		industry_str = "Industrie : <b>{} %</b> ({}, {})".format(industry_s, year_indus, source_indus)		
	
	year_serv = getDataLastYear("nsv","nations_services_value_added",pays)
	servicesetSource = getDataValueSource("nsv","nations_services_value_added",pays, year)
	services = servicesetSource[0]
	source_serv = servicesetSource[1]
	services = iAmAFloat(services)
	services_s = formatFloat(services)
	if (services_s == "0,00") or (services_s == ""):
		services_str = "Services : aucune donn&eacute;e"
	else:
		services_str = "Services : <b>{} %</b> ({}, {})".format(services_s, year_serv, source_serv)

	activities_str = setTD(agriculture_str)+"</tr>\n<tr>\n<td>&nbsp;</td>\n"+setTD(industry_str)+"</tr>\n<tr>\n<td>&nbsp;</td>\n"+setTD(services_str)
	
	# 3.1.20. Importations totales
	year_imp = getDataLastYear("nim","nations_imports",pays)
	vimportations_list = getDataValueSource("nim","nations_imports",pays, year_imp)
	vimportations_all = vimportations_list[0]
	vimportations_all_source = vimportations_list[1]
	vimportations_all = iAmAnInteger(vimportations_all)
	vimportations_all_s = formatInt(vimportations_all)
	year_imp = iAmAnInteger(year_imp)
	if vimportations_all_s == "0":
		vimportations_all_str = "Aucune donn&eacute;e"
	vimportations_all_str = "<b>{} USD</b> ({})".format(vimportations_all_s, year_imp)
		
	# 3.1.21. Exportations totales
	year_exp =  getDataLastYear("nex","nations_exports",pays)
	vexportations_list = getDataValueSource("nex","nations_exports",pays, year_exp)
	vexportations_all = vexportations_list[0]
	vexportations_all_source = vexportations_list[1]
	vexportations_all = iAmAnInteger(vexportations_all)
	vexportations_all_s = formatInt(vexportations_all)
	year_exp = iAmAnInteger(year_exp)
	if vexportations_all_s == "0":
		vexportations_all_str = "Aucune donn&eacute;e"
	vexportations_all_str = "<b>{} USD</b> ({})".format(vexportations_all_s, year_exp)
	
	# 3.1.23 Balance commerciale totale
	if year_exp == year_imp:
		balance_all = vexportations_all-vimportations_all
		balance_all_s = formatInt(balance_all)
		balance_all_str = "<b>{} USD</b>".format(balance_all_s)	
	else:
		balance_all_str = "Aucune donn&eacute;e ou donn&eacute;es incoh&eacute;rentes"

	# 3.1.24 Balance des paiements selon la Banque mondiale
	year = getDataLastYearSourced("nca","nations_account_balances",pays, unicode("Banque mondiale"))
	nac_BM = getDataValueSourced("nca","nations_account_balances",pays, year, unicode("Banque mondiale"))
	nac_BM = formatFloat(iAmAFloat(nac_BM))	
	if (nac_BM == "0,00") or (nac_BM ==""):
		nac_BM_str = "Aucune donn&eacute;e"
	else:
		nac_BM_str = "<b>{} %</b> ({})".format(nac_BM, year)
		
	# 3.1.25 Balance des paiements selon le FMI
	year = getDataLastYearSourced("nca","nations_account_balances",pays, unicode("FMI"))
	nac_FMI = getDataValueSourced("nca","nations_account_balances",pays, year, unicode("FMI"))
	nac_FMI = formatFloat(iAmAFloat(nac_FMI))	
	if (nac_FMI == "0,00") or (nac_FMI ==""):
		nac_FMI_str = "Aucune donn&eacute;e"
	else:
		nac_FMI_str = "<b>{} %</b> ({})".format(nac_FMI, year)
		
	# 3.1.26. Investissements directs étrangers
	# Nous n'avons que la dernière année dans notre BDD	
	year = getDataLastYear("nfdi","nations_fdi",pays)
	fdietSource = getDataValueSource("nfdi","nations_fdi",pays, year)
	fdi = fdietSource[0]
	source = fdietSource[1]
	fdi = iAmAnInteger(fdi)
	fdi_s = formatInt(fdi)
	if (fdi_s == "0") or (fdi_s == "") or (fdi_s == "00"):
		fdi_str = "Aucune donn&eacute;e"
	else:
		fdi_str = "<b>{} USD</b> ({}, {})".format(fdi_s, year, source)

	# 3.1.27. Rang dans le classement mondial "Doing Business"	
	doing_business_str = ""	
	doing_business = getValue("ndbr","nations_doing_business_ranks",pays)	
	doing_business = iAmAnInteger(doing_business)
	doing_business_s = formatInt(doing_business)
	if doing_business == 1:
		cardinal = "er"
	else:
		cardinal  = "&egrave;me"
	if (doing_business_s == "0") or (doing_business_s == "") or (doing_business_s == "00"):		
		doing_business_str = "Aucune donn&eacute;e"
	else:	
		doing_business_str = "<b>{}{}</b>".format(doing_business_s, cardinal)
		
	# 3.1.28. Echanges de produits depuis la France (Kiosque)
	years = getAllYears("ki","kiosque_imports",pays)
	if len(years) == 0:
		echanges_str = ""
	else:
		echanges_str = "<tr>\n<td>&nbsp;</td>"		
		for year in years:
			# Créons déjà notre tableau
			year = str(year)			
			year = setTHCenter(year)
			echanges_str = echanges_str+year
		echanges_str = echanges_str+"</tr>\n"
		echanges_str = echanges_str+"<tr>\n"+setTH("Importations en France (DGT, euros)</td>")
		for year in years:
			vimportations_FR = getDataValue("ki","kiosque_imports",pays,year)
			vimportations_FR_i = iAmAnInteger(vimportations_FR)
			vimportations_FR_s = formatInt(vimportations_FR_i)
			vimportations_FR_str = setTDRight("{}".format(vimportations_FR_s))
			echanges_str = echanges_str+vimportations_FR_str
		echanges_str = echanges_str+"</tr>\n"
		echanges_str = echanges_str+"<tr>\n"+setTH("Exportations depuis la France (DGT, euros)")
		for year in years:
			vexportations_FR = getDataValue("ke","kiosque_exports",pays,year)
			vexportations_FR_i = iAmAnInteger(vexportations_FR)
			vexportations_FR_s = formatInt(vexportations_FR_i)
			vexportations_FR_str = setTDRight("{}".format(vexportations_FR_s))
			echanges_str = echanges_str+vexportations_FR_str
		echanges_str = echanges_str+"</tr>\n"
		echanges_str = echanges_str+"<tr>\n"+setTH("Solde commercial (export.-import, DGT, euros)")
		for year in years:
			vimportations_2_FR = getDataValue("ki","kiosque_imports",pays,year)
			vimportations_2_FR_i = iAmAnInteger(vimportations_2_FR)			
			vexportations_2_FR = getDataValue("ke","kiosque_exports",pays,year)
			vexportations_2_FR_i = iAmAnInteger(vexportations_2_FR)
			vbalance_FR_i = vexportations_2_FR_i - vimportations_2_FR_i 
			vbalance_FR_s = formatInt(vbalance_FR_i)
			vbalance_FR_str = setTDRight("{}".format(vbalance_FR_s))
			echanges_str = echanges_str+vbalance_FR_str
		echanges_str = echanges_str+"</tr>\n"
			
	# 3.1.31 Cinq principaux fournisseurs
	liste_Fournisseurs = getBestProviders(pays)
	# Commençons par trier le résultat du moins important au plus important
	liste_Fournisseurs.sort(key=lambda fournisseur: fournisseur[1])
	try:
		fournisseur_5 = liste_Fournisseurs[0]
	except:
		fournisseur_5 = "N/A"
	try:	
		fournisseur_4 = liste_Fournisseurs[1]
	except:
		fournisseur_4 = "N/A"
	try:
		fournisseur_3 = liste_Fournisseurs[2]
	except:
		fournisseur_3 = "N/A"
	try:	
		fournisseur_2 = liste_Fournisseurs[3]
	except:
		fournisseur_2 = "N/A"
	try:	
		fournisseur_1 = liste_Fournisseurs[4]
	except:
		fournisseur_1 = "N/A"
	# 1er
	if fournisseur_1 != "N/A":	
		code_pays_1 = fournisseur_1[0]
		nom_pays_1 = reformatString(deAlpha2versLibelleFR(code_pays_1))
		pdm_pays_1 = fournisseur_1[1]
		annee_pays_1 = fournisseur_1[2]
		source_pays_1 = reformatString(fournisseur_1[3])
		pdm_pays_1 = iAmAFloat(pdm_pays_1)
		pdm_pays_1 = formatFloat(pdm_pays_1)
		f1_str = "1er - <b>{}</b> : {} % ({}, {})".format(nom_pays_1, pdm_pays_1, source_pays_1,annee_pays_1)
			
	else:
		f1_str = "Aucune donn&eacute;e"
	f1_str = setTD(f1_str)	
	# 2ème	
	if fournisseur_2 != "N/A":
		code_pays_2 = fournisseur_2[0]
		nom_pays_2 = reformatString(deAlpha2versLibelleFR(code_pays_2))
		pdm_pays_2 = fournisseur_2[1]
		annee_pays_2 = fournisseur_2[2]
		source_pays_2 = reformatString(fournisseur_2[3])
		pdm_pays_2 = iAmAFloat(pdm_pays_2)
		pdm_pays_2 = formatFloat(pdm_pays_2)
		f2_str = "2&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_2, pdm_pays_2, source_pays_2, annee_pays_2) 
	else:
		f2_str = "Aucune donn&eacute;e"
	f2_str = setTD(f2_str)
	f2_str = "</tr>\n<tr>\n<td>&nbsp;</td>\n"+f2_str	
	# 3ème	
	if fournisseur_3 != "N/A":
		code_pays_3 = fournisseur_3[0]
		nom_pays_3 = reformatString(deAlpha2versLibelleFR(code_pays_3))
		pdm_pays_3 = fournisseur_3[1]
		annee_pays_3 = fournisseur_3[2]
		source_pays_3 = reformatString(fournisseur_3[3])
		pdm_pays_3 = iAmAFloat(pdm_pays_3)
		pdm_pays_3 = formatFloat(pdm_pays_3)
		f3_str = "3&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_3, pdm_pays_3, source_pays_3, annee_pays_3) 
	else:
		f3_str = "Aucune donn&eacute;e"
	f3_str = setTD(f3_str)
	f3_str = "</tr>\n<tr>\n<td>&nbsp;</td>\n"+f3_str	
	# 4ème	
	if fournisseur_4 != "N/A":	
		code_pays_4 = fournisseur_4[0]
		nom_pays_4 = reformatString(deAlpha2versLibelleFR(code_pays_4))
		pdm_pays_4 = fournisseur_4[1]
		annee_pays_4 = fournisseur_4[2]
		source_pays_4 = reformatString(fournisseur_4[3])
		pdm_pays_4 = iAmAFloat(pdm_pays_4)
		pdm_pays_4 = formatFloat(pdm_pays_4)
		f4_str = "4&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_4,pdm_pays_4, source_pays_4,annee_pays_4) 
	else:
		f4_str = "Aucune donn&eacute;e"
	f4_str = setTD(f4_str)
	f4_str = "</tr>\n<tr>\n<td>&nbsp;</td>\n"+f4_str	
	# 5ème
	if fournisseur_5 != "N/A":
		code_pays_5 = fournisseur_5[0]
		nom_pays_5 = reformatString(deAlpha2versLibelleFR(code_pays_5))
		pdm_pays_5 = fournisseur_5[1]
		annee_pays_5 = fournisseur_5[2]
		source_pays_5 = reformatString(fournisseur_5[3])
		pdm_pays_5 = iAmAFloat(pdm_pays_5)
		pdm_pays_5 = formatFloat(pdm_pays_5)
		f5_str = "5&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_5, pdm_pays_5, source_pays_5, annee_pays_5) 
	else:
		f5_str = "Aucune donn&eacute;e"
	f5_str = setTD(f5_str)
	f5_str = "</tr>\n<tr>\n<td>&nbsp;</td>\n"+f5_str+"</tr>\n"	
	fournisseurs_str = f1_str+f2_str+f3_str+f4_str+f5_str
			
	# 3.1.32 Cinq principaux clients
	liste_Clients = getBestClients(pays)
	# Commençons par trier le résultat du moins important au plus important
	liste_Clients.sort(key=lambda client: client[1])
	try:
		client_5 = liste_Clients[0]
	except:
		client_5 = "N/A"
	try:	
		client_4 = liste_Clients[1]
	except:
		client_4 = "N/A"
	try:
		client_3 = liste_Clients[2]
	except:
		client_3 = "N/A"
	try:	
		client_2 = liste_Clients[3]
	except:
		client_2 = "N/A"
	try:	
		client_1 = liste_Clients[4]
	except:
		client_1 = "N/A"
	# 1er
	if client_1 != "N/A":	
		code_pays_1 = client_1[0]
		nom_pays_1 = reformatString(deAlpha2versLibelleFR(code_pays_1))
		pdm_pays_1 = client_1[1]
		annee_pays_1 = client_1[2]
		source_pays_1 = reformatString(client_1[3])
		pdm_pays_1 = iAmAFloat(pdm_pays_1)
		pdm_pays_1 = formatFloat(pdm_pays_1)
		c1_str = "1er - <b>{}</b> : {} % ({}, {})".format(nom_pays_1, pdm_pays_1, source_pays_1,annee_pays_1) 
	else:
		c1_str = "Aucune donn&eacute;e"
	c1_str = setTD(c1_str)
	c1_str = c1_str+"</tr>\n"
	# 2ème	
	if client_2 != "N/A":
		code_pays_2 = client_2[0]
		nom_pays_2 = reformatString(deAlpha2versLibelleFR(code_pays_2))
		pdm_pays_2 = client_2[1]
		annee_pays_2 = client_2[2]
		source_pays_2 = reformatString(client_2[3])
		pdm_pays_2 = iAmAFloat(pdm_pays_2)
		pdm_pays_2 = formatFloat(pdm_pays_2)
		c2_str = "2&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_2, pdm_pays_2, source_pays_2, annee_pays_2) 
	else:
		c2_str = "Aucune donn&eacute;e"
	c2_str = setTD(c2_str)
	c2_str = "<tr>\n<td>&nbsp;</td>\n"+c2_str+"</tr>\n"
	# 3ème	
	if client_3 != "N/A":
		code_pays_3 = client_3[0]
		nom_pays_3 = reformatString(deAlpha2versLibelleFR(code_pays_3))
		pdm_pays_3 = client_3[1]
		annee_pays_3 = client_3[2]
		source_pays_3 = reformatString(client_3[3])
		pdm_pays_3 = iAmAFloat(pdm_pays_3)
		pdm_pays_3 = formatFloat(pdm_pays_3)
		c3_str = "3&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_3, pdm_pays_3, source_pays_3, annee_pays_3) 
	else:
		c3_str = "Aucune donn&eacute;e"
	c3_str = setTD(c3_str)
	c3_str = "<tr>\n<td>&nbsp;</td>\n"+c3_str+"</tr>\n"
	# 4ème	
	if client_4 != "N/A":	
		code_pays_4 = client_4[0]
		nom_pays_4 = reformatString(deAlpha2versLibelleFR(code_pays_4))
		pdm_pays_4 = client_4[1]
		annee_pays_4 = client_4[2]
		source_pays_4 = reformatString(client_4[3])
		pdm_pays_4 = iAmAFloat(pdm_pays_4)
		pdm_pays_4 = formatFloat(pdm_pays_4)
		c4_str = "4&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_4,pdm_pays_4, source_pays_4,annee_pays_4) 
	else:
		c4_str = "Aucune donn&eacute;e"
	c4_str = setTD(c4_str)
	c4_str = "<tr>\n<td>&nbsp;</td>\n"+c4_str+"</tr>\n"

	# 5ème
	if client_5 != "N/A":
		code_pays_5 = client_5[0]
		nom_pays_5 = reformatString(deAlpha2versLibelleFR(code_pays_5))
		pdm_pays_5 = client_5[1]
		annee_pays_5 = client_5[2]
		source_pays_5 = reformatString(client_5[3])
		pdm_pays_5 = iAmAFloat(pdm_pays_5)
		pdm_pays_5 = formatFloat(pdm_pays_5)
		c5_str = "5&egrave;me - <b>{}</b> : {} % ({}, {})".format(nom_pays_5, pdm_pays_5, source_pays_5, annee_pays_5) 
	else:
		c5_str = "Aucune donn&eacute;e"
	c5_str = setTD(c5_str)
	c5_str = "<tr>\n<td>&nbsp;</td>\n"+c5_str+"</tr>\n"

	clients_str = c1_str+c2_str+c3_str+c4_str+c5_str
		
	# 3.1.33 Tonnes de CO2 produites par habitant	
	# Nous n'avons que la dernière année dans notre BDD	
	year = getDataLastYear("nce","nations_CO2_emissions_per_capita",pays)
	CO2etSource = getDataValueSource("nce","nations_CO2_emissions_per_capita",pays, year)
	CO2 = CO2etSource[0]
	source = CO2etSource[1]
	source = reformatString(source)
	CO2 = iAmAFloat(CO2)
	CO2_s = formatFloat(CO2)
	if CO2 > 2.00:
		tonne = "tonnes"
	else:
		tonne = "tonne"
	if (CO2_s == "0,00") or (CO2_s == ""):
		CO2_str = "Aucune donn&eacute;e"
	else:
		CO2_str = "<b>{} {}</b> ({}, {})".format(CO2_s, tonne, year, source)	

	# 3.1.34 Investissements français dans le pays
	years = getAllYears("nfi","nations_french_investments",pays)
	if len(years) == 0:
		investissements_FR_str = ""
	else:
		investissements_FR_str = "<tr>\n<td>&nbsp;</td>"		
		for year in years:
			year = str(year)
			year = setTHCenter(year)
			investissements_FR_str = investissements_FR_str+year
		investissements_FR_str = investissements_FR_str+"</tr>"
		investissements_FR_str = investissements_FR_str+"<tr>\n"+setTH("Investissements fran&ccedil;ais dans le pays (euros, Banque de France)")
		for year in years:
			fi = getDataValue("nfi","nations_french_investments",pays, year)		
			fi = formatInt(iAmAnInteger(fi))
			if (fi == "0") or (fi =="") or (fi == "00"):
				fi_str = "N/A"
			else:
				fi_str = setTDRight("{}".format(fi))
				investissements_FR_str = investissements_FR_str+fi_str
		investissements_FR_str = investissements_FR_str+"</tr>"
		investissements_FR_str = investissements_FR_str+"<tr>\n"+setTH("Investissements du pays en France (euros, Banque de France)")
		for year in years:
			nif = getDataValue("nif","nations_investment_in_France",pays, year)		
			nif = formatInt(iAmAnInteger(nif))
			if (nif == "0") or (nif =="") or (nif == "00"):
				nif_str = ""
			else:
				nif_str = setTDRight("{}".format(nif))
				investissements_FR_str = investissements_FR_str+nif_str
		investissements_FR_str = investissements_FR_str+"</tr>"
	
	# 3.1.36 Coefficient de Gini
	# Nous n'avons que la dernière année dans notre BDD	
	year = getDataLastYear("ngc","nations_gini_coefficients",pays)
	ginietSource = getDataValueSource("ngc","nations_gini_coefficients",pays, year)
	gini = ginietSource[0]
	source = ginietSource[1]
	source = reformatString(source)
	year = year.replace("/"," &grave; ")
	year = reformatString(year)
	gini_s = formatFloat(iAmAFloat(gini))
	if (gini_s == "0,00") or (gini_s == ""):
		gini_str = "Aucune donn&eacute;e"
	else:
		gini_str = "<b>{}</b> ({}, {})".format(gini_s, year, source)	

	# 3.1.37 Taux d'homicide
	# Nous n'avons que la dernière année dans notre BDD	
	year = getDataLastYear("nhr","nations_homicides_rates",pays)
	homicetSource = getDataValueSource("nhr","nations_homicides_rates",pays, year)
	homic = homicetSource[0]
	source = homicetSource[1]
	source = reformatString(source)
	year = year.replace("/"," &grave; ")
	year = reformatString(year)
	homic_s = formatFloat(iAmAFloat(homic))
	if (homic_s == "0,00") or (homic_s == ""):
		homic_str = "<b>Aucune donn&eacute;e</b>"
	else:
		homic_str = "<b>{} &permil;</b> ({}, {})".format(homic_s, year, source)	
	
	# 3.1.38 Utilisateurs d'Internet
	# Nous n'avons que la dernière année dans notre BDD	
	year = getDataLastYear("niu","nations_internet_users",pays)
	internetetSource = getDataValueSource("niu","nations_internet_users",pays, year)
	internet = internetetSource[0]
	source = internetetSource[1]
	source = reformatString(source)
	internet_s = formatFloat(iAmAFloat(internet))
	if (internet_s == "0,00") or (internet_s == ""):
		internet_str = "<b>Aucune donn&eacute;e</b>"
	else:
		internet_str = "<b>{} %</b>  de la population ({}, {})".format(internet_s, year, source)		
	
	# 3.1.39 Douanes : 	
	
	# 3.2. Construction du fichier
	
	# Le nom de mon fichier sera de la forme "code pays ISO 2 lettres.html"	
	file_name = pays+".html"
	log("Création et enregistrement du fichier %s" % file_name.encode("utf8"))
	complete_file_name = os.path.join(path,file_name)
	
	# Je crée mon fichier
	f_html = open(complete_file_name, 'w')
	f_html.write("<table border = \"0\" width = \"95 %\" align=\"center\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td colspan=3><p><font size=\"-2\"><i>"+date_MAJ_str+"</i></font></p></td>\n")		
	f_html.write("</tr>\n")
	f_html.write("</table>\n")	
	f_html.write("<table border = \"0\" width = \"95 %\" align=\"center\" style=\"border-top:5px solid;border-bottom:5px solid;border-color:#939597;border-color:#939597;\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td align=\"center\" width = \"110\" valign=middle >\n")
	f_html.write("<img src=\"http://www.mesfichespays.fr/wp-content/uploads/2016/08/Mesfichespaysfr.png\" width=\"100\">\n")
	f_html.write("</td>\n")
	f_html.write("<td align=\"center\" valign=middle><h2>")
	f_html.write("<p style=\"text-align:center\">"+nom_pays_str+"</p>")	
	f_html.write("</h2></td>\n")
	f_html.write("<td align=\"center\" width = \"110\" valign=middle>\n")
	f_html.write("<img src=\"http://mesfichespays.fr/wp-content/uploads/2016/08/"+pays+".svg\"  width=\"100\">\n")
	f_html.write("</td>\n")
	f_html.write("</tr>\n")
	f_html.write("</table>\n")
	f_html.write("<br />\n")	
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td colspan=2><h3>Donn&eacute;es politiques</h3></td>\n")	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Nom officiel"))
	f_html.write(setTD(off_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Code ISO 3166-1 alpha 2"))
	f_html.write(setTD("<b>"+pays+"</b>"))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Capitale"))
	f_html.write(setTD(capitale_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Nature du r&eacute;gime"))
	f_html.write(setTD(state_form_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Langue(s) officielle(s)"))
	f_html.write(setTD(langsOff_str))
	f_html.write("</tr>\n")
	f_html.write(chef1_str)
	f_html.write(chef2_str)
	f_html.write(chef3_str)
	f_html.write("</table>\n")

	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td colspan=2><h3>Donn&eacute;es g&eacute;ographiques</h3></td>\n")	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Superficie"))
	f_html.write(setTD(area_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH(mainTowns_label))
	f_html.write(mainTowns_str)
	f_html.write("<tr>\n")
	f_html.write(setTH(mainAgglos_label))
	f_html.write(mainAgglos_str)
	f_html.write("</table>\n")
	
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td colspan=2><h3>Donn&eacute;es d&eacute;mographiques</h3></td>\n")	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Population"))
	f_html.write(setTD(population_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Densit&eacute;"))
	f_html.write(setTD(density_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Croissance d&eacute;mographique"))
	f_html.write(setTD(population_growth_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Taux de f&eacute;condit&eacute;"))
	f_html.write(setTD(fertility_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Indice de d&eacute;veloppement humain"))
	f_html.write(setTD(HDI_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Esp&eacute;rance de vie &agrave; la naissance"))
	f_html.write(setTD(life_expectancy_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Taux d'alphab&eacute;tisation"))
	f_html.write(setTD(literacy_rate_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Taux d'homicides"))
	f_html.write(setTD(homic_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Taux d'acc&egrave;s &agrave; l'Internet"))
	f_html.write(setTD(internet_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH(langs_label))
	f_html.write(langs_str)
	f_html.write("<tr>\n")
	f_html.write(setTH(rels_label))
	f_html.write(rels_str)
	f_html.write("<tr>\n")
	f_html.write(setTH("CO2 &eacute;mis par habitant"))
	f_html.write(setTD(CO2_str))
	f_html.write("</tr>\n")
	f_html.write("</table>\n")

	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td colspan=2><h3>Donn&eacute;es &eacute;conomiques</h3></td>\n")	
	f_html.write("</tr>\n")
	f_html.write("</table>\n")
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write(macroeco_table_headers)
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(PIBs_BM_str)
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(PIBs_FMI_str)	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(PIBs_BM_pc_str)
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(PIBs_FMI_pc_str)
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(PIBgrowth_str)
	f_html.write("</tr>\n")
	f_html.write("</table>\n")
	f_html.write("<p style=\"text-align:center\"><img src=\"http://www.mesfichespays.fr/fichiers/PIB_BM_"+pays+".png\" width=\"500\"></p>\n")	
	f_html.write("<p style=\"text-align:center\"><img src=\"http://www.mesfichespays.fr/fichiers/PIB_BMpc_"+pays+".png\" width=\"500\"></p>\n")	
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Monnaie"))
	f_html.write(setTD(monnaie_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")
	f_html.write(setTH("Cat&eacute;gorie Banque mondiale"))
	f_html.write(setTD(cat_str))
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Part des principaux secteurs d'activit&eacute; dans le PIB :"))
	f_html.write(activities_str)	
	f_html.write("</tr>\n")	
	f_html.write("<tr>\n")	
	f_html.write(setTH("Rang au classement <i>Doing Business</i> (Banque mondiale)"))
	f_html.write(setTD(doing_business_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Taux de ch&ocirc;mage (Banque mondiale)"))
	f_html.write(setTD(unemployment_BM_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Taux de ch&ocirc;mage (FMI)"))
	f_html.write(setTD(unemployment_FMI_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Taux de ch&ocirc;mage (OIT)"))
	f_html.write(setTD(unemployment_ILO_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Taux de pauvret&eacute; (Pnud)"))
	f_html.write(setTD(poverty_UNDP_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Taux de pauvret&eacute; (Banque mondiale)"))
	f_html.write(setTD(poverty_BM_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Coefficient de Gini"))
	f_html.write(setTD(gini_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("D&eacute;ficit budg&eacute;taire"))
	f_html.write(setTD(deficit_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Dette publique"))
	f_html.write(setTD(debt_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Taux d'inflation (Banque mondiale)"))
	f_html.write(setTD(inflation_BM_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Taux d'inflation (FMI)"))
	f_html.write(setTD(inflation_FMI_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Balance des paiements (Banque mondiale)"))
	f_html.write(setTD(nac_BM_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Balance des paiements (FMI)"))
	f_html.write(setTD(nac_FMI_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Importations totales (Banque mondiale, WITS)"))
	f_html.write(setTD(vimportations_all_str))	
	f_html.write("</tr>\n")
	f_html.write("<tr>\n")	
	f_html.write(setTH("Exportations totales (Banque mondiale, WITS)"))
	f_html.write(setTD(vexportations_all_str))	
	f_html.write("</tr>\n")	
	f_html.write("<tr>\n")	
	f_html.write(setTH("Balance commerciale totale (export.-import.)"))
	f_html.write(setTD(balance_all_str))	
	f_html.write("</tr>\n")	
	f_html.write("<tr>\n")	
	f_html.write(setTH("5 principaux fournisseurs"))
	f_html.write(fournisseurs_str)	
	f_html.write("</tr>\n")	
	f_html.write("<tr>\n")	
	f_html.write(setTH("5 principaux clients"))
	f_html.write(clients_str)	
	f_html.write("</tr>\n")	
	f_html.write("<tr>\n")	
	f_html.write(setTH("Investissement direct &eacute;tranger dans le pays"))
	f_html.write(setTD(fdi_str))	
	f_html.write("</tr>\n")	
	f_html.write("</table>\n")
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td colspan=2><h3>Echanges &eacute;conomiques et commerciaux avec la France</h3></td>\n")	
	f_html.write("</tr>\n")
	f_html.write("</table>\n")		
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write(echanges_str)
	f_html.write("</table>\n")
	f_html.write("<p style=\"text-align:center\"><img src=\"http://www.mesfichespays.fr/fichiers/echanges_commerciaux_"+pays+".png\" width=\"500\"></p>\n")	
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write(investissements_FR_str)
	f_html.write("</table>\n")	
	f_html.write("<table border = \"0\" width = \"95 %\" align=center style=\"border-spacing:5px;padding:5px;border-collapse:separate;\">\n")
	f_html.write("<tr>\n")
	f_html.write("<td colspan=2><h3>T&eacute;l&eacute;chargements</h3></td>\n")	
	f_html.write("</tr>\n")
	f_html.write("</table>\n")		
	f_html.write("<h4>Fiche signal&eacute;tique</h4>\n")
	f_html.write("<p align = \"center\"><a href =\"http://www.mesfichespays.fr/fichiers/"+pays+"_fs.docx\" target=\"_blank\" /><img src =\"http://www.mesfichespays.fr/wp-content/uploads/2016/08/docx.png\" width=\"50\" /></img></a></p>\n")
	f_html.close()