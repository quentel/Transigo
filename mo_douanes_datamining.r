
##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                        mo_douanes_datamining.r                             #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script reprend les fichiers des douanes nettoyés avec mo_douanes_cleaning.r
# pour en sortir les informations pertinentes que nous insérerons en python dans
# notre base de données

# Récupérons le chemin de notre fichier, passé en argument au moment du lancement du script par Python
myArgs <- commandArgs(trailingOnly = FALSE)
myPath <- paste(myArgs[7], "/datas/douanes/", sep = "", collapse = NULL)
# Fixons un nouveau répertoire de travail
setwd(myPath)

# Récupérons la liste des fichiers .csv que nous avons modifiés avec mo_douanes_cleaning.r
files_list <- list.files(pattern="^exportations|^importations_.*\\.csv", full.names = FALSE)
# On ouvre nos fichiers un par un
for (file in files_list) 
{
  # Initialisation de la dataframe qui contiendra les informations de ce fichier
  dataset <- data.frame()
  dataset <- read.csv(file, header=TRUE, sep=",")
  # On récupère l'année ainsi traitée
  first_line <- head(dataset,1)
  my_year <- first_line$year
  # Le nom de notre fichier contient-il "importations" ?
  pos <- grepl("importations", file)
  # Si oui, nous l'enregistrerons sous ce nom
  if (pos == TRUE)
    {
    file_core = "importations"
    }
# Si non, ce seront des "exportations"
  else
    {
    file_core = "exportations"
    }
  # On peut maintenant compiler les données par partenaire et par an
  dataset1 <- aggregate(dataset$x, list(partner=dataset$partner, year=dataset$year), FUN=sum)
  # Puis par catégorie de produits, par partenaire et par an
  dataset2 <- aggregate(dataset$x, list(partner=dataset$partner, nc8=dataset$nc8, year=dataset$year), FUN=sum)
  # ...Et on enregistre le tout dans des fichiers distincts
  file_name1 <- paste(file_core, "_by_years_and_partners_", my_year, ".csv", sep = "", collapse = NULL)
  file_name2 <- paste(file_core, "_by_years_products_and_partners_", my_year, ".csv", sep = "", collapse = NULL)
  write.csv(dataset1, file_name1)
  write.csv(dataset2, file_name2)
  # Et on supprime le fichier traité
  file.remove(file)
}