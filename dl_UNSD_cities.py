#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                            dl_UNSD_cities.py                               #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge certains fichiers de la division des statistiques de l'ONU

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, urllib2, zipfile
from functions import log

log("")
log("=====================================")
log("Lancement du script dl_UNSD_cities.py")
log("=====================================")

# DONNEES SUR LES VILLES

# L'URL du premier fichier à télécharger est fixe :
# http://data.un.org/Handlers/DownloadHandler.ashx?DataFilter=tableCode:240&DataMartId=POP&Format=scsv&c=2,3,6,8,10,12,14,16,17,18&s=_countryEnglishNameOrderBy:asc,refYear:desc,areaCode:asc

# 2. SCRIPT

# Nom et chemin du fichier que nous allons créer
dl_path = "datas/unsd/cities/"
dl_file = dl_path+"cities_unsd.zip"

# Si le répertoire n'existe pas, créons-le
dl_dir = os.path.dirname(dl_path)
if not os.path.exists(dl_path):
	log("Création du répertoire destiné à accueillir notre fichier : datas/unsd/cities/")
	os.makedirs(dl_dir)

url =	"http://data.un.org/Handlers/DownloadHandler.ashx?DataFilter=tableCode:240&DataMartId=POP&Format=scsv&c=2,3,6,8,10,12,14,16,17,18&s=_countryEnglishNameOrderBy:asc,refYear:desc,areaCode:asc"
try:	
	log("Tentative de téléchargement sur l'URL :"+url)
	req = urllib2.Request(url)
	with open(dl_file,'w') as f:
		f.write(urllib2.urlopen(req).read())
	f.close()
	log("Téléchargement des données sur les villes de l'UNSD terminé !")
	try:
		with zipfile.ZipFile(dl_file, "r") as z:
			z.extractall(dl_path)
		log("Décompression du fichier "+dl_file)
		try:
			os.remove(dl_file)
		except:
			log("***Echec de la supression du fichier "+dl_file)
			pass
	except:
		log("***Echec de la décompression du fichier "+dl_file)
		pass
except:
	log("***Echec du téléchargement des données sur les villes de l'UNSD !")
	pass