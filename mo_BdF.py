#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                              mo_BdF.py                                     #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script parse les données publiées par la Banque de France afin de récupérer
# les stocks et flux d'investissements directs français à l'étranger et étrangers
# en France.

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, urllib2
from bs4 import BeautifulSoup
from functions import log, listePays

log("")
log("=============================")
log("Lancement du script mo_BdF.py")
log("=============================")

# 2. SCRIPT

compteur = 0

# Les données qui nous intéressent sont publiées à deux adresses :
#	- pour les IDE sortants (de la France vers le pays tiers) :
# http://webstat.banque-france.fr/fr/quickview.do?SERIES_KEY=315.CDIS.A.DO.FR.CODE_PAYS_PARTENAIRE_A_DEUX_LETTRES_.S1.IMC.LE.NO.FA.D.F.ALL.EUR.I._X._X.FDI_T._Z	
#	- pour les IDE entrants (du  pays tiers vers la France) :
# http://webstat.banque-france.fr/fr/quickview.do?SERIES_KEY=315.CDIS.A.DI.FR.CODE_PAYS_PARTENAIRE_A_DEUX_LETTRES.S1.IMC.LE.NI.FA.D.F.ALL.EUR.I._X._X.FDI_T._Z
# 	 http://webstat.banque-france.fr/fr/browseSelection.do

bdf_url_base = "http://webstat.banque-france.fr/fr/quickview.do?SERIES_KEY=315.CDIS.A."

for pays in listePays():

	str_pays = pays.encode('utf8')
		
	# 2.1. IDE sortants (de la France vers le pays tiers)

	log("Récupération et injection des investissements de la France vers %s" % str_pays)
	url_ide_sortants = bdf_url_base+"DO.FR."+pays	
	complement_url_ide_sortants = ".S1.IMC.LE.NO.FA.D.F.ALL.EUR.I._X._X.FDI_T._Z"
	url_ide_sortants = url_ide_sortants+complement_url_ide_sortants
	# Ouvrons cette page et lisons-la
	ide_sortants_reader = urllib2.urlopen(url_ide_sortants).read()
	# Faisons notre soupe avec le résultat
	ide_sortants_soup = BeautifulSoup(ide_sortants_reader, 'lxml')
	# Le plus simple est de chercher d'abord les cellules contenant les données, qui peuvent être de deux styles :
	# "light" ou "dark" (ce sont des tableaux avec des lignes de couleurs alternées)
	cells_light = ide_sortants_soup.find_all("td", class_="light value")
	for cell_light in cells_light:
		value = cell_light.string
		# On supprime les éventuelles espaces (oui, espace est féminin)
		# Ce coup-là mérite une explication : le chiffre originel, sur le site de la Banque de France, a un caractère insécable
		# en HTML (&nbsp;) comme séparateur de milliers. Mais BeautifulSoup convertit automatiquement tout le HTML en Unicode.
		# C'est donc un caractère insécable Unicode qu'il faut traquer et supprimer pour avoir un nombre propre au delà de 999... 		
		value = value.replace(u'\xa0','')
		try:
			value = int(value)
			value = value*1000000
		except:
			value = "N/A"
		# On convertit le résultat en Unicode
		value = unicode(value)
		# Une fois que nous avons la donnée, nous pouvons trouver l'année à laquelle elle se rattache, dans la cellule précédente
		prev_cell_light = cell_light.find_previous_siblings("td")
		# Notre année est le premier résultat que nous cherchons		
		year = unicode(prev_cell_light[0].string)
		# On colle le tout dans notre base de données
		try:
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_french_investments (nfi_ISO_3166_1_alpha_2, nfi_years, nfi_values) VALUES (?, ?, ?)",(pays,year,value))
			conn.commit()
			conn.close()
			compteur = compteur+1
		except:
			log("***Erreur lors de l'injection dans la base de données")
		
	cells_dark = ide_sortants_soup.find_all("td", class_="dark value")
	for cell_dark in cells_dark:
		value = cell_dark.string
		# On supprime les éventuelles espaces (oui, espace est féminin)		
		value = value.replace(u'\xa0','')
		try:
			value = int(value)
			value = value*1000000
		except:
			value = "N/A"
		# On convertit le résultat en Unicode
		value = unicode(value)
		# Une fois que nous avons la donnée, nous pouvons trouver l'année à laquelle elle se rattache, dans la cellule précédente
		prev_cell_dark = cell_dark.find_previous_siblings("td")
		# Notre année est le premier résultat que nous cherchons		
		year = unicode(prev_cell_dark[0].string)
		# On insère le tout dans notre base de données
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_french_investments (nfi_ISO_3166_1_alpha_2, nfi_years, nfi_values) VALUES (?, ?, ?)",(pays,year,value))
			conn.commit()
			conn.close()
			compteur = compteur+1
		except:
			log("***Erreur lors de l'injection dans la base de données")
		

	# 2.2. IDE entrants (en France depuis le pays tiers)

	log("Récupération et injection des investissements de %s en France" % str_pays)

	url_ide_entrants = bdf_url_base+"DI.FR."+pays	
	complement_url_ide_entrants = ".S1.IMC.LE.NI.FA.D.F.ALL.EUR.I._X._X.FDI_T._Z"
	url_ide_entrants = url_ide_entrants+complement_url_ide_entrants
	# Ouvrons cette page et lisons-la
	ide_entrants_reader = urllib2.urlopen(url_ide_entrants).read()
	# Faisons notre soupe avec le résultat
	ide_entrants_soup = BeautifulSoup(ide_entrants_reader, 'lxml')
	# Le plus simple est de chercher d'abord les cellules contenant les données, qui peuvent être de deux styles :
	# "light" ou "dark" (ce sont des tableaux avec des lignes de couleurs alternées)
	cells_light = ide_entrants_soup.find_all("td", class_="light value")
	for cell_light in cells_light:
		value = cell_light.string
		# On supprime les éventuelles espaces (oui, espace est féminin)		
		value = value.replace(u'\xa0','')
		try:
			value = int(value)
			value = value*1000000
		except:
			value = "N/A"
		# On convertit le résultat en Unicode
		value = unicode(value)
		# Une fois que nous avons la donnée, nous pouvons trouver l'année à laquelle elle se rattache, dans la cellule précédente
		prev_cell_light = cell_light.find_previous_siblings("td")
		# Notre année est le premier résultat que nous cherchons		
		year = unicode(prev_cell_light[0].string)
		# On colle le tout dans notre base de données
		try:
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_investment_in_France (nif_ISO_3166_1_alpha_2, nif_years, nif_values) VALUES (?, ?, ?)",(pays,year,value))
			conn.commit()
			conn.close()
			compteur = compteur+1
		except:
			log("***Erreur lors de l'injection dans la base de données")
		
	cells_dark = ide_entrants_soup.find_all("td", class_="dark value")
	for cell_dark in cells_dark:
		value = cell_dark.string
		# On supprime les éventuelles espaces (oui, espace est féminin)		
		value = value.replace(u'\xa0','')
		try:
			value = int(value)
			value = value*1000000
		except:
			value = "N/A"
		# On convertit le résultat en Unicode
		value = unicode(value)
		# Une fois que nous avons la donnée, nous pouvons trouver l'année à laquelle elle se rattache, dans la cellule précédente
		prev_cell_dark = cell_dark.find_previous_siblings("td")
		# Notre année est le premier résultat que nous cherchons		
		year = unicode(prev_cell_dark[0].string)
		# On insère le tout dans notre base de données
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_investment_in_France (nif_ISO_3166_1_alpha_2, nif_years, nif_values) VALUES (?, ?, ?)",(pays,year,value))
			conn.commit()
			conn.close()
			compteur = compteur+1
		except:
			log("***Erreur lors de l'injection dans la base de données")
		
log("%s données sur les investissements français à l'étranger et étrangers en France saisis dans notre base de données" % compteur)