#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                           mo_DoingBusiness.py                              #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script parse la page "Ranking" du site "Doing Business" de la Banque mondiale
# pour en importer les données qui nous intéressent. Ce serait bien sûr trop facile
# si ces données étaient disponibles via l'API de la BM, ou si elles étaient aisément
# téléchargeables (un javascript en bloque le téléchargement par des voies classiques,
# et celles qui le sont moins ne sont pas légales partout...)

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, urllib2, re
#from bs4 import BeautifulSoup
from functions import log, deAlpha3versAlpha2

log("")
log("=======================================")
log("Lancement du script mo_DoingBusiness.py")
log("=======================================")

compteur = 0
# L'URL de la page qui nous intéresse est fixe
db_page = "http://www.doingbusiness.org/rankings"
# Ouvrons cette page et lisons-la
db_reader = urllib2.urlopen(db_page).read()

# Nous rechercherons des URL annoncés par le tag... "EconomyID" ! Nous n'allons pas au-delà de "EconomyURLName". Nous récupérons tout ce qui se trouve entre les deux
pattern = re.compile('EconomyID(.*?)EconomyURLName')
# Le résultat est stocké dans la liste "results"
results = pattern.findall(db_reader)

for result in results:
	pattern_ISO_Code = re.compile('EconomyCode\":\"(.*?)\"')
	ISO_Code_alpha_3 = pattern_ISO_Code.findall(result)
	code_ISO_alpha_2 = deAlpha3versAlpha2(ISO_Code_alpha_3[0])
	pattern_rank = re.compile('\"Rank\":(.*?),')
	rank_ls = pattern_rank.findall(result)
	rank = rank_ls[0]
	#rank = int(rank)
	conn = sqlite3.connect("datas/bases/general.db")
	conn.execute("INSERT INTO nations_doing_business_ranks (ndbr_ISO_3166_1_alpha_2, ndbr_values) VALUES (?, ?)",(code_ISO_alpha_2,rank))
	conn.commit()
	conn.close()
	compteur = compteur+1
log("%s classements Doing Business entrés dans notre base de données" % compteur)