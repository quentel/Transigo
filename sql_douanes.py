#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                              sql_douane.py                                 #
#                                                                            #
#                        Christophe Quentel, 2015-2016                       #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée une base SQLite 3 dans laquelle nous stockerons les données
# en provenance des douanes françaises.

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, shutil, time
from codecs import open
from functions import log

log("")
log("=================================")
log("Lancement du script sql_douane.py")
log("=================================")

#2. GERER L'ANCIENNE BASE SI ELLE EXISTE

path_to_check = "datas/bases/douanes.db"
old_bases_path = "old_datas/bases/douanes/"
if os.path.exists(path_to_check):
	log("Une base de données douanes.db existe déjà")
	# On crée le répertoire de destination s'il n'existe pas
	if not os.path.exists(old_bases_path):
		log("Le répertoire accueillant les anciennes bases de données douanes.db n'existe pas")
		os.makedirs(old_bases_path)
		log ("Le répertoire accueillant les anciennes bases de données douanes.db a été créé")
	# On copie la base existante dans le répertoire old_datas/bases en lui donnant pour nom un timestamp
	old_bases_complete_path = old_bases_path+time.strftime("%Y%m%d-%H%M%S")+".db"
	log("L'ancienne base de données douanes.db a été déplacée et renommée dans le répertoire adéquat")	
	shutil.move(path_to_check,old_bases_complete_path)


# 3. CREER LA BASE ET SES TABLES

# Si le répertoire de la base n'existe pas, on le crée
new_base_path = "datas/bases/"
if not os.path.exists(new_base_path):
	log("Création du répertoire destiné à accueillir la base de données douanes.db")
	os.makedirs(new_base_path)

# On crée la base qui, forcément, n'existe pas (puisque dans le cas contraire, on l'a déplacée à l'étape précédente)
new_base_complete_path = new_base_path+"douanes.db"
conn = sqlite3.connect(new_base_complete_path)
log("Création de la nouvelle base de données douanes.db")

# 3.1. Créer la table stockant les exportations de la France vers le pays concerné

try:
	conn.execute('''CREATE TABLE nations_export
		(ne_ids	INT	PRIMARY KEY,
		ne_eu_codes	VARCHAR(2)	NOT NULL,
		ne_years	YEAR	NOT NULL,
		ne_values	INT NOT NULL,
		ne_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_export")
except:
	log("**Erreur lors de la création de la table nations_export")

# 3.2. Créer la table stockant les importations de la France depuis le pays concerné

try:
	conn.execute('''CREATE TABLE nations_import
		(ni_ids	INT	PRIMARY KEY,
		ni_eu_codes	VARCHAR(2)	NOT NULL,
		ni_years	YEAR	NOT NULL,
		ni_values	INT NOT NULL,
		ni_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_import")
except:
	log("**Erreur lors de la création de la table nations_import")

# 3.3. Créer la table stockant la totalité des exportations de la France vers le pays concerné

try:
	conn.execute('''CREATE TABLE nations_export_all
		(nea_ids	INT	PRIMARY KEY,
		nea_eu_codes	VARCHAR(2)	NOT NULL,
		nea_years	YEAR	NOT NULL,
		nea_values	INT NOT NULL);''')
	log("Création de la table nations_export_all")
except:
	log("**Erreur lors de la création de la table nations_export_all")

# 3.4. Créer la table stockant la totalité des importations de la France depuis le pays concerné

try:
	conn.execute('''CREATE TABLE nations_import_all
		(nia_ids	INT	PRIMARY KEY,
		nia_eu_codes	VARCHAR(2)	NOT NULL,
		nia_years	YEAR	NOT NULL,
		nia_values	INT NOT NULL);''')
	log("Création de la table nations_import_all")
except:
	log("**Erreur lors de la création de la table nations_import_all")

# 3.5. Créer la table stockant la totalité des exportations de la France vers le pays concerné pour les 12 derniers mois

try:
	conn.execute('''CREATE TABLE nations_export_last_12_months_all
		(nea12_ids	INT	PRIMARY KEY,
		nea12_eu_codes	VARCHAR(2)	NOT NULL,
		nea12_years	YEAR	NOT NULL,
		nea12_values	INT NOT NULL);''')
	log("Création de la table nations_export_last_12_months_all")
except:
	log("**Erreur lors de la création de la table nations_export_last_12_months_all")

# 3.6. Créer la table stockant la totalité des importations de la France depuis le pays concerné pour les 12 derniers mois

try:
	conn.execute('''CREATE TABLE nations_import_last_12_months_all
		(nia12_ids	INT	PRIMARY KEY,
		nia12_eu_codes	VARCHAR(2)	NOT NULL,
		nia12_years	YEAR	NOT NULL,
		nia12_values	INT NOT NULL);''')
	log("Création de la table nations_import_last_12_months_all")
except:
	log("**Erreur lors de la création de la table nations_import_last_12_months_all")

# 3.7. Créer la table stockant la totalité des exportations de la France vers le pays concerné pour les 12 précédents mois

try:
	conn.execute('''CREATE TABLE nations_export_prev_12_months_all
		(neap12_ids	INT	PRIMARY KEY,
		neap12_eu_codes	VARCHAR(2)	NOT NULL,
		neap12_years	YEAR	NOT NULL,
		neap12_values	INT NOT NULL);''')
	log("Création de la table nations_export_prev_12_months_all")
except:
	log("**Erreur lors de la création de la table nations_export_prev_12_months_all")

# 3.8. Créer la table stockant la totalité des importations de la France depuis le pays concerné pour les 12 mois précédents

try:
	conn.execute('''CREATE TABLE nations_import_prev_12_months_all
		(niap12_ids	INT	PRIMARY KEY,
		niap12_eu_codes	VARCHAR(2)	NOT NULL,
		niap12_years	YEAR	NOT NULL,
		niap12_values	INT NOT NULL);''')
	log("Création de la table nations_import_prev_12_months_all")
except:
	log("**Erreur lors de la création de la table nations_import_prev_12_months_all")

# 3.9. Créer la table stockant la totalité des exportations de la France vers le pays concerné depuis le début de l'année

try:
	conn.execute('''CREATE TABLE nations_export_first_months_all
		(neaf_ids	INT	PRIMARY KEY,
		neaf_eu_codes	VARCHAR(2)	NOT NULL,
		neaf_years	YEAR	NOT NULL,
		neaf_values	INT NOT NULL);''')
	log("Création de la table nations_export_first_months_all")
except:
	log("**Erreur lors de la création de la table nations_export_first_months_all")

# 3.10. Créer la table stockant la totalité des importations de la France depuis le pays concerné depuis le début de l'année

try:
	conn.execute('''CREATE TABLE nations_import_first_months_all
		(niaf_ids	INT	PRIMARY KEY,
		niaf_eu_codes	VARCHAR(2)	NOT NULL,
		niaf_years	YEAR	NOT NULL,
		niaf_values	INT NOT NULL);''')
	log("Création de la table nations_import_first_months_all")
except:
	log("**Erreur lors de la création de la table nations_import_first_months_all")

# 3.11. Créer la table stockant la totalité des exportations de la France vers le pays concerné depuis le début de l'année précédente

try:
	conn.execute('''CREATE TABLE nations_export_prev_months_all
		(neap_ids	INT	PRIMARY KEY,
		neap_eu_codes	VARCHAR(2)	NOT NULL,
		neap_years	YEAR	NOT NULL,
		neap_values	INT NOT NULL);''')
	log("Création de la table nations_export_prev_months_all")
except:
	log("**Erreur lors de la création de la table nations_export_prev_months_all")

# 3.12. Créer la table stockant la totalité des importations de la France depuis le pays concerné depuis le début de l'année précédente

try:
	conn.execute('''CREATE TABLE nations_import_prev_months_all
		(niap_ids	INT	PRIMARY KEY,
		niap_eu_codes	VARCHAR(2)	NOT NULL,
		niap_years	YEAR	NOT NULL,
		niap_values	INT NOT NULL);''')
	log("Création de la table nations_import_prev_months_all")
except:
	log("**Erreur lors de la création de la table nations_import_prev_months_all")

# 3.13. Créer la table stockant les exportations de la France vers le pays concerné sur les 12 derniers mois

try:
	conn.execute('''CREATE TABLE nations_export_last_12_months
		(ne12_ids	INT	PRIMARY KEY,
		ne12_eu_codes	VARCHAR(2)	NOT NULL,
		ne12_years	YEAR	NOT NULL,
		ne12_values	INT NOT NULL,
		ne12_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_export_last_12_months")
except:
	log("**Erreur lors de la création de la table nations_export_last_12_months")

# 3.14. Créer la table stockant les importations de la France depuis le pays concerné sur les 12 derniers mois

try:
	conn.execute('''CREATE TABLE nations_import_last_12_months
		(ni12_ids	INT	PRIMARY KEY,
		ni12_eu_codes	VARCHAR(2)	NOT NULL,
		ni12_years	YEAR	NOT NULL,
		ni12_values	INT NOT NULL,
		ni12_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_import_last_12_months")
except:
	log("**Erreur lors de la création de la table nations_import_last_12_months")

# 3.15. Créer la table stockant les exportations de la France vers le pays concerné sur les 12 mois précédents

try:
	conn.execute('''CREATE TABLE nations_export_prev_12_months
		(ne12p_ids	INT	PRIMARY KEY,
		ne12p_eu_codes	VARCHAR(2)	NOT NULL,
		ne12p_years	YEAR	NOT NULL,
		ne12p_values	INT NOT NULL,
		ne12p_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_export_prev_12_months")
except:
	log("**Erreur lors de la création de la table nations_export_prev_12_months")

# 3.16. Créer la table stockant les importations de la France depuis le pays concerné sur les 12 mois précédents

try:
	conn.execute('''CREATE TABLE nations_import_prev_12_months
		(ni12p_ids	INT	PRIMARY KEY,
		ni12p_eu_codes	VARCHAR(2)	NOT NULL,
		ni12p_years	YEAR	NOT NULL,
		ni12p_values	INT NOT NULL,
		ni12p_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_import_prev_12_months")
except:
	log("**Erreur lors de la création de la table nations_import_prev_12_months")
	
# 3.17. Créer la table stockant les exportations de la France vers le pays concerné sur les premiers mois de l'année

try:
	conn.execute('''CREATE TABLE nations_export_first_months
		(nef_ids	INT	PRIMARY KEY,
		nef_eu_codes	VARCHAR(2)	NOT NULL,
		nef_years	YEAR	NOT NULL,
		nef_values	INT NOT NULL,
		nef_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_export_first_months")
except:
	log("**Erreur lors de la création de la table nations_export_first_months")

# 3.18. Créer la table stockant les importations de la France depuis le pays concerné sur les premiers mois de l'année

try:
	conn.execute('''CREATE TABLE nations_import_first_months
		(nif_ids	INT	PRIMARY KEY,
		nif_eu_codes	VARCHAR(2)	NOT NULL,
		nif_years	YEAR	NOT NULL,
		nif_values	INT NOT NULL,
		nif_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_import_first_months")
except:
	log("**Erreur lors de la création de la table nations_import_first_months")

# 3.19. Créer la table stockant les exportations de la France vers le pays concerné sur les premiers mois de l'année précédente

try:
	conn.execute('''CREATE TABLE nations_export_prev_months
		(nep_ids	INT	PRIMARY KEY,
		nep_eu_codes	VARCHAR(2)	NOT NULL,
		nep_years	YEAR	NOT NULL,
		nep_values	INT NOT NULL,
		nep_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_export_prev_months")
except:
	log("**Erreur lors de la création de la table nations_export_prev_months")

# 3.20. Créer la table stockant les importations de la France depuis le pays concerné sur les premiers mois de l'année précédente

try:
	conn.execute('''CREATE TABLE nations_import_prev_months
		(nip_ids	INT	PRIMARY KEY,
		nip_eu_codes	VARCHAR(2)	NOT NULL,
		nip_years	YEAR	NOT NULL,
		nip_values	INT NOT NULL,
		nip_na129	VARCHAR(4)	NOT NULL);''')
	log("Création de la table nations_import_prev_months")
except:
	log("**Erreur lors de la création de la table nations_import_prev_months")
	
conn.commit()
conn.close()