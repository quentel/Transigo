#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                           vw_tableaux_qo.py                                #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée un fichier Excel présentant sous forme de tableaux les principales
# données issues de notre base, classé par direction et sous-direction géographiques
# du Quai d'Orsay

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, math, locale
import xlwt
from xlwt import *
from functions import *


locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

log("")
log("=====================================")
log("Lancement du script vw_tableaux_qo.py")
log("=====================================")

# 2. FONCTIONS UTILES

# 2.1 Recherche dans une table dédiée à la donnée

def connectTable(table,prefixe,pays):
	cur = conn.cursor()						
	cur.execute("SELECT * from "+table+" where "+prefixe+"_ISO_3166_1_alpha_2 = ?", (pays,))
	return cur


path = "output/tableaux/qo/"

# Si le répertoire n'existe pas, créons-le
tableau_dir = os.path.dirname(path)
if not os.path.exists(tableau_dir):
	log("Création du répertoire destiné à accueillir notre tableau : output/tableau/qo/")
	os.makedirs(tableau_dir)

# 3. SCRIPT

# Création du fichier
book = Workbook()

style_soustotal = xlwt.XFStyle()
font = xlwt.Font()
font.bold = True
style_soustotal.font = font

style_total = xlwt.XFStyle()
font = xlwt.Font()
font.bold = True
font.italic = True
style_total.font = font

style_nombre_entier = xlwt.Style.easyxf(num_format_str='#,##0')
style_nombre_entier_sous_total = xlwt.Style.easyxf(num_format_str='#,##0')
style_nombre_entier_sous_total.font.bold = True
style_nombre_entier_total = xlwt.Style.easyxf(num_format_str='#,##0')
style_nombre_entier_total.font.bold = True
style_nombre_entier_total.font.italic = True

style_pourcentage = xlwt.Style.easyxf(num_format_str='0.00 %')
style_pourcentage_sous_total = xlwt.Style.easyxf(num_format_str='0.00 %')
style_pourcentage_sous_total.font.bold = True
style_pourcentage_total = xlwt.Style.easyxf(num_format_str='0.00 %')
style_pourcentage_total.font.bold = True
style_pourcentage_total.font.italic = True

# 3.1. Feuille Pays traités

# Création de la feuille
feuil1 = book.add_sheet(u'Pays traités')

# Création des en-tête
feuil1.write(0,0,'Direction')
feuil1.write(0,1,'Sous-direction')
feuil1.write(0,2,'Nom du pays')
feuil1.write(0,3,'Code ISO')
feuil1.write(0,4,'Compteur sous-direction')
feuil1.write(0,5,'Compteur direction')
feuil1.write(0,6,'Compteur mondial')

# Largeur des colonnes
feuil1.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil1.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil1.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil1.col(3).width = 2500 # Largeur de la colonne "Code ISO"
feuil1.col(4).width = 5500 # Largeur de la colonne "Compteur sous-direction"
feuil1.col(5).width = 4500 # Largeur de la colonne "Compteur direction"
feuil1.col(6).width = 4000 # Largeur de la colonne "Compteur mondial"

# Création des lignes de données
compteur_ligne = 1
compteur_mondial = 1
for direction in qoListeDir():
	ligne = feuil1.row(compteur_ligne)
	compteur_direction = 1	
	for sousdirection in qoListeSousDir(direction):
		compteur_sous_direction = 1
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			ligne = feuil1.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,pays)
			ligne.write(4,compteur_sous_direction)
			ligne.write(5,compteur_direction)
			ligne.write(6,compteur_mondial)
			compteur_ligne = compteur_ligne+1			
			compteur_mondial = compteur_mondial+1
			compteur_sous_direction = compteur_sous_direction+1
			compteur_direction = compteur_direction+1		
	
		ligne = feuil1.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "Nombre total des pays pour cette sous-direction", style = style_soustotal)
		ligne.write(4, compteur_sous_direction-1, style = style_soustotal)
		compteur_ligne = compteur_ligne+2

	ligne = feuil1.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "Nombre total des pays pour cette direction", style = style_total)
	ligne.write(5, compteur_direction-1, style = style_total)
	compteur_ligne = compteur_ligne+2
		
# 3.2. Feuille Superficie

# Création de la feuille
feuil2 = book.add_sheet('Superficie')

# Création des en-tête
feuil2.write(0,0,'Direction')
feuil2.write(0,1,'Sous-direction')
feuil2.write(0,2,'Nom du pays')
feuil2.write(0,3,'Superficie')
feuil2.write(0,4,'Source')

# Largeur des colonnes
feuil2.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil2.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil2.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil2.col(3).width = 5500 # Largeur de la colonne "Superficie"
feuil2.col(4).width = 4500 # Largeur de la colonne "Source"

# Création des lignes de données
compteur_ligne = 1
for direction in qoListeDir():
	ligne = feuil2.row(compteur_ligne)
	compteur_direction = 1	
	superf_d = 0	
	for sousdirection in qoListeSousDir(direction):
		superf_sd = 0		
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			superficie = getArea(pays)
			try:
				int(superficie)
				superf_sd = superf_sd+superficie
				superf_d = superf_d+superficie
			except:
				superf_sd = superf_sd
				superf_d = superf_d	 
			ligne = feuil2.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,superficie, style = style_nombre_entier)
			ligne.write(4,"Banque mondiale")
			compteur_ligne = compteur_ligne+1			
	
		ligne = feuil2.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "Superficie pour cette sous-direction", style = style_soustotal)
		ligne.write(3, superf_sd, style = style_nombre_entier_sous_total)
		compteur_ligne = compteur_ligne+2

	ligne = feuil2.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "Superficie pour cette direction", style = style_total)
	ligne.write(3, superf_d, style = style_nombre_entier_total)
	compteur_ligne = compteur_ligne+2	

# 3.3. Feuille Population

# Création de la feuille
feuil3 = book.add_sheet('Population')

# Création des en-tête
feuil3.write(0,0,'Direction')
feuil3.write(0,1,'Sous-direction')
feuil3.write(0,2,'Nom du pays')
feuil3.write(0,3,'Population')
feuil3.write(0,4,'Annee')
feuil3.write(0,5,'Source')

# Largeur des colonnes
feuil3.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil3.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil3.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil3.col(3).width = 5500 # Largeur de la colonne "Population"
feuil3.col(4).width = 4500 # Largeur de la colonne "Année"
feuil3.col(5).width = 4500 # Largeur de la colonne "Source"

# Création des lignes de données
compteur_ligne = 1
source = "Banque mondiale"
for direction in qoListeDir():
	ligne = feuil3.row(compteur_ligne)
	compteur_direction = 1	
	pop_d = 0	
	for sousdirection in qoListeSousDir(direction):
		pop_sd = 0		
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			annee = getPopLastYear(pays, source)
			pop = getPop(pays,annee,source)		
			try:
				int(pop)
				pop_sd = pop_sd+pop
				pop_d = pop_d+pop
			except:
				pop_sd = pop_sd
				pop_d = pop_d	 
			ligne = feuil3.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,pop, style = style_nombre_entier)
			ligne.write(4,annee)
			ligne.write(5,source)
			compteur_ligne = compteur_ligne+1			
	
		ligne = feuil3.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "Population pour cette sous-direction", style = style_soustotal)
		ligne.write(3, pop_sd, style = style_nombre_entier_sous_total)
		compteur_ligne = compteur_ligne+2

	ligne = feuil3.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "Population pour cette direction", style = style_total)
	ligne.write(3, pop_d, style = style_nombre_entier_total)
	compteur_ligne = compteur_ligne+2	

# 3.4. Feuille PIB

# Création de la feuille
feuil4 = book.add_sheet('PIB (FMI)')

# Création des en-tête
feuil4.write(0,0,'Direction')
feuil4.write(0,1,'Sous-direction')
feuil4.write(0,2,'Nom du pays')
feuil4.write(0,3,'PIB')
feuil4.write(0,4,'Annee')
feuil4.write(0,5,'Source')

# Largeur des colonnes
feuil4.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil4.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil4.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil4.col(3).width = 5500 # Largeur de la colonne "PIB"
feuil4.col(4).width = 4500 # Largeur de la colonne "Année"
feuil4.col(5).width = 4500 # Largeur de la colonne "Source"

# Création des lignes de données
compteur_ligne = 1
source = "FMI"
for direction in qoListeDir():
	ligne = feuil4.row(compteur_ligne)
	compteur_direction = 1	
	pib_d = 0	
	for sousdirection in qoListeSousDir(direction):
		pib_sd = 0		
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			annee = getDataLastYearSourced("ng","nations_gdp",pays,source)
			pib = getDataValue("ng","nations_gdp",pays,annee)
			try:
				int(pib)
				pib_sd = pib_sd+pib
				pib_d = pib_d+pib
			except:
				pib_sd = pib_sd
				pib_d = pib_d	 
			ligne = feuil4.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,pib, style = style_nombre_entier)
			ligne.write(4,annee)
			ligne.write(5,source)
			compteur_ligne = compteur_ligne+1			
	
		ligne = feuil4.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "PIB pour cette sous-direction", style = style_soustotal)
		ligne.write(3, pib_sd, style = style_nombre_entier_sous_total)
		compteur_ligne = compteur_ligne+2

	ligne = feuil4.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "PIB pour cette direction", style = style_total)
	ligne.write(3, pib_d, style = style_nombre_entier_total)
	compteur_ligne = compteur_ligne+2	

# 3.5. Feuille IDE français dans les pays

# Création de la feuille
feuil5 = book.add_sheet(u'Investissements français (BdF)')

# Création des en-tête
feuil5.write(0,0,'Direction')
feuil5.write(0,1,'Sous-direction')
feuil5.write(0,2,'Nom du pays')
feuil5.write(0,3,'Investissements')
feuil5.write(0,4,'Annee')
feuil5.write(0,5,'Source')

# Largeur des colonnes
feuil5.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil5.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil5.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil5.col(3).width = 5500 # Largeur de la colonne "Investissements"
feuil5.col(4).width = 4500 # Largeur de la colonne "Année"
feuil5.col(5).width = 4500 # Largeur de la colonne "Source"

# Création des lignes de données
compteur_ligne = 1
source = "Banque de France"
for direction in qoListeDir():
	ligne = feuil5.row(compteur_ligne)
	compteur_direction = 1	
	inv_d = 0	
	for sousdirection in qoListeSousDir(direction):
		inv_sd = 0		
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			annee = getDataLastYear("nfi","nations_french_investments",pays)
			inv = getDataValue("nfi","nations_french_investments",pays,annee)
			try:
				int(inv)
				inv_sd = inv_sd+inv
				inv_d = inv_d+inv
			except:
				inv_sd = inv_sd
				inv_d = inv_d	 
			ligne = feuil5.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,inv, style = style_nombre_entier)
			ligne.write(4,annee)
			ligne.write(5,source)
			compteur_ligne = compteur_ligne+1			
	
		ligne = feuil5.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "Investissements pour cette sous-direction", style = style_soustotal)
		ligne.write(3, inv_sd, style = style_nombre_entier_sous_total)
		compteur_ligne = compteur_ligne+2

	ligne = feuil5.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "Investissements pour cette direction", style = style_total)
	ligne.write(3, inv_d, style = style_nombre_entier_total)
	compteur_ligne = compteur_ligne+2	

# 3.6. Feuille Investissements du pays en France

# Création de la feuille
feuil6 = book.add_sheet(u'Investissements étrangers (BdF)')

# Création des en-tête
feuil6.write(0,0,'Direction')
feuil6.write(0,1,'Sous-direction')
feuil6.write(0,2,'Nom du pays')
feuil6.write(0,3,'Investissements')
feuil6.write(0,4,'Annee')
feuil6.write(0,5,'Source')

# Largeur des colonnes
feuil6.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil6.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil6.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil6.col(3).width = 5500 # Largeur de la colonne "Investissements"
feuil6.col(4).width = 4500 # Largeur de la colonne "Année"
feuil6.col(5).width = 4500 # Largeur de la colonne "Source"

# Création des lignes de données
compteur_ligne = 1
source = "Banque de France"
for direction in qoListeDir():
	ligne = feuil6.row(compteur_ligne)
	compteur_direction = 1	
	inv_d = 0	
	for sousdirection in qoListeSousDir(direction):
		inv_sd = 0		
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			annee = getDataLastYear("nif","nations_investment_in_france",pays)
			inv = getDataValue("nif","nations_investment_in_france",pays,annee)
			try:
				int(inv)
				inv_sd = inv_sd+inv
				inv_d = inv_d+inv
			except:
				inv_sd = inv_sd
				inv_d = inv_d	 
			ligne = feuil6.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,inv, style = style_nombre_entier)
			ligne.write(4,annee)
			ligne.write(5,source)
			compteur_ligne = compteur_ligne+1			
	
		ligne = feuil6.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "Investissements pour cette sous-direction", style = style_soustotal)
		ligne.write(3, inv_sd, style = style_nombre_entier_sous_total)
		compteur_ligne = compteur_ligne+2

	ligne = feuil6.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "Investissements pour cette direction", style = style_total)
	ligne.write(3, inv_d, style = style_nombre_entier_total)
	compteur_ligne = compteur_ligne+2	

# 3.7. Feuille Exportations françaises dans le pays

# Création de la feuille
feuil7 = book.add_sheet(u'Exportations françaises (DGT)')

# Création des en-tête

# Nous avons toujours trois années en stock : en cours, n-1 et n-2
# Piquons celles disponibles avec notre principal partenaire commercial
mesAnnees = getAllYears("ke","kiosque_exports","DE")
feuil7.write(0,0,'Direction')
feuil7.write(0,1,'Sous-direction')
feuil7.write(0,2,'Nom du pays')
feuil7.write(0,3,mesAnnees[0])
feuil7.write(0,4,mesAnnees[1])
feuil7.write(0,5,mesAnnees[2])
feuil7.write(0,6,'Croissance')
feuil7.write(0,7,'Source')

# Largeur des colonnes
feuil7.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil7.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil7.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil7.col(3).width = 5500 # Largeur de la colonne "Exportations"
feuil7.col(4).width = 5500 # Largeur de la colonne "Exportations"
feuil7.col(5).width = 5500 # Largeur de la colonne "Exportations"
feuil7.col(6).width = 4500 # Largeur de la colonne "Croissance"
feuil7.col(7).width = 4500 # Largeur de la colonne "Source"

# Création des lignes de données
compteur_ligne = 1
source = "DGT"
for direction in qoListeDir():
	ligne = feuil7.row(compteur_ligne)
	compteur_direction = 1	
	exp_d = 0
	expmoinsun_d = 0
	expmoinsdeux_d = 0	
	for sousdirection in qoListeSousDir(direction):
		exp_sd = 0
		expmoinsun_sd = 0
		expmoinsdeux_sd = 0		
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			annee = getDataLastYear("ke","kiosque_exports",pays)
			expmoinsdeux = getDataValue("ke","kiosque_exports",pays,mesAnnees[0])
			expmoinsun = getDataValue("ke","kiosque_exports",pays,mesAnnees[1])
			exp = getDataValue("ke","kiosque_exports",pays,mesAnnees[2])
			try:
				int(exp)
				exp_sd = exp_sd+exp
				exp_d = exp_d+exp
			except:
				exp_sd = exp_sd
				exp_d = exp_d
			try:
				int(expmoinsun)
				expmoinsun_sd = expmoinsun_sd+expmoinsun
				expmoinsun_d = expmoinsun_d+expmoinsun
				v1 = True
			except:
				expmoinsun_sd = expmoinsun_sd
				expmoinsun_d = expmoinsun_d
				v1 = False
			try:
				int(expmoinsdeux)
				expmoinsdeux_sd = expmoinsdeux_sd+expmoinsdeux
				expmoinsdeux_d = expmoinsdeux_d+expmoinsdeux
				v2 = True
			except:
				expmoinsdeux_sd = expmoinsdeux_sd
				expmoinsdeux_d = expmoinsdeux_d
				v2 = False

			# Si les données des exportations pour les années n-1 et n-2 sont des integers, nous pouvons calculer leur delta
			if v1 == True and v2 == True:
				expmoinsun = float(expmoinsun)
				expmoinsdeux = float(expmoinsdeux)
				# Vérifions qu'aucun de ces chiffres n'est nul				
				if expmoinsun > 0 and expmoinsdeux > 0:
					croissance = (expmoinsun-expmoinsdeux)/expmoinsdeux
				else:
					croissance = "N/A"
			else:
				croissance = "N/A"
			ligne = feuil7.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,expmoinsdeux, style = style_nombre_entier)
			ligne.write(4,expmoinsun, style = style_nombre_entier)
			ligne.write(5,exp, style = style_nombre_entier)
			ligne.write(6,croissance, style = style_pourcentage)
			ligne.write(7,source)
			compteur_ligne = compteur_ligne+1			
	
		croissance_sd = (float(expmoinsun_sd)-float(expmoinsdeux_sd))/float(expmoinsdeux_sd)
		ligne = feuil7.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "Exportations pour cette sous-direction", style = style_soustotal)
		ligne.write(3, expmoinsdeux_sd, style = style_nombre_entier_sous_total)
		ligne.write(4, expmoinsun_sd, style = style_nombre_entier_sous_total)
		ligne.write(5, exp_sd, style = style_nombre_entier_sous_total)
		ligne.write(6, croissance_sd, style = style_pourcentage_sous_total)
		compteur_ligne = compteur_ligne+2

	croissance_d = (float(expmoinsun_d)-float(expmoinsdeux_d))/float(expmoinsdeux_d)	
	ligne = feuil7.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "Exportations pour cette direction", style = style_total)
	ligne.write(3, expmoinsdeux_d, style = style_nombre_entier_total)
	ligne.write(4, expmoinsun_d, style = style_nombre_entier_total)
	ligne.write(5, exp_d, style = style_nombre_entier_total)
	ligne.write(6, croissance_d, style = style_pourcentage_total)
	compteur_ligne = compteur_ligne+2	

# 3.8. Feuille Importations françaises depuis le pays

# Création de la feuille
feuil8 = book.add_sheet(u'Importations en France (DGT)')

# Création des en-tête

# Nous avons toujours trois années en stock : en cours, n-1 et n-2
# Piquons celles disponibles avec notre principal partenaire commercial
mesAnnees = getAllYears("ki","kiosque_imports","DE")
feuil8.write(0,0,'Direction')
feuil8.write(0,1,'Sous-direction')
feuil8.write(0,2,'Nom du pays')
feuil8.write(0,3,mesAnnees[0])
feuil8.write(0,4,mesAnnees[1])
feuil8.write(0,5,mesAnnees[2])
feuil8.write(0,6,'Croissance')
feuil8.write(0,7,'Source')

# Largeur des colonnes
feuil8.col(0).width = 2000 # Largeur de la colonne "Direction"
feuil8.col(1).width = 3200 # Largeur de la colonne "Sous-direction"
feuil8.col(2).width = 10500 # Largeur de la colonne "Nom du pays"
feuil8.col(3).width = 5500 # Largeur de la colonne "Importations"
feuil8.col(4).width = 5500 # Largeur de la colonne "Importations"
feuil8.col(5).width = 5500 # Largeur de la colonne "Importations"
feuil8.col(6).width = 4500 # Largeur de la colonne "Croissance"
feuil8.col(7).width = 4500 # Largeur de la colonne "Source"

# Création des lignes de données
compteur_ligne = 1
source = "DGT"
for direction in qoListeDir():
	ligne = feuil8.row(compteur_ligne)
	compteur_direction = 1	
	imp_d = 0
	impmoinsun_d = 0
	impmoinsdeux_d = 0	
	for sousdirection in qoListeSousDir(direction):
		imp_sd = 0
		impmoinsun_sd = 0
		impmoinsdeux_sd = 0		
		for pays in qoListePays(sousdirection):
			lib_pays = deAlpha2versLibelleFR(pays)
			annee = getDataLastYear("ki","kiosque_imports",pays)
			impmoinsdeux = getDataValue("ki","kiosque_imports",pays,mesAnnees[0])
			impmoinsun = getDataValue("ki","kiosque_imports",pays,mesAnnees[1])
			imp = getDataValue("ki","kiosque_imports",pays,mesAnnees[2])
			try:
				int(imp)
				imp_sd = imp_sd+imp
				imp_d = imp_d+imp
			except:
				imp_sd = imp_sd
				imp_d = imp_d
			try:
				int(impmoinsun)
				impmoinsun_sd = impmoinsun_sd+impmoinsun
				impmoinsun_d = impmoinsun_d+impmoinsun
				v1 = True
			except:
				impmoinsun_sd = impmoinsun_sd
				impmoinsun_d = impmoinsun_d
				v1 = False
			try:
				int(impmoinsdeux)
				impmoinsdeux_sd = impmoinsdeux_sd+impmoinsdeux
				impmoinsdeux_d = impmoinsdeux_d+impmoinsdeux
				v2 = True
			except:
				impmoinsdeux_sd = impmoinsdeux_sd
				impmoinsdeux_d = impmoinsdeux_d
				v2 = False

			# Si les données des exportations pour les années n-1 et n-2 sont des integers, nous pouvons calculer leur delta
			if v1 == True and v2 == True:
				impmoinsun = float(impmoinsun)
				impmoinsdeux = float(impmoinsdeux)
				# Vérifions qu'aucun de ces chiffres n'est nul				
				if impmoinsun > 0 and impmoinsdeux > 0:
					croissance = (impmoinsun-impmoinsdeux)/impmoinsdeux
				else:
					croissance = "N/A"
			else:
				croissance = "N/A"
			ligne = feuil8.row(compteur_ligne)
			ligne.write(0,direction)
			ligne.write(1,sousdirection)
			ligne.write(2,lib_pays)
			ligne.write(3,impmoinsdeux, style = style_nombre_entier)
			ligne.write(4,impmoinsun, style = style_nombre_entier)
			ligne.write(5,imp, style = style_nombre_entier)
			ligne.write(6,croissance, style = style_pourcentage)
			ligne.write(7,source)
			compteur_ligne = compteur_ligne+1			
	
		croissance_sd = (float(impmoinsun_sd)-float(impmoinsdeux_sd))/float(impmoinsdeux_sd)
		ligne = feuil8.row(compteur_ligne)	
		ligne.write(0, direction, style = style_soustotal)
		ligne.write(1, sousdirection, style = style_soustotal)
		ligne.write(2, "Importations pour cette sous-direction", style = style_soustotal)
		ligne.write(3, impmoinsdeux_sd, style = style_nombre_entier_sous_total)
		ligne.write(4, impmoinsun_sd, style = style_nombre_entier_sous_total)
		ligne.write(5, imp_sd, style = style_nombre_entier_sous_total)
		ligne.write(6, croissance_sd, style = style_pourcentage_sous_total)
		compteur_ligne = compteur_ligne+2

	croissance_d = (float(impmoinsun_d)-float(impmoinsdeux_d))/float(impmoinsdeux_d)	
	ligne = feuil8.row(compteur_ligne)	
	ligne.write(0, direction, style = style_total)
	ligne.write(2, "Importations pour cette direction", style = style_total)
	ligne.write(3, impmoinsdeux_d, style = style_nombre_entier_total)
	ligne.write(4, impmoinsun_d, style = style_nombre_entier_total)
	ligne.write(5, imp_d, style = style_nombre_entier_total)
	ligne.write(6, croissance_d, style = style_pourcentage_total)
	compteur_ligne = compteur_ligne+2	

book.save("output/tableaux/qo/tableaux_qo.xls")