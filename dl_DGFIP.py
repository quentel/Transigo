#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                              dl_DGFIP.py                                   #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge les derniers taux de chancellerie mis à jour par la 
# direction générale des finances publiques (DGFIP) du ministère de l'Economie et
# des Finances

# 1. IMPORTATION DES MODULES NECESSAIRES

import urllib2, os, sys, zipfile
from functions import log

log("")
log("===============================")
log("Lancement du script dl_DGFIP.py")
log("===========================))==")

# L'URL du fichier à télécharger est fixe : http://www.economie.gouv.fr/dgfip/fichiers_taux_chancellerie/taux


# 2. SCRIPT

# Notre URL
url = "http://www.economie.gouv.fr/dgfip/fichiers_taux_chancellerie/taux"

# Nom et chemin du fichier que nous allons créer
dl_zipfile = "datas/dgfip/last_currencies.zip"

# Si le répertoire n'existe pas, créons-le
dl_dir = os.path.dirname(dl_zipfile)
if not os.path.exists(dl_dir):
	log("Création du répertoire destiné à accueillir notre fichier : datas/dgfip/")
	os.makedirs(dl_dir)

log("Tentative de téléchargement sur l'URL : "+url)
try:
	with open(dl_zipfile,'w') as f:
		f.write(urllib2.urlopen(url).read())
	f.close()
	log("Téléchargement du fichier .zip contenant les derniers taux de chancellerie de la DGFIP terminé !")
	# Nous décompactons le dossier concerné
	try:
		with zipfile.ZipFile(dl_zipfile, "r") as z:
			z.extractall(dl_dir)
			log("Décompression du fichier "+dl_zipfile)
		# On peut alors supprimer le fichier ZIP
		try:		
			os.remove(dl_zipfile)
			log("Suppression du fichier "+dl_zipfile)
		except:
			log("***Echec lors de la suppression du fichier "+dl_zipfile)
	except:
		log("***Echec lors de la décompression du fichier "+dl_zipfile)
		pass
except:
	log("***Echec du téléchargement du fichier .zip contenant les derniers taux de chancellerie de la DGFIP")
	pass