#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                dl_WITS.py                                  #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge toutes les fiches pays du World Integrated Trade Solution
# (WITS), le programme de la Banque mondiale qui collecte et diffuse des statistiques
# sur le commerce international

# 1. IMPORTATION DES MODULES NECESSAIRES

import urllib2, os, sys, zipfile
from functions import log

log("")
log("==============================")
log("Lancement du script dl_WITS.py")
log("==============================")

# # L'URL du fichier à télécharger est de la forme :
# http://wits.worldbank.org/data/public/country_profile_at_a_glance/wits_en_at-a-glance_allcountries_allyears.zip
# Il faut le télécharger dans un répertoire, le dézipper puis supprimer le fichier compressé.

# 2. SCRIPT

# Nom et chemin du répertoire où nous allons stocker cela
dl_path = "datas/wits/"
dl_file = dl_path+"last_wits.zip"

# Si le répertoire n'existe pas, créons-le
dl_dir = os.path.dirname(dl_path)
if not os.path.exists(dl_path):
	log("Création du répertoire destiné à accueillir notre fichier : datas/wits/")
	os.makedirs(dl_dir)

# Téléchargement du fichier zippé
dl_URL =	"http://wits.worldbank.org/data/public/country_profile_at_a_glance/wits_en_at-a-glance_allcountries_allyears.zip"
log("Tentative de téléchargement sur l'URL :"+dl_URL)	
try:
	with open(dl_file,'w') as f:
		f.write(urllib2.urlopen(dl_URL).read())
	f.close()
	log("Téléchargement réussi des dernières statistiques du WITS au format .zip")
except:
	log("=== Echec du téléchargement des dernières statistiques du WITS !")		
	pass
	
# Décompression du fichier zippé
try:
	with zipfile.ZipFile(dl_file, "r") as z:
		z.extractall(dl_path)
		log("Décompression réussie du fichier "+dl_file)
except:
	log("=== Echec de la décompression du fichier "+dl_file)
	pass

# Suppression du fichier zippé
try:
	os.remove(dl_file)
	log("Suppression réussie du fichier "+dl_file)
except:
	log("=== Echec de la suppression du fichier "+dl_file)
	pass