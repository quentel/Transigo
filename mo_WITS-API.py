#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                            mo_WITS-API.py                                  #
#                                                                            #
#                        Christophe Quentel, 2017                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_WITS.py les données qui
# nous intéressent dans les fiches pays du World Integrated Trade Solution (WITS),
# que nous avons téléchargées avec dl_WITS-APIO.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os
import xml.etree.ElementTree as ET
from functions import log, deAlpha3versAlpha2

log("")
log("==================================")
log("Lancement du script mo_WITS-API.py")
log("==================================")

# 2. SCRIPT

compteur = 0

path_WITS  = "datas/wits/"

# 2.1. Parts de marché

path_pdm  =path_WITS+"pdm/"

# 2.1.1. Parts de marché - importations

path_pdm_importations = path_pdm+"importations/"

liste_fichiers_pdm_import = os.listdir(path_pdm_importations)

for fichier_pdm_import in liste_fichiers_pdm_import:
	log("Traitement du fichier %s." % fichier_pdm_import)
	mon_fichier_pdm_import_path = path_pdm_importations+fichier_pdm_import
	try:
		tree = ET.parse(mon_fichier_pdm_import_path)
		root = tree.getroot()
		for child in root:
			if child.tag == "{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}DataSet":
				etape1 = child
				for child in etape1:
					series = child
					reporter = deAlpha3versAlpha2(series.attrib["REPORTER"])
					partner = deAlpha3versAlpha2(series.attrib["PARTNER"])
					for child in series:
						year = child.attrib["TIME_PERIOD"]
						value = child.attrib["OBS_VALUE"]
						print reporter,partner,year,value
						conn = sqlite3.connect("datas/bases/wits.db")
						conn.execute("INSERT INTO import_shares (is_cn_reporter, is_cn_partner, is_years, is_values) VALUES (?, ?, ?, ?)",(reporter,partner,year,value))
						conn.commit()
						conn.close()
		# Et on supprime le fichier traité
		os.remove(mon_fichier_pdm_import_path)

	except:
		log("---ECHEC !")		
		pass

# 2.1.2. Parts de marché - exportations

path_pdm_exportations = path_pdm+"exportations/"

liste_fichiers_pdm_export = os.listdir(path_pdm_exportations)

for fichier_pdm_export in liste_fichiers_pdm_export:
	log("Traitement du fichier %s." % fichier_pdm_export)
	mon_fichier_pdm_export_path = path_pdm_exportations+fichier_pdm_export
	try:
		tree = ET.parse(mon_fichier_pdm_export_path)
		root = tree.getroot()
		for child in root:
			if child.tag == "{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}DataSet":
				etape1 = child
				for child in etape1:
					series = child
					reporter = deAlpha3versAlpha2(series.attrib["REPORTER"])
					partner = deAlpha3versAlpha2(series.attrib["PARTNER"])
					for child in series:
						year = child.attrib["TIME_PERIOD"]
						value = child.attrib["OBS_VALUE"]
						print reporter,partner,year,value
						conn = sqlite3.connect("datas/bases/wits.db")
						conn.execute("INSERT INTO export_shares (es_cn_reporter, es_cn_partner, es_years, es_values) VALUES (?, ?, ?, ?)",(reporter,partner,year,value))
						conn.commit()
						conn.close()
		# Et on supprime le fichier traité
		os.remove(mon_fichier_pdm_export_path)
						
						
	except:
		log("---ECHEC !")		
		pass