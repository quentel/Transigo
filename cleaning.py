#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                cleaning.py                                 #
#                                                                            #
#                        Christophe Quentel, 2015-2016                       #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script effectue un léger nettoyage avant de lancer l'application elle-même

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, shutil, time
from functions import log

old_logs_path = "old_datas/logs/"
old_general_bases_path = "old_datas/bases/general/"
old_nomenclatures_bases_path = "old_datas/bases/nomenclatures/"
old_douane_bases_path = "old_datas/bases/douane/"

#2. GERER L'ANCIEN FICHIER DE LOGS, S'IL EXISTE

path_to_check = "logs/logs.txt"
old_logs_path = "old_datas/logs/"
if os.path.exists(path_to_check):
	# On crée le répertoire de destination s'il n'existe pas
	if not os.path.exists(old_logs_path):
		os.makedirs(old_logs_path)
	# On copie le fichier de logs existant dans le répertoire old_datas/logs en lui donnant pour nom un timestamp
	old_logs_complete_path = old_logs_path+time.strftime("%Y%m%d-%H%M%S")+".txt"
	shutil.move(path_to_check,old_logs_complete_path)

# On peut ensuite initier le nouveau fichier de logs...

log("===============================")
log("Lancement du script cleaning.py")
log("===============================")

#3. SUPPRIMER LES FICHIERS DE DONNEES TROP ANCIENS, S'ILS EXISTENT

def cleaning(path,nombre_a_conserver):
	filesInDir = os.listdir(path)
	filesInDir.sort(reverse=True)
	if len(filesInDir) > nombre_a_conserver:
		log("Nécessaire suppression de fichiers surnuméraires")
		for f in filesInDir[nombre_a_conserver:]:
			os.remove(os.path.join(path, f))
			log("Supression du fichier : %s" % f)

# On ne garde que les 5 derniers fichiers de logs et les 4 dernières bases
if os.path.exists(old_logs_path):
	cleaning(old_logs_path,5)
if os.path.exists(old_general_bases_path):
	cleaning(old_general_bases_path,5)
if os.path.exists(old_douane_bases_path):
	cleaning(old_douane_bases_path,5)
if os.path.exists(old_nomenclatures_bases_path):
	cleaning(old_nomenclatures_bases_path,5)