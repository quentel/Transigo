#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                         vw_tableaux_douane.py                              #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée un fichier Excel présentant sous forme de tableaux synthétiques les 
# principales données issues de notre base construite à partir des données de la douane

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, math, locale
from xlwt import Workbook
from functions import log, listeTerritoiresUE, deCodeUEversLibelleFR

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

log("")
log("==================================")
log("Lancement du script vw_tableaux.py")
log("==================================")

# 2. FONCTIONS UTILES

# 2.1 Recherche dans une table dédiée à la donnée

def connectTable(table,prefixe,pays):
	cur = conn.cursor()						
	cur.execute("SELECT * from "+table+" where "+prefixe+"_ISO_3166_1_alpha_2 = ?", (pays,))
	return cur


path = "output/douane/tableaux_synthétiques/"

# Si le répertoire n'existe pas, créons-le
tableau_dir = os.path.dirname(path)
if not os.path.exists(tableau_dir):
	log("Création du répertoire destiné à accueillir notre tableau : output/douane/tableaux_synthétiques/")
	os.makedirs(tableau_dir)

# 3. SCRIPT

# Création du fichier
book = Workbook()

# 3.1. Feuille "Importations simples"
# On ne s'intéresse qu'aux nations (pas aux territoires), et on ne retient que les chiffres attribués directement par les douanes
# à chacun d'entre eux, sans les consolider (on n'ajoute pas les importations vers la Martinique et la Guyane à celles orientées
# vers la France, on ne prend pas en compte les importations vers l'île de Man dans le total des importations britanniques, p. ex.)

# Création de la feuille
feuil1 = book.add_sheet(u'Importations')

# Création des en-tête
feuil1.write(0,0,'Code UE')
feuil1.write(0,1,'Nom du territoire')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/douane.db")
cur = conn.cursor()						
cur.execute("SELECT ni_years from nations_importations")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil1.col(num_colonne).width = 4000
	feuil1.write(0,num_colonne,year)
	compteur_ligne = 1
	for territoire in listeTerritoiresUE():
		ligne = feuil1.row(compteur_ligne)
		cur2 = conn.cursor()		
		# COLLER UNE FONCTION SUM PAR ICI				
		cur2.execute("SELECT SUM(ni_values) from nations_importations where ni_years = ? and ni_eu_codes = ?", (year,territoire))
		for row2 in cur2:
			print row2[0]
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés territoire
compteur = 1
for territoire in listeTerritoiresUE():
	ligne = feuil1.row(compteur)
	ligne.write(0,territoire)
	# Récupération du nom du pays en français
	lib_territoire = deCodeUEversLibelleFR(territoire)
	ligne.write(1,lib_territoire)
	compteur = compteur+1
feuil1.col(1).width = 10500 # Largeur de la colonne "Nom du territoire"


# 3.2. Feuille Exportations "simples"
# On ne s'intéresse qu'aux nations (pas aux territoires), et on ne retient que les chiffres attribués directement par les douanes
# à chacun d'entre eux, sans les consolider (on n'ajoute pas les importations vers la Martinique et la Guyane à celles orientées
# vers la France, on ne prend pas en compte les importations vers l'île de Man dans le total des importations britanniques, p. ex.)

# Création de la feuille
feuil2 = book.add_sheet(u'Exportations')

# Création des en-tête
feuil2.write(0,0,'Code UE')
feuil2.write(0,1,'Nom du territoire')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/douane.db")
cur = conn.cursor()						
cur.execute("SELECT ne_years from nations_exportations")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil2.col(num_colonne).width = 4000
	feuil2.write(0,num_colonne,year)
	compteur_ligne = 1
	for territoire in listeTerritoiresUE():
		ligne = feuil2.row(compteur_ligne)
		cur2 = conn.cursor()		
		# COLLER UNE FONCTION SUM PAR ICI				
		cur2.execute("SELECT SUM(nE_values) from nations_exportations where ne_years = ? and ne_eu_codes = ?", (year,territoire))
		for row2 in cur2:
			print row2[0]
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés territoire
compteur = 1
for territoire in listeTerritoiresUE():
	ligne = feuil2.row(compteur)
	ligne.write(0,territoire)
	# Récupération du nom du pays en français
	lib_territoire = deCodeUEversLibelleFR(territoire)
	ligne.write(1,lib_territoire)
	compteur = compteur+1
feuil2.col(1).width = 10500 # Largeur de la colonne "Nom du territoire"

book.save("output/douane/tableaux_synthétiques/tableaux_douane.xls")