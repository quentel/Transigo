#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                          vw_tableaux_pays.py                               #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée un fichier Excel présentant sous forme de tableaux les principales
# données issues de notre base

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, math, locale
from xlwt import Workbook
from functions import log, listePays, deAlpha2versLibelleFR

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

log("")
log("=======================================")
log("Lancement du script vw_tableaux_pays.py")
log("=======================================")

# 2. FONCTIONS UTILES

# 2.1 Recherche dans une table dédiée à la donnée

def connectTable(table,prefixe,pays):
	cur = conn.cursor()						
	cur.execute("SELECT * from "+table+" where "+prefixe+"_ISO_3166_1_alpha_2 = ?", (pays,))
	return cur


path = "output/tableaux/pays/"

# Si le répertoire n'existe pas, créons-le
tableau_dir = os.path.dirname(path)
if not os.path.exists(tableau_dir):
	log("Création du répertoire destiné à accueillir notre tableau : output/tableau/pays/")
	os.makedirs(tableau_dir)

# 3. SCRIPT

# Création du fichier
book = Workbook()

# 3.1. Feuille Superficies

# Création de la feuille
feuil1 = book.add_sheet('Superficies')

# Création des en-tête
feuil1.write(0,0,'Code ISO')
feuil1.write(0,1,'Nom du pays')
feuil1.write(0,2,'Superficie (en km2)')
feuil1.write(0,3,'Source')

# Largeur des colonnes
feuil1.col(1).width = 10500 # Largeur de la colonne "Nom du pays"
feuil1.col(2).width = 5000 # Largeur de la colonne "Superficie"
feuil1.col(3).width = 4000 # Largeur de la colonne "Source"

# Création des lignes de données
compteur = 1
for pays in listePays():
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	# Récupération de la superficie
 	conn = sqlite3.connect("datas/bases/general.db")
	cur = connectTable("nations_datas","nd",pays)					 						
	for row in cur:
		area = row[4]
	ligne = feuil1.row(compteur)
	ligne.write(0,pays)
	ligne.write(1,lib_pays)
	ligne.write(2,area)
	ligne.write(3,"Banque mondiale")
	compteur = compteur+1
	conn.close()	

# 3.2. Catégorie Banque mondiale

# Création de la feuille
feuil2 = book.add_sheet(u'Catégories')

# Création des en-tête
feuil2.write(0,0,'Code ISO')
feuil2.write(0,1,'Nom du pays')
feuil2.write(0,2,u'Catégorie')
feuil2.write(0,3,'Source')

# Largeur des colonnes
feuil2.col(1).width = 10500 # Largeur de la colonne "Nom du pays"
feuil2.col(2).width = 5000 # Largeur de la colonne "Catégorie"
feuil2.col(3).width = 4000 # Largeur de la colonne "Source"

# Création des lignes de données
compteur = 1
for pays in listePays():
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	# Récupération de la superficie
 	conn = sqlite3.connect("datas/bases/general.db")
	cur = connectTable("nations_datas","nd",pays)					 						
	for row in cur:
		category = row[3]
		category = unicode(category)
		#try:
		#	category = category.encode('utf-8')
		#except:
		#	category = "Indeterminee"
	ligne = feuil2.row(compteur)
	ligne.write(0,pays)
	ligne.write(1,lib_pays)
	ligne.write(2,category)
	ligne.write(3,"Banque mondiale")
	compteur = compteur+1
	conn.close()	

# 3.3. PIB (FMI)

# Création de la feuille
feuil3 = book.add_sheet(u'PIB (FMI)')

# Création des en-tête
feuil3.write(0,0,'Code ISO')
feuil3.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT ng_years from nations_gdp where ng_sources = 'FMI' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil3.col(num_colonne).width = 4000
	feuil3.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil3.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT ng_values,ng_years from nations_gdp where ng_sources = 'FMI' and ng_years = ? and ng_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil3.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil3.col(1).width = 10500 # Largeur de la colonne "Nom du pays"

# 3.4. PIB (Banque mondiale)

# Création de la feuille
feuil4 = book.add_sheet(u'PIB (BM)')

# Création des en-tête
feuil4.write(0,0,'Code ISO')
feuil4.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT ng_years from nations_gdp where ng_sources = 'Banque mondiale' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil4.col(num_colonne).width = 4000
	feuil4.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil4.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT ng_values,ng_years from nations_gdp where ng_sources = 'Banque mondiale' and ng_years = ? and ng_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil4.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil4.col(1).width = 10500 # Largeur de la colonne "Nom du pays"

# 3.5. Croissance du PIB (Banque mondiale)

# Création de la feuille
feuil5 = book.add_sheet(u'Croissance (BM)')

# Création des en-tête
feuil5.write(0,0,'Code ISO')
feuil5.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT ngg_years from nations_gdp_growth where ngg_sources = 'Banque mondiale' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil5.col(num_colonne).width = 3000
	feuil5.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil5.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT ngg_values,ngg_years from nations_gdp_growth where ngg_sources = 'Banque mondiale' and ngg_years = ? and ngg_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil5.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil5.col(1).width = 10500 # Largeur de la colonne "Nom du pays"

#3.6 PIB per capita (FMI)

# Création de la feuille
feuil6 = book.add_sheet(u'PIB par hab. (FMI)')

# Création des en-tête
feuil6.write(0,0,'Code ISO')
feuil6.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT ngc_years from nations_gdp_per_capita where ngc_sources = 'FMI' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil6.col(num_colonne).width = 4000
	feuil6.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil6.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT ngc_values,ngc_years from nations_gdp_per_capita where ngc_sources = 'FMI' and ngc_years = ? and ngc_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil6.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil6.col(1).width = 10500 # Largeur de la colonne "Nom du pays"
		
#3.7 PIB per capita (Banque mondiale)

# Création de la feuille
feuil7 = book.add_sheet(u'PIB par hab. (BM)')

# Création des en-tête
feuil7.write(0,0,'Code ISO')
feuil7.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT ngc_years from nations_gdp_per_capita where ngc_sources = 'Banque mondiale' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil7.col(num_colonne).width = 4000
	feuil7.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil7.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT ngc_values,ngc_years from nations_gdp_per_capita where ngc_sources = 'Banque mondiale' and ngc_years = ? and ngc_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil7.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil7.col(1).width = 10500 # Largeur de la colonne "Nom du pays"

#3.8 Population (FMI)

# Création de la feuille
feuil8 = book.add_sheet(u'Population (FMI)')

# Création des en-tête
feuil8.write(0,0,'Code ISO')
feuil8.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT np_years from nations_populations where np_sources = 'FMI' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil8.col(num_colonne).width = 5000
	feuil8.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil8.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT np_values,np_years from nations_populations where np_sources = 'FMI' and np_years = ? and np_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil8.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil8.col(1).width = 10500 # Largeur de la colonne "Nom du pays"
		
#3.9 Population (Banque mondiale)

# Création de la feuille
feuil9 = book.add_sheet(u'Population (BM)')

# Création des en-tête
feuil9.write(0,0,'Code ISO')
feuil9.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT np_years from nations_populations where np_sources = 'Banque mondiale' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil9.col(num_colonne).width = 5000
	feuil9.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil9.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT np_values,np_years from nations_populations where np_sources = 'Banque mondiale' and np_years = ? and np_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil9.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil9.col(1).width = 10500 # Largeur de la colonne "Nom du pays"

#3.10 Taux de chômage (FMI)

# Création de la feuille
feuil10 = book.add_sheet(u'Chômage (FMI)')

# Création des en-tête
feuil10.write(0,0,'Code ISO')
feuil10.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nu_years from nations_unemployment where nu_sources = 'FMI' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil10.col(num_colonne).width = 4000
	feuil10.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil10.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nu_values,nu_years from nations_unemployment where nu_sources = 'FMI' and nu_years = ? and nu_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil10.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil10.col(1).width = 10500 # Largeur de la colonne "Nom du pays"

#3.11 Taux de chômage (Banque mondiale)

# Création de la feuille
feuil11 = book.add_sheet(u'Chômage (BM)')

# Création des en-tête
feuil11.write(0,0,'Code ISO')
feuil11.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nu_years from nations_unemployment where nu_sources = 'Banque mondiale' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil11.col(num_colonne).width = 4000
	feuil11.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil11.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nu_values,nu_years from nations_unemployment where nu_sources = 'Banque mondiale' and nu_years = ? and nu_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil11.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil11.col(1).width = 10500 # Largeur de la colonne "Nom du pays"			

#3.12 Balance des comptes courants (FMI)

# Création de la feuille
feuil12 = book.add_sheet(u'Balance comptes (FMI)')

# Création des en-tête
feuil12.write(0,0,'Code ISO')
feuil12.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nca_years from nations_account_balances where nca_sources = 'FMI' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil12.col(num_colonne).width = 4000
	feuil12.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil12.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nca_values,nca_years from nations_account_balances where nca_sources = 'FMI' and nca_years = ? and nca_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil12.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil12.col(1).width = 10500 # Largeur de la colonne "Nom du pays"			

#3.13 Balance des comptes courants (Banque mondiale)

# Création de la feuille
feuil13 = book.add_sheet(u'Balance comptes (BM)')

# Création des en-tête
feuil13.write(0,0,'Code ISO')
feuil13.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nca_years from nations_account_balances where nca_sources = 'Banque mondiale' ")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil13.col(num_colonne).width = 4000
	feuil13.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil13.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nca_values,nca_years from nations_account_balances where nca_sources = 'Banque mondiale' and nca_years = ? and nca_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil13.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil13.col(1).width = 10500 # Largeur de la colonne "Nom du pays"			

#3.14 Investisseements français à l'étranger (Banque de France)

# Création de la feuille
feuil14 = book.add_sheet(u'Investissements français (BdF)')

# Création des en-tête
feuil14.write(0,0,'Code ISO')
feuil14.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nfi_years from nations_french_investments")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil14.col(num_colonne).width = 4000
	feuil14.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil14.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nfi_values,nfi_years from nations_french_investments where nfi_years = ? and nfi_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil14.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil14.col(1).width = 10500 # Largeur de la colonne "Nom du pays"			

#3.15 Investissements étrangers en France (Banque de France)

# Création de la feuille
feuil15 = book.add_sheet(u'IDE en France (BdF)')

# Création des en-tête
feuil15.write(0,0,'Code ISO')
feuil15.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nif_years from nations_investment_in_France")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil15.col(num_colonne).width = 4000
	feuil15.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil15.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nif_values,nif_years from nations_investment_in_France where nif_years = ? and nif_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil15.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil15.col(1).width = 10500 # Largeur de la colonne "Nom du pays"			

# 3.16. Classement Doing Business

# Création de la feuille
feuil16 = book.add_sheet(u'Doing Business')

# Création des en-tête
feuil16.write(0,0,'Code ISO')
feuil16.write(0,1,'Nom du pays')
feuil16.write(0,2,'Rang')
feuil16.write(0,3,'Source')

# Largeur des colonnes
feuil16.col(1).width = 10500 # Largeur de la colonne "Nom du pays"
feuil16.col(2).width = 5000 # Largeur de la colonne "Rang"
feuil16.col(3).width = 4000 # Largeur de la colonne "Source"

# Création des lignes de données
compteur = 1
for pays in listePays():
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	# Récupération de la superficie
 	conn = sqlite3.connect("datas/bases/general.db")
	cur = connectTable("nations_doing_business_ranks","ndbr",pays)					 						
	for row in cur:
		rang = row[1]	
		ligne = feuil16.row(compteur)
		ligne.write(0,pays)
		ligne.write(1,lib_pays)
		ligne.write(2,rang)
		ligne.write(3,"Banque mondiale")
		compteur = compteur+1
	conn.close()	

#3.17 Indicateur de développement humain - valeur (Pnud)

# Création de la feuille
feuil17 = book.add_sheet(u'Valeur IDH (PNUD)')

# Création des en-tête
feuil17.write(0,0,'Code ISO')
feuil17.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nh_years from nations_hdi")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil17.col(num_colonne).width = 4000
	feuil17.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil17.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nh_values, nh_years from nations_hdi where nh_years = ? and nh_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil17.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil17.col(1).width = 10500 # Largeur de la colonne "Nom du pays"			

#3.18 Indicateur de développement humain - rang (Pnud)

# Création de la feuille
feuil18 = book.add_sheet(u'Rang IDH (PNUD)')

# Création des en-tête
feuil18.write(0,0,'Code ISO')
feuil18.write(0,1,'Nom du pays')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years_init = []
conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT nh_years from nations_hdi")
for row in cur:
	# Ajout de chaque année trouvée à la liste	
	my_years_init.append(row[0])
# Suppression des doublons dans la liste
my_years_set = set(my_years_init)
my_years = []
for y in my_years_set:
	my_years.append(y)
my_years.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in my_years:
	# Largeur des colonnes
	feuil18.col(num_colonne).width = 4000
	feuil18.write(0,num_colonne,year)
	compteur_ligne = 1
	for pays in listePays():
		ligne = feuil18.row(compteur_ligne)
		cur2 = conn.cursor()						
		cur2.execute("SELECT nh_ranks, nh_years from nations_hdi where nh_years = ? and nh_ISO_3166_1_alpha_2 = ?", (year,pays))
		for row2 in cur2:
			ligne.write(num_colonne,row2[0])
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	
conn.close()		

# Création, enfin, des libellés pays
compteur = 1
for pays in listePays():
	ligne = feuil18.row(compteur)
	ligne.write(0,pays)
	# Récupération du nom du pays en français
	lib_pays = deAlpha2versLibelleFR(pays)
	ligne.write(1,lib_pays)
	compteur = compteur+1
feuil18.col(1).width = 10500 # Largeur de la colonne "Nom du pays"			

book.save("output/tableaux/pays/tableaux_pays.xls")