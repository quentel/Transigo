#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                transigo.py                                 #
#                                                                            #
#                        Christophe Quentel, 2015-2016                       #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Script racine permettant de lancer l'application, i.e. les différents scripts qui la compose, dans le bon ordre

# 1. IMPORTATION DES MODULES NECESSAIRES

import sys

# 2. LANCEMENT DES SCRIPTS

execfile("cleaning.py")
execfile("checking.py")
execfile("sql_general.py")
execfile("sql_nomenclatures.py")
execfile("sql_WITS.py")
#execfile("sql_douanes.py")
execfile("dl_DGFIP.py")
execfile("mo_DGFIP.py")
execfile("dl_FMI.py")
execfile("mo_FMI.py")
#execfile("dl_WITS-API.py")
#execfile("mo_WITS-API.py")
execfile("dl_PNUD.py")
execfile("mo_PNUD.py")
execfile("dl_Kiosque.py")
execfile("mo_Kiosque.py")
execfile("dl_UNSD_cities.py")
execfile("mo_UNSD_cities.py")
execfile("dl_UNSD_religions.py")
execfile("mo_UNSD_religions.py")
execfile("dl_UNSD_languages.py")
execfile("mo_UNSD_languages.py")
execfile("dl_ILO_unemployment.py")
execfile("mo_ILO_unemployment.py")
#execfile("dl_douanes.py")
#execfile("mo_douanes.py")
execfile("mo_BM.py")
execfile("mo_DoingBusiness.py")
execfile("mo_BdF.py")
execfile("dl_Wikipedia.py")
execfile("mo_Wikipedia.py")
#execfile("dl_alliances.py")
#execfile("dl_apd.py")
#execfile("dl_campus.py")
#execfile("dl_coordonnees_postes.py")
#execfile("dl_deplacements_de_raincourt.py")
#execfile("dl_deplacements_fekl.py")
#execfile("dl_deplacements_juppe.py")
#execfile("dl_deplacements_leonetti.py")
#execfile("dl_deplacements_mae.py")
#execfile("dl_etudiants.py")
#execfile("dl_francais.py")
#execfile("dl_geolocalisation.py")
#execfile("dl_geo_qo.py")
#execfile("mo_geo_qo.py")
#execfile("dl_IFRE.py")
#execfile("dl_visas_etudiants.py")
execfile("vw_fiches_signaletiques.py")
execfile("vw_tableaux_pays.py")
execfile("vw_tableaux_qo.py")
execfile("vw_tableaux_wits.py")
execfile("vw_html.py")
execfile("graph.py")
execfile("ul_FTP.py")