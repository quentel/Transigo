#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

code_pays = "FR"
city_name = "Paris"
is_capital_city = True
city_type = "aglo"

conn = sqlite3.connect("datas/bases/general.db")
cur = conn.cursor()						
cur.execute("SELECT mt_years from countries_main_towns where mt_ISO_3166_1_alpha_2 = ? and mt_towns = ? and mt_capital_cities = ? and mt_types = ?", (code_pays, unicode(city_name), is_capital_city, unicode(city_type)))		
print len(cur.fetchall())