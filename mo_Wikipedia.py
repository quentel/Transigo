#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                            mo_Wikipedia.py                                 #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script lit les fichiers téléchargés avec dl_Wikipedia.py et en extrait les
# informations dont nous avons besoin.

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sqlite3, urllib2, re
import xml.etree.ElementTree as ET
from functions import log, listeTerritoiresFR, deWikiversISO, getCapitale


log("")
log("===================================")
log("Lancement du script mo_Wikipedia.py")
log("===================================")


# 2. FONCTIONS

# 2.1. Une fonction qui nettoie les noms de pays que nous trouvons sur ces pages

def nameCleaning(str):
	# On ne prend que ce qui est après le signe =	
	result = str[str.find('=')+1:]
	# On supprime, le cas échéant, ce qui arrive après un signe <
	if result.find('<') > -1:	
		result = result[:result.find('<')]
	# On supprime, le cas échéant, ce qui arrive après un signe [, s'il y en a un :
	if result.find('[') > -1:		
		result = result[:result.find('[')]
	# On supprime, le cas échéant, les caractères ''':
	if result.find('\'\'\'') > -1:		
		result = result.replace("\'\'\'", "")
	# On supprime, le cas échéant, ce qui arrive après un signe |, s'il y en a un :
	if result.find('|') > -1:		
		result = result[:result.find('|')]
	# On supprime les espaces avant et après	
	result = result.strip()
	# Si la chaîne est vide, le résultat est N/A
	if result == "":
		result = "N/A"
	return result

# 2.2. Une fonction qui nettoie les types de régime politique que nous trouvons sur ces pages
def polCleaning(str):
	# On ne prend que ce qui est après le signe =	
	result = str[str.find('=')+1:]
	# On nettoie les liens Wiki, en ne gardant que leur libellé (ils sont de la forme [libelle|lien])
	pattern_lk = re.compile(u"\[(.*?)\|(.*?)\]", re.UNICODE)
	matchLk = re.findall(pattern_lk, result)
	# Si aucun lien n'apparaît dans le texte, pas de difficulté	
	if len(matchLk) == 0:
		result = result
	# Si un lien Wiki apparaît, on supprime tout ce qui, dans le texte, apparaît entre un | et un ]
	else:
		pattern_remp = re.compile(u"\|(.*?)\]", re.UNICODE)
		result = re.sub(pattern_remp, "", result)		
	# On supprime, le cas échéant, tout ce qui arrive après les caractères ]]]:
	if result.find("]]]") > -1:		
		result = result[:result.find(']]]')]	
	# On supprime, le cas échéant, les caractères [:
	if result.find("[") > -1:		
		result = result.replace("[", "")
	# On supprime, le cas échéant, les caractères ]:
	if result.find("]") > -1:		
		result = result.replace("]", "")
	# On supprime, le cas échéant, ce qui arrive après un signe <
	if result.find('<') > -1:	
		result = result[:result.find('<')]	
	# On supprime les espaces avant et après	
	result = result.strip()
	# Si la chaîne est vide, le résultat est N/A
	if result == "":
		result = "N/A"
	return result

# 2.3. Une fonction qui nettoie les noms de langues officielles que nous trouvons sur ces pages
def lngCleaning(str):
	# On ne prend que ce qui est après le signe =	
	result = str[str.find('=')+1:]
	# On nettoie les liens Wiki, en ne gardant que leur libellé (ils sont de la forme [libelle|lien])
	pattern_lk = re.compile(u"\[(.*?)\|(.*?)\]", re.UNICODE)
	matchLk = re.findall(pattern_lk, result)
	# Si aucun lien n'apparaît dans le texte, pas de difficulté	
	if len(matchLk) == 0:
		result = result
	# Si un lien Wiki apparaît, on supprime tout ce qui, dans le texte, apparaît entre un | et un ]
	else:
		pattern_remp = re.compile(u"\|(.*?)\]", re.UNICODE)
		result = re.sub(pattern_remp, "", result)		
	# On remplace, le cas échéant, le caractère <br /> par une simple virgule
	if result.find("<br />") > -1:		
		result = result.replace("<br />", ", ")	
	# On supprime, le cas échéant, les caractères [:
	if result.find("[") > -1:		
		result = result.replace("[", "")
	# On supprime, le cas échéant, les caractères ]:
	if result.find("]") > -1:		
		result = result.replace("]", "")
	# On supprime, le cas échéant, tout ce qui arrive après le caractère <:
	if result.find("<") > -1:		
		result = result[:result.find('<')]	
	# On supprime les espaces avant et après	
	result = result.strip()
	# Si la chaîne est vide, le résultat est N/A
	if result == "":
		result = "N/A"
	return result

# 2.4. Une fonction qui nettoie les titres des chefs d'Etat que nous trouvons sur ces pages
def hdTitleCleaning(str):
	# On ne prend que ce qui est après le signe =	
	result = str[str.find('=')+1:]
	# On nettoie les liens Wiki, en ne gardant que leur lien (ils sont de la forme [libelle|lien])
	pattern_lk = re.compile(u"\[(.*?)\|(.*?)\]", re.UNICODE)
	matchLk = re.findall(pattern_lk, result)
	# Si aucun lien n'apparaît dans le texte, pas de difficulté	
	if len(matchLk) == 0:
		result = result
	# Si un lien Wiki apparaît, on supprime tout ce qui, dans le texte, apparaît entre un | et un ]
	else:
		pattern_remp = re.compile(u"\[(.*?)\|", re.UNICODE)
		result = re.sub(pattern_remp, "", result)		
	# On supprime, le cas échéant, les caractères '''
	if result.find("\'\'\'") > -1:		
		result = result.replace(("\'\'\'"), "")	
	# On supprime, le cas échéant, les caractères [:
	if result.find("[") > -1:		
		result = result.replace("[", "")
	# On supprime, le cas échéant, les caractères ]:
	if result.find("]") > -1:		
		result = result.replace("]", "")
	# On supprime les espaces avant et après	
	result = result.strip()
	# Si la chaîne est vide, le résultat est N/A
	if result == "":
		result = "N/A"
	return result

# 2.5. Une fonction qui nettoie les noms des chefs d'Etat que nous trouvons sur ces pages
def hdNameCleaning(str):
	# On ne prend que ce qui est après le signe =	
	result = str[str.find('=')+1:]
	# On nettoie les liens Wiki, en ne gardant que leur lien (ils sont de la forme [libelle|lien])
	pattern_lk = re.compile(u"\[(.*?)\|(.*?)\]", re.UNICODE)
	matchLk = re.findall(pattern_lk, result)
	# Si aucun lien n'apparaît dans le texte, pas de difficulté	
	if len(matchLk) == 0:
		result = result
	# Si un lien Wiki apparaît, on supprime tout ce qui, dans le texte, apparaît entre un | et un ]
	else:
		pattern_remp = re.compile(u"\[(.*?)\|", re.UNICODE)
		result = re.sub(pattern_remp, "", result)	
	# On vire si nécessaire les metatags Wiki
	pattern_lk2 = re.compile(u"\{\{(.*?)\|(.*?)\]", re.UNICODE)
	matchLk2 = re.findall(pattern_lk2, result)
	# Si aucun lien n'apparaît dans le texte, pas de difficulté	
	if len(matchLk2) == 0:
		result = result
	# Si un lien Wiki apparaît, on supprime tout ce qui, dans le texte, apparaît entre un | et un ]
	else:
		pattern_remp = re.compile(u"\{\{(.*?)\|", re.UNICODE)
		result = re.sub(pattern_remp, "", result)	
	# On supprime, le cas échéant, les caractères '''
	if result.find("\'\'\'") > -1:		
		result = result.replace(("\'\'\'"), "")
	# On supprime, le cas échéant, les caractères ''
	if result.find("\'\'") > -1:		
		result = result.replace(("\'\'"), "")	
	# On supprime, le cas échéant, les caractères [:
	if result.find("[") > -1:		
		result = result.replace("[", "")
	# On supprime, le cas échéant, les caractères ]:
	if result.find("]") > -1:		
		result = result.replace("]", "")
		# On supprime, le cas échéant, les caractères {:
	if result.find("{") > -1:		
		result = result.replace("{", "")
	# On supprime, le cas échéant, les caractères }:
	if result.find("}") > -1:		
		result = result.replace("}", "")
	# On supprime, le cas échéant, tout ce qui arrive après le caractère <:
	if result.find("<") > -1:		
		result = result[:result.find('<')]	
	# On supprime les espaces avant et après	
	result = result.strip()
	# Si la chaîne est vide, le résultat est N/A
	if result == "":
		result = "N/A"
	return result

# 2.6. Une fonction qui nettoie les noms de capitales que nous trouvons sur ces pages
def capCleaning(str):
	# On ne prend que ce qui est après le signe =	
	result = str[str.find('=')+1:]
	# On supprime, le cas échéant, tout ce qui arrive après le caractère <:
	if result.find("<") > -1:		
		result = result[:result.find('<')]	
	# On nettoie les liens Wiki, en ne gardant que leur lien (ils sont de la forme [libelle|lien])
	pattern_lk = re.compile(u"\[(.*?)\|(.*?)\]", re.UNICODE)
	matchLk = re.findall(pattern_lk, result)
	# Si aucun lien n'apparaît dans le texte, pas de difficulté	
	if len(matchLk) == 0:
		result = result
	# Si un lien Wiki apparaît, on supprime tout ce qui, dans le texte, apparaît entre un | et un ]
	else:
		pattern_remp = re.compile(u"\[(.*?)\|", re.UNICODE)
		result = re.sub(pattern_remp, "", result)	
	# On vire si nécessaire les metatags Wiki
	pattern_lk2 = re.compile(u"\{\{(.*?)\|(.*?)\]", re.UNICODE)
	matchLk2 = re.findall(pattern_lk2, result)
	# Si aucun lien n'apparaît dans le texte, pas de difficulté	
	if len(matchLk2) == 0:
		result = result
	# Si un lien Wiki apparaît, on supprime tout ce qui, dans le texte, apparaît entre un | et un ]
	else:
		pattern_remp = re.compile(u"\{\{(.*?)\|", re.UNICODE)
		result = re.sub(pattern_remp, "", result)	
	# On supprime, le cas échéant, tout ce qui arrive après le caractère |:
	if result.find("|") > -1:		
		result = result[:result.find('|')]	
	# On supprime, le cas échéant, les caractères '''
	if result.find("\'\'\'") > -1:		
		result = result.replace(("\'\'\'"), "")
	# On supprime, le cas échéant, les caractères ''
	if result.find("\'\'") > -1:		
		result = result.replace(("\'\'"), "")	
	# On supprime, le cas échéant, les caractères [:
	if result.find("[") > -1:		
		result = result.replace("[", "")
	# On supprime, le cas échéant, les caractères ]:
	if result.find("]") > -1:		
		result = result.replace("]", "")
		# On supprime, le cas échéant, les caractères {:
	if result.find("{") > -1:		
		result = result.replace("{", "")
	# On supprime, le cas échéant, les caractères }:
	if result.find("}") > -1:		
		result = result.replace("}", "")
	# On supprime les espaces avant et après	
	result = result.strip()
	# Si la chaîne est vide ou inepte, le résultat est N/A
	if result == "":
		result = "N/A"
	return result

# 2.7. Une fonction de normalisation des noms des régimes politiques utilisés sur Wikipedia
def normalizeTypeGov(typeGov):
	
	# Wikipedia mélange nature du régime, type de gouvernement et forme de l'Etat. Mettons un peu d'ordre là dedans.	
	if (typeGov == "Absolutisme de Monarchie de droit divin") or (typeGov == "Monarchie absolue") or (typeGov == "Monarchie absolue de droit divin"):
		typeGov = "Monarchie"
	if (typeGov == "Monarchie Monarchie constitutionnelle Régime parlementaire État unitaire") or (typeGov == "Monarchie Monarchie constitutionnelle Régime parlementaire État unitaire Régionalisation") or (typeGov == "Monarchie constitutionnelle régime parlementaire État unitaire"):
		typeGov = "Monarchie constitutionnelle (régime parlementaire, Etat unitaire)"
	if (typeGov == "Monarchie constitutionnelle Régime parlementaire État fédéral") or (typeGov == "Monarchie constitutionnelle régime parlementaire État fédéral"):
		typeGov = "Monarchie constitutionnelle (régime parlementaire, Etat fédéral)"
	if typeGov == "Monarchie constitutionnelle monarchie élective":
		typeGov = "Monarchie constitutionnelle"
	if (typeGov == "Monarchie constitutionnelle État fédéral") or (typeGov == "Monarchie constitutionnelle, monarchie élective et État fédéral"):
		typeGov = "Monarchie constitutionnelle (Etat fédéral)"
	if typeGov == "Monarchie constitutionnelle État unitaire":
		typeGov = "Monarchie constitutionnelle (Etat unitaire)"
	if typeGov == "Monarchie constitutionnelle, parlementaire":
		typeGov = "Monarchie constitutionnelle (régime parlementaire)"
	if (typeGov == "Principauté monarchie constitutionnelle régime parlementaire État unitaire") or (typeGov == "Principauté régime parlementaire"):
		typeGov = "Principauté (régime parlementaire)"	
	if (typeGov == "République  à parti unique") or (typeGov =="République à parti unique") or (typeGov == "République à parti unique Nationalisme"):
		typeGov = "République à parti unique"
	if (typeGov == "République (Régime présidentiel)") or (typeGov == "République présidentielle") or (typeGov == "République régime présidentiel") or (typeGov == "République à régime présidentiel"):
		typeGov = "République (régime présidentiel)"
	if (typeGov == "République Parlement") or (typeGov == "République Régime parlementaire") or (typeGov == "République République parlementaire") or (typeGov == "République parlementaire") or (typeGov == "République parlementarisme Fédéralisme") or (typeGov == "République régime parlementaire"):
		typeGov = "République (régime parlementaire)"
	if (typeGov == "République Régime semi-présidentiel") or (typeGov == "République régime semi-présidentiel") or (typeGov == "République semi-présidentielle") or (typeGov == "République État unitaire Régime semi-présidentiel"):
		typeGov = "République (régime semi-présidentiel)"
	if typeGov == "République Régime semi-présidentiel État fédéral":
		typeGov = "République (régime semi-présidentiel, Etat fédéral)"
	if typeGov == "République Régime semi-présidentiel État fédéral":
		typeGov = "République (régime semi-présidentiel, Etat fédéral)"
	if typeGov == "République République constitutionnelle Régime parlementaire État fédéral":
		typeGov = "République (régime parlementaire, Etat fédéral)"
	if typeGov == "République République constitutionnelle État unitaire Régime présidentiel":
		typeGov = "République (régime présidentiel, Etat unitaire)"
	if typeGov == "République constitutionnelle régime présidentiel État fédéral":
		typeGov = "République (régime présidentiel, Etat fédéral)"
	if typeGov == "République constitutionnelle régime parlementaire État unitaire":
		typeGov = "République (régime parlementaire, Etat unitaire)"
	if (typeGov == "République fédérale") or (typeGov == "République fédéralisme") or (typeGov =="République État fédéral") or (typeGov =="République état fédéral"):
		typeGov = "République fédérale"
	if (typeGov == "République islamique") or (typeGov == "République islamique Régime présidentiel à :en:Dominant-party system"):
		typeGov = "République islamique"	
	if (typeGov == "République présidentielle fédéralisme") or (typeGov == "République État fédéral Régime présidentiel") or (typeGov == "République régime présidentiel État fédéral"):
		typeGov = "République (régime présidentiel, Etat fédéral"
	
# 3. SCRIPT

compteur = 0

path  = "datas/wikipedia/"
liste_fichiers = os.listdir(path)

for fichier in liste_fichiers:
	log("Traitement du fichier %s." % fichier)
	# On récupère le code ISO du pays à partir du nom de fichier
	nom_pays = fichier.replace(".xml", "")
	code_ISO = deWikiversISO(nom_pays)
	# On ouvre le fichier
	my_path = path+fichier
	tree = ET.parse(my_path)
	# On parse le fichier XML jusqu'à arriver à la partie qui nous intéresse (dans root/page/revision/)	
	root = tree.getroot()
	for child in root:
		if child.tag == "{http://www.mediawiki.org/xml/export-0.10/}page":
			my_page = child
			for child in my_page:
				if child.tag == "{http://www.mediawiki.org/xml/export-0.10/}revision":
					my_revision = child
					for child in my_revision:
						if child.tag == "{http://www.mediawiki.org/xml/export-0.10/}text":
							my_text = child.text
							# Et là, fini le XML : on attaque les expressions régulières
							
							# 2.1. Nom officiel
							# Il apparaît généralement après le tag "nom_français", mais parfois aussi après "nom_officiel".
							nomFr_s = ""
							nomOff = ""
							pattern_no = re.compile(u"nom_officiel.*\n", re.UNICODE)							
							pattern_nf = re.compile(u"nom_français.*\n", re.UNICODE)
							matchNomOff = re.search(pattern_no, my_text)							
							try:
								nomOff_s = matchNomOff.group()
								nomOff = nameCleaning(nomOff_s)
							except:
								matchNomFr = re.search(pattern_nf, my_text)
								try:
									nomFr_s = matchNomFr.group()
									nomOff = nameCleaning(nomFr_s)
								except:
									nomOff = "N/A"
														
							# 2.2. Régime politique
							# Il apparaît après "type_gouvernement"
							typeGvt_s = ""
							typeGvt = ""
							pattern_gvt = re.compile(u"type_gouvernement.*\n", re.UNICODE)							
							matchTypeGvt = re.search(pattern_gvt, my_text)							
							try:
								typeGvt_s = matchTypeGvt.group()
								typeGvt = polCleaning(typeGvt_s)
							except:
								typeGvt = "N/A"
							
							# 2.3. Langue officielle
							# Elle apparaît après "langues_officielles"
							lngOff_s = ""								
							lngOff = ""
							pattern_lng = re.compile(u"langues_officielles.*\n", re.UNICODE)
							pattern2_lng = re.compile(u"langues.*\n", re.UNICODE)							
							matchOffLng = re.search(pattern_lng, my_text)							
							try:
								lngOff_s = matchOffLng.group()								
								lngOff = lngCleaning(lngOff_s)
							except:
								matchOffLng = re.search(pattern2_lng, my_text)							
								try:
									lngOff_s = matchOffLng.group()								
									lngOff = lngCleaning(lngOff_s)								
								except:
									lngOff = "N/A"
							
							try:
								conn = sqlite3.connect("datas/bases/general.db")
								conn.execute("UPDATE nations_datas SET nd_official_name = ?, nd_state_form = ?, nd_off_languages = ? where nd_ISO_3166_1_alpha_2 = ?", (nomOff, typeGvt, lngOff, code_ISO)) 
								conn.commit()
								conn.close()
							except Exception, e:
								log("===Erreur sur l'insertion en base de données d'un nom officiel, d'une langue officielle ou d'une forme de l'Etat : %s" % e)
								pass
								
							# 2.4. Dirigeants
							# Nous avons jusqu'à trois niveaux de dirigeants, pourvus d'un titre et d'un nom
							# 2.4.1. Titres
							# 2.4.1.1. Titre du Grand Chef							
							hd1_title_s = ""									
							hd1_title = ""							
							pattern_hd1_title = re.compile(u"titre_dirigeant.*\n", re.UNICODE)
							pattern2_hd1_title = re.compile(u"titres_dirigeants.*\n", re.UNICODE)							
							match_hd1_title = re.search(pattern_hd1_title, my_text)							
							try:
								hd1_title_s = match_hd1_title.group()							
								hd1_title = hdTitleCleaning(hd1_title_s)
							except:
								match_hd1_title = re.search(pattern2_hd1_title, my_text)							
								try:
									hd1_title_s = match_hd1_title.group()							
									hd1_title = hdTitleCleaning(hd1_title_s)
								except:
									hd1_title = "N/A"
							# 2.4.1.2. Titre du sous-chef							
							hd2_title_s = ""									
							hd2_title = ""							
							pattern_hd2_title = re.compile(u"titre_dirigeant2.*\n", re.UNICODE)							
							match_hd2_title = re.search(pattern_hd2_title, my_text)							
							try:
								hd2_title_s = match_hd2_title.group()							
								hd2_title = hdTitleCleaning(hd2_title_s)
							except:
								hd2_title = "N/A"
							# 2.4.1.3. Titre du sous-sous-chef							
							hd3_title_s = ""									
							hd3_title = ""							
							pattern_hd3_title = re.compile(u"titre_dirigeant3.*\n", re.UNICODE)							
							match_hd3_title = re.search(pattern_hd3_title, my_text)							
							try:
								hd3_title_s = match_hd3_title.group()							
								hd3_title = hdTitleCleaning(hd3_title_s)
							except:
								hd3_title = "N/A"
							# 2.4.2. Noms
							# 2.4.2.1. Nom du Grand Chef							
							hd1_name_s = ""									
							hd1_name = ""							
							pattern_hd1_name = re.compile(u"nom_dirigeant.*\n", re.UNICODE)
							pattern2_hd1_name = re.compile(u"noms_dirigeants.*\n", re.UNICODE)							
							match_hd1_name = re.search(pattern_hd1_name, my_text)							
							try:
								hd1_name_s = match_hd1_name.group()									
								hd1_name = hdNameCleaning(hd1_name_s)
							except:
								match_hd1_name = re.search(pattern2_hd1_name, my_text)
								try:
									hd1_name_s = match_hd1_name.group()									
									hd1_name = hdNameCleaning(hd1_name_s)
								except:
									hd1_name = "N/A"
							# 2.4.2.2. Nom du sous-chef							
							hd2_name_s = ""									
							hd2_name = ""							
							pattern_hd2_name = re.compile(u"nom_dirigeant2.*\n", re.UNICODE)							
							match_hd2_name = re.search(pattern_hd2_name, my_text)							
							try:
								hd2_name_s = match_hd2_name.group()									
								hd2_name = hdNameCleaning(hd2_name_s)
							except:
								hd2_name = "N/A"
							# 2.4.2.3. Nom du sous-sous-chef							
							hd3_name_s = ""									
							hd3_name = ""						
							pattern_hd3_name = re.compile(u"nom_dirigeant3.*\n", re.UNICODE)							
							match_hd3_name = re.search(pattern_hd3_name, my_text)							
							try:
								hd3_name_s = match_hd3_name.group()									
								hd3_name = hdNameCleaning(hd3_name_s)
							except:
								hd3_name = "N/A"						
							# Injection dans la base de données							
							try:
								conn = sqlite3.connect("datas/bases/general.db")
								conn.execute("INSERT INTO nations_leaders (nl_ISO_3166_1_alpha_2, nl_ranks, nl_functions, nl_names) VALUES (?, ?, ?, ?)",(code_ISO,1,hd1_title,hd1_name))
								conn.commit()
								conn.close()
							except Exception, e:
								log("===Erreur sur l'insertion en base de données du dirigeant de tier 1 : %s" % e)
								pass
							try:
								conn = sqlite3.connect("datas/bases/general.db")
								conn.execute("INSERT INTO nations_leaders (nl_ISO_3166_1_alpha_2, nl_ranks, nl_functions, nl_names) VALUES (?, ?, ?, ?)",(code_ISO,2,hd2_title,hd2_name))
								conn.commit()
								conn.close()
							except Exception, e:
								log("===Erreur sur l'insertion en base de données du dirigeant de tier 2 : %s" % e)
								pass
							try:
								conn = sqlite3.connect("datas/bases/general.db")
								conn.execute("INSERT INTO nations_leaders (nl_ISO_3166_1_alpha_2, nl_ranks, nl_functions, nl_names) VALUES (?, ?, ?, ?)",(code_ISO,3,hd3_title,hd3_name))
								conn.commit()
								conn.close()
							except Exception, e:
								log("===Erreur sur l'insertion en base de données du dirigeant de tier 3 : %s" % e)
								pass

							# 2.5. Capitale
							# Seulement pour les pays pour lesquels l'ONU ne nous a pas fourni cette information
							cap_s = ""
							cap = ""
							pattern_cap = re.compile(u"capitale.*\n", re.UNICODE)							
							matchOffCap = re.search(pattern_cap, my_text)							
							try:
								cap_s = matchOffCap.group()																
								cap = capCleaning(cap_s)
							except:
								cap = "N/A"
							# Maintenant, on cherche si on a déjà une capitale dans la base de données (remplie par l'ONU, jusqu'ici)
							info_Capitale = getCapitale(code_ISO)													
							if info_Capitale[0] == "N/A" and cap != "N/A":
								conn = sqlite3.connect("datas/bases/general.db")
								conn.execute("INSERT INTO countries_main_towns (mt_towns, mt_ISO_3166_1_alpha_2, mt_capital_cities, mt_populations,mt_years,mt_types) VALUES (?, ?, ?, ?, ?, ?)",(unicode(cap),code_ISO,True,u"N/A",u"N/A",u"N/A"))
								conn.commit()
								conn.close()
													
	# Et on supprime le fichier traité
	file_path = path+fichier	
	os.remove(file_path)