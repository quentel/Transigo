Transigo
========

**Transigo** (pour *"Traitement normalisé et synthèse d'informations géographiquement orientées"*) est une application qui télécharge, traite, stocke et restitue des données ouvertes, produites par des institutions publiques et privées, sur la situation politique, économique et sociale de tous les pays du monde et leurs relations avec la France.

## Licence

Transigo est distribuée sous la [licence publique générale GNU](http://www.gnu.org/licenses/) (GNU GPL).

## Principes de fonctionnement

Transigo exploite des données provenant notamment du [ministère des Affaires étrangères et du Développement international](http://www.diplomatie.gouv.fr), de la [Banque mondiale](http://www.worldbank.org), du [Fonds monétaire international](http://www.imf.org), du système des [Nations unies](http://www.un.org), de la [direction générale des douanes](http://www.douane.gouv.fr), de la [direction générale des Finances publiques](http://www.economie.gouv.fr/dgfip/), de la [direction générale du Trésor](http://lekiosque.finances.gouv.fr/) et de la [Banque de France](https://www.banque-france.fr),mais aussi de la version française de [Wikipedia](http://fr.wikipedia.org.
Lorsque ces institutions proposent une interface de programmation (API), l'application utilise celle-ci pour récupérer directement chaque donnée intéressante en ligne ; lorsqu'elles publient leurs productions sous la forme de fichiers téléchargeables, Transigo détecte automatiquement la dernière version de ceux-ci et la stocke dans son arborescence, avant de les lire pour y récupérer les informations pertinentes ; lorsque ces données sont simplement accessibles sur des pages web, l'application "parse" ces dernières afin d'identifier les éléments qui lui sont nécessaires.
Ceux-ci sont stockés dans des bases de données. C'est dans celles-ci que l'application puise pour élaborer diverses fiches (fiches-pays, fiches-région, fiches-organisation internationale), graphiques et tableaux de synthèse : ce sont ces productions que l'utilisateur peut ensuite exploiter à sa guise.

## Choix technologiques

Transigo a été écrite en Python (v. 2.7) afin d'être portable sur n'importe quelle plateforme, mais aussi en langage R afin de traiter dans un temps raisonnable les fichiers les plus lourds. Elle n'a pas d'interface graphique : une fois lancée, l'application travaille de façon autonome pendant plusieurs heures (en fonction des données mises à jour et de la puissance de l'ordinateur utilisé), avant de produire les fichiers recherchés. Les données traitées n'étant mises à jour qu'entre deux fois par mois (taux de chancellerie) et une fois tous les deux ou trois ans (données sociales de la Banque mondiale), il est inutile de la lancer plus d'une fois par semaine.

Transigo s'appuie notamment sur de solides nomenclatures, élaborées pour l'occasion - afin de rendre compatibles des données émanant d'institutions nationales et internationales qui ne traitent pas des mêmes réalités géographiques, ou leur donnent des noms différents (y compris dans une même langue, et parfois même aussi entre plusieurs programmes pilotés par une même institution). 

Python 2 a été préféré à Python 3, dans la mesure où la programmation orientée objet et le paradigme MVC ne s'imposaient pas pour un logiciel de ce type (sans GUI, au fonctionnement très linéaire).

L'application est par ailleurs conçue de façon très modulaire (avec de nombreux scripts distincts) afin de faciliter sa maintenance comme la réutilisation de ses composants. Ses scripts partagent néanmoins des fonctions en commun.

Les données sont stockées dans trois bases SQLlite - plus faciles à manipuler avec Python, et amplement suffisantes pour gérer quelques milliers d'accès sur quelques heures par an. Il était toutefois préférable de les segmenter en plusieurs bases afin de pouvoir mettre à jour séparément des éléments divers (les nomenclatures n'évoluent guère qu'une fois par an ; les données des douanes, très lourdes, une fois par mois ; les données du Pnud, de la banque mondiale et du FMI, entre une et trois fois par an). 

## Modules requis

Transigo utilise plusieurs modules Python qui ne sont pas fournis par défaut avec la plupart des distributions de la version 2.7 :
- *beautifulsoup4*, afin de lire directement des données sur des pages web,
- *python-docx*, afin de créer des documents aux formats Microsoft Word ou LibreOffice Writer,
- *xlrd*, afin de consulter des fichiers de données aux formats Excel ou LibreOffice Calc, 
- *xlwt*, qui permet de créer des fichiers aux formats Excel et LibreOffice Calc.
- *libarchive* et *libarchive-c*, pour décompacter les fichiers au format 7z.
- et *matplotlib*, *pylab* et *numpy* afin de dessiner des graphiques à partir de nos données.

Pour les installer, n'hésitez pas à utiliser *pip* : `pip install xlwt`. Le paquet matplotlib peut être installé séparément (sous Ubuntu et Linux Mint : 'sudo apt-get install python-matplotlib')
Et si vous utilisez python sous Ubuntu (ou Mint) et êtes aussi distrait que moi, n'oubliez pas que cette installation ne fonctionnera qu'à la condition de respecter le [sacro-saint paradigme de la fabrication du sandwich](https://xkcd.com/149/) :`sudo pip install xlwt`.

## Mode d'emploi

Vous utiliserez sans doute Transigo sans même vous en rendre compte : cette application est d'abord conçue pour être installée sur un serveur, où un cron la déclenchera régulièrement, afin de faire en sorte que les fichiers qu'elle produit soient accessibles via FTP ou HTTP.

Dans l'hypothèse où vous souhaiteriez néanmoins vous emparer vous-même du sujet, voici ce qu'il faut faire :

- Téléchargez tous les fichiers ci-joint dans un répertoire sur votre ordinateur ;
- Si vous utilisez Windows, [installez Python 2.7 sur votre ordinateur](http://lmgtfy.com/?q=install+python+2.7+windows). Si vous utilisez Mac OS X ou Linux, il est déjà présent sur votre machine (ou, si vous utilisez une distro Linux suffisamment exotique pour qu'il ne le soit pas, c'est que vous savez déjà comment remédier à ce problème) ;
- Installez si nécessaire les modules Python complémentaires listés ci-dessus ;
- Si vous souhaitez télécharger automatiquement les données produites par Transigo vers un serveur Web (comme je le fais vers http://www.mesfichespays.fr), éditez le fichier *config.txt* dans le répertoire *config* pour y placer les coordonnées complètes de votre serveur (URL, chemin, nom d'utilisateur, mot de passe) ; 
- Ouvrez votre console python, ou votre terminal sous Linux ou Mac ;
- Ouvrez, dans cette console ou ce terminal, le répertoire dans lequel vous avez téléchargé Transigo
`cd Le_chemin_de_mon_dossier/Transigo`
- Saisissez la commande suivante :
`python transigo.py`
- Patientez quelques heures, en suivant l'évolution des opérations dans le fichier *log.txt* du dossier *logs*. Si tout se passe bien, vous retrouverez tous les fichiers produits par l'application dans le répertoire "output".
- Si le répertoire "output" n'existe pas, qu'il est vide et/ou votre console Python vous indique qu'un problème s'est produit, il est probable que votre connexion internet est tombée pendant l'opération, ou que l'un des serveurs que vous avez tenté de consulter n'a pas supporté cette charge supplémentaire. Relancez l'application.
- Si le problème persiste, envoyez-moi un mail, et les copies d'écran pertinentes à l'adresse [transigo@quentel.com](mailto:transigo@quentel.com)

## Liste des fichiers

### Fichiers graphiques
Le répertoire *graphismes* accueille un dossier *flags*, contenant une représentation au format .svg des drapeaux de tous les pays au monde et de quelques territoires qui nous intéressent (téléchargés sous Wikipedia, oui), ainsi qu'un dossier *logos*, accueillant des représentations de celui du projet, aux formats .png et .xcf (nous sommes bien d'accord, il n'a rien d'original, ni d'extraordinaire. Si vous pouvez faire mieux, faites moi signe).

### Fichiers de données
- Le répertoire *datas* accueille nos données :
- le sous-dossier "bases" accueille *general.db*, qui recense les informations collectées, *nomenclatures.db*, dont le nom est assez explicite (et qui n'a pas à être mise à jour plus d'une fois par an) et *wits.db*, qui reprend les données publiées par le programme de la Banque mondiale consacrés aux échanges commerciaux entre les pays ;
- les sous-dossiers *alliances*, *apd*, *campus*, *déplacements*, *douanes*, *dgfip*, *ilo*, *kiosque*, *pnud*, *wits*, *fmi*, *wikipedia* et *unsd* accueillent les fichiers produits par ces organisations et téléchargés sur notre disque, le temps nécessaire pour leur traitement (ils sont normalement supprimés après celui-ci).
- Le répertoire *old_datas* accueille les anciennes versions de nos données (5 dernières bases SQL, 5 derniers fichiers de logs).
Lors du premier lancement de l'application, seul le répertoire *nomenclatures* existe.

### Logs
Transigo crée automatiquement un nouveau fichiers de logs à chacun de ses lancements. Celui-ci, installé dans le répertoire *logs* (également créé par le logiciel), recense toutes les actions menées et toutes les erreurs rencontrées par l'application.

### Config
Le fichier *config.txt*, dans le répertoire *config*, accueille les adresses et identifiants nécessaires à la connexion de Transigo à nos propres servers (ceux sur lesquels nous souhaitons publier les résulats de nos travaux).

### Fichiers de nomenclatures
Le répertoire "nomenclatures" accueille plusieurs fichiers indispensables au paramétrage de la base de données :
- *countries_list.txt* est un fichier conçu spécifiquement pour Transigo. Il reprend les dominations de "pays" dans la norme ISO 3166-2, et met en relation les codes de ceux-ci à 2 et 3 trois lettres (FR et FRA pour la France, p. ex.), avec leur nom complet en français et en anglais. Une colonne permet de rattacher ces "pays" à la personne morale qui les représente en droit international, et notamment à l'ONU (la France pour la Guadeloupe ou la Martinique, par exemple).
- *WITS_list.txt* est un autre fichier conçu spécialement pour Transigo. Il vise à transcrire dans un format correct les noms de pays, parfois fantaisistes ("Anguila" au lieu de "Anguilla", "Wallis et Futura" au lieu de "Wallis et Futuna") ou mal formatés ("Curaçao"), qui apparaissent dans les fiches pays produites par le *World Integrated Trade Solutions" de la Banque mondiale.
- *doingbusiness.txt* a été écrit afin de permettre l'intégration dans Transigo des données d'un autre programme de la Banque mondiale : *Doing Business*. Cette nomenclature établit une relation entre la dénomination choisie par ce projet pour chaque pays (qui, bien sûr, ne correspond pas toujours aux deux précédentes...) et le code ISO en deux lettres de ce territoire.
- *wiki_list.txt* lie le nom (en français) d'une page pays sous Wikipedia aux codes ISO de ce territoire.
- toujours créé spécialement pour Transigo, *currencies_list.txt* met en relation le code ISO 3166 à deux lettres désignant un pays avec le code à trois lettres de sa monnaie, ainsi que le nom en français de celle-ci.
- *continents_list.txt* classe les pays par continent, région et sous-région.
- *cities_list* traduit en français le nom en anglais des villes du monde tel qu'il apparaît dans les bases de données de l'ONU.
- *religions_list* traduit en français le nom en anglais des religions pratiquées dans le monde tel qu'il apparaît dans les bases de données de l'ONU. 
- *languages_list* traduit en français le nom en anglais des langues pratiquées dans le monde tel qu'il apparaît dans les bases de données de l'ONU (merci à Ninon pour ce travail).
- *currencies_list* établit une correspondance entre le nom des monnaies utilisées dans le monde (en français) et les codes ISO en deux lettres ou trois lettres utilisés pour les désigner. 
- *cpf2008_liste_n4.csv* provient du [site de l'INSEE](http://www.insee.fr/fr/methodes/default.asp?page=nomenclatures/cpf2008/liste_n4.htm). Nous avons converti ce fichier Excel au format .csv, puis nous avons supprimé ses lignes de titre et d'en-tête.
- les *table(s)-correspondance_nomenclature-(annee).csv* sont fournies en même temps que les fichiers des douanes. Nous les avons téléchargées une bonne fois pour toute, puisqu'elles ne varient qu'à la marge d'une année l'autre.

### Scripts
Les fichiers portant le préfixe "sql_" créent et paramètrent des bases de données ; ceux intitulés "mo_" (pour *"model"*), alimentent ces dernières ; ceux dont le nom commence par "dl_" téléchargent (*"download"*) des données ; les fichiers intitulés "graph_" produisent des graphiques susceptibles d'être utilisés dans nos "vues" ; les scripts débutant par "vw_" créent des "vues" (*"views"*), i.e. les fichiers construits à partir de nos données récupérées ; les fichiers commençant par les lettres "ul_" téléversent ("upload") nos vues sur un serveur internet.
- *transigo.py* lance l'application elle-même, i.e. tous les scripts, dans le bon ordre
- *functions.py* accueille les fonctions qui nous sont utiles dans plusieurs scripts différents
- *cleaning.py* nettoie les répertoires de l'application avant le déclenchement des premières opérations, en déplaçant les fichiers de logs dans le répertoire "old_datas" et en supprimant les versions de ceux-ci, comme de la base de données, trop anciennes pour nous être utiles.
- *checking.py* vérifie la présence des modules Python nécessaire au fonctionnement de l'application. En cas d'absence, un message d'alerte s'affiche dans le fichier de logs. 
- *sql_general.py* crée et initialise la base *general.db*, après avoir éventuellement déplacé la base existante dans le répertoire "old_datas".
- *sql_nomenclatures.py* crée et initialise la base *nomenclatures.db*, après avoir éventuellement déplacé la base existante dans le répertoire "old_datas".
- *sql_douane.py* crée et initialise la base *douane.db*, après avoir éventuellement déplacé la base existante dans le répertoire "old_datas".
- *dl_DGFIP* récupère le dernier fichier contenant les taux de chancellerie (i.e. les taux de changes officiels de toutes les monnaies par rapport à l'euro) produit deux fois par mois par la direction générale des Finances publiques du ministère français de l'Economie et des Finances.
- *dl_Kiosque.py* télécharge les fiches résumant les importations et les exportations de la France vers ou depuis chaque pays, sur le site ad hoc du ministère français de l'Economie et des Finances ("le Kiosque de Bercy").  
- *dl_WITS-API.py* télécharge certaines des données produites par le *World Integrated Trade Solutions*, le programme de la Banque mondiale qui collecte et diffuse des données sur le commerce international (5 principaux clients, 5 principaux fournisseurs, parts de marché de la France à l'importation et à l'exportation).
- *dl_PNUD.py* télécharge sur notre disque les annexes statistiques du dernier rapport sur le développement humain du Programme des Nations Unies pour le développement (PNUD).
- *dl_FMI_py* télécharge sur notre disque la dernière version du *World Economic Outlook* du Fonds monétaire international, au format Excel.
- *dl_douane.py* télécharge tous les fichiers de données publiés par la Direction générale des douanes françaises, recensant toutes les déclarations réalisées par les heureux contribuables. Oui, c'est (du) lourd. Ils sont mis à jour tous les mois.
- *dl_ILO_unemployment.py* télécharge sur notre disque dur la base donnée de l'Organisation internationale du Travail sur le chômage dans le monde.
- *dl_UNSD_cities* télécharge sur notre disque dur une base de données des Nations unies sur les villes du monde et leur population.
- *dl_UNSD_languages* télécharge sur notre disque dur une base de données des Nations unies sur les langues parlées dans le monde.
- *dl_UNSD_religions* télécharge sur notre disque dur une base de données des Nations unies sur les religions pratiquées dans le monde.
- *dl_Wikipedia* télécharge sur notre disque dur la dernière sauvegarde (dump) des sites francophones Wikimedia, dont la version française de Wikipedia.
- *mo_FMI.py* explore le dernier WEO pour en tirer les informations qui nous intéressent et les stocker dans notre base de données, avant de supprimer cette version Excel du WEO de notre disque.
- *mo_DGFIP.py* parse le fichier téléchargé avec *dl_DGFIP.py* pour insérer dans notre base de données les derniers taux de chancellerie de toutes les monnaies.
- *mo_Kiosque.py* traite les fiches résumant les importations et les exportations téléchargées grâce à *dl_Kiosque.py* afin d'en tirer les informations utiles à notre application.
- *mo_PNUD.py* explore les annexes statistiques du dernier rapport sur le développement humain pour en tirer les informations qui nous intéressent.
- *mo_WITS-API.py* ouvre une par une les fiches pays du *World Integrated Trade Solutions* pour en extraire les 5 principaux clients et les 5 principaux fournisseurs de chaque pays au monde, ainsi que la totalité de leurs importations et exportations, et les parts de marché de la France, avant de les insérer dans notre base de données.
- *mo_DoingBusiness.py* parse directement le site internet de ce programme de la Banque mondiale pour en tirer le classement des pays selon les critères de celui-ci, et l'insérer dans notre base de données.
- *mo_BdF.py* parse le site internet de la Banque de France pour en sortir les investissements étrangers en France, et français à l'étranger, avant de les insérer dans notre base de données.
- *mo_douane.py* (en travaux) traite ces fichiers de données un par un, pour insérer les informations pertinentes dans notre base de données, avant de supprimer leur source.
- *mo_BM.py* exploite l'API de la Banque mondiale pour en retirer automatiquement les informations qui nous intéressent et les insérer dans notre base de données.
- *mo_Wikipedia* (en travaux) parse l'encyclopédie collaborative pour en tirer les informations dont nous avons besoin alors qu'elles ne sont pas disponibles sur des sites publics.
- *mo_UNSD_cities* exploite la base de données téléchargées avec *dl_UNSD_cities* pour en retirer les informations qui nous intéressent et les stocker dans notre base de données.
- *mo_UNSD_languages* exploite la base de données téléchargées avec *dl_UNSD_languages* pour en retirer les informations qui nous intéressent et les stocker dans notre base de données.
- *mo_UNSD_religions* exploite la base de données téléchargées avec *dl_UNSD_religions* pour en retirer les informations qui nous intéressent et les stocker dans notre base de données.
- *mo_ILO_unemployment.py* exploite la base de données téléchargées avec *dl_ILO_unemployment.py* pour en retirer les informations qui nous intéressent et les stocker dans notre base de données.
- *vw_test* (en travaux) produit un fichier .txt à peine formaté regroupant toutes les informations disponibles sur chaque pays.
- *vw_tableaux* (en travaux) produit un fichier au format Excel présentant sous la forme de tableaux les principales informations stockées dans notre base de données.
- *vw_fiches_signalétiques* (en travaux) produit des fiches signalétiques pour chaque pays, présentant les informations essentielles les concernant, sans fioriture, sur le recto d'une page A4, au format Microsoft Word.
- *graph_echanges_commerciaux.py* (en travaux) produit des graphiques sur les échanges commerciaux entre chaque pays du monde.
- *ul_FTP.py* téléverse le contenu de notre dossier output sur le serveur FTP dont l'adresse et les autorisations de connexion sont stockées dans le fichier *config.txt*, lui-même situé dans le dossier *config*.