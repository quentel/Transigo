#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                         mo_ILO_unemployment.py                             #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_general.py) les données qui
# nous intéressent depuis les dumps du système de statistiques des Nations Unies,
# que nous avons téléchargés avec dl_ILO_unemployment.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, os
from functions import log, deLibelleENversAlpha2UNSD, deVilleENversVilleFR, listeVillesEN

log("")
log("==========================================")
log("Lancement du script mo_ILO_unemployment.py")
log("==========================================")

# 2. SCRIPT

compteur = 0

# On a un seul fichier dans le dossier, mais son nom varie
path_ILO  = "datas/ilo/unemployment/"

liste_fichiers_ILO = os.listdir(path_ILO)

for fichier in liste_fichiers_ILO:
	
	log("Traitement du fichier %s." % fichier)		
	# On ouvre le fichier	
	ILO_path = path_ILO+fichier
	mon_fichier_ILO = open(ILO_path, "rb")
	reader_ILO = csv.reader(mon_fichier_ILO, delimiter=';')
	
	liste = list()	
	compteur = 0
	# Et on le lit ligne à ligne
	for row in reader_ILO:
		try:		
			if row[2] == "Rates, total":
				country_name = row[0]
				year = row[1]
				rate = row[7]
				country_code = deLibelleENversAlpha2UNSD(country_name)
				rate = float(rate)
				year = int(year)
				# Voyons si nous avons déjà un taux de chômage de l'OIT pour ce pays
				conn = sqlite3.connect("datas/bases/general.db")
 				cur = conn.cursor()						
				cur.execute("SELECT nu_years from nations_unemployment where nu_ISO_3166_1_alpha_2 = ? and nu_sources = ?", (country_code, unicode("OIT")))		
				presence = len(cur.fetchall())
				if presence == 0:
					# Si n'avons pas de taux de chômage OIT pour ce pays dans la base de données, on l'insère
					conn.execute("INSERT INTO nations_unemployment (nu_values, nu_sources, nu_years, nu_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(rate, unicode("OIT"),year, country_code))						
					conn.commit()
					compteur = compteur+1
				else:
					# Si nous avons un taux de chômage OIT pour ce pays, voyons quelle est la fraicheur de ses informations				
					for row in cur:
						old_year = row[0]
						old_year = int(old_year)
						if year > old_year:
							conn.execute("UPDATE nations_unemployment set nu_values = ?, nu_years = ? where nu_ISO_3166_1_alpha_2 = ? and nu_sources = ?", (rate, year, country_code, unicode("OIT"))) 
							conn.commit()				

		except:
			pass
		
	mon_fichier_ILO.close()
		
	# Et on supprime le fichier traité
	path_to_rm = path_ILO+fichier
	os.remove(path_to_rm)
	
log("%s données de l'OIT sur le chômage entrées dans notre base" % compteur)