#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                               mo_PNUD.py                                   #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script exploite la dernière annexe statistique au rapport annuel du PNUD, 
# sur le développement humain, que nous avons téléchargée avec dl_PNUD.py.
# Il doit être modifié tous les ans : le PNUD ne publie en effet ces données que
# via un fichier Excel, dont la structure et la configuration sont modifiées à chaque
# édition du rapport.

## ATTENTION : LA COREE DU NORD APPARAIT EN XX APRES deLibelleENversAlpha2

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, datetime, xlrd, os
from functions import log, listePays, deLibelleENversAlpha2

log("")
log("==============================")
log("Lancement du script mo_PNUD.py")
log("==============================")

# 1. SCRIPT

# Ouvrir le classeur Excel ad hoc
UNDP_file = "datas/pnud/last_PNUD_stats.xls"
workbook = xlrd.open_workbook(UNDP_file)

# 1.1. Récupérer la valeur et le rang de l'index de développement humain (IDH)

# Ouvrir la bonne feuille du classeur 
worksheet_IDH = workbook.sheet_by_name("Table 1")

# Récupérer l'année de mise à jour des données
year = worksheet_IDH.cell(4, 2).value
# Nous ne voulons pas d'une décimale sur l'année
year = int(year)

# Nous commençons à récupérer les données à la septième rangée (donc à l'index 6)
row_index = 6

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin du classement IDH, i.e. à
# la rubrique "Other countries or territories":
while worksheet_IDH.cell(row_index, 1).value != "OTHER COUNTRIES OR TERRITORIES":
	rank = worksheet_IDH.cell(row_index, 0).value
	country = worksheet_IDH.cell(row_index, 1).value
	value = worksheet_IDH.cell(row_index, 2).value
	try:
		rank = int(rank)
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_hdi (nh_values, nh_ranks, nh_years, nh_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value),unicode(rank),unicode(year),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_hdi :" % e)
	except:
		# Si le rang ne peut pas être converti en integer, nous sommes sur un sous-titre
		pass
	row_index = row_index+1
log("%s rangs (et autant de valeurs) d'IDH entrés dans la base de données" % counter)


# 1.2. Récupérer le taux de croissance démographique

# Ouvrir la bonne feuille du classeur 
worksheet_Pop_Growth = workbook.sheet_by_name("Table 7")

# Nous ne nous intéressons qu'aux dernières données disponibles
years = worksheet_Pop_Growth.cell(6, 9).value

# Nous commençons à récupérer les données à la huitième rangée (donc à l'index 7)
row_index = 8

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_Pop_Growth.cell(row_index, 1).value != "Human development groups":
	value = worksheet_Pop_Growth.cell(row_index, 9).value
	country = worksheet_Pop_Growth.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_population_growths (npg_values, npg_sources, npg_years, npg_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(years),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_population_growths :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s taux de croissance de la population entrés dans la base de données" % counter)

# 1.3. Récupérer l'espérance de vie

# Ouvrir la bonne feuille du classeur 
worksheet_Life_expectancy = workbook.sheet_by_name("Table 1")

# Nous ne nous intéressons qu'aux dernières données disponibles
year = worksheet_Life_expectancy.cell(4, 4).value
year = int(year)

# Nous commençons à récupérer les données à la septième rangée (donc à l'index 6)
row_index = 6

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_Life_expectancy.cell(row_index, 1).value != "Human development groups":
	value = worksheet_Life_expectancy.cell(row_index, 4).value
	country = worksheet_Life_expectancy.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_life_expectancies (nle_values, nle_sources, nle_years, nle_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(year),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_life_expectancies :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s espérances de vies entrées dans la base de données" % counter)

# 1.4. Récupérer le taux d'alphabétisation

# Ouvrir la bonne feuille du classeur 
worksheet_Literacy = workbook.sheet_by_name("Table 9")

# Nous ne nous intéressons qu'aux dernières données disponibles
years = worksheet_Literacy.cell(5, 2).value

# Nous commençons à récupérer les données à la huitième rangée (donc à l'index 7)
row_index = 7

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_Literacy.cell(row_index, 1).value != "Human development groups":
	value = worksheet_Literacy.cell(row_index, 2).value
	country = worksheet_Literacy.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_literacy_rates (nlr_values, nlr_sources, nlr_years, nlr_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(years),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_literacy_rates :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s taux d'alphabétisation saisis dans la base de données" % counter)

# 1.5. Récupérer le coefficient de Gini

# Ouvrir la bonne feuille du classeur 
worksheet_Gini = workbook.sheet_by_name("Table 3")

# Nous ne nous intéressons qu'aux dernières données disponibles
years = worksheet_Gini.cell(4, 28).value

# Nous commençons à récupérer les données à la septième rangée (donc à l'index 6)
row_index = 6

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_Gini.cell(row_index, 1).value != "Human development groups":
	value = worksheet_Gini.cell(row_index, 28).value
	country = worksheet_Gini.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_gini_coefficients (ngc_values, ngc_sources, ngc_years, ngc_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(years),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_gini_coefficients :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s coefficients de Gini saisis dans la base de données" % counter)

# 1.6. Récupérer le taux d'homicide

# Ouvrir la bonne feuille du classeur 
worksheet_Homicides = workbook.sheet_by_name("Table 12")

# Nous ne nous intéressons qu'aux dernières données disponibles
years = worksheet_Homicides.cell(5, 14).value

# Nous commençons à récupérer les données à la huitième rangée (donc à l'index 7)
row_index = 7

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_Homicides.cell(row_index, 1).value != "Human development groups":
	value = worksheet_Homicides.cell(row_index, 14).value
	country = worksheet_Homicides.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_homicides_rates (nhr_values, nhr_sources, nhr_years, nhr_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(years),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_homicides_rates :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass	
	row_index = row_index+1
log("%s taux d'homicide saisis dans la base de données" % counter)

# 1.7. Récupérer le % d'internautes dans la population totale

# Ouvrir la bonne feuille du classeur 
worksheet_Internet_users = workbook.sheet_by_name("Table 13")

# Nous ne nous intéressons qu'aux dernières données disponibles
year = worksheet_Internet_users.cell(5, 20).value
year = int(year)

# Nous commençons à récupérer les données à la huitième rangée (donc à l'index 7)
row_index = 7

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_Internet_users.cell(row_index, 1).value != "Human development groups":
	value = worksheet_Internet_users.cell(row_index, 20).value
	country = worksheet_Internet_users.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_internet_users (niu_values, niu_sources, niu_years, niu_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(year),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_internet_users :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s taux d'utilisateurs d'Internet entrés dans la base de données" % counter)

# 1.8. Récupérer le nombre de touristes

# Ouvrir la bonne feuille du classeur 
worksheet_Tourists = workbook.sheet_by_name("Table 13")

# Nous ne nous intéressons qu'aux dernières données disponibles
year = worksheet_Tourists.cell(5, 18).value
year = int(year)

# Nous commençons à récupérer les données à la huitième rangée (donc à l'index 7)
row_index = 7

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_Tourists.cell(row_index, 1).value != "Human development groups":
	value = worksheet_Tourists.cell(row_index, 18).value
	# Ce nombre est enregistré en milliers
	try:
		value = int(value)
		value = value*1000
	except:
		pass
	country = worksheet_Tourists.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)

		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_international_inbound_tourists (nit_values, nit_sources, nit_years, nit_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(year),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_international_inbound_tourists :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s nombres de touristes entrés dans la base de données" % counter)

# 1.9. Récupérer le nombre de tonnes de CO2 produites par habitant

# Ouvrir la bonne feuille du classeur 
worksheet_CO2 = workbook.sheet_by_name("Dashboard 2")

# Nous ne nous intéressons qu'aux dernières données disponibles
year = worksheet_CO2.cell(5, 4).value
year = int(year)

# Nous commençons à récupérer les données à la huitième rangée (donc à l'index 7)
row_index = 7

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# la rubrique "Human development groups":
while worksheet_CO2.cell(row_index, 1).value != "Human development groups":
	value = worksheet_CO2.cell(row_index, 4).value
	country = worksheet_CO2.cell(row_index, 1).value
	try:
		country_code = deLibelleENversAlpha2(country)
		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_CO2_emissions_per_capita (nce_values, nce_sources, nce_years, nce_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD"), unicode(year),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_CO2_emissions_per_capita :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s nombres de tonnes de CO2 produites par habitant entrées dans la base de données" % counter)

# 1.10. Récupérer le taux de pauvreté multidimensionnel

# Ouvrir la bonne feuille du classeur 
worksheet_poverty = workbook.sheet_by_name("Table 6")

# Nous commençons à récupérer les données à la sixième rangée (donc à l'index 5)
row_index = 5

# Créons un compteur
counter = 0

# Nous lisons toute la feuille, jusqu'à arriver à la fin des données, i.e. à
# "Notes":
while worksheet_poverty.cell(row_index, 0).value != "Notes":
	year = worksheet_poverty.cell(row_index,1).value	
	value = worksheet_poverty.cell(row_index, 5).value
	country = worksheet_poverty.cell(row_index, 0).value
	try:
		# On supprime les caractères incompréhensibles dans les dates
		chars_to_remove = ['M','D','N','P']
		for char in chars_to_remove:
			if char in year:
				year = year.replace(char,'')
		country_code = deLibelleENversAlpha2(country)

		try:		
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_poverty (npov_values, npov_sources, npov_years, npov_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(value), unicode("PNUD - Indice multidimensionnel"), unicode(year),country_code))
			conn.commit()
			conn.close()
			counter = counter+1
		except Exception, e:
			log("***Impossible de saisir ce pays dans la table nations_poverty :" % e)
	except:
		# Si le nom du pays ne peut pas être converti, abandonnons
		pass
	row_index = row_index+1
log("%s indices multidimensionnel de pauvreté entrés dans la base de données" % counter)


#3. Suppression du fichier ainsi exploité
os.remove(UNDP_file)