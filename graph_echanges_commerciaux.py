#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                     	graph_echanges_commerciaux.py                        #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script dessine un histogramme résumant l'évolution de nos relations commerciales
# avec un pays, sur la base des données de la DGT

# 1. IMPORTATION DES MODULES NECESSAIRES

from __future__ import division
from matplotlib import pyplot as plt
from pylab import *
import numpy as np, os
from functions import log, listePays, deAlpha2versLibelleFR, getAllYears, getDataValue, ordreDeGrandeur

log("")
log("=================================================")
log("Lancement du script graph_echanges_commerciaux.py")
log("=================================================")

# 2. SCRIPT

for pays in listePays():
	
	# 2.1 Récupération des données

	# Nom du pays
	nom_pays = deAlpha2versLibelleFR(pays)
	
	# Exportations
	c_exports = list()
	years_exp = getAllYears("ke","kiosque_exports",pays)
	for year in years_exp:
		value = getDataValue("ke","kiosque_exports",pays,year)
		c_exports.append(value)
	
	# Importations
	c_imports = list()
	years_imp = getAllYears("ki","kiosque_imports",pays)
	for year in years_imp:
		value = getDataValue("ki","kiosque_imports",pays, year)
		c_imports.append(value)

	if years_imp != years_exp:
		print "Les données disponibles pour les importations et les exportations ne correspondent pas aux mêmes années !"
	else:
		x = years_imp
	
	# Ordres de grandeur	
	c_imports.sort(reverse = True)
	c_exports.sort(reverse = True)	
	
	try:
		if c_imports[0] > c_exports[0]:
			odg = list()			
			odg = ordreDeGrandeur(c_imports[0])
			unite = odg[0]
			multiplicateur = odg[1]
		else:
			odg = list()
			odg = ordreDeGrandeur(c_exports[0])
			unite = odg[0]
			multiplicateur = odg[1]
	except:
		pass	
		
	# 2.2 Traçage du graphique
	
	Y1 = [resultat/1000000 for resultat in c_imports]
	Y2 = [resultat/1000000 for resultat in c_exports]
	print Y1
	# Nous avons besoin d'afficher les importations en négatif pour qu'elles apparaissent en miroir des exportations sur le graphique
	Y2 = [-nombre for nombre in Y2]

	x_pos = np.arange(len(x))

	plt.bar(x_pos, Y1, facecolor='#9999ff', edgecolor='#9999ff', align="center")
	plt.bar(x_pos, Y2, facecolor='#ff9999', edgecolor='#ff9999', align="center")

	graph_title = "Echanges commerciaux France-"+nom_pays+" (DGT)"
	plt.title(graph_title)
	plt.xlabel(u'Années')
	label = "Montants ("+unite+" d'euros)"
	label = unicode(label)
	plt.ylabel(label)
	plt.xticks(x_pos, x)
	plt.grid(False)

	# 2.3 Enregistrement du fichier
	graph_path = "output/graphs/"

	# Si le chemin n'existe pas, créons-le
	graph_dir = os.path.dirname(graph_path)
	if not os.path.exists(graph_path):
		log("Création des répertoires destinés à accueillir nos graphiques : output/graphs/")
		os.makedirs(graph_dir)

	# Fixation du nom du fichier
	file_name = "xchange_FR_"+pays+"_DGT.png"
	
	# Chemin de sauvegarde complet
	graph_file = os.path.join(graph_dir, file_name)
	savefig(graph_file)