#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                          vw_tableaux_wits.py                               #
#                                                                            #
#                        Christophe Quentel, 2017                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée un fichier Excel présentant sous forme de tableaux les données sur les
# parts de marché tirées de notre base de données WITS

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, math, locale
from xlwt import Workbook
from functions import log, listePays, deAlpha2versLibelleFR

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

log("")
log("=======================================")
log("Lancement du script vw_tableaux_wits.py")
log("=======================================")

# 2. FONCTIONS UTILES

# 2.1 Recherche dans une table dédiée à la donnée

def connectTable(table,prefixe,reporter,partner,year):
	conn = sqlite3.connect("datas/bases/wits.db")
	cur = conn.cursor()						
	cur.execute("SELECT "+prefixe+"_values from "+table+" where "+prefixe+"_cn_reporter = ? AND "+prefixe+"_cn_partner = ? AND "+prefixe+"_years = ?", (reporter,partner,year))
	return cur

# 2.2 Liste des années pour lesquelles nous disposons d'une donnée

def allYearsList(table, prefixe):
	my_years_init = []
	conn = sqlite3.connect("datas/bases/wits.db")
	cur = conn.cursor()						
	cur.execute("SELECT "+prefixe+"_years from "+table+"")
	for row in cur:
		# Ajout de chaque année trouvée à la liste	
		my_years_init.append(row[0])
	# Suppression des doublons dans la liste
	my_years_set = set(my_years_init)
	my_years = []
	for y in my_years_set:
		my_years.append(y)
	my_years.sort()
	return my_years


path = "output/tableaux/wits/"

# Si le répertoire n'existe pas, créons-le
tableau_dir = os.path.dirname(path)
if not os.path.exists(tableau_dir):
	log("Création du répertoire destiné à accueillir notre tableau : output/tableaux/wits/")
	os.makedirs(tableau_dir)

# Nombre de pays dans le monde
nb_pays = len(listePays())

# Liste des pays du monde
liste = listePays()

# 3. Importations

# Création du fichier
book_import = Workbook()

# Recherche des années pour lesquelles nous avons au moins une donnée
years_import = allYearsList("import_shares", "is")

# On crée une feuille par année

for year_import in years_import :
 	year_import_str = str(year_import)
	feuille = book_import.add_sheet(year_import_str)
	# Création des en-têtes de colonnes
	feuille.write(0,0, u'Pays concernés')
	feuille.write(1,0,u'Code ISO')
	feuille.write(1,1,u'Nom du pays')
	feuille.write(0,2, u'Pays partenaires')
	# On crée une colonne par pays
	c = 0
	while c < nb_pays:
		feuille.write(1,c+2,liste[c])
		c = c+1
	# On crée une ligne par pays
	l = 0
	while l < nb_pays:
		feuille.write(l+2,0,liste[l])
		lib_pays = deAlpha2versLibelleFR(liste[l])
		feuille.write(l+2,1,lib_pays)
		d=0
		while d < nb_pays:
			data_import = connectTable("import_shares", "is",liste[l],liste[d],year_import)
			for row in data_import:
				ma_donnee_import = row[0]
				print liste[l],liste[d], year_import,ma_donnee_import
				try:
					feuille.write(l+2,d+2,ma_donnee_import)
				except:
					pass
			d = d+1
		l = l+1
	
	# Largeur des colonnes
	feuille.col(1).width = 10500 # Largeur de la colonne "Nom du pays"

book_import.save("output/tableaux/wits/tableaux_import.xls")

# 4. Exportations

# Création du fichier
book_export = Workbook()

# Recherche des années pour lesquelles nous avons au moins une donnée
years_export = allYearsList("export_shares", "es")

# On crée une feuille par année

for year_export in years_export :
	year_export_str = str(year_export)
	feuille2 = book_export.add_sheet(year_export_str)
	# Création des en-têtes de colonnes
	feuille2.write(0,0, u'Pays concernés')
	feuille2.write(1,0,u'Code ISO')
	feuille2.write(1,1,u'Nom du pays')
	feuille2.write(0,2, u'Pays partenaires')
	# On crée une colonne par pays
	c2 = 0
	while c2 < nb_pays:
		feuille2.write(1,c2+2,liste[c2])
		c2 = c2+1
	# On crée une ligne par pays
	l2 = 0
	while l2 < nb_pays:
		feuille2.write(l2+2,0,liste[l2])
		lib_pays = deAlpha2versLibelleFR(liste[l2])
		feuille2.write(l2+2,1,lib_pays)
		d2=0
		while d2 < nb_pays:
			data_export = connectTable("export_shares", "es",liste[l2],liste[d2],year_export)
			for row in data_export:
				ma_donnee_export = row[0]
				print liste[l2],liste[d2], year_export,ma_donnee_export
				try:
					feuille2.write(l2+2,d2+2,ma_donnee_export)
				except:
					pass
			d2 = d2+1
		l2 = l2+1
		
	# Largeur des colonnes
	feuille2.col(1).width = 10500 # Largeur de la colonne "Nom du pays"
	

book_export.save("output/tableaux/wits/tableaux_export.xls")