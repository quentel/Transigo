
##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                      mo_douanes_last_12_months.r                           #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script reprend les fichiers des douanes préparés avec mo_douanes_cleaning.r
# et mo_douanes_datamiing.r pour en sortir les totaux d'importation et d'exportation
# sur les douze derniers mois, et les douze mois précédents

# Récupérons le chemin de notre fichier, passé en argument au moment du lancement du script par Python
myArgs <- commandArgs(trailingOnly = FALSE)
myPath <- paste(myArgs[7], "/datas/douanes/", sep = "", collapse = NULL)
# Fixons un nouveau répertoire de travail
setwd(myPath)

# 1. EXPORTATIONS

# Récupérons la liste des fichiers .csv que nous avons modifiés avec mo_douanes_datamining.r
files_export_list <- list.files(pattern="exportations_.*\\.csv", full.names = FALSE)
# On trie nos fichiers en ordre inverse
files_export_list <- sort(files_export_list, decreasing=TRUE)
# On ne prend que les 3 premiers fichiers (non, je ne pouvais pas passer seulement par "tail" : il vaut quand même mieux trier d'abord, dans un sens ou dans l'autre)
files_export_list <-head(files_export_list,3)
# Initialisation de la dataframe qui contiendra les exportations
dataset_exportations <- data.frame()
# On ouvre nos 3 derniers fichiers un par un
for (file_export in files_export_list) 
{
  minidataset_exportations <- data.frame()
  minidataset_exportations <- read.csv(file_export, header=TRUE, sep=",")
  # J'ajoute ma nouvelle dataset à ma dataset globale
  dataset_exportations <- rbind(minidataset_exportations,dataset_exportations)
}
# On peut virer la colonne qui ne sert à rien
colnames(dataset_exportations) <- c("Inutile","partner","nc8","date","year","value")
dataset_exportations$Inutile <- NULL
# On trouve la date maximum
ordered_dates <- sort(dataset_exportations$date)
max_date_exportations <- tail(ordered_dates,1)
# On convertit en POSIXlt pour pouvoir manipuler les années
max_date_exportations <- as.POSIXlt(as.Date(max_date_exportations))
one_year_ago_exportations <- max_date_exportations
two_years_ago_exportations <- max_date_exportations
one_year_ago_exportations$year <- max_date_exportations$year-1
one_year_ago_exportations <- as.Date( as.character(one_year_ago_exportations), "%Y-%m-%d")
two_years_ago_exportations$year <- max_date_exportations$year-2
two_years_ago_exportations <- as.Date(two_years_ago_exportations, "%Y-%m-%d")
max_date_exportations <- as.Date( as.character(max_date_exportations), "%Y-%m-%d")

last_12_months_export <- dataset_exportations[as.Date(dataset_exportations$date, "%Y-%m-%d") > one_year_ago_exportations & as.Date(dataset_exportations$date, "%Y-%m-%d") <= max_date_exportations,]
# On peut maintenant compiler les données par partenaire et par an
last_12_months_export_dataset1 <- aggregate(last_12_months_export$value, list(partner=last_12_months_export$partner, year=last_12_months_export$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# last_12_months_export_dataset2 <- aggregate(last_12_months_export$value, list(partner=last_12_months_export$partner, nc8=last_12_months_export$nc8, year=last_12_months_export$year), FUN=sum)
write.csv(last_12_months_export_dataset1, "sub_exportations_last_12_months_by_partners.csv")
# write.csv(last_12_months_export_dataset2, "sub_exportations_last_12_months_by_partners_and_products.csv")
prev_12_months_export <- dataset_exportations[as.Date(dataset_exportations$date, "%Y-%m-%d") > two_years_ago_exportations & as.Date(dataset_exportations$date, "%Y-%m-%d") <= one_year_ago_exportations,]
# On peut maintenant compiler les données par partenaire et par an
prev_12_months_export_dataset1 <- aggregate(prev_12_months_export$value, list(partner=prev_12_months_export$partner, year=prev_12_months_export$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# prev_12_months_export_dataset2 <- aggregate(prev_12_months_export$value, list(partner=prev_12_months_export$partner, nc8=prev_12_months_export$nc8, year=prev_12_months_export$year), FUN=sum)
write.csv(prev_12_months_export_dataset1, "sub_exportations_prev_12_months_by_partners.csv")
# write.csv(prev_12_months_export_dataset2, "sub_exportations_prev_12_months_by_partners_and_products.csv")

# 2. IMPORTATIONS

# Récupérons la liste des fichiers .csv que nous avons modifiés avec mo_douanes_datamining.r
files_import_list <- list.files(pattern="importations_.*\\.csv", full.names = FALSE)
# On trie nos fichiers en ordre inverse
files_import_list <- sort(files_import_list, decreasing=TRUE)
# On ne prend que les 3 premiers fichiers (non, je ne pouvais pas passer seulement par "tail" : il vaut quand même mieux trier d'abord, dans un sens ou dans l'autre)
files_import_list <-head(files_import_list,3)
# Initialisation de la dataframe qui contiendra les exportations
dataset_importations <- data.frame()
# On ouvre nos 3 derniers fichiers un par un
for (file_import in files_import_list) 
{
  minidataset_importations <- data.frame()
  minidataset_importations <- read.csv(file_import, header=TRUE, sep=",")
  # J'ajoute ma nouvelle dataset à ma dataset globale
  dataset_importations <- rbind(minidataset_importations,dataset_importations)
}
# On peut virer la colonne qui ne sert à rien
colnames(dataset_importations) <- c("Inutile","partner","nc8","date","year","value")
dataset_importations$Inutile <- NULL
# On trouve la date maximum
ordered_dates_imp <- sort(dataset_importations$date)
max_date_importations <- tail(ordered_dates_imp,1)
# On convertit en POSIXlt pour pouvoir manipuler les années
max_date_importations <- as.POSIXlt(as.Date(max_date_importations))
one_year_ago_importations <- max_date_importations
two_years_ago_importations <- max_date_importations
one_year_ago_importations$year <- max_date_importations$year-1
one_year_ago_importations <- as.Date( as.character(one_year_ago_importations), "%Y-%m-%d")
two_years_ago_importations$year <- max_date_importations$year-2
two_years_ago_importations <- as.Date(two_years_ago_importations, "%Y-%m-%d")
max_date_importations <- as.Date( as.character(max_date_importations), "%Y-%m-%d")

last_12_months_import <- dataset_importations[as.Date(dataset_importations$date, "%Y-%m-%d") > one_year_ago_importations & as.Date(dataset_importations$date, "%Y-%m-%d") <= max_date_importations,]
# On peut maintenant compiler les données par partenaire et par an
last_12_months_import_dataset1 <- aggregate(last_12_months_import$value, list(partner=last_12_months_import$partner, year=last_12_months_import$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# last_12_months_import_dataset2 <- aggregate(last_12_months_import$value, list(partner=last_12_months_import$partner, nc8=last_12_months_import$nc8, year=last_12_months_import$year), FUN=sum)
write.csv(last_12_months_import_dataset1, "sub_importations_last_12_months_by_partners.csv")
# write.csv(last_12_months_import_dataset2, "sub_importations_last_12_months_by_partners_and_products.csv")
prev_12_months_import <- dataset_importations[as.Date(dataset_importations$date, "%Y-%m-%d") > two_years_ago_importations & as.Date(dataset_importations$date, "%Y-%m-%d") <= one_year_ago_importations,]
# On peut maintenant compiler les données par partenaire et par an
prev_12_months_import_dataset1 <- aggregate(prev_12_months_import$value, list(partner=prev_12_months_import$partner, year=prev_12_months_import$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# prev_12_months_import_dataset2 <- aggregate(prev_12_months_import$value, list(partner=prev_12_months_import$partner, nc8=prev_12_months_import$nc8, year=prev_12_months_import$year), FUN=sum)
write.csv(prev_12_months_import_dataset1, "sub_importations_prev_12_months_by_partners.csv")
# write.csv(prev_12_months_export_dataset2, "sub_importations_prev_12_months_by_partners_and_products.csv")
