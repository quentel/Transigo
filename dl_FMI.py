#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                dl_FMI.py                                   #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge la dernière version du World Economic Outlook du FMI

# 1. IMPORTATION DES MODULES NECESSAIRES

import time, datetime, urllib2, os, sys
from functions import log

# Nous avons besoin de travailler, dans ce seul script, sur un format de dates américain...
import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

log("")
log("=============================")
log("Lancement du script dl_FMI.py")
log("=============================")

# Le WEO est publié deux fois par an - en avril, puis en septembre ou en octobre.
# L'URL du fichier à télécharger n'est pas fixe. Il est de la forme :
# http://www.imf.org/external/pubs/ft/weo/ANNEE/NUMERO_DU_SET_DE_DONNEES_SUR_L_ANNEE(01_ou_02)/weodata/WEOMOISENTROISLETTRESANNEEall.xls
# Nous n'allons pas le jouer subtilement : nous allons tester toutes les dernières URL possibles, par ordre chronologique inverse,
# jusqu'à ce que nous récupérions un fichier (en allant jusqu'à voir si un troisième set a été publié l'année concernée)...

#2. FONCTIONS

#2.1. Construction de l'URL
# Cette fonction retourne un URL susceptible de contenir le fichier à télécharger (dl_URL) en prenant en arguments un numéro 
#de "set" de données (data_set) et un objet datetime (dt_object)

def construct_URL(data_set,dt_object):
	# On transforme l'objet datetime (plus facile à manipuler) en objet time (plus facile à formater)
	t_object = dt_object.timetuple()
	# On récupère l'année en 4 chiffres
	year_URL = time.strftime('%Y',t_object)
	# On récupère le mois en trois lettres (avec initiale capitalisée) et l'année en 4 chiffres
	month_year_URL = time.strftime('%b%Y', t_object)
	# Sur cette base, on construit l'URL
	dl_URL = "http://www.imf.org/external/pubs/ft/weo/"+year_URL+"/"+data_set+"/weodata/WEO"+month_year_URL+"all.xls"
	return dl_URL
	
# 2.2 Tester l'URL
# Le FMI n'affiche pas d'erreur 404 sur son serveur. Nous devons donc vérifier si nous sommes à la bonne adresse en vérifiant
# si le mot "error" apparaît dans l'adresse réellement affichée par le serveur

def check_URL(URL_to_check):
	# On ouvre l'URL
	# Un vrai-faux header
	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko)',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
			'Accept-Encoding': 'none',
			'Accept-Language': 'en_US,en;q=0.8',
			'Connection': 'keep-alive'}
	try:
		req = urllib2.Request(URL_to_check, headers=hdr)
		handle = urllib2.urlopen(req)
		# On regarde si le mot "error" apparaît dans la réponse
		is_error = handle.geturl().find("error")
	except:
		is_error = 1
	return is_error

# 3. SCRIPT

# Nom et chemin du fichier que nous allons créer
dl_file = "datas/fmi/last_WEO.xls"

# Si le répertoire n'existe pas, créons-le
dl_dir = os.path.dirname(dl_file)
if not os.path.exists(dl_dir):
	log("Création du répertoire destiné à accueillir notre fichier : datas/fmi/")
	os.makedirs(dl_dir)

# Quel jour sommes-nous ?
aujourdhui = datetime.date.today()

# Commençons par vérifier si le fichier a été mis à jour ce mois-ci
date_a_tester = aujourdhui

# Créons un marqueur booléen
is_found = False

# Tant que le marqueur est faux, testons les URL, en reculant de quinzaine en quinzaine
while is_found == False:
	#Soyons prudents, et imaginons que le FMI ait publié, cette fois-ci, trois sets de données...
	les_sets_possibles = ("03","02","01")
	# Testons tous les sets possibles pour le mois actuel
	for set_a_tester in les_sets_possibles:
		URL_a_tester = construct_URL(set_a_tester,date_a_tester)
		log("Tentative de téléchargement sur l'URL :"+URL_a_tester)		
		erreur_ou_pas = check_URL(URL_a_tester)
		# Si on ne trouve pas "Error" dans l'adresse, c'est que c'est la bonne
		if erreur_ou_pas == -1:
			# Du coup, on peut télécharger le fichier
			try:
				with open(dl_file,'w') as f:
  					f.write(urllib2.urlopen(URL_a_tester).read())
  				f.close()
				log("Téléchargement du dernier World Economic Outlook terminé !")
			except:
				log("***Le téléchargement du dernier World Economic Outlook a échoué.")
				pass
			# On met également le marqueur à "vrai" pour interrompre la boucle While
			is_found = True
			# Et on sort avec élégance
			break
	# Si cette adresse n'est pas la bonne, on recule de 15 jours, et on recommence
	date_a_tester = date_a_tester - datetime.timedelta(days = 15)