#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                            dl_Wikipedia.py                                 #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge en format .xml tous les articles relatifs à des pays sur
# la version française de Wikipedia

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, urllib2
from functions import log, listeTerritoiresFR

log("")
log("===================================")
log("Lancement du script dl_Wikipedia.py")
log("===================================")

# 2. SCRIPT

base_source = "https://fr.wikipedia.org/wiki/Special:Export/"
base_destination = "datas/wikipedia/"

# Si le répertoire de destination n'existe pas, créons-le
dl_dir = os.path.dirname(base_destination)
if not os.path.exists(base_destination):
	log("Création du répertoire destiné à accueillir nos fichiers : datas/wikipedia/")
	os.makedirs(dl_dir)

# Téléchargement de tous les fichiers pays, un par un

for pays in listeTerritoiresFR():
	source = base_source+pays
	destination = base_destination+pays+".xml"
	try:
		with open(destination,'w') as f:
			f.write(urllib2.urlopen(source.encode("utf-8")).read())
			f.close()
	except:
		log("=== Erreur :Le téléchargement de la page Wikipedia pour %s a échoué" % pays)