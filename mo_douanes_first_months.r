
##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                        mo_douanes_first_months.r                           #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script reprend les fichiers des douanes préparés avec mo_douanes_cleaning.r
# et mo_douanes_datamiing.r pour en sortir les totaux d'importation et d'exportation
# sur les premiers mois de l'année en cours, puis sur la même période de l'année précédente

# Récupérons le chemin de notre fichier, passé en argument au moment du lancement du script par Python
myArgs <- commandArgs(trailingOnly = FALSE)
myPath <- paste(myArgs[7], "/datas/douanes/", sep = "", collapse = NULL)
# Fixons un nouveau répertoire de travail
setwd(myPath)

# 1. EXPORTATIONS

# Récupérons la liste des fichiers .csv que nous avons modifiés avec mo_douanes_datamining.r
files_export_list <- list.files(pattern="^exportations_.*\\.csv", full.names = FALSE)
# On trie nos fichiers en ordre inverse
files_export_list <- sort(files_export_list, decreasing=TRUE)
# On ne prend que les 2 premiers fichiers (non, je ne pouvais pas passer seulement par "tail" : il vaut quand même mieux trier d'abord, dans un sens ou dans l'autre)
files_export_list <-head(files_export_list,2)
# Initialisation de la dataframe qui contiendra les exportations
dataset_exportations <- data.frame()
# On ouvre nos 3 derniers fichiers un par un
for (file_export in files_export_list) 
{
  minidataset_exportations <- data.frame()
  minidataset_exportations <- read.csv(file_export, header=TRUE, sep=",")
  # J'ajoute ma nouvelle dataset à ma dataset globale
  dataset_exportations <- rbind(minidataset_exportations,dataset_exportations)
}
# On peut virer la colonne qui ne sert à rien
colnames(dataset_exportations) <- c("Inutile","partner","nc8","date","year","value")
dataset_exportations$Inutile <- NULL
# On trouve la date maximum
ordered_dates <- sort(dataset_exportations$date)
max_date_exportations <- tail(ordered_dates,1)
# On convertit en POSIXlt pour pouvoir manipuler les années
max_date_exportations <- as.POSIXlt(as.Date(max_date_exportations))
# On sort l'année (en ajoutant au chiffre POSIXlt l'année de départ, soit 1900)
current_year <- 1900+max_date_exportations$year
# On sort l'année dernière
last_year = current_year-1
# On sort la date à laquelle devront s'arrêter les données de l'année dernière
one_year_ago_exportations_year <- 1900+max_date_exportations$year-1
# Attention, en POSIXlt, l'index des mois commence à zéro
one_year_ago_exportations_month <- max_date_exportations$mon+1
# Pour les jours, c'est simple
one_year_ago_exportations_day <- max_date_exportations$mday
# On crée ensuite les dates une à une
one_year_ago_exportations <-paste(one_year_ago_exportations_year,one_year_ago_exportations_month, one_year_ago_exportations_day, sep = "-", collapse = NULL)
one_year_ago_exportations <- as.Date(one_year_ago_exportations, "%Y-%m-%d")
beginning_of_this_year <-paste(current_year,"-01-01", sep = "", collapse = NULL)
beginning_of_this_year <- as.Date(beginning_of_this_year, "%Y-%m-%d")
beginning_of_last_year <-paste(last_year,"-01-01", sep = "", collapse = NULL)
beginning_of_last_year <- as.Date(beginning_of_last_year, "%Y-%m-%d")
max_date_exportations <- as.Date( as.character(max_date_exportations), "%Y-%m-%d")

nbmois <- one_year_ago_exportations_month

last_first_months_export <- dataset_exportations[as.Date(dataset_exportations$date, "%Y-%m-%d") >= beginning_of_this_year & as.Date(dataset_exportations$date, "%Y-%m-%d") <= max_date_exportations,]
# On peut maintenant compiler les données par partenaire et par an
last_first_months_export_dataset1 <- aggregate(last_first_months_export$value, list(partner=last_first_months_export$partner, year=last_first_months_export$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# last_first_months_export_dataset2 <- aggregate(last_first_months_export$value, list(partner=last_first_months_export$partner, nc8=last_first_months_export$nc8, year=last_first_months_export$year), FUN=sum)
file_name_1 = paste("sub_exportations_last_first_months_by_partners_",nbmois,"_mois.csv", sep = "", collapse = NULL)
write.csv(last_first_months_export_dataset1, file_name_1)
# write.csv(last_first_months_export_dataset2, "sub_exportations_last_first_months_by_partners_and_products.csv")
prev_first_months_export <- dataset_exportations[as.Date(dataset_exportations$date, "%Y-%m-%d") >= beginning_of_last_year & as.Date(dataset_exportations$date, "%Y-%m-%d") <= one_year_ago_exportations,]
# On peut maintenant compiler les données par partenaire et par an
prev_first_months_export_dataset1 <- aggregate(prev_first_months_export$value, list(partner=prev_first_months_export$partner, year=prev_first_months_export$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# prev_first_months_export_dataset2 <- aggregate(prev_first_months_export$value, list(partner=prev_first_months_export$partner, nc8=prev_first_months_export$nc8, year=prev_first_months_export$year), FUN=sum)
file_name_2 = paste("sub_exportations_prev_first_months_by_partners_",nbmois,"_mois.csv", sep = "", collapse = NULL)
write.csv(prev_first_months_export_dataset1, file_name_2)
#write.csv(prev_first_months_export_dataset2, "sub_exportations_prev_first_months_by_partners_and_products.csv")

# 2. IMPORTATIONS

# Récupérons la liste des fichiers .csv que nous avons modifiés avec mo_douanes_datamining.r
files_import_list <- list.files(pattern="^importations_.*\\.csv", full.names = FALSE)
# On trie nos fichiers en ordre inverse
files_import_list <- sort(files_import_list, decreasing=TRUE)
# On ne prend que les 2 premiers fichiers (non, je ne pouvais pas passer seulement par "tail" : il vaut quand même mieux trier d'abord, dans un sens ou dans l'autre)
files_import_list <-head(files_import_list,2)
# Initialisation de la dataframe qui contiendra les exportations
dataset_importations <- data.frame()
# On ouvre nos 3 derniers fichiers un par un
for (file_import in files_import_list) 
{
  minidataset_importations <- data.frame()
  minidataset_importations <- read.csv(file_import, header=TRUE, sep=",")
  # J'ajoute ma nouvelle dataset à ma dataset globale
  dataset_importations <- rbind(minidataset_importations,dataset_importations)
}
# On peut virer la colonne qui ne sert à rien
colnames(dataset_importations) <- c("Inutile","partner","nc8","date","year","value")
dataset_importations$Inutile <- NULL
# On trouve la date maximum
ordered_dates <- sort(dataset_importations$date)
max_date_importations <- tail(ordered_dates,1)
# On convertit en POSIXlt pour pouvoir manipuler les années
max_date_importations <- as.POSIXlt(as.Date(max_date_importations))
# On sort l'année (en ajoutant au chiffre POSIXlt l'année de départ, soit 1900)
current_year <- 1900+max_date_importations$year
# On sort l'année dernière
last_year = current_year-1
# On sort la date à laquelle devront s'arrêter les données de l'année dernière
one_year_ago_importations_year <- 1900+max_date_importations$year-1
# Attention, en POSIXlt, l'index des mois commence à zéro
one_year_ago_importations_month <- max_date_importations$mon+1
# Pour les jours, c'est simple
one_year_ago_importations_day <- max_date_importations$mday
# On crée ensuite les dates une à une
one_year_ago_importations <-paste(one_year_ago_importations_year,one_year_ago_importations_month, one_year_ago_importations_day, sep = "-", collapse = NULL)
one_year_ago_importations <- as.Date(one_year_ago_importations, "%Y-%m-%d")
beginning_of_this_year <-paste(current_year,"-01-01", sep = "", collapse = NULL)
beginning_of_this_year <- as.Date(beginning_of_this_year, "%Y-%m-%d")
beginning_of_last_year <-paste(last_year,"-01-01", sep = "", collapse = NULL)
beginning_of_last_year <- as.Date(beginning_of_last_year, "%Y-%m-%d")
max_date_importations <- as.Date( as.character(max_date_importations), "%Y-%m-%d")

nbmois2 <- one_year_ago_importations_month

last_first_months_import <- dataset_importations[as.Date(dataset_importations$date, "%Y-%m-%d") >= beginning_of_this_year & as.Date(dataset_importations$date, "%Y-%m-%d") <= max_date_importations,]
# On peut maintenant compiler les données par partenaire et par an
last_first_months_import_dataset1 <- aggregate(last_first_months_import$value, list(partner=last_first_months_import$partner, year=last_first_months_import$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# last_first_months_import_dataset2 <- aggregate(last_first_months_import$value, list(partner=last_first_months_import$partner, nc8=last_first_months_import$nc8, year=last_first_months_import$year), FUN=sum)
file_name_3 = paste("sub_importations_last_first_months_by_partners_",nbmois2,"_mois.csv", sep = "", collapse = NULL)
write.csv(last_first_months_import_dataset1,file_name_3)
# write.csv(last_first_months_import_dataset2, "sub_importations_last_first_months_by_partners_and_products.csv")
prev_first_months_import <- dataset_importations[as.Date(dataset_importations$date, "%Y-%m-%d") >= beginning_of_last_year & as.Date(dataset_importations$date, "%Y-%m-%d") <= one_year_ago_importations,]
# On peut maintenant compiler les données par partenaire et par an
prev_first_months_import_dataset1 <- aggregate(prev_first_months_import$value, list(partner=prev_first_months_import$partner, year=prev_first_months_import$year), FUN=sum)
# Puis par catégorie de produits, par partenaire et par an
# prev_first_months_import_dataset2 <- aggregate(prev_first_months_import$value, list(partner=prev_first_months_import$partner, nc8=prev_first_months_import$nc8, year=prev_first_months_import$year), FUN=sum)
file_name_4 = paste("sub_importations_prev_first_months_by_partners_",nbmois2,"_mois.csv", sep = "", collapse = NULL)
write.csv(prev_first_months_import_dataset1, file_name_4)
# write.csv(prev_first_months_import_dataset2, "sub_importations_prev_first_months_by_partners_and_products.csv")
