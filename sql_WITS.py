#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                              sql_WITS.py                                   #
#                                                                            #
#                        Christophe Quentel, 2015-2016                       #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée une base SQLite 3 dans laquelle nous stockerons les données
# en provenance de la base de données WITS de la Banque mondiale.

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, shutil, time
from codecs import open
from functions import log

log("")
log("===============================")
log("Lancement du script sql_WITS.py")
log("===============================")

#2. GERER L'ANCIENNE BASE SI ELLE EXISTE

path_to_check = "datas/bases/wits.db"
old_bases_path = "old_datas/bases/wits/"
if os.path.exists(path_to_check):
	log("Une base de données wits.db existe déjà")
	# On crée le répertoire de destination s'il n'existe pas
	if not os.path.exists(old_bases_path):
		log("Le répertoire accueillant les anciennes bases de données wits.db n'existe pas")
		os.makedirs(old_bases_path)
		log ("Le répertoire accueillant les anciennes bases de données wits.db a été créé")
	# On copie la base existante dans le répertoire old_datas/bases en lui donnant pour nom un timestamp
	old_bases_complete_path = old_bases_path+time.strftime("%Y%m%d-%H%M%S")+".db"
	log("L'ancienne base de données wits.db a été déplacée et renommée dans le répertoire adéquat")	
	shutil.move(path_to_check,old_bases_complete_path)


# 3. CREER LA BASE ET SES TABLES

# Si le répertoire de la base n'existe pas, on le crée
new_base_path = "datas/bases/"
if not os.path.exists(new_base_path):
	log("Création du répertoire destiné à accueillir la base de données wits.db")
	os.makedirs(new_base_path)

# On crée la base qui, forcément, n'existe pas (puisque dans le cas contraire, on l'a déplacée à l'étape précédente)
new_base_complete_path = new_base_path+"wits.db"
conn = sqlite3.connect(new_base_complete_path)
log("Création de la nouvelle base de données wits.db")

# 3.1. Créer la table stockant les parts de marché de chaque pays dans les exportations de chaque pays

try:
	conn.execute('''CREATE TABLE export_shares
		(es_ids	INT	PRIMARY KEY,
		es_cn_reporter	VARCHAR(2)	NOT NULL,
		es_cn_partner	VARCHAR(2)	NOT NULL,
		es_years	YEAR	NOT NULL,
		es_values	INT NOT NULL);''')
	log("Création de la table export_shares")
except:
	log("**Erreur lors de la création de la table export_shares")


# 3.2. Créer la table stockant les parts de marché de chaque pays dans les importations de chaque pays

try:
	conn.execute('''CREATE TABLE import_shares
		(is_ids	INT	PRIMARY KEY,
		is_cn_reporter	VARCHAR(2)	NOT NULL,
		is_cn_partner	VARCHAR(2)	NOT NULL,
		is_years	YEAR	NOT NULL,
		is_values	INT NOT NULL);''')
	log("Création de la table import_shares")
except:
	log("**Erreur lors de la création de la table import_shares")
conn.commit()
conn.close()