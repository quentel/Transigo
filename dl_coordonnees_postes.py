#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                        dl_coordonnees_postes.py                            #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge le fichier mis à disposition par le ministère des Affaires étrangères et du Développement
# international contenant les adresses de ses postes diplomatiques et consulaires. Il trouve ce fichier via le site 
# Opendata du gouvernement français (data.gouv.fr/), télécharge cette page pour y chercher le lien pertinent, puis
# télécharge le fichier qui nous intéresse.

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, shutil, urllib2, re
from functions import log

log("")
log("============================================")
log("Lancement du script dl_coordonnees_postes.py")
log("============================================")

# 2. SCRIPT

# L'identifiant du jeu de données qui nous intéresse
id_dataset = "536991c4a3a729239d203d48"

# L'URL qui en découle
URL = "https://www.data.gouv.fr/api/1/datasets/"+id_dataset+"/"
# Création du répertoire temporaire destiné à accueillir nos fichiers avant décompression
dl_dir = "datas/coordonnees"
# Si le répertoire n'existe pas, créons-le
if not os.path.exists(dl_dir):
	log("Création du répertoire destiné à accueillir nos fichiers")
	os.makedirs(dl_dir)

# On ouvre le fichier contenant le menu
response = urllib2.urlopen(URL)
# On stocke son contenu dans la variable "menu"
menu = response.read()
# Nous rechercherons des URL annoncés par le tag... "url" ! Les lignes se terminent par une accolade fermante. Nous récupérons tout ce qui se trouve entre les deux
pattern = re.compile('url(.*?)}')
# Le résultat est stocké dans la liste "results"
results = pattern.findall(menu)
# Il nous reste à ouvrir notre liste de résultat
for result in results:
	link = result[4:-1]
	# Trouvons le nom du fichier lui-même, après le dernier slash
	index = link.rfind("/")
	# Nous avons besoin du nom du fichier, pour le réenregistrer
	dl_name = link[(index+1):]
	dl_file = dl_dir+"/"+dl_name
	try:
		with open(dl_file,'w') as f:
			f.write(urllib2.urlopen(link).read())
			f.close()
			log("Téléchargement du fichier "+dl_file)
	except Exception, e:	
		log("===Echec du téléchargement d'un fichier : %s" % e)
		pass	


