#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                              sql_general.py                                #
#                                                                            #
#                        Christophe Quentel, 2015-2016                       #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée une base SQLite 3 dans laquelle nous stockerons les données
# en provenance du Pnud, de la Banque mondiale, du FMI, etc. - bref, tout, sauf
# les données des douanes et les nomenclatures

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, shutil, time
from codecs import open
from functions import log

log("")
log("==================================")
log("Lancement du script sql_general.py")
log("==================================")

# 2. GERER L'ANCIENNE BASE SI ELLE EXISTE

path_to_check = "datas/bases/general.db"
old_bases_path = "old_datas/bases/general/"
if os.path.exists(path_to_check):
	log("Une base de données general.db existe déjà")
	# On crée le répertoire de destination s'il n'existe pas
	if not os.path.exists(old_bases_path):
		log("Le répertoire accueillant les anciennes bases de données general.db n'existe pas")
		os.makedirs(old_bases_path)
		log ("Le répertoire accueillant les anciennes bases de données general.db a été créé")
	# On copie la base existante dans le répertoire old_datas/bases/general/ en lui donnant pour nom un timestamp
	old_bases_complete_path = old_bases_path+time.strftime("%Y%m%d-%H%M%S")+".db"
	log("L'ancienne base de données general.db a été déplacée et renommée dans le répertoire adéquat")	
	shutil.move(path_to_check,old_bases_complete_path)

# 3. CREER LA BASE ET SES TABLES

# Si le répertoire de la base n'existe pas, on le crée
new_base_path = "datas/bases/"
if not os.path.exists(new_base_path):
	log("Création du répertoire destiné à accueillir la base de données general.db")
	os.makedirs(new_base_path)

# On crée la base qui, forcément, n'existe pas (puisque dans le cas contraire, on l'a déplacée à l'étape précédente)
new_base_complete_path = new_base_path+"general.db"
conn = sqlite3.connect(new_base_complete_path)
log("Création de la nouvelle base de données general.db")

# 3.1. Créer la "core table" accueillant les données sur chaque nation

try:
	conn.execute('''CREATE TABLE nations_datas
		(nd_ISO_3166_1_alpha_2	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		nd_official_name	TEXT,
		nd_state_form	TEXT,
		nd_off_languages	TEXT,
		nd_area	INT,
		nd_worldbank_category VARCHAR(20));''')
	log("Création de la table nations_datas")
except:
	log("***Erreur lors de la création de la table nations_datas")

# 3.2. Créer la table stockant le solde de la balance des comptes courants, en % du PIB

try:
	conn.execute('''CREATE TABLE nations_account_balances
		(nca_ids	INT	PRIMARY KEY,
		nca_values	DECIMAL(3) NOT NULL,
		nca_sources	VARCHAR(20)	NOT NULL,
		nca_years	YEAR	NOT NULL,
		nca_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nca_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_account_balances")
except:
	log("***Erreur lors de la création de la table nations_account_balances")
	
# 3.3. Créer la table stockant le rang au classement Doing Business

try:
	conn.execute('''CREATE TABLE nations_doing_business_ranks
		(ndbr_ISO_3166_1_alpha_2	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		ndbr_values	INT	NOT NULL,
		ndbr_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_doing_business_ranks")
except:
	log("***Erreur lors de la création de la table nations_doing_business_ranks")

# 3.4. Créer la table stockant les investissements français dans le pays

try:
	conn.execute('''CREATE TABLE nations_french_investments
		(nfi_ids	INT	PRIMARY KEY,
		nfi_values	INT	NOT NULL,
		nfi_years	YEAR	NOT NULL,
		nfi_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nfi_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_french_investments")
except:
	log("***Erreur lors de la création de la table nations_french_investments")
	
# 3.5. Créer la table stockant le PIB du pays

try:
	conn.execute('''CREATE TABLE nations_gdp
		(ng_ids	INT	PRIMARY KEY,
		ng_values	INT	NOT NULL,
		ng_sources	VARCHAR(20),
		ng_years	YEAR	NOT NULL,
		ng_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		ng_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_gdp")
except:
	log("***Erreur lors de la création de la table nations_gdp")

# 3.6. Créer la table stockant la croissance du PIB du pays

try:
	conn.execute('''CREATE TABLE nations_gdp_growth
		(ngg_ids	INT	PRIMARY KEY,
		ngg_values	DECIMAL(2) NOT NULL,
		ngg_sources	VARCHAR(20),
		ngg_years	YEAR	NOT NULL,
		ngg_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		ngg_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_gdp_growth")
except:
	log("***Erreur lors de la création de la table nations_gdp_growth")
	
# 3.7. Créer la table stockant le PIB par habitant du pays

try:
	conn.execute('''CREATE TABLE nations_gdp_per_capita
		(ngc_ids	INT	PRIMARY KEY,
		ngc_values	DECIMAL(3) NOT NULL,
		ngc_sources	VARCHAR(20),
		ngc_years	YEAR	NOT NULL,
		ngc_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		ngc_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_gdp_per_capita")
except:
	log("***Erreur lors de la création de la table nations_gdp_per_capita")
		
# 3.8. Créer la table stockant l'indicateur de développement humain (valeur et rang) du pays

try:
	conn.execute('''CREATE TABLE nations_hdi
		(nh_ids	INT	PRIMARY KEY,
		nh_values	INT	NOT NULL,
		nh_ranks	INT	NOT NULL,
		nh_years	YEAR	NOT NULL,
		nh_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nh_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_hdi")
except:
	log("***Erreur lors de la création de la table nations_hdi")
	
# 3.9. Créer la table stockant les investissements du pays en France

try:
	conn.execute('''CREATE TABLE nations_investment_in_France
		(nif_ids	INT	PRIMARY KEY,
		nif_values	INT	NOT NULL,
		nif_years	YEAR	NOT NULL,
		nif_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nif_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_investment_in_France")
except:
	log("***Erreur lors de la création de la table nations_investment_in_France")
	
# 3.10. Créer la table stockant la population du pays

try:
	conn.execute('''CREATE TABLE nations_populations
		(np_ids	INT	PRIMARY KEY,
		np_values	INT NOT NULL,
		np_sources	VARCHAR(20),
		np_years	YEAR	NOT NULL,
		np_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		np_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_populations")
except:
	log("***Erreur lors de la création de la table nations_populations")

# 3.11. Créer la table stockant le taux de croissance démographique du pays
# (ou plutôt le taux de croissance annuel moyen par période de cinq ans)

try:
	conn.execute('''CREATE TABLE nations_population_growths
		(npg_ids	INT	PRIMARY KEY,
		npg_values	INT	NOT NULL,
		npg_sources	VARCHAR(20),
		npg_years	VARCHAR(9)	NOT NULL,
		npg_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		npg_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_populations_growths")
except:
	log("***Erreur lors de la création de la table nations_populations_growths")

# 3.12 Créer la table stockant l'espérance de vie à la naissance

try:
	conn.execute('''CREATE TABLE nations_life_expectancies
		(nle_ids	INT	PRIMARY KEY,
		nle_values	INT	NOT NULL,
		nle_sources	VARCHAR(20),
		nle_years	YEAR	NOT NULL,
		nle_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nle_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_life_expectancies")
except:
	log("***Erreur lors de la création de la table nations_life_expectancies")

# 3.13. Créer la table stockant le taux de chômage du pays

try:
	conn.execute('''CREATE TABLE nations_unemployment
		(nu_ids	INT	PRIMARY KEY,
		nu_values	DECIMAL(1) NOT NULL,
		nu_sources	VARCHAR(20),
		nu_years	YEAR	NOT NULL,
		nu_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nu_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_unemployment") 
except:
	log("***Erreur lors de la création de la table nations_unemployment")

# 3.14. Créer la table stockant les taux d'alphabétisation
# Attention : sur une période  ans
try:
	conn.execute('''CREATE TABLE nations_literacy_rates
		(nlr_ids	INT	PRIMARY KEY,
		nlr_values	INT	NOT NULL,
		nlr_sources	VARCHAR(20),
		nlr_years	VARCHAR(9)	NOT NULL,
		nlr_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nlr_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_literacy_rates") 
except:
	log("***Erreur lors de la création de la table nations_literacy_rates")

# 3.15. Créer la table stockant le coefficient de Gini
# Attention : sur une période 8 ans

try:
	conn.execute('''CREATE TABLE nations_gini_coefficients
		(ngc_ids	INT	PRIMARY KEY,
		ngc_values	INT	NOT NULL,
		ngc_sources	VARCHAR(20),
		ngc_years	VARCHAR(9)	NOT NULL,
		ngc_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		ngc_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_gini_coefficients") 
except:
	log("***Erreur lors de la création de la table nations_gini_coefficients")

# 3.16 Créer la table stockant le nombre de touristes étrangers entrant dans chaque pays

try:
	conn.execute('''CREATE TABLE nations_international_inbound_tourists
		(nit_ids	INT	PRIMARY KEY,
		nit_values	INT	NOT NULL,
		nit_sources	VARCHAR(20),
		nit_years	YEAR	NOT NULL,
		nit_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nit_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_international_inbound_tourists") 
except:
	log("***Erreur lors de la création de la table nations_international_inbound_tourists")

# 3.17 Créer la table stockant le nombre d'internautes en % de la population

try:
	conn.execute('''CREATE TABLE nations_internet_users
		(niu_ids	INT	PRIMARY KEY,
		niu_values	INT	NOT NULL,
		niu_sources	VARCHAR(20),
		niu_years	YEAR	NOT NULL,
		niu_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		niu_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_internet_users") 
except:
	log("***Erreur lors de la création de la table nations_internet_users")

# 3.18 Créer la table stockant les tonnes de CO2 émises par habitant

try:
	conn.execute('''CREATE TABLE nations_CO2_emissions_per_capita
		(nce_ids	INT	PRIMARY KEY,
		nce_values	INT	NOT NULL,
		nce_sources	VARCHAR(20),
		nce_years	YEAR	NOT NULL,
		nce_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nce_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_CO2_emissions_per_capita") 
except:
	log("***Erreur lors de la création de la table nations_CO2_emissions_per_capita")

# 3.19 Créer la table stockant le taux d'homicides (pour 100 000 habitants)
# Attention : sur une période de 5 ans

try:
	conn.execute('''CREATE TABLE nations_homicides_rates
		(nhr_ids	INT	PRIMARY KEY,
		nhr_values	INT	NOT NULL,
		nhr_sources	VARCHAR(20),
		nhr_years	VARCHAR(9)	NOT NULL,
		nhr_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nhr_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_homicides_rates") 
except:
	log("***Erreur lors de la création de la table nations_homicides_rates")

# 3.20 Créer la table stockant les 5 meilleurs clients de chaque pays

try:
	conn.execute('''CREATE TABLE nations_best_clients
		(nbc_ids	INT	PRIMARY KEY,
		nbc_declarants	VARCHAR(2)	NOT NULL,
		nbc_clients	VARCHAR(2)	NOT NULL,
		nbc_values	INT	NOT NULL,
		nbc_years	YEAR	NOT NULL,
		nbc_sources	VARCHAR(20),
		nbc_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_best_clients") 
except:
	log("***Erreur lors de la création de la table nations_best_clients")

# 3.21 Créer la table stockant les 5 meilleurs fournisseurs de chaque pays

try:
	conn.execute('''CREATE TABLE nations_best_providers
		(nbp_ids	INT	PRIMARY KEY,
		nbp_declarants	VARCHAR(2)	NOT NULL,
		nbp_providers	VARCHAR(2)	NOT NULL,
		nbp_values	INT	NOT NULL,
		nbp_years	YEAR	NOT NULL,
		nbp_sources	VARCHAR(20),
		nbp_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_best_clients") 
except:
	log("***Erreur lors de la création de la table nations_best_clients")

# 3.22 Créer la table stockant la valeur totale des exportations de chaque pays

try:
	conn.execute('''CREATE TABLE nations_exports
		(nex_ids	INT	PRIMARY KEY,
		nex_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nex_values	INT	NOT NULL,
		nex_years	YEAR	NOT NULL,
		nex_sources	VARCHAR(20),
		nex_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_exports") 
except:
	log("***Erreur lors de la création de la table nations_exports")

# 3.23 Créer la table stockant la valeur totale des importations de chaque pays

try:
	conn.execute('''CREATE TABLE nations_imports
		(nim_ids	INT	PRIMARY KEY,
		nim_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nim_values	INT	NOT NULL,
		nim_years	YEAR	NOT NULL,
		nim_sources	VARCHAR(20),
		nim_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_imports") 
except:
	log("***Erreur lors de la création de la table nations_imports")

# 3.24. Créer la table stockant les taux d'inflation

try:
	conn.execute('''CREATE TABLE nations_inflations
		(nif_ids	INT	PRIMARY KEY,
		nif_values	DECIMAL(1) NOT NULL,
		nif_sources	VARCHAR(20),
		nif_years	YEAR	NOT NULL,
		nif_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nif_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_inflations") 
except:
	log("***Erreur lors de la création de la table nations_inflations")

# 3.25. Créer la table stockant la valeur ajoutée de l'agriculture dans le PIB, en %

try:
	conn.execute('''CREATE TABLE nations_agriculture_value_added
		(nav_ids	INT	PRIMARY KEY,
		nav_values	DECIMAL(1) NOT NULL,
		nav_sources	VARCHAR(20),
		nav_years	YEAR	NOT NULL,
		nav_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nav_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_agriculture_value_added") 
except:
	log("***Erreur lors de la création de la table nations_agriculture_value_added")

# 3.26. Créer la table stockant la valeur ajoutée de l'industrie dans le PIB, en %

try:
	conn.execute('''CREATE TABLE nations_industry_value_added
		(niv_ids	INT	PRIMARY KEY,
		niv_values	DECIMAL(1) NOT NULL,
		niv_sources	VARCHAR(20),
		niv_years	YEAR	NOT NULL,
		niv_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		niv_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_industry_value_added") 
except:
	log("***Erreur lors de la création de la table nations_industry_value_added")

# 3.27. Créer la table stockant la valeur ajoutée des services dans le PIB, en %

try:
	conn.execute('''CREATE TABLE nations_services_value_added
		(nsv_ids	INT	PRIMARY KEY,
		nsv_values	DECIMAL(1) NOT NULL,
		nsv_sources	VARCHAR(20),
		nsv_years	YEAR	NOT NULL,
		nsv_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nsv_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_services_value_added") 
except:
	log("***Erreur lors de la création de la table nations_services_value_added")

# 3.28 Créer la table stockant les investissements directs étrangers (net) vers chaque pays

try:
	conn.execute('''CREATE TABLE nations_fdi
		(nfdi	ids	INT	PRIMARY KEY,
		nfdi_values	INT	NOT NULL,
		nfdi_sources	VARCHAR(20),
		nfdi_years	YEAR	NOT NULL,
		nfdi_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nfdi_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_fdi") 
except:
	log("***Erreur lors de la création de la table nations_fdi")

# 3.29 Créer la table stockant le dernier taux de chancellerie pour chaque monnaie

try:
	conn.execute('''CREATE TABLE currencies_rates
		(cr_codes	VARCHAR(3)	PRIMARY KEY NOT NULL,
		cr_dates	DATE,
		cr_values	INT,
		cr_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table currencies_rates") 
except:
	log("***Erreur lors de la création de la table currencies_rates")

# 3.30. Créer la table stockant le taux de pauvreté du pays

try:
	conn.execute('''CREATE TABLE nations_poverty
		(npov_ids	INT	PRIMARY KEY,
		npov_values	DECIMAL(1) NOT NULL,
		npov_sources	VARCHAR(20),
		npov_years	YEAR	NOT NULL,
		npov_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		npov_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_poverty") 
except:
	log("***Erreur lors de la création de la table nations_poverty")

# 3.31. Créer la table stockant le taux de fécondité du pays

try:
	conn.execute('''CREATE TABLE nations_fertility
		(nfer_ids	INT	PRIMARY KEY,
		nfer_values	DECIMAL(1) NOT NULL,
		nfer_sources	VARCHAR(20),
		nfer_years	YEAR	NOT NULL,
		nfer_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nfer_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_fertility") 
except:
	log("***Erreur lors de la création de la table nations_fertility")

# 3.32. Créer la table stockant la dette publique (nette), exprimée en % du PIB

try:
	conn.execute('''CREATE TABLE general_gov_net_debt
		(gg_ids	INT	PRIMARY KEY,
		gg_values	DECIMAL(2) NOT NULL,
		gg_sources	VARCHAR(20),
		gg_years	YEAR	NOT NULL,
		gg_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		gg_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table general_gov_net_debt") 
except:
	log("***Erreur lors de la création de la table general_gov_net_debt")

# 3.33. Créer la table stockant le déficit budgétaire, exprimée en % du PIB

try:
	conn.execute('''CREATE TABLE general_gov_deficit
		(gd_ids	INT	PRIMARY KEY,
		gd_values	DECIMAL(2) NOT NULL,
		gd_sources	VARCHAR(20),
		gd_years	YEAR	NOT NULL,
		gd_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		gd_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table general_gov_net_debt") 
except:
	log("***Erreur lors de la création de la table general_gov_deficit")

# 3.34 Créer la table stockant les leaders d'un pays

try:
	conn.execute('''CREATE TABLE nations_leaders
		(nl_ids	INT	PRIMARY KEY,
		nl_functions	TEXT NOT NULL,
		nl_names	TEXT NOT NULL,
		nl_ranks	INT NOT NULL,
		nl_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nl_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_leaders") 
except:
	log("***Erreur lors de la création de la table nations_leaders")

# 3.35 Créer la table stockant les exportations françaises totale vers un pays, selon la DGT

try:
	conn.execute('''CREATE TABLE kiosque_exports
		(ke_ids	INT	PRIMARY KEY,
		ke_years	YEAR NOT NULL,
		ke_values	INT NOT NULL,
		ke_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		ke_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table kiosque_exports") 
except:
	log("***Erreur lors de la création de la table kiosque_exports")

# 3.36 Créer la table stockant les importations françaises totale depuis un pays, selon la DGT

try:
	conn.execute('''CREATE TABLE kiosque_imports
		(ki_ids	INT	PRIMARY KEY,
		ki_years	YEAR NOT NULL,
		ki_values	INT NOT NULL,
		ki_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		ki_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table kiosque_imports") 
except:
	log("***Erreur lors de la création de la table kiosque_imports")

# 3.37 Créer la table stockant les villes principales, pour un pays

try:
	conn.execute('''CREATE TABLE countries_main_towns
		(mt_ids	INT	PRIMARY KEY,
		mt_towns	TEXT NOT NULL,
		mt_populations	INT NOT NULL,
		mt_years	YEAR NOT NULL,
		mt_types	TEXT NOT NULL,
		mt_capital_cities	BOOL NOT NULL,		
		mt_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		mt_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table countries_main_towns") 
except:
	log("***Erreur lors de la création de la table countries_main_towns")

# 3.38 Créer la table stockant le nombre de fidèles de chaque religion, pour chaque pays

try:
	conn.execute('''CREATE TABLE nations_religions
		(nr_ids	INT	PRIMARY KEY,
		nr_labels	TEXT NOT NULL,
		nr_values	INT NOT NULL,
		nr_years	YEAR NOT NULL,
		nr_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nr_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_religions") 
except:
	log("***Erreur lors de la création de la table nations_religions")

# 3.38 Créer la table stockant le nombre de locuteurs de chaque langue, pour chaque pays

try:
	conn.execute('''CREATE TABLE nations_languages
		(nl_ids	INT	PRIMARY KEY,
		nl_labels	TEXT NOT NULL,
		nl_values	INT NOT NULL,
		nl_years	YEAR NOT NULL,
		nl_ISO_3166_1_alpha_2	VARCHAR(2)	NOT NULL,
		nl_timestamps	DATETIME	DEFAULT CURRENT_TIMESTAMP);''')
	log("Création de la table nations_languages") 
except:
	log("***Erreur lors de la création de la table nations_languages")

conn.commit()
conn.close()