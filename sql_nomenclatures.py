#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                           sql_nomenclatures.py                             #
#                                                                            #
#                        Christophe Quentel, 2015-2016                       #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée une base SQLite 3 dans laquelle nous stockerons les nomenclatures utiles
# à l'application

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, shutil, time
from codecs import open
from functions import log

log("")
log("========================================")
log("Lancement du script sql_nomenclatures.py")
log("========================================")

#2. GERER L'ANCIENNE BASE SI ELLE EXISTE

path_to_check = "datas/bases/nomenclatures.db"
old_bases_path = "old_datas/bases/nomenclatures/"
if os.path.exists(path_to_check):
	log("Une base de données nomenclatures.db existe déjà")
	# On crée le répertoire de destination s'il n'existe pas
	if not os.path.exists(old_bases_path):
		log("Le répertoire accueillant les anciennes bases de données nomenclatures.db n'existe pas")
		os.makedirs(old_bases_path)
		log ("Le répertoire accueillant les anciennes bases de données nomenclatures.db a été créé")
	# On copie la base existante dans le répertoire old_datas/bases en lui donnant pour nom un timestamp
	old_bases_complete_path = old_bases_path+time.strftime("%Y%m%d-%H%M%S")+".db"
	log("L'ancienne base de données nomenclatures.db a été déplacée et renommée dans le répertoire adéquat")	
	shutil.move(path_to_check,old_bases_complete_path)


# 3. CREER LA BASE ET SES TABLES

# Si le répertoire de la base n'existe pas, on le crée
new_base_path = "datas/bases/"
if not os.path.exists(new_base_path):
	log("Création du répertoire destiné à accueillir la base de données nomenclature.db")
	os.makedirs(new_base_path)

# On crée la base qui, forcément, n'existe pas (puisque dans le cas contraire, on l'a déplacée à l'étape précédente)
new_base_complete_path = new_base_path+"nomenclatures.db"
conn = sqlite3.connect(new_base_complete_path)
log("Création de la nouvelle base de données nomenclatures.db")

# 3.1. Créer la table gérant les noms de pays (qui ne sont pas des nations, d'ailleurs)
# On met en index le code ISO à deux chiffres des pays : c'est la donnée que nous utiliserons le plus
# On place à la fin le "nation code", i.e. le code de la nation à laquelle appartient le "pays" en question : FR pour GD (Guadeloupe),
# par exemple

try:
	conn.execute('''CREATE TABLE countries_names
		(cn_ISO_3166_1_alpha_2	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		cn_french	TEXT	NOT NULL,
		cn_english	TEXT	NOT NULL,
		cn_ISO_3166_1_alpha_3	VARCHAR(3),
		cn_eu_code	VARCHAR(2),
		cn_nation_code VARCHAR(2));''')
	log("Création de la table countries_names")
except:
	log("**Erreur lors de la création de la table countries_names")

# 3.2. Créer la table stockant la nomenclature NA_17

try:
	conn.execute('''CREATE TABLE na17
		(na17_codes VARCHAR(2)	NOT NULL,
		na17_labels TEXT	NOT NULL);''')
	log("Création de la table na17")
except:
	log("**Erreur lors de la création de la table na17")

# 3.3. Créer la table stockant la nomenclature NA_38

try:
	conn.execute('''CREATE TABLE na38
		(na38_codes VARCHAR(2)	NOT NULL,
		na38_labels TEXT	NOT NULL);''')
	log("Création de la table na38")
except:
	log("**Erreur lors de la création de la table na38")

# 3.4. Créer la table stockant la nomenclature NA_129

try:
	conn.execute('''CREATE TABLE na129
		(na129_codes VARCHAR(4)	NOT NULL,
		na129_labels TEXT	NOT NULL);''')
	log("Création de la table na129")
except:
	log("**Erreur lors de la création de la table na129")

# 3.3 Créer la table de concordance entre les nomenclatures NC8, NA17, NA38 et NA129
try:
	conn.execute('''CREATE TABLE concordances_douanes
		(nvc_codes INT	PRIMARY KEY,
		nvc_year	YEAR	NOT NULL,
		nvc_nc8	varchar(8)	NOT NULL,
		nvc_na17	varchar(2)	NOT NULL,
		nvc_na38	varchar(2)	NOT NULL,
		nvc_na129	varchar(4)	NOT NULL);''')
	log("Création de la table concordances_douanes")
except Exception, e:
	log("**Erreur lors de la création de la table concordances_douanes : %s" % e)

# 3.4. Créer la table gérant les nomenclatures utiles à l'exploitation des données WITS
# Les noms de pays utilisés par ce programme de la Banque mondiale ne correspondent souvent à aucun autre
# ...Outre les purs et simples coquilles (Wallis et FutuRa, AnguiLa, p. ex.). Les noms de pays en 
# caractères apparaissent plusieurs fois : on ne peut pas les prendre comme identifiants

try:
	conn.execute('''CREATE TABLE wits_names
		(wn_codes	INT	PRIMARY KEY,
		wn_french	TEXT	NOT NULL,
		wn_english	TEXT	NOT NULL,
		wn_nation_code VARCHAR(2));''')
	log("Création de la table wits_names")
except:
	log("**Erreur lors de la création de la table countries_names")


# 3.5. Créer la table gérant la concordance entre le noms de pays utilisés par le
# programme "Doing Business" de la Banque mondiale et les codes ISO des mêmes territoires

try:
	conn.execute('''CREATE TABLE db_names
		(db_codes	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		db_english	TEXT	NOT NULL);''')
	log("Création de la table db_names")
except:
	log("**Erreur lors de la création de la table db_names")

# 3.6. Créer la table stockant les monnaies nationales

try:
	conn.execute('''CREATE TABLE currencies
		(cu_ISO_3166_1_alpha_2	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		cu_codes	VARCHAR(3)	NOT NULL,
		cu_currencies	TEXT	NOT NULL);''')
	log("Création de la table currencies")
except:
	log("**Erreur lors de la création de la table currencies")

# 3.7. Créer la table gérant les noms de pays selon Wikipedia...

try:
	conn.execute('''CREATE TABLE wiki_names
		(cn_ISO_3166_1_alpha_2	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		cn_french	TEXT	NOT NULL,
		cn_english	TEXT	NOT NULL,
		cn_ISO_3166_1_alpha_3	VARCHAR(3),
		cn_eu_code	VARCHAR(2),
		cn_nation_code VARCHAR(2));''')
	log("Création de la table wiki_names")
except:
	log("**Erreur lors de la création de la table wiki_names")

# 3.8. Créer la table gérant les noms de villes, en français et en anglais

try:
	conn.execute('''CREATE TABLE cities_names
		(ci_ids	INT	PRIMARY KEY,
		ci_french	TEXT	NOT NULL,
		ci_english	TEXT	NOT NULL);''')
	log("Création de la table city_names")
except:
	log("**Erreur lors de la création de la table cities_names")

# 3.9. Créer la table gérant les noms de pays (qui ne sont pas des nations, d'ailleurs) selon le
# Département des Statistiques des Nations Unies (UNSD)

try:
	conn.execute('''CREATE TABLE UNSD_names
		(un_ISO_3166_1_alpha_2	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		un_french	TEXT	NOT NULL,
		un_english	TEXT	NOT NULL,
		un_ISO_3166_1_alpha_3	VARCHAR(3),
		un_eu_code	VARCHAR(2),
		un_nation_code VARCHAR(2));''')
	log("Création de la table UNSD_names")
except:
	log("**Erreur lors de la création de la table countries_names")

# 3.10. Créer la table gérant les noms de religions selon le
# Département des Statistiques des Nations Unies (UNSD)

try:
	conn.execute('''CREATE TABLE religions
		(re_id	INT	PRIMARY KEY,
		re_french	TEXT,
		re_english	TEXT);''')
	log("Création de la table religions")
except:
	log("**Erreur lors de la création de la table religions")

# 3.11. Créer la table gérant les noms de langues, en français et en anglais

try:
	conn.execute('''CREATE TABLE languages_names
		(li_ids	INT	PRIMARY KEY,
		li_french	TEXT	NOT NULL,
		li_english	TEXT	NOT NULL);''')
	log("Création de la table languages_names")
except:
	log("**Erreur lors de la création de la table languages_names")

# 3.12. Créer la table gérant les noms de pays et de capitales selon le Quai d'Orsay

try:
	conn.execute('''CREATE TABLE qo_names
		(qo_ids	INT	PRIMARY KEY,
		qo_pays	TEXT	NOT NULL,
		qo_capitale	TEXT	NOT NULL);''')
	log("Création de la table qo_names")
except:
	log("**Erreur lors de la création de la table qo_names")

# 3.13. Créer la table gérant l'organigramme des directions géographiques du Quai d'Orsay

try:
	conn.execute('''CREATE TABLE qo_orga
		(qorg_ISO_3166_1_alpha_2	VARCHAR(2)	PRIMARY KEY	NOT NULL,
		qorg_direction	TEXT	NOT NULL,
		qorg_sous_direction	TEXT	NOT NULL);''')
	log("Création de la table qo_orga")
except:
	log("**Erreur lors de la création de la table qo_orga")


# 4. INITIALISER LES NOMENCLATURES
# ...Sauf qo_names, qui sera iniitialisée par mo_geographie_diplomatique.py 

# 4.1. Initialiser la table des noms de territoire

chemin_co = "nomenclatures/countries_list.txt"

with open(chemin_co, encoding='utf8') as fichier_co:
	for ligne in fichier_co:
		liste_co = ligne.split(";")
		try:
			conn.execute("INSERT INTO countries_names VALUES (?, ?, ?, ?, ?, ?)",(unicode(liste_co[2]),unicode(liste_co[0]),unicode(liste_co[1]),unicode(liste_co[3]),unicode(liste_co[4]),unicode(liste_co[5])))
		except:
			log("erreur sur la création du pays :"+liste_co[0])
log("Table countries_names initialisée")

# 4.2 Initialiser la table de concordance des nomenclatures NC 8, NA 17, NA 38 et NA 129

chemin_nvc = "nomenclatures/douane/concordances/"
identifiant = 1
liste_fichier = os.listdir(chemin_nvc)
for fichier in liste_fichier:
	log("Traitement du fichier "+fichier)
	chemin_fichier = chemin_nvc+fichier
	with open(chemin_fichier, encoding='utf8') as fichier_nvc:
		# On passe la première ligne, celle d'en-tête
		next(fichier_nvc)
		for ligne in fichier_nvc:
			liste_nvc = ligne.split(",")
			code_nc8 = liste_nvc[1]
			code_na17 = liste_nvc[3]
			code_na38 = liste_nvc[4]
			code_na129 = liste_nvc[5]
			# On enlève les guillemets
			code_nc8 = code_nc8[1:-1]
			code_na17 = code_na17[1:-1]
			code_na38 = code_na38[1:-1]
			code_na129 = code_na129[1:-1]
			try:
				conn.execute("INSERT INTO concordances_douanes VALUES (?, ?, ?, ?, ?, ?)",(unicode(identifiant),unicode(liste_nvc[0]),unicode(code_nc8),unicode(code_na17),unicode(code_na38),unicode(code_na129)))
				identifiant = identifiant + 1
			except Exception, e:
				log("Erreur sur la création d'un code : %s" % e)
log("Table concordances_douanes initialisée")

# 4.3 Initialiser NA17

chemin_na17 = "nomenclatures/douane/libelles/libelles_na17.csv"

with open(chemin_na17, encoding='utf8') as fichier_na17:
	for ligne in fichier_na17:
		try:
			liste_na17 = ligne.split(";")
			conn.execute('''INSERT INTO na17 (na17_codes,na17_labels) VALUES (?,?);''', (liste_na17[0], liste_na17[1]) )		
		except:
			log("Impossible d'ajouter à la table na17 l'identifiant :"+liste_na17[0])	
fichier_na17.close()
log("Table na17 initialisée")

# 4.4 Initialiser NA38

chemin_na38 = "nomenclatures/douane/libelles/libelles_na38.csv"

with open(chemin_na38, encoding='utf8') as fichier_na38:
	for ligne in fichier_na38:
		try:
			liste_na38 = ligne.split(";")
			conn.execute('''INSERT INTO na38 (na38_codes,na38_labels) VALUES (?,?);''', (liste_na38[0], liste_na38[1]) )		
		except:
			log("Impossible d'ajouter à la table na38 l'identifiant :"+liste_na38[0])	
fichier_na38.close()
log("Table na38 initialisée")

# 4.5 Initialiser NA129

chemin_na129 = "nomenclatures/douane/libelles/libelles_na129.csv"

with open(chemin_na129, encoding='utf8') as fichier_na129:
	for ligne in fichier_na129:
		try:
			liste_na129 = ligne.split(";")
			conn.execute('''INSERT INTO na129 (na129_codes,na129_labels) VALUES (?,?);''', (liste_na129[0], liste_na129[1]) )		
		except:
			log("Impossible d'ajouter à la table na129 l'identifiant :"+liste_na129[0])	
fichier_na129.close()
log("Table na129 initialisée")

# 4.6. Initialiser la table WITS

chemin_wits = "nomenclatures/WITS_list.txt"

with open(chemin_wits, encoding='utf8') as fichier_wits:
	for ligne in fichier_wits:
		liste_wits = ligne.split(";")
		try:			
			conn.execute("INSERT INTO wits_names (wn_french, wn_english, wn_nation_code) VALUES (?, ?, ?);",(unicode(liste_wits[0]),unicode(liste_wits[1]),unicode(liste_wits[2])))
		except Exception, e:
			log("***Erreur sur la création du pays : %s" % e)
log("Table wits_names initialisée")

# 4.7. Initialiser la table des noms utilisés par le programme 'Doing Business'

chemin_db = "nomenclatures/doingbusiness.txt"

with open(chemin_db, encoding='utf8') as fichier_db:
	for ligne in fichier_db:
		liste_db = ligne.split(";")
		try:
			conn.execute("INSERT INTO db_names VALUES (?, ?)",(unicode(liste_db[1]),unicode(liste_db[0])))
		except Exception, e:
			log("***Erreur sur la création du pays : %s" % e)
log("Table db_names initialisée")

# 4.7. Initialiser la table des monnaies

chemin_db = "nomenclatures/currencies_list.txt"

with open(chemin_db, encoding='utf8') as fichier_db:
	for ligne in fichier_db:
		liste_db = ligne.split(";")
		try:
			conn.execute("INSERT INTO currencies VALUES (?, ?, ?)",(unicode(liste_db[0]),unicode(liste_db[1]),unicode(liste_db[2])))
		except Exception, e:
			log("***Erreur sur la création du pays : %s" % e)
log("Table currencies initialisée")

# 4.8. Initialiser la table des noms de territoire selon Wikipedia

chemin_co = "nomenclatures/wiki_list.txt"

with open(chemin_co, encoding='utf8') as fichier_co:
	for ligne in fichier_co:
		liste_co = ligne.split(";")
		try:
			conn.execute("INSERT INTO wiki_names VALUES (?, ?, ?, ?, ?, ?)",(unicode(liste_co[2]),unicode(liste_co[0]),unicode(liste_co[1]),unicode(liste_co[3]),unicode(liste_co[4]),unicode(liste_co[5])))
		except:
			log("***Erreur sur la création du pays :"+liste_co[0])
log("Table wiki_names initialisée")

# 4.9. Initialiser la table des noms de villes

chemin_cl = "nomenclatures/cities_list.txt"

with open(chemin_cl, encoding='utf8') as fichier_cl:
	for ligne in fichier_cl:
		liste_cl = ligne.split(";")
		try:
			conn.execute("INSERT INTO cities_names (ci_french, ci_english) VALUES (?, ?)",(unicode(liste_cl[0]),unicode(liste_cl[1])))
		except:		
			log("***Erreur sur la création de la ville" % e)
log("Table cities_names initialisée")

# 4.10. Initialiser la table des noms de territoires selon l'UNSD

chemin_co = "nomenclatures/UNSD_list.txt"

with open(chemin_co, encoding='utf8') as fichier_co:
	for ligne in fichier_co:
		liste_co = ligne.split(";")
		try:
			conn.execute("INSERT INTO UNSD_names VALUES (?, ?, ?, ?, ?, ?)",(unicode(liste_co[2]),unicode(liste_co[0]),unicode(liste_co[1]),unicode(liste_co[3]),unicode(liste_co[4]),unicode(liste_co[5])))
		except:
			log("***Erreur sur la création du pays :"+liste_co[0])
log("Table UNSD_names initialisée")

# 4.11. Initialiser la table des noms de religions selon l'UNSD

chemin_re = "nomenclatures/religions_list.txt"

with open(chemin_re, encoding='utf8') as fichier_re:
	for ligne in fichier_re:
		liste_re = ligne.split(";")
		try:
			conn.execute("INSERT INTO religions (re_french, re_english) VALUES (?, ?)",(unicode(liste_re[1]),unicode(liste_re[0])))
		except Exception, e:
			log("***Erreur sur l'injection du nom de religion : %s" % e)
log("Table religions initialisée")

# 4.12. Initialiser la table des noms de langues

chemin_la = "nomenclatures/languages_list.txt"

with open(chemin_la, encoding='utf8') as fichier_la:
	for ligne in fichier_la:
		liste_la = ligne.split(";")
		try:
			conn.execute("INSERT INTO languages_names (li_english, li_french) VALUES (?, ?)",(unicode(liste_la[0]),unicode(liste_la[1])))
		except:		
			log("***Erreur sur la création de la langue" % e)
log("Table languages_names initialisée")

# 4.13. Initialiser la table de l'organigramme des directions géographiques au Quai d'Orsay

chemin_gd = "nomenclatures/geodiplo_list.txt"

with open(chemin_gd, encoding='utf8') as fichier_gd:
	for ligne in fichier_gd:
		liste_gd = ligne.split(";")
		try:
			conn.execute("INSERT INTO qo_orga (qorg_ISO_3166_1_alpha_2, qorg_direction, qorg_sous_direction) VALUES (?, ?, ?)",(unicode(liste_gd[1]),unicode(liste_gd[2]),unicode(liste_gd[3])))
		except:		
			log("***Erreur sur la création de l'organigramme du Quai d'Orsay" % e)
log("Table qo_orga initialisée")

conn.commit()
conn.close()