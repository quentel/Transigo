#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                          	  graph.py                                     #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script dessine nos graphiques

# 1. IMPORTATION DES MODULES NECESSAIRES

from __future__ import division
from matplotlib import pyplot as plt
from matplotlib import patches as mpatches
from pylab import *
import numpy as np, datetime
from functions import *

log("")
log("===================================")
log("Lancement du script graph.py")
log("===================================")

# 2. Fonctions

def getUnite(values_list):
	#values_list.sort(reverse = True)
	odg = list()			
	odg = ordreDeGrandeur(values_list[0])
	unite = odg[0]
	return unite
	
def getMultiplicateur(values_list):
	#values_list.sort(reverse = True)
	odg = list()			
	odg = ordreDeGrandeur(values_list[0])
	multiplicateur = odg[1]
	return multiplicateur
	
def getDiviseur(values_list):
	#values_list.sort(reverse = True)
	odg = list()			
	odg = ordreDeGrandeur(values_list[0])
	diviseur = odg[2]
	return diviseur

def saveGraphFile(my_file_name,country_code):
	graph_path = "output/graphs/"
	graph_dir = os.path.dirname(graph_path)
	if not os.path.exists(graph_path):
		log("Création des répertoires destinés à accueillir nos graphiques : output/graphs/")
		os.makedirs(graph_dir)
	file_name = my_file_name+country_code+".png"
	graph_file = os.path.join(graph_dir, file_name)
	print graph_file
 	savefig(graph_file, bbox_inches="tight")

def simpleSourcedBarsCompileDatas(prefixe,table,country_code,source):
	years_available = list()
	years = list()
	values = list()
	years_available = getAllYearsSourced(prefixe,table,country_code,source)
	for year in years_available:
		value = getDataValueSourced(prefixe,table,country_code,year,source)
		values.append(value)
		years.append(year)	
	return years, values
	
def mirroredBarsCompileDatas(prefixe1,table1,prefixe2,table2,country_code):
	years_available_1 = list()
	years_1 = list()
	values_1 = list()
	years_available_2 = list()
	years_2 = list()
	values_2 = list()
	years_available_1 = getAllYears(prefixe1,table1,country_code)
	for year in years_available_1:
		value = getDataValue(prefixe1,table1,country_code,year)
		values_1.append(value)
		years_1.append(year)	
	years_available_2 = getAllYears(prefixe2,table2,country_code)
	for year in years_available_2:
		value = getDataValue(prefixe2,table2,country_code,year)
		values_2.append(value)
		years_2.append(year)	
	return years_1, values_1, years_2, values_2

def douanesTerritoryMirroredBarsCompileDatas(prefixe1,table1,prefixe2,table2,country_code):
	years_available_1 = list()
	years_1 = list()
	values_1 = list()
	years_available_2 = list()
	years_2 = list()
	values_2 = list()
	years_available_1 = getAllYearsinDouanesTerritory(prefixe1,table1,country_code)
	for year in years_available_1:
		value = getDataValueinDouanesTerritory(prefixe1,table1,country_code,year)
		values_1.append(value)
		years_1.append(year)	
	years_available_2 = getAllYearsinDouanesTerritory(prefixe2,table2,country_code)
	for year in years_available_2:
		value = getDataValueinDouanesTerritory(prefixe2,table2,country_code,year)
		values_2.append(value)
		years_2.append(year)	
	return years_1, values_1, years_2, values_2


# Deux courbes + un diagramme en barre pour la différence
def tripleGraphCompileDatas(prefixe1,table1,prefixe2,table2,country_code):
	years_available = list()
	years = list()
	values_1 = list()
	values_2 = list()
	values_3 = list()
	years_available = getAllYears(prefixe1,table1,country_code)
	for year in years_available:
		years.append(year)		
		value_1 = getDataValue(prefixe1,table1,country_code,year)
		value_2 = getDataValue(prefixe2,table2,country_code,year)
		value_3 = value_1-value_2
		values_1.append(value_1)
		values_2.append(value_2)
		values_3.append(value_3)
	return years, values_1, values_2, values_3

def drawSimpleBars(datas,country_code,graph_title,xlabel,ylabel1,ylabel2):
	plt.close("all")	
	fig = plt.figure()
	fig.clf("all")
	x = datas[0]
	y = datas[1]
	# Taille du graphique (généralement 10 et 7,5 ou 12 et 9)	
	plt.figure(figsize=(12,9))
	ax = plt.subplot(111)
	# On vire les traits en haut et à droite du graphique
	ax.spines["top"].set_visible(False)
	ax.spines["right"].set_visible(False)
	# On n'affiche les ticks qu'en bas et à gauche	
	ax.get_xaxis().tick_bottom()	
	ax.xaxis.set_ticks_position("none")	
	ax.get_yaxis().tick_left()
	# Nous voulons que nos légendes d'axes soient lisibles
	plt.xticks(fontsize=14)
	plt.yticks(fontsize=14)
	# Légendes et titres
	country_name = deAlpha2versLibelleFR(country_code)
	graph_title = country_name+" - "+graph_title
	plt.title(graph_title, fontsize=18,fontweight="bold")
	plt.xlabel(xlabel,fontsize=16, fontstyle="italic")
	ylabel = ylabel1+" ("+getUnite(y)+ylabel2+")"
	ylabel = unicode(ylabel)
	plt.ylabel(ylabel, fontsize=16, fontstyle="italic")
	y = [resultat/getDiviseur(y) for resultat in y]	
	width = 0.8
	ax.bar(range(len(x)), y, width, facecolor='#9999ff', edgecolor='#9999ff', align="center")
	ax.set_xticks(np.arange(len(x)))
	ax.set_xticklabels(x) # Pour afficher verticalement : (x, rotation=90)

def drawMirroredBars(datas,country_code,graph_title,xlabel,ylabel1,ylabel2):
	plt.close("all")	
	fig = plt.figure()
	fig.clf("all")
	x1 = datas[0]
	y1 = datas[1]
	x2 = datas[2]
	y2 = datas[3]
	# Taille du graphique (généralement 10 et 7,5 ou 12 et 9)	
	plt.figure(figsize=(12,9))
	ax = plt.subplot(111)
	# On vire les traits en haut et à droite du graphique
	ax.spines["top"].set_visible(False)
	ax.spines["right"].set_visible(False)
	# On n'affiche les ticks qu'en bas et à gauche	
	ax.get_xaxis().tick_bottom()	
	ax.xaxis.set_ticks_position("none")	
	ax.get_yaxis().tick_left()
	# Nous voulons que nos légendes d'axes soient lisibles
	plt.xticks(fontsize=14)
	plt.yticks(fontsize=14)
	# Légendes et titres
	country_name = deAlpha2versLibelleFR(country_code)
	graph_title = country_name+" : "+graph_title
	plt.title(graph_title, fontsize=18,fontweight="bold")
	plt.xlabel(xlabel,fontsize=16, fontstyle="italic")
	ylabel = ylabel1+" ("+getUnite(y1)+ylabel2+")"
	ylabel = unicode(ylabel)
	plt.ylabel(ylabel, fontsize=16, fontstyle="italic")
	y1 = [resultat/getDiviseur(y1) for resultat in y1]	
	y2 = [resultat/getDiviseur(y2) for resultat in y2]	
	y2 = [-nombre for nombre in y2]
	width = 0.8
	ax.bar(range(len(x1)), y1, width, facecolor='#9999ff', edgecolor='#9999ff', align="center", label="Exportations")
	ax.bar(range(len(x1)), y2, width, facecolor='#ff9999', edgecolor='#ff9999', align="center", label="Importations")
	plt.legend()	
	ax.set_xticks(np.arange(len(x1)))
	ax.set_xticklabels(x1) # Pour afficher verticalement : (x, rotation=90)

def drawEnrobbedBars(datas,country_code,graph_title,xlabel,ylabel1,ylabel2):
	plt.close("all")	
	fig = plt.figure()
	fig.clf("all")
	x = datas[0]
	y = datas[1]
	# Taille du graphique	
	plt.figure(figsize=(15,5))
	ax = plt.subplot(111)
	# On vire les traits en haut, à gauche et à droite du graphique
	ax.spines["top"].set_visible(False)
	ax.spines["right"].set_visible(False)
	ax.spines["left"].set_visible(False)
	# On n'affiche les ticks qu'en bas	
	ax.get_xaxis().tick_bottom()	
	ax.xaxis.set_ticks_position("none")	
	ax.yaxis.set_ticks_position("none")
	ax.get_yaxis().set_visible(False)
	# Nous voulons que nos légendes d'axes soient lisibles
	plt.xticks(fontsize=14)
	# Légendes et titres
	country_name = deAlpha2versLibelleFR(country_code)
	graph_title = country_name+" - "+graph_title
	#plt.set_bbox(dict(facecolor='yellow', edgecolor='black', alpha=0.8))
	plt.title(graph_title, fontsize=18,fontweight="bold")
	width = 0.5
	ax.bar(range(len(x)), y, width, linewidth=10.0, facecolor='#4f81bd', edgecolor='#d9d9d9', align="center")
	# Etiquettes de données	
	for a,b in zip(range(len(x)), y):
		s = '% .2f' % b
		s = s+" %"		
		text(a, b+0.1, s, ha='center', va='bottom')
	ax.set_xticks(np.arange(len(x)))
	ax.set_xticklabels(x)

def drawTripleGraph(datas,country_code,graph_title):
	plt.close("all")	
	fig = plt.figure()
	fig.clf("all")
	x = datas[0]
	y1 = datas[1]
	y2 = datas[2]
	y3 = datas[3]
	# Taille du graphique	
	plt.figure(figsize=(15,5))
	ax = plt.subplot(111)
	# On vire les traits en haut et à droite du graphique
	ax.spines["top"].set_visible(False)
	ax.spines["right"].set_visible(False)
	# On n'affiche les ticks qu'en bas et à gauche	
	ax.get_xaxis().tick_bottom()
	ax.xaxis.set_ticks_position("none")	
	ax.yaxis.set_ticks_position("none")
	# Nous voulons que nos légendes d'axes soient lisibles
	plt.xticks(fontsize=14)
	plt.yticks(fontsize=14)
	# Légendes et titres
	plt.title(graph_title, fontsize=18,fontweight="bold")
	plot(range(len(x)), y1, color="#ffc000", linewidth=5, linestyle="-", label="Exportations")
	plot(range(len(x)), y2, color="#ff0000", linewidth=5, linestyle="-", label="Importations")
	width = 0.5
	ax.bar(range(len(x)), y3, width, linewidth=10.0, edgecolor='#d9d9d9', facecolor='#4f81bd', align="center", label="Solde")
	plt.legend()
	ax.set_xticks(np.arange(len(x)))
	ax.set_xticklabels(x)
	
def simpleSourcedBars(prefixe,table,country_code,source,graph_title,xlabel,ylabel1,ylabel2,file_name):
	try:
		datas = simpleSourcedBarsCompileDatas(prefixe,table,country_code,source)
		try:
			drawSimpleBars(datas, country_code, graph_title,xlabel,ylabel1,ylabel2)
			try:
				saveGraphFile(file_name,country_code)
			except:
				print "Pas moyen d'enregistrer le graphique"
				pass
		except:
			print "Pas moyen de dessiner le graphique"
			pass
	except:
		print "Pas moyen de formater les données"
		pass

def mirroredBars(prefixe1,table1,prefixe2,table2,country_code,graph_title,xlabel,ylabel1,ylabel2,file_name):
	try:
		datas = mirroredBarsCompileDatas(prefixe1,table1,prefixe2,table2,country_code)
		try:
			drawMirroredBars(datas, country_code, graph_title,xlabel,ylabel1,ylabel2)
			try:
				saveGraphFile(file_name,country_code)
			except:
				print "Pas moyen d'enregistrer le graphique"
				pass
		except:
			print "Pas moyen de dessiner le graphique"
			pass
	except:
		print "Pas moyen de formater les données"
		pass

def douanesTerritoryMirroredBars(prefixe1,table1,prefixe2,table2,country_code,graph_title,xlabel,ylabel1,ylabel2,file_name):
	try:
		datas = douanesTerritoryMirroredBarsCompileDatas(prefixe1,table1,prefixe2,table2,country_code)
		try:
			drawMirroredBars(datas, country_code, graph_title,xlabel,ylabel1,ylabel2)
			try:
				saveGraphFile(file_name,country_code)
			except:
				print "Pas moyen d'enregistrer le graphique"
				pass
		except:
			print "Pas moyen de dessiner le graphique"
			pass
	except:
		print "Pas moyen de formater les données"
		pass

def enrobbedSourcedBars(prefixe,table,country_code,source,graph_title,xlabel,ylabel1,ylabel2,file_name):
	try:
		datas = simpleSourcedBarsCompileDatas(prefixe,table,country_code,source)
		try:
			drawEnrobbedBars(datas, country_code, graph_title,xlabel,ylabel1,ylabel2)
			try:
				saveGraphFile(file_name,country_code)
			except:
				print "Pas moyen d'enregistrer le graphique"
				pass
		except:
			print "Pas moyen de dessiner le graphique"
			pass
	except:
		print "Pas moyen de formater les données"
		pass

def tripleGraph(prefixe1,table1,prefixe2,table2,country_code,graph_title,file_name):
	try:
		datas = tripleGraphCompileDatas(prefixe1,table1,prefixe2,table2,country_code)
		try:
			drawTripleGraph(datas, country_code, graph_title)
			try:
				saveGraphFile(file_name,country_code)
			except:
				print "Pas moyen d'enregistrer le graphique"
				pass
		except:
			print "Pas moyen de dessiner le graphique"
			pass
	except:
		print "Pas moyen de formater les données"
		pass




for pays in listePays():
	simpleSourcedBars("ng","nations_gdp",pays,"Banque mondiale","PIB (Banque mondiale)",u"Années","Montants "," de dollars","PIB_BM_")
	enrobbedSourcedBars("ngg","nations_gdp_growth",pays,"Banque mondiale","Taux de croissance du PIB",u"Années","Pour cent","Truc","PIB_BM_growth_")
	mirroredBars("ke", "kiosque_exports", "ki", "kiosque_imports", pays,u"échanges commerciaux avec la France (DGT)",u"Années", "Montants", u" d'euros", "echanges_commerciaux_DGT_")
	douanesTerritoryMirroredBars("nea","nations_export_all","nia","nations_import_all",pays,u"échanges commerciaux avec la France (douanes)",u"Années", "Montants", u" d'euros", "echanges_commerciaux_douanes_")
	tripleGraph("ke", "kiosque_exports", "ki", "kiosque_imports", pays,u"Echanges entre la France et le pays","echanges_commerciaux_bis_")
