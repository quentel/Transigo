#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                          mo_UNSD_religions.py                              #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_general.py) les données qui
# nous intéressent depuis les dumps du système de statistiques des Nations Unies,
# que nous avons téléchargés avec dl_UNSD_religions.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, os
from functions import log, deLibelleENversAlpha2UNSD, deReligionENversReligionFR

log("")
log("========================================")
log("Lancement du script mo_UNSD_religions.py")
log("========================================")

# 2. SCRIPT

compteur = 0

# On a un seul fichier dans le dossier, mais son nom varie
path_UNSD  = "datas/unsd/religions/"

liste_fichiers_UNSD = os.listdir(path_UNSD)

for fichier in liste_fichiers_UNSD:
	
	log("Traitement du fichier %s." % fichier)		
	# On ouvre le fichier	
	UNSD_path = path_UNSD+fichier
	mon_fichier_UNSD = open(UNSD_path, "rb")
	reader_UNSD = csv.reader(mon_fichier_UNSD, delimiter=';')
	
	liste = list()	
	compteur = 0
	# Et on le lit ligne à ligne
	for row in reader_UNSD:
		try:		
			if row[3] == "Both Sexes":
				country_name = row[0]
				year = row[1]
				area = row[2]
				label_en = row[4]
				label_fr = deReligionENversReligionFR(label_en)
				value = row[8]
				country_code = deLibelleENversAlpha2UNSD(country_name)
				value = int(value)
				year = int(year)
				if area == "Total":
					conn = sqlite3.connect("datas/bases/general.db")
 					conn.execute("INSERT INTO nations_religions (nr_labels, nr_years, nr_values, nr_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(unicode(label_fr), year, value, country_code))						
					conn.commit()
					compteur = compteur+1
		except:
			pass		
	mon_fichier_UNSD.close()
		
	# Et on supprime le fichier traité
	path_to_rm = path_UNSD+fichier
	os.remove(path_to_rm)
	
log("%s données sur les religions entrées dans notre base" % compteur)