#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                         mo_UNSD_languages.py                               #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_general.py) les données qui
# nous intéressent depuis les dumps du système de statistiques des Nations Unies,
# que nous avons téléchargés avec dl_UNSD_languages.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, os, sys  
from functions import log, deLibelleENversAlpha2UNSD, listeLanguesEN, deLangueENversLangueFR

reload(sys)  
sys.setdefaultencoding('utf8')

log("")
log("========================================")
log("Lancement du script mo_UNSD_languages.py")
log("========================================")

# 2. SCRIPT

compteur = 0

# On a un seul fichier dans le dossier, mais son nom varie
path_UNSD  = "datas/unsd/languages/"

liste_fichiers_UNSD = os.listdir(path_UNSD)

for fichier in liste_fichiers_UNSD:
	
	log("Traitement du fichier %s." % fichier)		
	# On ouvre le fichier	
	UNSD_path = path_UNSD+fichier
	mon_fichier_UNSD = open(UNSD_path, "rb")
	reader_UNSD = csv.reader(mon_fichier_UNSD, delimiter=';')
	
	liste = list()	
	compteur = 0
	# Et on le lit ligne à ligne
	list_lang = list()
	for row in reader_UNSD:
		try:		
			if row[3] == "Both Sexes":
				country_name = row[0]
				year = row[1]
				language_name = row[4]
				pop = row[8]
				list_lang.append(language_name)
				country_code = deLibelleENversAlpha2UNSD(country_name)
				# Un try/except afin de gérer d'étranges fractions de locuteurs présentes dans les bases de données de l'ONU...			
				try:
					pop = int(pop)
				except:
					pop = float(pop)
					pop = int(pop)
				year = int(year)
				# Un try/except afin de gérer les langues dont le nom s'écrit en caractères diacritiques, même en anglais				
				try:
					language_unicode = unicode(language_name)
				except:
					language_name = unicode(language_name).encode('utf-8')
					language_unicode = unicode(language_name)
				if language_unicode in listeLanguesEN():
					language_name = deLangueENversLangueFR(language_name)
				# Voyons si cette langue existe déjà dans la base de données
				conn = sqlite3.connect("datas/bases/general.db")
 				cur = conn.cursor()
				cur.execute("SELECT nl_years from nations_languages where nl_ISO_3166_1_alpha_2 = ? and nl_labels = ?", (country_code,language_unicode))		
				presence = len(cur.fetchall())
				if presence == 0:
					# Si cette langue n'existe pas dans la base de données, on l'insère
					conn.execute("INSERT INTO nations_languages (nl_labels, nl_values, nl_years, nl_ISO_3166_1_alpha_2) VALUES (?, ?, ?, ?)",(language_unicode, pop, year, country_code))						
					conn.commit()
					compteur = compteur+1
				else:
					# Si cette langue existe dans la base de données, voyons quelle est la fraicheur de ses informations				
					for row in cur:
						old_year = row[0]
						old_year = int(old_year)
						if year > old_year:
							conn.execute("UPDATE nations_languages set nl_values = ?, nl_years = ? where nl_ISO_3166_1_alpha_2 = ?", (country_code)) 
							conn.commit()	
																
		except:
			pass

	mon_fichier_UNSD.close()
		
	# Et on supprime le fichier traité
	path_to_rm = path_UNSD+fichier
	os.remove(path_to_rm)
	
 #log("%s langues entrées dans notre base" % compteur)