#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                             mo_Kiosque.py                                  #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_general.py) les données qui
# nous intéressent dans les fiches pays du Kiosque de Bercy, que nous avons 
# téléchargées avec dl_Kiosque.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, os, decimal
from functions import log

log("")
log("=================================")
log("Lancement du script mo_Kiosque.py")
log("=================================")

# 2. SCRIPT

compteur = 0

# On ouvre tous les fichiers, un par un
path_Kiosque  = "datas/kiosque/"

liste_fichiers_Kiosque = os.listdir(path_Kiosque)

for fichier in liste_fichiers_Kiosque:
	
	# On traite d'abord les importations
	if fichier.find("imports")>-1:
		log("Traitement du fichier %s." % fichier)		
		# On retrouve le code du territoire concerné
		code_territoire = fichier[8:]
		code_territoire = code_territoire[:2]
		# On ouvre le fichier	
		mon_Kiosque_path = path_Kiosque+fichier
		mon_fichier_Kiosque = open(mon_Kiosque_path, "rb")
		reader_Kiosque = csv.reader(mon_fichier_Kiosque, delimiter=';')
		rownum = 0
		
		for row in reader_Kiosque:
				
			if rownum == 4:
				annee1 = row[2]
				print "Annee 1 "+annee1
				annee2 = row[3]
				print "Annee 2 "+annee2
				annee3 = row[4]
				print "Annee 3 "+annee2
				
			if rownum == 5:
				valeur1 = row[2]
				valeur2 = row[3]
				valeur3 = row[4]
			
			rownum = rownum+1

		valeur1 = int(valeur1)
		valeur2 = int(valeur2)
		valeur3 = int(valeur3)
		valeur1 = valeur1*1000
		valeur2 = valeur2*1000
		valeur3 = valeur3*1000
		conn = sqlite3.connect("datas/bases/general.db")
		conn.execute("INSERT INTO kiosque_imports (ki_ISO_3166_1_alpha_2, ki_years, ki_values) VALUES (?, ?, ?)",(code_territoire,annee1,valeur1))
		conn.commit()
		conn.execute("INSERT INTO kiosque_imports (ki_ISO_3166_1_alpha_2, ki_years, ki_values) VALUES (?, ?, ?)",(code_territoire,annee2,valeur2))
		conn.commit()
		conn.execute("INSERT INTO kiosque_imports (ki_ISO_3166_1_alpha_2, ki_years, ki_values) VALUES (?, ?, ?)",(code_territoire,annee3,valeur3))
		conn.commit()
		conn.close()
		compteur = compteur+3
		
		mon_fichier_Kiosque.close()
	
	# On traite ensuite les exportations
	if fichier.find("exports")>-1:
		log("Traitement du fichier %s." % fichier)		
		# On retrouve le code du territoire concerné
		code_territoire = fichier[8:]
		code_territoire = code_territoire[:2]
		# On ouvre le fichier	
		mon_Kiosque_path = path_Kiosque+fichier
		mon_fichier_Kiosque = open(mon_Kiosque_path, "rb")
		reader_Kiosque = csv.reader(mon_fichier_Kiosque, delimiter=';')
		rownum = 0
		
		for row in reader_Kiosque:
				
			if rownum == 4:
				annee1 = row[2]
				annee2 = row[3]
				annee3 = row[4]
				
			if rownum == 5:
				valeur1 = row[2]
				valeur2 = row[3]
				valeur3 = row[4]
			
			rownum = rownum+1

		valeur1 = int(valeur1)
		valeur2 = int(valeur2)
		valeur3 = int(valeur3)
		valeur1 = valeur1*1000
		valeur2 = valeur2*1000
		valeur3 = valeur3*1000
		conn = sqlite3.connect("datas/bases/general.db")
		conn.execute("INSERT INTO kiosque_exports (ke_ISO_3166_1_alpha_2, ke_years, ke_values) VALUES (?, ?, ?)",(code_territoire,annee1,valeur1))
		conn.commit()
		conn.execute("INSERT INTO kiosque_exports (ke_ISO_3166_1_alpha_2, ke_years, ke_values) VALUES (?, ?, ?)",(code_territoire,annee2,valeur2))
		conn.commit()
		conn.execute("INSERT INTO kiosque_exports (ke_ISO_3166_1_alpha_2, ke_years, ke_values) VALUES (?, ?, ?)",(code_territoire,annee3,valeur3))
		conn.commit()
		conn.close()
		compteur = compteur+3
		
	# Et on supprime le fichier traité
	path_to_rm = path_Kiosque+fichier
	os.remove(path_to_rm)
	
log("%s données sur les échanges entre la France et ses partenaires saisies dans notre base" % compteur)