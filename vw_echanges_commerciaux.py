#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                        vw_echanges_commerciaux.py                          #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script crée un fichier Excel présentant sous forme de tableaux synthétiques les 
# principales données issues de notre base construites à partir des données de la douane
# d'une part, et de la direction générale du Trésor d'une part.

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, math, locale
from xlwt import Workbook
from functions import *

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

log("")
log("==============================================")
log("Lancement du script vw_echanges_commerciaux.py")
log("==============================================")

# 2. FONCTIONS UTILES

path = "output/echanges_commerciaux/tableaux_synthetiques/"

# Si le répertoire n'existe pas, créons-le
tableau_dir = os.path.dirname(path)
if not os.path.exists(tableau_dir):
	log("Création du répertoire destiné à accueillir notre tableau : output/echanges_commerciaux/tableaux_synthetiques/")
	os.makedirs(tableau_dir)
 
# 3. SCRIPT

# Création du fichier
book = Workbook()

# 3.1. Feuille "Importations (territoires) - DGT"
# D'abord les importations calculées par la direction générale du Trésor, dans sa nomenclature des territoires

# Création de la feuille
feuil1 = book.add_sheet(u'Importations (territoires) DGT')

# Création des en-tête
feuil1.write(0,0,'Code UE')
feuil1.write(0,1,'Nom du territoire')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years = list()
for territoire in listeTerritoiresUE():
	years = getAllYears("ki","kiosque_imports",territoire)
	for year in years:
		my_years.append(year)
# Suppression des doublons dans la liste
my_years = set(my_years)
mes_annees = []
for y in my_years:
	mes_annees.append(y)
mes_annees.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in mes_annees:
	# Largeur des colonnes
	feuil1.col(num_colonne).width = 4000
	feuil1.write(0,num_colonne,year)
	compteur_ligne = 1
	for territoire in listeTerritoiresUE():
		ligne = feuil1.row(compteur_ligne)
		valeur = getDataValue("ki","kiosque_imports",territoire,year)
		ligne.write(num_colonne,valeur)
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	

# Création, enfin, des libellés territoire
compteur = 1
for territoire in listeTerritoiresUE():
	ligne = feuil1.row(compteur)
	ligne.write(0,territoire)
	# Récupération du nom du pays en français
	lib_territoire = deCodeUEversLibelleFR(territoire)
	ligne.write(1,lib_territoire)
	compteur = compteur+1

feuil1.col(1).width = 10500 # Largeur de la colonne "Nom du territoire"

# 3.2. Feuille "Exportations (territoires) - DGT"
# Ensuite les exportations calculées par la direction générale du Trésor, dans sa nomenclature des territoires

# Création de la feuille
feuil2 = book.add_sheet(u'Exportations (territoires) DGT')

# Création des en-tête
feuil2.write(0,0,'Code UE')
feuil2.write(0,1,'Nom du territoire')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years = list()
for territoire in listeTerritoiresUE():
	years = getAllYears("ke","kiosque_exports",territoire)
	for year in years:
		my_years.append(year)
# Suppression des doublons dans la liste
my_years = set(my_years)
mes_annees = []
for y in my_years:
	mes_annees.append(y)
mes_annees.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in mes_annees:
	# Largeur des colonnes
	feuil2.col(num_colonne).width = 4000
	feuil2.write(0,num_colonne,year)
	compteur_ligne = 1
	for territoire in listeTerritoiresUE():
		ligne = feuil2.row(compteur_ligne)
		valeur = getDataValue("ke","kiosque_exports",territoire,year)
		ligne.write(num_colonne,valeur)
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	

# Création, enfin, des libellés territoire
compteur = 1
for territoire in listeTerritoiresUE():
	ligne = feuil2.row(compteur)
	ligne.write(0,territoire)
	# Récupération du nom du pays en français
	lib_territoire = deCodeUEversLibelleFR(territoire)
	ligne.write(1,lib_territoire)
	compteur = compteur+1

feuil2.col(1).width = 10500 # Largeur de la colonne "Nom du territoire"

# 3.3. Feuille "Importations (territoires) - Douanes"
# D'abord les importations calculées par la direction générale des Douanes, dans sa nomenclature des territoires

# Création de la feuille
feuil3 = book.add_sheet(u'Importations (territoires) DGD')

# Création des en-tête
feuil3.write(0,0,'Code UE')
feuil3.write(0,1,'Nom du territoire')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years = list()
for territoire in listeTerritoiresUE():
	years = getAllYearsinDouanesTerritory("nia","nations_import_all",territoire)
	for year in years:
		my_years.append(year)
# Suppression des doublons dans la liste
my_years = set(my_years)
mes_annees = []
for y in my_years:
	mes_annees.append(y)
mes_annees.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in mes_annees:
	# Largeur des colonnes
	feuil3.col(num_colonne).width = 4000
	feuil3.write(0,num_colonne,year)
	compteur_ligne = 1
	for territoire in listeTerritoiresUE():
		ligne = feuil3.row(compteur_ligne)
		valeur = getDataValueinDouanesTerritory("nia","nations_import_all",territoire,year)
		ligne.write(num_colonne,valeur)
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	

# Création, enfin, des libellés territoire
compteur = 1
for territoire in listeTerritoiresUE():
	ligne = feuil3.row(compteur)
	ligne.write(0,territoire)
	# Récupération du nom du pays en français
	lib_territoire = deCodeUEversLibelleFR(territoire)
	ligne.write(1,lib_territoire)
	compteur = compteur+1

feuil3.col(1).width = 10500 # Largeur de la colonne "Nom du territoire"

# 3.4. Feuille "Exportations (territoires) - Douanes"
# Les exportations calculées par la direction générale des Douanes, dans sa nomenclature des territoires

# Création de la feuille
feuil4 = book.add_sheet(u'Exportations (territoires) DGD')

# Création des en-tête
feuil4.write(0,0,'Code UE')
feuil4.write(0,1,'Nom du territoire')

# Création d'une colonne par année pour laquelle nous disposons d'au moins une donnée...
my_years = list()
for territoire in listeTerritoiresUE():
	years = getAllYearsinDouanesTerritory("nea","nations_export_all",territoire)
	for year in years:
		my_years.append(year)
# Suppression des doublons dans la liste
my_years = set(my_years)
mes_annees = []
for y in my_years:
	mes_annees.append(y)
mes_annees.sort()

# On remplit d'abord les colonnes de données
num_colonne = 2
for year in mes_annees:
	# Largeur des colonnes
	feuil4.col(num_colonne).width = 4000
	feuil4.write(0,num_colonne,year)
	compteur_ligne = 1
	for territoire in listeTerritoiresUE():
		ligne = feuil4.row(compteur_ligne)
		valeur = getDataValueinDouanesTerritory("nea","nations_export_all",territoire,year)
		ligne.write(num_colonne,valeur)
		compteur_ligne = compteur_ligne+1
	num_colonne = num_colonne+1	

# Création, enfin, des libellés territoire
compteur = 1
for territoire in listeTerritoiresUE():
	ligne = feuil4.row(compteur)
	ligne.write(0,territoire)
	# Récupération du nom du pays en français
	lib_territoire = deCodeUEversLibelleFR(territoire)
	ligne.write(1,lib_territoire)
	compteur = compteur+1

feuil4.col(1).width = 10500 # Largeur de la colonne "Nom du territoire"

book.save("output/echanges_commerciaux/tableaux_synthetiques/echanges_commerciaux.xls")