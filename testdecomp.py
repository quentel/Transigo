#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from libarchive import public

dl_zip = "tmp/donnees.7z"
path_unzipped = "decompresse/"									
# Si le répertoire n'existe pas, on le crée
if not os.path.exists(path_unzipped):
	os.makedirs(path_unzipped)
print dl_zip				
with libarchive.public.file_reader(dl_zip) as e:
	for entry in e:
		with open(path_unzipped + str(entry), 'wb') as f:
			for block in entry.get_blocks():
				f.write(block)

#libarchive.extract_file(dl_zip)
