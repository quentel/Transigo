#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                dl_PNUD.py                                  #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge la dernière version des annexes statistiques au rapport
# annuel du PNUD, mis-à-jour une fois par an

# 1. IMPORTATION DES MODULES NECESSAIRES

import time, datetime, os, sys, urllib2
from bs4 import BeautifulSoup
from functions import log

log("")
log("==============================")
log("Lancement du script dl_PNUD.py")
log("==============================")

# Le PNUD publie bel et bien les données de son programme "Indicateur du développement humain" chaque année sur une
# interface programmatiquement accessible... Mais jamais son dernier jeu : l'année en cours n'est disponible que
# dans le texte du rapport lui-même et sur un fichier Excel à télécharger sur son site - dont l'adresse et la configuration
# changent chaque année. Le présent script - ainsi que mo_PNUD.py doivent donc être modifiés tous les ans !

# 2. SCRIPT

# Nom et chemin du fichier que nous allons créer
dl_path = "datas/pnud/"
dl_file = dl_path+"last_PNUD_stats.xls"

# Si le répertoire n'existe pas, créons-le
dl_dir = os.path.dirname(dl_path)
if not os.path.exists(dl_path):
	log("Création du répertoire destiné à accueillir notre fichier : datas/pnud/")
	os.makedirs(dl_dir)

# C'est cet URL qui change tous les ans
URL_a_tester =	"http://hdr.undp.org/sites/default/files/tables_1-15_6a_dashboard1_dashboard2_online_version.xlsx"

# Le serveur du PNUD bloque les programmes comme le nôtre. Nous devons contourner cette interdiction en
# lui faisant croire que nous utilisons un navigateur standard
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko)',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
		'Accept-Encoding': 'none',
		'Accept-Language': 'en_US,en;q=0.8',
		'Connection': 'keep-alive'}

try:	
	log("Tentative de téléchargement sur l'URL :"+URL_a_tester)
	req = urllib2.Request(URL_a_tester, headers=hdr)
	with open(dl_file,'w') as f:
		f.write(urllib2.urlopen(req).read())
	f.close()
	log("Téléchargement des dernières statistiques du PNUD terminé !")
except:
	log("===ERREUR : l'adresse du fichier .xlsx stockant les données du PNUD a changé...")	
	#date_a_tester = date_a_tester - datetime.timedelta(days = 364)		
	pass