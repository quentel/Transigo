#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                            dl_WITS-API.py                                  #
#                                                                            #
#                        Christophe Quentel, 2017                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge toutes les fiches pays du World Integrated Trade Solution
# (WITS), le programme de la Banque mondiale qui collecte et diffuse des statistiques
# sur le commerce international, en utilisant l'interface de programmation (API)
# proposée depuis 2016 par cet organisme.

# 1. IMPORTATION DES MODULES NECESSAIRES

import urllib2, os, sys
from functions import log, listePays, deAlpha2versAlpha3

log("")
log("==================================")
log("Lancement du script dl_WITS_API.py")
log("==================================")

# 2. SCRIPT

# Nom et chemin des répertoires où nous allons stocker nos fichiers
dl_path_pdm_import = "datas/wits/pdm/importations/"
# Si le répertoire n'existe pas, créons-le
dl_dir_pdm_import = os.path.dirname(dl_path_pdm_import)
if not os.path.exists(dl_path_pdm_import):
	log("Création du répertoire destiné à accueillir notre fichier : datas/wits/pdm/importations/")
	os.makedirs(dl_dir_pdm_import)

dl_path_pdm_export = "datas/wits/pdm/exportations/"
# Si le répertoire n'existe pas, créons-le
dl_dir_pdm_export = os.path.dirname(dl_path_pdm_export)
if not os.path.exists(dl_path_pdm_export):
	log("Création du répertoire destiné à accueillir notre fichier : datas/wits/pdm/exportations/")
	os.makedirs(dl_dir_pdm_export)
	
dl_path_valeur_import_bilat = "datas/wits/valeurs/importationsdepuislafrance/"
# Si le répertoire n'existe pas, créons-le
dl_dir_valeur_import_bilat = os.path.dirname(dl_path_valeur_import_bilat)
if not os.path.exists(dl_path_valeur_import_bilat):
	log("Création du répertoire destiné à accueillir notre fichier : datas/wits/valeurs/importationsdepuislafrance/")
	os.makedirs(dl_path_valeur_import_bilat)
	
dl_path_valeur_export_bilat = "datas/wits/valeurs/exportationsverslafrance/"
# Si le répertoire n'existe pas, créons-le
dl_dir_valeur_export_bilat = os.path.dirname(dl_path_valeur_export_bilat)
if not os.path.exists(dl_path_valeur_export_bilat):
	log("Création du répertoire destiné à accueillir notre fichier : datas/wits/valeurs/exportationsverslafrance/")
	os.makedirs(dl_path_valeur_export_bilat)

# On traite tous les pays tour à tour
for pays in listePays():
	code_pays = deAlpha2versAlpha3(pays)
	dl_URL_pdm_import = "http://wits.worldbank.org/API/V1/SDMX/V21/datasource/tradestats-trade/reporter/"+code_pays+"/year/ALL/partner/ALL/product/ALL/indicator/MPRT-PRTNR-SHR"
	dl_URL_pdm_export = "http://wits.worldbank.org/API/V1/SDMX/V21/datasource/tradestats-trade/reporter/"+code_pays+"/year/ALL/partner/ALL/product/ALL/indicator/XPRT-PRTNR-SHR"
	dl_URL_valeur_import_bilat = "http://wits.worldbank.org/API/V1/SDMX/V21/datasource/tradestats-trade/reporter/"+code_pays+"/year/ALL/partner/fra/product/ALL/indicator/MPRT-TRD-VL"
	dl_URL_valeur_export_bilat = "http://wits.worldbank.org/API/V1/SDMX/V21/datasource/tradestats-trade/reporter/"+code_pays+"/year/ALL/partner/fra/product/ALL/indicator/XPRT-TRD-VL"
	
	dl_file_pdm_import = dl_path_pdm_import+"pdm_importations_"+pays
	dl_file_pdm_export = dl_path_pdm_export+"pdm_exportations_"+pays
	dl_file_valeur_import_bilat = dl_path_valeur_import_bilat+"valeur_importations_bilat_"+pays
	dl_file_valeur_export_bilat = dl_path_valeur_export_bilat+"valeur_exportations_bilat_"+pays

	#log("Tentative de telechargement des donnees WITS pour "+pays)	

	try:
		with open(dl_file_pdm_import,'w') as f:
			f.write(urllib2.urlopen(dl_URL_pdm_import).read())
		f.close()
		log("Succes du telechargement des parts de marche dans les importations")
	except:
		log("=== Echec  du telechargement des parts de marche dans les importations !")		
		pass
	
	try:
		with open(dl_file_pdm_export,'w') as f:
			f.write(urllib2.urlopen(dl_URL_pdm_export).read())
		f.close()
		log("Succes  du telechargement des parts de marche dans les exportations")
	except:
		log("=== Echec  du telechargement des parts de marche dans les exportations !")		
		pass

	try:
		with open(dl_file_valeur_import_bilat,'w') as f:
			f.write(urllib2.urlopen(dl_URL_valeur_import_bilat).read())
		f.close()
		log("Succes  du telechargement de la valeur de la France dans les importations")
	except:
		log("=== Echec  du telechargement de la valeur de la France dans les importations !")		
		pass

	try:
		with open(dl_file_valeur_export_bilat,'w') as f:
			f.write(urllib2.urlopen(dl_URL_valeur_export_bilat).read())
		f.close()
		log("Succes  du telechargement de la valeur de la France dans les exportations")
	except:
		log("=== Echec  du telechargement de la valeur de la France dans les exportations !")		
		pass