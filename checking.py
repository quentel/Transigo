#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                checking.py                                 #
#                                                                            #
#                        Christophe Quentel, 2015-2016                       #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script vérifie que les modules complémentaires nécessaires au fonctionnement de
# Transigo sont bien installés dans le système     for i in installed_packages])     for i in installed_packages])

# 1. IMPORTATION DES MODULES NECESSAIRES

import pip
from functions import log

# 2. LANCEMENT

log("===============================")
log("Lancement du script checking.py")
log("===============================")

# 3. TUPLE DES MODULES REQUIS

#needed = ("beautifulsoup4", "python-docx", "matplotlib", "xlrd", "xlwt")
needed = ("beautifulsoup4", "python-docx", "xlrd", "xlwt","matplotlib","pylab","numpy")

# 4. LISTE DES MODULES INSTALLES

installed_packages = pip.get_installed_distributions()
installed_packages_list = sorted(["%s" % (i.key)
     for i in installed_packages])

# 5. VERIFICATION DE LA PRESENCE DES MODULES REQUIS DANS LA LISTE DES MODULES INSTALLES

for package in needed:
	if package in installed_packages_list:
		log("Le module %s est bien installé sur cet ordinateur" % package)
	else:
		log("=== ATTENTION : le module %s n'est pas installé sur cet ordinateur, alors qu'il est requis pour le fonctionnement de Transigo !" % package)
