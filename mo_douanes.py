#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                             mo_douanes.py                                  #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe, puis stocke dans notre base les données les données publiées par les Douanes
# françaises et téléchargées par notre script dl_douane.py. Il commute au passage les références
# à la nomenclature NC8 en références à la nomenclature NA 129.

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, os, subprocess, csv, inspect, re
from functions import log, deNC8versNA129

# 2. FONCTIONS

# 2.1. Injection simple d'un fichier dans une base douanière (quand les produits n'apparaissent pas)
def injectionSimple(fichier,table,prefixe,duree):
	csv_reader = csv.DictReader(fichier)
	for row in csv_reader:
		partner = row["partner"]
		year = row["year"]
		if duree > 0:
			year = year+" ("+duree+" mois)"
		value = row["x"]
		conn = sqlite3.connect("datas/bases/douanes.db")
		conn.execute("INSERT INTO "+table+" ("+prefixe+"_eu_codes, "+prefixe+"_years, "+prefixe+"_values) VALUES (?, ?, ?)",(partner,year,value))
		conn.commit()
		conn.close()

# 2.2. Injection conditionnelle d'un fichier dans une base douanière (quand les produits sont détaillés)
def injectionComplexe(fichier,table,prefixe,duree):
	csv_reader = csv.DictReader(fichier)
	for row in csv_reader:
		partner = row["partner"]
		year = row["year"]
		if duree > 0:
			year = year+" ("+duree+" mois)"
		value = row["x"]
		value = float(value)
		nc8 = row["nc8"]
		na129 = deNC8versNA129(nc8,year)
		# Notre base contient-elle déjà une saisie pour le type de produit, le partenaire et l'année considérés ?
		conn = sqlite3.connect("datas/bases/douanes.db")
		cur = conn.cursor()						
		cur.execute("SELECT "+prefixe+"_values from "+table+" where "+prefixe+"_eu_codes = ? and "+prefixe+"_years = ? and "+prefixe+"_na129 = ?", (partner,year,na129))		
		if len(cur.fetchall()) == 0:
			# Si l'entrée n'existe pas déjà dans la table, on la saisit							
			conn.execute("INSERT INTO "+table+" ("+prefixe+"_eu_codes, "+prefixe+"_years, "+prefixe+"_values, "+prefixe+"_na129) VALUES (?, ?, ?, ?)",(partner,year,value,na129))
			conn.commit()
			conn.close()
		else:
			# Si l'entrée existe déjà dans la base, on récupère sa valeur, qu'on additionne à celle que nous souhaitons insérer, puis nous mettons à jour l'entrée				
			cur.execute("SELECT "+prefixe+"_values from "+table+" where "+prefixe+"_eu_codes = ? and "+prefixe+"_years = ? and "+prefixe+"_na129 = ?", (partner,year,na129))			
			for row in cur:
				valeur_prec = row[0]
				valeur_a_MAJ = valeur_prec+value
				conn2 = sqlite3.connect("datas/bases/douanes.db")					
				conn2.execute("UPDATE "+table+" set "+prefixe+"_values = ? where "+prefixe+"_eu_codes = ? and "+prefixe+"_years = ? and "+prefixe+"_na129 = ?",(valeur_a_MAJ,partner,year,na129))
				conn2.commit()
				conn2.close()

# 2.3. Parlons-nous de données sur une année, ou sur quelques mois ?
def quelleDuree(maChaine):
	if "_mois" in maChaine:
		# Trouvons le nombre de mois
		nombre = re.findall("\d+", maChaine)		
		duree = nombre[0]
	else:
		duree = 0	
	return duree

# 2.4 Mettre à jour une date
def updateDate(table,prefixe,last_year, complete_date):
	conn = sqlite3.connect("datas/bases/douanes.db")
	cur = conn.cursor()						
	conn.execute("UPDATE "+table+" set "+prefixe+"_years = ? where "+prefixe+"_years = ?",(complete_date,last_year))
	conn.commit()
	conn.close()


# 3. SCRIPT

# Les fichiers des douanes sont très lourds. Nous utilisons donc des scripts en langage R pour préparer
# leur traitement.

# Identifions déjà le chemin de nos fichiers, pour que R n'ait pas à le faire (il n'est vraiment, vraiment pas bon à ce jeu-là !)
my_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

# 3.1 Préparation des données

# Lancer mo_douanes_cleaning.r
log("")
log("=========================================")
log("Lancement du script mo_douanes_cleaning.r")
log("=========================================")
cleaningString = "/usr/bin/Rscript --vanilla mo_douanes_cleaning.r "+my_path
subprocess.call(cleaningString, shell=True)

# Lancer mo_douanes_last_12_months.r
log("")
log("===============================================")
log("Lancement du script mo_douanes_last_12_months.r")
log("===============================================")
last12String = "/usr/bin/Rscript --vanilla mo_douanes_last_12_months.r "+my_path
subprocess.call(last12String, shell=True)

# Lancer mo_douanes_first_months.r
log("")
log("===============================================")
log("Lancement du script mo_douanes_first_months.r")
log("===============================================")
firstMonthsString = "/usr/bin/Rscript --vanilla mo_douanes_first_months.r "+my_path
subprocess.call(firstMonthsString, shell=True)


# Lancer mo_douanes_datamining.r
log("")
log("=========================================")
log("Lancement du script mo_douanes_datamining.r")
log("=========================================")
dataminingString = "/usr/bin/Rscript --vanilla mo_douanes_datamining.r "+my_path
subprocess.call(dataminingString, shell=True)

# Traitement des fichiers obtenus

log("")
log("=================================")
log("Lancement du script mo_douanes.py")
log("=================================")

# 3.2. Injection en base de données par pays

chemin_douanier = "datas/douanes/"
liste_fichiers_douane = os.listdir(chemin_douanier)
liste_fichiers_douane.sort(reverse = True)

compteur_donnees = 0
total_imports =                      "importations_by_years_and_partners"
total_exports =                      "exportations_by_years_and_partners"
detailed_imports =                   "importations_by_years_products_and_partners"
detailed_exports =                   "exportations_by_years_products_and_partners"

last_12_months_total_imports =       "sub_importations_last_12_months_by_partners.csv"
last_12_months_total_exports =       "sub_exportations_last_12_months_by_partners.csv"
last_12_months_detailed_imports =    "sub_importations_last_12_months_by_partners_and_products.csv"
last_12_months_detailed_exports =    "sub_exportations_last_12_months_by_partners_and_products.csv"
prev_12_months_total_imports =       "sub_importations_prev_12_months_by_partners.csv"
prev_12_months_total_exports =       "sub_exportations_prev_12_months_by_partners.csv"
prev_12_months_detailed_imports =    "sub_importations_prev_12_months_by_partners_and_products.csv"
prev_12_months_detailed_exports =    "sub_exportations_prev_12_months_by_partners_and_products.csv"

last_first_months_total_imports =    "sub_importations_last_first_months_by_partners"
last_first_months_total_exports =    "sub_exportations_last_first_months_by_partners"
last_first_months_detailed_imports = "sub_importations_last_first_months_by_partners_and_products"
last_first_months_detailed_exports = "sub_exportations_last_first_months_by_partners_and_products"
prev_first_months_total_imports =    "sub_importations_prev_first_months_by_partners"
prev_first_months_total_exports =    "sub_exportations_prev_first_months_by_partners"
prev_first_months_detailed_imports = "sub_importations_prev_first_months_by_partners_and_products"
prev_first_months_detailed_exports = "sub_exportations_prev_first_months_by_partners_and_products"



for fichier in liste_fichiers_douane:
	log("Traitement du fichier : "+fichier)
	chemin_fichier = chemin_douanier+fichier
	with open(chemin_fichier) as fichier_douanier:
		# Si ce fichier traite des importations totales sur une année
###### TESTE ######
		if total_imports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_import_all","nia",duree)			

		# Si ce fichier traite des exportations totales sur une année
###### PROBLEME : ON N'A QUE 2012 ET 2013 !!!!
		elif total_exports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_export_all","nea",duree)			
				
#		# Si ce fichier traite des importations par produits sur une année
#		elif detailed_imports in fichier :
#			duree = quelleDuree(fichier)
#			injectionComplexe(fichier_douanier,"nations_import","ni",duree)			
		
		# Si ce fichier traite des exportations par produits sur une année
#		elif detailed_exports in fichier :
#			duree = quelleDuree(fichier)
#			injectionComplexe(fichier_douanier,"nations_export","ne",duree)			

		# Si ce fichier traite des importations totales sur les 12 derniers mois
###### TESTE ######
		elif last_12_months_total_imports in fichier :
			duree = quelleDuree(fichier)
			print duree
			injectionSimple(fichier_douanier,"nations_import_last_12_months_all","nia12",duree)			

		# Si ce fichier traite des exportations totales sur les 12 derniers mois
###### TESTE ######
		elif last_12_months_total_exports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_export_last_12_months_all","nea12",duree)			
				
		# Si ce fichier traite des importations par produits sur les 12 derniers mois
###### TESTE #####
#		elif last_12_months_detailed_imports in fichier :
#			injectionComplexe(fichier_douanier,"nations_import_last_12_months","ni12")			
		
		# Si ce fichier traite des exportations par produits sur les 12 derniers mois
##### TESTE ######
#		elif last_12_months_detailed_exports in fichier :
#			injectionComplexe(fichier_douanier,"nations_export_last_12_months","ne12")			

		# Si ce fichier traite des importations totales sur les 12 mois précédents
###### TESTE ######
		elif prev_12_months_total_imports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_import_prev_12_months_all","niap12",duree)			

		# Si ce fichier traite des exportations totales sur les 12 mois précédents
###### TESTE ######
		elif prev_12_months_total_exports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_export_prev_12_months_all","neap12",duree)			
				
		# Si ce fichier traite des importations par produits sur les 12 mois précédents
###### TESTE ####
#		elif prev_12_months_detailed_imports in fichier :
#			injectionComplexe(fichier_douanier,"nations_import_prev_12_months","ni12p")			
		
		# Si ce fichier traite des exportations par produits sur les 12 mois précédents
###### TESTE ##
#		elif prev_12_months_detailed_exports in fichier :
#			injectionComplexe(fichier_douanier,"nations_export_prev_12_months","ne12p")			
	
		# Si ce fichier traite des importations totales sur les premiers mois de l'année
#### TESTE 
		elif last_first_months_total_imports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_import_first_months_all","niaf",duree)			

		# Si ce fichier traite des exportations totales sur les premiers mois de l'année
##### TESTE
		elif last_first_months_total_exports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_export_first_months_all","neaf",duree)			
				
		# Si ce fichier traite des importations par produits sur les premiers mois de l'année
#		elif last_first_months_detailed_imports in fichier :
#			injectionComplexe(fichier_douanier,"nations_import_first_months","nif")			
		
		# Si ce fichier traite des exportations par produits sur les premiers mois de l'année
#		elif last_first_months_detailed_exports in fichier :
#			injectionComplexe(fichier_douanier,"nations_export_first_months","nef")			

		# Si ce fichier traite des importations totales sur les premiers mois de l'année précédente
###### TESTE 
		elif prev_first_months_total_imports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_import_prev_months_all","niap",duree)			

		# Si ce fichier traite des exportations totales sur les premiers mois de l'année précédente
##### TESTE
		elif prev_first_months_total_exports in fichier :
			duree = quelleDuree(fichier)
			injectionSimple(fichier_douanier,"nations_export_prev_months_all","neap",duree)			
				
		# Si ce fichier traite des importations par produits sur les premiers mois de l'année précédente
#		elif prev_first_months_detailed_imports in fichier :
#			injectionComplexe(fichier_douanier,"nations_import_prev_months","nip")			
		
		# Si ce fichier traite des exportations par produits sur les 12 premiers mois de l'année précédente
#		elif prev_first_months_detailed_exports in fichier :
#			injectionComplexe(fichier_douanier,"nations_export_prev_months","nep")			
						
	# Et on supprime le fichier traité
	os.remove(chemin_fichier)

# 3.3 Mise à jour du nombre de mois pour la dernière année dans toutes les tables qui ne la précisent pas déjà

conn = sqlite3.connect("datas/bases/douanes.db")

# Récupérer l'intitulé dans les tables "first months"
cur = conn.cursor()					
cur.execute("SELECT max(niaf_years) AS 'Most_Recent_Year' from nations_import_first_months_all")
for row in cur:
	last_value = row[0]

# Récupérer la dernière année des autres tables
cur2 = conn.cursor()					
cur2.execute("SELECT max(ne_years) AS 'Most_Recent_Year' from nations_export")
for row in cur2:
	last_year = row[0]

# Injecter l'intitulé avec le mois dans toutes les tables concernées
updateDate("nations_export","ne",last_year,last_value)
updateDate("nations_import","ni",last_year,last_value)
updateDate("nations_export_all","nea",last_year,last_value)
updateDate("nations_import_all","nia",last_year,last_value)