#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                           dl_etudiants.py                                  #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge les fichiers ZIP mis à disposition par le ministère des Affaires étrangères et du Développement
# international relatif aux étudiants étrangers candidats à des études en France. Il trouve ces fichiers via le site Opendata du gouvernement français 
# télécharge cette page pour y chercher les liens pertinents,  télécharge les fichiers .7z qui nous intéressent, ouvre ces conteneurs, 
# récupère en leur sein les données elles-mêmes, enregistre le tout dans un répertoire et supprime tout le reste de notre disque dur.

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, shutil, urllib2, re, libarchive
from functions import log

log("")
log("==================================")
log("Lancement du script dl_etudiants.py")
log("==================================")

# 2. SCRIPT

# L'identifiant du jeu de données qui nous intéresse
id_dataset = "5832fc9ec751df2df1c0bb7e"

# L'URL qui en découle
URL = "https://www.data.gouv.fr/api/1/datasets/"+id_dataset+"/"
# Création du répertoire temporaire destiné à accueillir nos fichiers avant décompression
dl_dir = "datas/etudiants/"
# Si le répertoire n'existe pas, créons-le
if not os.path.exists(dl_dir):
	log("Création du répertoire destiné à accueillir nos fichiers")
	os.makedirs(dl_dir)

# 2.1. Téléchargeons

# On ouvre le fichier contenant le menu
response = urllib2.urlopen(URL)
# On stocke son contenu dans la variable "menu"
menu = response.read()
# Nous rechercherons des URL annoncés par le tag... "url" ! Les lignes se terminent par une accolade fermante. Nous récupérons tout ce qui se trouve entre les deux
pattern = re.compile('url(.*?)}')
# Le résultat est stocké dans la liste "results"
results = pattern.findall(menu)
# Il nous reste à ouvrir notre liste de résultat
for result in results:
	link = result[4:-1]
	# Trouvons le nom du fichier lui-même, après le dernier slash
	index = link.rfind("/")
	# Nous avons besoin du nom du fichier, pour le réenregistrer
	dl_name = link[(index+1):]
	# Si ce fichier se termine par .7z, c'est celui que nous cherchons
	if dl_name.find(".7z") > -1:
		# Nous allons l'enregistrer dans le répertoire tmp, sous son appelation diplomatique							
		dl_zip = dl_dir+"/"+dl_name
		try:
			with open(dl_zip,'w') as f:
				f.write(urllib2.urlopen(link).read())
				f.close()
			log("Téléchargement du fichier "+dl_zip)
		except Exception, e:	
			log("===Echec du téléchargement d'un fichier .7z : %s" % e)
			pass	

# 2.2 Décompactons

# Récupérons le chemin actuel
normal_dir = os.getcwd()
# Listons les fichiers dans le répertoire qui nous intéresse
liste_fichiers = os.listdir(dl_dir)

for fichier in liste_fichiers:
	
	# Décompactons si nécessaire
	if fichier.find(".7z") > -1:
		# Créons un répertoire
		new_dir = dl_dir+fichier[:-3]+"/"
		# Si le répertoire n'existe pas, créons-le
		if not os.path.exists(new_dir):
			log("Création du répertoire destiné à accueillir nos fichiers")
			os.makedirs(new_dir)
		# Copions notre archive dans le nouveau répertoire
		shutil.copy(dl_dir+fichier,new_dir+fichier)		
		# Positionnons nous provisoirement dans le nouveau répertoire
		os.chdir(new_dir)
		libarchive.extract_file(fichier)
		# Revenons à notre répertoire normal
		os.chdir(normal_dir)				
		# Supprimons les fichiers compactés
		os.remove(dl_dir+fichier)
		os.remove(new_dir+fichier)	
		log("Décompression du fichier "+fichier)