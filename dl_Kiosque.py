#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                             dl_Kiosque.py                                  #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge les fiches sur le commerce extérieur avec chaque
# pays publiées sur le kiosque de Bercy afin de récupérer dans notre base de données
# celles qui nous intéressent.

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, urllib2, cookielib
from functions import log, listeTerritoiresUE

log("")
log("=================================")
log("Lancement du script dl_Kiosque.py")
log("=================================")

# 2. SCRIPT

# Chemin des fichiers que nous allons télécharger
dl_path = "datas/kiosque/"

# Si le répertoire n'existe pas, créons-le
dl_dir = os.path.dirname(dl_path)
if not os.path.exists(dl_path):
	log("Création du répertoire destiné à accueillir nos fichiers : datas/kiosque/")
	os.makedirs(dl_dir)

# 2.1 Ouvrir une session

# Le kiosque de Bercy nous interdira de browser ses pages si nous n'ouvrons pas une session, même implicitement.
# Nous devons donc aller sur sa première page, puis stocker les cookies envoyés par le serveur

welcome_url = "http://lekiosque.finances.gouv.fr/portail_default.asp"

# Paramétrage du stockage des cookies
cookiefile = "datas/cookies.lwp"
urlopen = urllib2.urlopen
Request = urllib2.Request
cj = cookielib.LWPCookieJar()
if os.path.isfile(cookiefile):
	cj.load(cookiefile)

# On installe notre pot de cookies dans notre opener
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
urllib2.install_opener(opener)

# Un vrai-faux header
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko)',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
		'Accept-Encoding': 'none',
		'Accept-Language': 'en_US,en;q=0.8',
		'Connection': 'keep-alive'}

# J'ouvre la page de garde
req = urllib2.Request(welcome_url, headers=hdr)
#r = requests.get(welcome_url, cookies=jar)
handle = urlopen(req)
# J'enregistre le cookie de session
cj.save(cookiefile)

compteur = 0

# Les données qui nous intéressent sont publiées à 5 adresses différentes (avec des variations ultérieures pour le pays et le sens du flux)

kiosque_url_base1 = "http://lekiosque.finances.gouv.fr/pays/transfert_long_cpf.asp?serie=P10"
kiosque_url_base2 = "http://lekiosque.finances.gouv.fr/pays/transfert_long_cpf.asp?serie=P20"
kiosque_url_base3 = "http://lekiosque.finances.gouv.fr/pays/transfert_long_cpf.asp?serie=P30"
kiosque_url_base4 = "http://lekiosque.finances.gouv.fr/pays/transfert_long_cpf.asp?serie=P40"
kiosque_url_base5 = "http://lekiosque.finances.gouv.fr/pays/transfert_long_cpf.asp?serie=P50"
compteur = 0

for pays in listeTerritoiresUE():

	str_pays = pays.encode('utf8')
	log("Récupération et injection du total des échanges de la France avec %s" % str_pays)	
			
	# 2.2. Exportations (de la France vers le pays tiers)

	dl_file = dl_path+"exports-"+pays+".csv"
	url_exports = kiosque_url_base1+pays+"&Flux=2"

	try:	
		log("Tentative de téléchargement du fichier")
		req = urllib2.Request(url_exports, headers=hdr)
		with open(dl_file,'w') as f:
			f.write(urllib2.urlopen(req).read())
		f.close()
		cj.save(cookiefile)
		compteur = compteur+1
	except:
		log("Echec du téléchargement en P10. Tentative en P20")
		url_exports = kiosque_url_base2+pays+"&Flux=2"
		try:
			req = urllib2.Request(url_exports, headers=hdr)
			with open(dl_file,'w') as f:
				f.write(urllib2.urlopen(req).read())
			f.close()
			cj.save(cookiefile)
			compteur = compteur+1
		except:
			log("Echec du téléchargement en P20. Tentative en P30")
			url_exports = kiosque_url_base3+pays+"&Flux=2"
			try:
				req = urllib2.Request(url_exports, headers=hdr)
				with open(dl_file,'w') as f:
					f.write(urllib2.urlopen(req).read())
				f.close()
				cj.save(cookiefile)
				compteur = compteur+1
			except:
				log("Echec du téléchargement en P30. Tentative en P40")
				url_exports = kiosque_url_base4+pays+"&Flux=2"
				try:
					req = urllib2.Request(url_exports, headers=hdr)
					with open(dl_file,'w') as f:
						f.write(urllib2.urlopen(req).read())
					f.close()
					cj.save(cookiefile)
					compteur = compteur+1	
				except:
					log("Echec du téléchargement en P40. Tentative en P50")
					url_exports = kiosque_url_base5+pays+"&Flux=2"
					try:
						req = urllib2.Request(url_exports, headers=hdr)
						with open(dl_file,'w') as f:
							f.write(urllib2.urlopen(req).read())
						f.close()
						cj.save(cookiefile)
						compteur = compteur+1
					except:
						log("Echec du téléchargement")
	statinfo = os.stat(dl_file)
	if statinfo.st_size == 0:
		os.remove(dl_file)
	# 2.3. Importations (de la France depuis le pays tiers)

	dl_file = dl_path+"imports-"+pays+".csv"
	url_imports = kiosque_url_base1+pays+"&Flux=1"

	try:	
		log("Tentative de téléchargement du fichier")
		req = urllib2.Request(url_imports, headers=hdr)
		with open(dl_file,'w') as f:
			f.write(urllib2.urlopen(req).read())
		f.close()
		cj.save(cookiefile)
		compteur = compteur+1
	except:
		log("Echec du téléchargement en P10. Tentative en P20")
		url_imports = kiosque_url_base2+pays+"&Flux=1"
		try:
			req = urllib2.Request(url_imports, headers=hdr)
			with open(dl_file,'w') as f:
				f.write(urllib2.urlopen(req).read())
			f.close()
			cj.save(cookiefile)
			compteur = compteur+1
		except:
			log("Echec du téléchargement en P20. Tentative en P30")
			url_imports = kiosque_url_base3+pays+"&Flux=1"
			try:
				req = urllib2.Request(url_imports, headers=hdr)
				with open(dl_file,'w') as f:
					f.write(urllib2.urlopen(req).read())
				f.close()
				cj.save(cookiefile)
				compteur = compteur+1
			except:
				log("Echec du téléchargement en P30. Tentative en P40")
				url_imports = kiosque_url_base4+pays+"&Flux=1"
				try:
					req = urllib2.Request(url_imports, headers=hdr)
					with open(dl_file,'w') as f:
						f.write(urllib2.urlopen(req).read())
					f.close()
					cj.save(cookiefile)
					compteur = compteur+1
				except:
					log("Echec du téléchargement en P40. Tentative en P50")
					url_imports = kiosque_url_base5+pays+"&Flux=1"
					try:
						req = urllib2.Request(url_imports, headers=hdr)
						with open(dl_file,'w') as f:
							f.write(urllib2.urlopen(req).read())
						f.close()
						cj.save(cookiefile)
						compteur = compteur+1
					except:
						log("Echec du téléchargement")
	
	statinfo = os.stat(dl_file)
	if statinfo.st_size == 0:
		os.remove(dl_file)

# Suppression du fichier de cookies
os.remove(cookiefile)

log("%s données sur les investissements français à l'étranger et étrangers en France saisis dans notre base de données" % compteur)