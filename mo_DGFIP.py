#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                             mo_DGIFP.py                                    #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script parse les taux de chancellerie publiés par la Direction générale
# des finances publiques (DGFIP) du ministère de l'Economie et des Finances, que
# nous avons téléchargé avec dl_DGFIP.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3
from datetime import datetime
from functions import log

log("")
log("===============================")
log("Lancement du script mo_DGFIP.py")
log("===============================")

# 2. SCRIPT

# Créons un compteur
compteur = 0

file_path = "datas/dgfip/Webtaux.txt"

with open(file_path) as file:
		for line in file:
			# Supprimons déjà le saut de ligne à la fin de chacune d'entre elle
			clean_line = line[:-1]
			rates_list = clean_line.split("\t")
			# Récupérons le code de la devise
			currency_code = rates_list[0]
			# Récupérons la date du taux de change et mettons-là au format date
			rate_date = rates_list[1]
			rate_date_obj = datetime.strptime(rate_date, "%d/%m/%Y")
			# Récupérons le taux de change et convertissons-le en nombre
			rate_value = float(rates_list[2])
			# Pour une raison mystérieuse, la valeur des taux est multipliée par 10 millions...
			rate_value = rate_value/10000000000
			currency_code = currency_code.decode("utf-8")
			currency_code = unicode(currency_code)
			rate_date_obj = unicode(rate_date_obj)
			rate_value = unicode(rate_value)
			conn = sqlite3.connect("datas/bases/general.db")
			# On vérifie d'abord si cette monnaie existe déjà dans la table 						 						
			cur = conn.cursor()						
			cur.execute("SELECT * from currencies_rates where cr_codes = ?", (currency_code,))		
 			if len(cur.fetchall()) == 0:
				# Si elle n'existe pas, on insère notre jeu de données (le dernier taux de change est toujours donné en premier)	
				conn.execute("INSERT INTO currencies_rates (cr_codes, cr_dates, cr_values) VALUES (?, ?, ?)",(currency_code,rate_date_obj,rate_value))
				conn.commit()
				conn.close()
				compteur = compteur+1		
			# Si la monnaie est déjà dans la table, on ne fait rien...

# Et on supprime le fichier traité
os.remove(file_path)

log("%s taux de chancellerie saisis dans notre base de données" % compteur)