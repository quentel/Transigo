#!/usr/bin/env python
# -*- coding: utf-8 -*-


##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                                ul_FTP.py                                   #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script téléverse (upload) tout notre répertoire "Output" sur un serveur FTP

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys
from ftplib import FTP
from functions import log

log("")
log("=============================")
log("Lancement du script ul_FTP.py")
log("=============================")

# 2. FONCTIONS

# L'élément considéré est-il un fichier ou un répertoire ?
def isFileorDirectory(path):
	if os.path.isdir(path) == True:
		return "Directory"
	elif os.path.isfile(path) == True:
		return "File"
	else:
		return "Enigme nimbée de mystères"

# Téléverser le fichier concerné

def upload(ftp, file):
#	with open(ul_file,'rb') as f:
	ftp.storlines("STOR "+ file, open(file))
	
	#connect.storbinary('STOR ', ul_file, file)
	
# 3. SCRIPT

# 3.1 Récupérer les données de configuration

config_file = "config/config.txt"

compteur = 0
with open(config_file) as fichier:
		for ligne in fichier:
			donnees = ligne.split("=")
			if compteur == 0:
				host = donnees[1]
			if compteur == 1:
				user = donnees[1]
			if compteur == 2:
				password = donnees[1]
			if compteur == 3:
				path = donnees[1]
			compteur = compteur+1
# Supprimons guillemets et sauts de ligne
host = host[1:-2]
user = user[1:-2]
password = password[1:-2]
remote_path = path[1:-2]

# 3.2. Connexion au serveur FTP
try :
	connect = FTP(host,user,password)
	connect.cwd(remote_path)
	log("Connexion au serveur %s réussie" % host)
except :
		log("=== Impossible de me connecter au serveur %s" % host)
		pass

# 3.3. Envoi de nos fichiers dossier par dossier
local_path = "output/"
dirList = os.listdir(local_path)
compteur = 0
for d in dirList:
	if isFileorDirectory(local_path+d) == "File":
		try:
			connect.storbinary("STOR " + d, open(local_path+d, "rb"), 1024)
			log("Téléversement du fichier %s" % d)		
			compteur = compteur+1
		except:
			log("=== Impossible de téléverser le fichier %s" % d)
			pass
	elif isFileorDirectory(local_path+d) == "Directory":
		dirList2 = os.listdir(local_path+d+"/")
		for d2 in dirList2:
			if isFileorDirectory(local_path+d+"/"+d2) == "File":
				try:				
					connect.storbinary("STOR " + d2, open(local_path+d+"/"+d2, "rb"), 1024)
					compteur = compteur+1
					log("Téléversement du fichier %s" % d2)
				except:
					log("=== Impossible de téléverser le fichier %s" % d2)
					pass				
			elif isFileorDirectory(local_path+d+"/"+d2) == "Directory":
				dirList3 = os.listdir(local_path+d+"/"+d2+"/")
				for d3 in dirList3:
					if isFileorDirectory(local_path+d+"/"+d2+"/"+d3) == "File":
						try:
							connect.storbinary("STOR " + d2, open(local_path+d+"/"+d2+"/"+d3, "rb"), 1024)						
							log("Téléversement du fichier %s" % d3)
							compteur = compteur+1
						except:
							log("=== Impossible de téléverser le fichier %s" % d)
							pass
					else:
						log("=== Impossible de téléverser le dossier %s : je ne sais descendre plus de trois niveaux." % d3)
try:
	connect.quit()
	log("Connexion au serveur %s fermée" % host)
except:
	log("=== Impossible de fermer la connexion au serveur %s" % host)
	
log("%s fichiers ont été téléversés sur notre serveur" % compteur)