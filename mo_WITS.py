#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                            mo_WITS-API.py                                  #
#                                                                            #
#                        Christophe Quentel, 2017                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_WITS.py les données qui
# nous intéressent dans les fiches pays du World Integrated Trade Solution (WITS),
# que nous avons téléchargées avec dl_WITS-APIO.py

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, os, decimal
from functions import log, deWitsversAlpha2

log("")
log("==============================")
log("Lancement du script mo_WITS.py")
log("==============================")

# 2. SCRIPT

compteur = 0

# On ouvre tous les fichiers, un par un
path_WITS  = "datas/wits/"

liste_fichiers_WITS = os.listdir(path_WITS)

for fichier in liste_fichiers_WITS:
	log("Traitement du fichier %s." % fichier)
	mon_WITS_path = path_WITS+fichier
	mon_fichier_WITS = open(mon_WITS_path, "rb")
	reader_WITS = csv.reader(mon_fichier_WITS, delimiter=',')
	rownum = 0
	for row in reader_WITS:
		# Cherchons d'abord les 5 meilleurs clients	
		if row[5]=="Partner share(%)-Top 5 Export Partner":
			declarant = deWitsversAlpha2(row[0])
			year = int(row[1])
			try:
				client = deWitsversAlpha2(row[2])
			except:
				# Les fichiers du WITS sont encodés avec les pieds, avec une erreur sur Curaçao
				client = "CW"
			value = row[6]
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_best_clients (nbc_declarants, nbc_years, nbc_clients, nbc_values, nbc_sources) VALUES (?, ?, ?, ?, ?)",(declarant,year,client,value,unicode("Banque mondiale (WITS)")))
			conn.commit()
			conn.close()
			compteur = compteur+1
			rownum = rownum+1
		# Cherchons ensuite les 5 meilleurs fournisseurs	
		elif row[5]=="Partner share(%)-Top 5 Import Partner":	
			declarant = deWitsversAlpha2(row[0])
			year = int(row[1])
			try:
				provider = deWitsversAlpha2(row[2])
			except:
				# Les fichiers du WITS sont encodés avec les pieds, avec une erreur sur Curaçao
				client = "CW"
			value = row[6]
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_best_providers (nbp_declarants, nbp_years, nbp_providers, nbp_values, nbp_sources) VALUES (?, ?, ?, ?, ?)",(declarant,year,provider,value,unicode("Banque mondiale (WITS)")))
			conn.commit()
			conn.close()
			compteur = compteur+1
			rownum = rownum+1
		# Cherchons les exportations totales		
		elif row[2]=="World" and row[3]=="All Products" and row[4]=="Export":			
			declarant = deWitsversAlpha2(row[0])
			year = int(row[1])
			value = row[6]
			try:
				value = decimal.Decimal(value)
				# La valeur est donnée en millions de dollars
				value = value*1000000
				value = int(value)
			except:
				value = int(0)
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_exports (nex_ISO_3166_1_alpha_2, nex_years, nex_values, nex_sources) VALUES (?, ?, ?, ?)",(declarant,year,value,unicode("Banque mondiale (WITS)")))
			conn.commit()
			conn.close()
			compteur = compteur+1
			rownum = rownum+1
		elif row[2]=="World" and row[3]== "All Products" and row[4]=="Import":
			declarant = deWitsversAlpha2(row[0])
			year = int(row[1])
			value = row[6]
			try:
				value = decimal.Decimal(value)
				# La valeur est donnée en millions de dollars
				value = value*1000000
				value = int(value)
			except:
				value = int(0)
			conn = sqlite3.connect("datas/bases/general.db")
			conn.execute("INSERT INTO nations_imports (nim_ISO_3166_1_alpha_2, nim_years, nim_values, nim_sources) VALUES (?, ?, ?, ?)",(declarant,year,value,unicode("Banque mondiale (WITS)")))
			conn.commit()
			conn.close()
			compteur = compteur+1
			rownum = rownum+1	
		# Les autres lignes n'ont pas d'importance pour nous	
		else:
			rownum = rownum+1
	mon_fichier_WITS.close()
	# Et on supprime le fichier traité
	path_to_rm = path_WITS+fichier
	os.remove(path_to_rm)
log("%s données sur le commerce extérieur des pays saisies dans notre base" % compteur)