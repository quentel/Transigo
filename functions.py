#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                             functions.py                                   #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script accueille toutes les fonctions nécessaires à plusieurs de nos scripts

# 1. IMPORTATION DES MODULES NECESSAIRES

from __future__ import division
import os, shutil, time, sqlite3, locale
from decimal import getcontext

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
getcontext().prec = 3

# 2. FONCTIONS DIVERSES

# 2.1 Gestion et mise-à-jour du fichier de logs

# Ce fichier txt est complété à chaque action sur l'application. Il permet de savoir ce qui vient de se passer en son coeur...
def log(my_log_text):
	my_string = time.strftime("%d/%m/%Y à %Hh%Mmn%Ssec.")+" : "+my_log_text+"\n"
	my_logs_dir = "logs/"
	my_logs_file = my_logs_dir+"logs.txt"
	# Si le répertoire n'existe pas, on le crée
	if not os.path.exists(my_logs_dir):
		os.makedirs(my_logs_dir)
	# Si le fichier n'existe pas, on le crée
	if not os.path.exists(my_logs_file):
		f = open(my_logs_file, 'w')
		f.write(my_string)
		f.close
	else:
		f = open(my_logs_file, 'a')
		f.write(my_string)
		f.close

# 2.2 Détermination du trimestre à partir du mois

def whichQuarter(month):
	if month in ("01", "02", "03"):
		quarter = 1
	elif month in ("04", "05", "06"):
		quarter = 2
	elif month in ("07", "08", "09"):
		quarter = 3
	else:
		quarter = 4
	return quarter

# 2.3 Formatter un nombre entier à la française

def formatInt(integer):
	conv = locale.localeconv()
	locale.format("%d", integer, grouping=True)
	integer = locale.format_string("%.*d",(conv['frac_digits'],integer),grouping=True)
	return integer
	
# 2.4. Formatter un nombre décimal à la française

def formatFloat(floating):
	conv = locale.localeconv()
	locale.format("%d", floating, grouping=True)
	floating = locale.format_string("%.*f",(conv['frac_digits'],floating),grouping=True)
	return floating

# 2.5. Transformer n'importe quelle donnée en nombre entier

def iAmAnInteger(data):
	new_data = int()
	if isinstance(data, int):
		new_data = data
	elif type(data)==float:
		new_data = int(data)	
	else:
		new_data = 0
	return new_data

# 2.6. Transformer n'importe quelle donnée en nombre décimal

def iAmAFloat(data):
	new_data = float(3)
	if isinstance(data, float):
		new_data = data
	elif type(data)==int:
		new_data = float(data)
	else:
		new_data = 0.00
	return new_data

# 2.7. Milliers, millions, milliards

def ordreDeGrandeur(integ):
	if integ/1000000000 >= 2:
		ordre = "milliards"
		chiffre = integ/1000000000
		diviseur = 1000000000
	elif integ/1000000000 > 1:
		ordre = "milliard"
		chiffre = integ/1000000000
		diviseur = 1000000000
	elif integ/1000000 >= 2:
		ordre = "millions"
		chiffre = integ/1000000
		diviseur = 1000000
	elif integ/1000000 > 1:
		ordre = "million"
		chiffre = integ/1000000
		diviseur = 1000000
	elif integ/1000 >= 2:
		ordre = "milliers"
		chiffre = integ/1000
		diviseur = 1000
	elif integ/1000 > 1:
		ordre = "millier"
		chiffre = integ/1000
		diviseur = 1000
	else:
		ordre = "N/A"
		chiffre = "N/A"
		diviseur = "N/A"
	if chiffre != "N/A":	
		conv = locale.localeconv()
		locale.format("%d", chiffre, grouping=True)
		chiffre = locale.format_string("%.*f",(conv['frac_digits'],chiffre),grouping=True)	
	return ordre, chiffre, diviseur

# 3. ACCESSEURS DE NOMENCLATURES

# 3.1.  Récupérer un code ISO 3166 1 alpha 2 à partir d'un code ISO 3166 1 alpha 3

def deAlpha3versAlpha2(my_alpha_3):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_ISO_3166_1_alpha_2 FROM countries_names WHERE cn_ISO_3166_1_alpha_3 = ?",[my_alpha_3] )
	my_alpha_2 = None
	for row in cur:
		my_alpha_2 = row[0]
	if my_alpha_2 is None:
		# La Banque mondiale (et son programme Doing Business) utilise des codes alpha 3 anciens. Il faut le corriger.
		if my_alpha_3 == "ROM":
			my_alpha_2 = "RO"
		elif my_alpha_3 == "TMP":
			my_alpha_2 = "TL"
		elif my_alpha_3 == "ZAR":
			my_alpha_2 = "CD"
		# La Banque mondiale (et son programme Doing Business) utilise un code alpha 3 maison pour le Kosovo. Il faut le corriger.
		elif my_alpha_3 == "KSV":
			my_alpha_2 = "XK"
		# La Banque mondiale (et son programme Doing Business) utilise un code alpha 3 maison pour Gaza. Il faut le corriger.
		elif my_alpha_3 == "WBG":
			my_alpha_2 = "PS"
		else:
			my_alpha_2 = "XX"	
	return my_alpha_2
	conn.close()
	
# 3.2. Récupérer un code ISO 3166 1 alpha 3 à partir d'un code ISO 3166 1 alpha 2

def deAlpha2versAlpha3(my_alpha_2):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_ISO_3166_1_alpha_3 FROM countries_names WHERE cn_ISO_3166_1_alpha_2 = ?",[my_alpha_2] )
	my_alpha_3 = None
	for row in cur:
		my_alpha_3 = row[0]
	if my_alpha_3 is None:
		my_alpha_3 = "XXX"	
	return my_alpha_3
	conn.close()
	
# 3.3. Récupérer le libellé en français d'un pays à partir de son code ISO 3166-1 alpha 2
		
def deAlpha2versLibelleFR(my_alpha_2):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_french FROM countries_names WHERE cn_ISO_3166_1_alpha_2 = ?",[my_alpha_2] )
	my_label	= None
	for row in cur:
		my_label = row[0]
	if my_label is None:
		my_label = "N/A"
	return my_label
	conn.close()	

# 3.4. Récupérer le libellé en français d'un pays à partir de son code UE
		
def deCodeUEversLibelleFR(my_EU_Code):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_french FROM countries_names WHERE cn_eu_code = ?",[my_EU_Code] )
	my_label = None	
	for row in cur:
		my_label = row[0]
	if my_label is None:
		my_label = "N/A"
	return my_label
	conn.close()	

# 3.5. Récupérer un code NA129 à partir d'un code NC8

def deNC8versNA129(my_NC8, my_Year):	
	my_code = None
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT nvc_na129 FROM concordances_douanes WHERE nvc_nc8 = ? and nvc_year = ?",[my_NC8,my_Year] )
	for row in cur:
		my_code = row[0]
	if my_code is None:
		my_NC8b = "0"+my_NC8
		cur.execute("SELECT nvc_na129 FROM concordances_douanes WHERE nvc_nc8 = ? and nvc_year = ?",[my_NC8b,my_Year] )
		for row in cur:
			my_code = row[0]
		if my_code is None:
			my_Year = "2015"
			cur.execute("SELECT nvc_na129 FROM concordances_douanes WHERE nvc_nc8 = ? and nvc_year = ?",[my_NC8,my_Year] )
			for row in cur:
				my_code = row[0]
			if my_code is None:
				my_NC8b = "0"+my_NC8
				cur.execute("SELECT nvc_na129 FROM concordances_douanes WHERE nvc_nc8 = ? and nvc_year = ?",[my_NC8b,my_Year] )
				for row in cur:
					my_code = row[0]
				if my_code is None:
					my_code = "N/A"
	return my_code
	conn.close()

# 3.6. Récupérer le code ISO 3166-1 alpha 2 d'un pays à partir de son libellé en anglais
		
def deLibelleENversAlpha2(my_label_EN):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_ISO_3166_1_alpha_2 FROM countries_names WHERE cn_english = ?",[my_label_EN] )
	my_code = None
	for row in cur:
		my_code = row[0]
	# Correction de quelques écarts entre les listes utilisées par les institutions concernées	
	if my_code is None:
		if my_label_EN == "Korea (Democratic People's Rep. of)":
			my_code = "KP"
		else:
			my_code = "XX"
	return my_code
	conn.close()	

# 3.7. Récupérer le code ISO 3166-1 alpha 2 d'un pays ou le code d'une région
# à partir de son libellé en anglais dans la seule table WITS
		
def deWitsversAlpha2(my_label_EN):
	# Les fichiers du WITS sont encodés avec les pieds !
	my_label_EN = unicode(my_label_EN)
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT wn_nation_code FROM wits_names WHERE wn_english = ?",[my_label_EN] )
	my_code = None	
	for row in cur:
		my_code = row[0]
	if my_code is None:
		my_code = "XX"	
	return my_code
	conn.close()	

# 3.8. Récupérer le code ISO 3166-1 alpha 2 d'un pays à partir de son libellé "Doing Business"
		
#def deLibelleDBversAlpha2(my_label_DB):
#	conn = sqlite3.connect("datas/bases/nomenclatures.db")
#	cur = conn.cursor()
#	cur.execute("SELECT db_codes FROM db_names WHERE db_english = ?",[my_label_DB] )
#	my_code = None	
#	for row in cur:
#		my_code = row[0]
#	if my_code is None:
#		my_code = "XX"	
#	return my_code
#	conn.close()

# 3.9. Liste de toutes les Nations

def listePays():
	my_Nations = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_nation_code FROM countries_names")
	for row in cur:
		my_Nation = row[0]
		my_Nations.append(my_Nation)
	# On retire les doublons	
	my_Nations = list(set(my_Nations))
	# On classe par ordre
	my_Nations.sort()
	conn.close()
	return my_Nations

# 3.10. Liste de tous les territoires à la norme UE

def listeTerritoiresUE():
	my_Territories = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_eu_code FROM countries_names")
	for row in cur:
		my_Territory = row[0]
		my_Territories.append(my_Territory)
	# On retire les doublons	
	my_Territories = list(set(my_Territories))
	# On classe par ordre
	my_Territories.sort()
	conn.close()
	return my_Territories

# 3.11. Liste tous les territoires à la norme Doing Business

def listeTerritoiresDB():
	my_Territories = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT db_english FROM db_names")
	for row in cur:
		my_Territory = row[0]
		my_Territories.append(my_Territory)
	conn.close()
	return my_Territories
	
# 3.12. Liste de tous les noms de territoires en Français selon Wikipedia

def listeTerritoiresFR():
	mes_Territoires = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_french FROM wiki_names")
	for row in cur:
		mon_Territoire = row[0]
		mes_Territoires.append(mon_Territoire)
	conn.close()
	return mes_Territoires

# 3.13 Code d'une monnaie à partir du code ISO 3166 alpha 2 du pays

def getCurrencyCode(my_country_code):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cu_codes FROM currencies WHERE cu_ISO_3166_1_alpha_2 = ?",[my_country_code])
	my_currency_code = None	
	for row in cur:
		my_currency_code = row[0]
	if my_currency_code is None:
		my_currency_code = "XXX"	
	return my_currency_code
	conn.close()

# 3.14 Nom d'une monnaie à partir du code ISO 3166 alpha 2 du pays

def getCurrencyLabel(my_country_code):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cu_currencies FROM currencies WHERE cu_ISO_3166_1_alpha_2 = ?",[my_country_code])
	my_currency_label = None	
	for row in cur:
		my_currency_label = row[0]
	if my_currency_label is None:
		my_currency_label = "N/A"	
	return my_currency_label
	conn.close()

# 3.15 Récupérer le libellé en français d'une ville à partir de son libellé en anglais
		
def deVilleENversVilleFR(my_EN_Town):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT ci_french FROM cities_names WHERE ci_english = ?",[my_EN_Town] )
	my_label = None	
	for row in cur:
		my_label = row[0]
	if my_label is None:
		my_label = "N/A"
	return my_label
	conn.close()
	
# 3.16 Liste des villes en anglais

def listeVillesEN():
	mes_Villes = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT ci_english FROM cities_names")
	for row in cur:
		ma_Ville = row[0]
		mes_Villes.append(ma_Ville)
	conn.close()
	return mes_Villes

# 3.17. Récupérer le code ISO 3166-1 alpha 2 d'un pays à partir de son libellé en anglais dans la table UNSD
		
def deLibelleENversAlpha2UNSD(my_label_EN):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	# Ce text_factory résoud élégamment un problème d'encodage
	conn.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
	cur = conn.cursor()
	cur.execute("SELECT un_ISO_3166_1_alpha_2 FROM UNSD_names WHERE un_english = ?",[my_label_EN])
	my_code = None
	for row in cur:
		my_code = row[0]
	if my_code is None:
		my_code = "XX"	
	return my_code
	conn.close()	

# 3.18 Récupérer le libellé en français d'une religion donnée en anglais

def deReligionENversReligionFR(my_label_EN):	
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT re_french FROM religions WHERE re_english = ?",[my_label_EN] )
	my_re = None
	for row in cur:
		my_re = row[0]
	if my_re is None:
		my_re = "N/A"	
	return my_re
	conn.close()	

# 3.19 Liste des langues en anglais

def listeLanguesEN():
	mes_Langues = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT li_english FROM languages_names")
	for row in cur:
		ma_Langue = row[0]
		mes_Langues.append(ma_Langue)
	conn.close()
	return mes_Langues

# 3.20 Récupérer le libellé en français d'une langue à partir de son libellé en anglais
		
def deLangueENversLangueFR(my_EN_Languages):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	# Ce text_factory résoud élégamment un problème d'encodage
	conn.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
	cur.execute("SELECT li_french FROM languages_names WHERE li_english = ?",[my_EN_Languages] )
	my_label = None	
	for row in cur:
		my_label = row[0]
	if my_label is None:
		my_label = "N/A"
	return my_label
	conn.close()

# 3.21 Récupérer le code ISO d'un pays à partir de son libellé Wikipedia
		
def deWikiversISO(my_Wiki_name):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	# Ce text_factory résoud élégamment un problème d'encodage
	conn.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
	cur.execute("SELECT cn_ISO_3166_1_alpha_2 FROM wiki_names WHERE cn_french = ?",[my_Wiki_name] )
	my_label = None	
	for row in cur:
		my_label = row[0]
	if my_label is None:
		my_label = "N/A"
	return my_label
	conn.close()
	
# 3.22 Liste de toutes les directions géographique du Quai d'Orsay

def qoListeDir():
	mes_Directions = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT qorg_direction FROM qo_orga")
	for row in cur:
		ma_Direction = row[0]
		mes_Directions.append(ma_Direction)
	# On vire les doublons	
	mes_Directions = list(set(mes_Directions))	
	conn.close()
	return mes_Directions

# 3.23 Liste de toutes les sous-directions du Quai d'Orsay rattachées à une direction géographique

def qoListeSousDir(direction):
	mes_Sous_directions = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT qorg_sous_direction FROM qo_orga WHERE qorg_direction = ?",[direction])
	for row in cur:
		ma_Sous_direction = row[0]
		mes_Sous_directions.append(ma_Sous_direction)
	# On vire les doublons	
	mes_Sous_directions = list(set(mes_Sous_directions))	
	conn.close()
	return mes_Sous_directions
	
# 3.24 Liste de tous les pays gérés par une sous-direction géographique du Quai d'Orsay

def qoListePays(sousdirection):
	mes_Pays = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT qorg_ISO_3166_1_alpha_2 FROM qo_orga WHERE qorg_sous_direction = ?",[sousdirection])
	for row in cur:
		mon_Pays = row[0]
		mes_Pays.append(mon_Pays)
	# On vire les doublons	
	# mes_Sous_directions = list(set(mes_Sous_directions))	
	conn.close()
	return mes_Pays
 
# 3.35. Liste de tous les noms de territoires en Français selon le Conseil de la langue

def listeTerritoiresFRcanoniques():
	mes_Territoires = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_french FROM countries_names")
	for row in cur:
		mon_Territoire = row[0]
		mes_Territoires.append(mon_Territoire)
	conn.close()
	return mes_Territoires 
 
# 3.36 Récupérer le code ISO d'un pays à partir de son libellé canonique en Français
		
def deCanoniqueversISO(mon_nom_canonique):
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	# Ce text_factory résoud élégamment un problème d'encodage
	conn.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
	cur.execute("SELECT cn_ISO_3166_1_alpha_2 FROM countries_names WHERE cn_french = ?",[mon_nom_canonique] )
	my_label = None	
	for row in cur:
		my_label = row[0]
	if my_label is None:
		my_label = "N/A"
	return my_label
	conn.close()


 
 
# 4. ACCESSEURS DE DONNEES

# 4.1. Dernière année (ou dernière période exprimée en année) disponible pour une table et un pays donnés

def getDataLastYear(prefixe,table,country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT max("+prefixe+"_years) AS 'Most_Recent_Year' FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ?",[country_code])
	my_year = None	
	for row in cur:
		my_year = row[0]
	if my_year is None:
		my_year = "N/A"
	return my_year
	conn.close()

# 4.2. Dernière année (ou dernière période exprimée en année) disponible pour une table, un pays et une source donnés

def getDataLastYearSourced(prefixe,table,country_code,source):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT max("+prefixe+"_years) AS 'Most_Recent_Year' FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ? and "+prefixe+"_sources = ?",[country_code, source])
	my_year = None	
	for row in cur:
		my_year = row[0]
	if my_year is None:
		my_year = "N/A"
	return my_year
	conn.close()

# 4.2. Toutes les années disponibles pour une table et un pays donnés

def getAllYears(prefixe,table,country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_years FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ?",[country_code])
	my_years = []	
	my_date = None	
	for row in cur:
		my_date = row[0]
		my_years.append(my_date)
	if my_date is None:
		my_date = "N/A"
	my_years.sort()	
	return my_years
	conn.close()

# 4.3. Toutes les années disponibles pour une table, une source et un pays donnés

def getAllYearsSourced(prefixe,table,country_code,source):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_years FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ? and "+prefixe+"_sources = ?",[country_code, source])
	my_years = []	
	my_date = None	
	for row in cur:
		my_date = row[0]
		my_years.append(my_date)
	if my_date is None:
		my_date = "N/A"
	return my_years
	conn.close()

# 4.4. Valeur d'une donnée selon le pays, quand l'année et la source n'importent pas

def getValue(prefixe,table,country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_values FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ?",[country_code])
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

# 4.5. Valeur d'une donnée pour une table, une année et un pays donnés

def getDataValue(prefixe, table, country_code,year):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_values FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ? AND "+prefixe+"_years = ?",[country_code, year])
	my_value = None	
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

# 4.6. Valeur d'une donnée pour une table, une année, un pays et une source donnés

def getDataValueSourced(prefixe, table, country_code,year,source):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_values FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ? AND "+prefixe+"_years = ? AND "+prefixe+"_sources =?",[country_code, year, source])
	my_value = None	
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()


# 4.6. Source d'une donnée pour une table, une année et un pays donnés

def getDataSource(prefixe, table, country_code, year):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_sources FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ? AND "+prefixe+"_years = ?",[country_code, year])
	my_source = None	
	for row in cur:
		my_source = str()
		my_source = row[0]
	if my_source is None:
		my_source = "N/A"
	return my_source
	conn.close()

# 4.7. Valeur et source d'une donnée pour un pays et une année donnés

def getDataValueSource(prefixe, table, country_code, year):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_values, "+prefixe+"_sources FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ? and "+prefixe+"_years = ?",[country_code, year])
	my_value = None
	my_source = None
	result = list()
	for row in cur:
		my_value = row[0]
		my_source = row[1]
	if my_value is None:
		my_value = "N/A"
	if my_source is None:
		my_source = "N/A"
	result.append(my_value)
	result.append(my_source)	
	return result
	conn.close()

# 4.8. Valeur et année d'une donnée pour un pays et une source donnés

def getDataValueYear(prefixe, table, country_code, source):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_values, "+prefixe+"_years FROM "+table+" WHERE "+prefixe+"_ISO_3166_1_alpha_2 = ? and "+prefixe+"_sources = ?",[country_code, source])
	my_value = None
	my_year = None
	result = list()
	for row in cur:
		my_value = row[0]
		my_year = row[1]
	if my_value is None:
		my_value = "N/A"
	if my_year is None:
		my_year = "N/A"
	result.append(my_value)
	result.append(my_year)	
	return result
	conn.close()

# 4.9. Superficie

def getArea(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nd_area FROM nations_datas WHERE nd_ISO_3166_1_alpha_2 = ?",[country_code])
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

# 4.10. Catégorie Banque mondiale

def getCategory(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nd_worldbank_category FROM nations_datas WHERE nd_ISO_3166_1_alpha_2 = ?",[country_code])
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

# 4.11. Taux de chancellerie

def getLastXChangeRateValue(currency_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT cr_values FROM currencies_rates WHERE cr_codes = ?",[currency_code])
	my_value = None	
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

def getLastXChangeRateDate(currency_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT cr_dates FROM currencies_rates WHERE cr_codes = ?",[currency_code])
	my_date = None	
	for row in cur:
		my_date = row[0]
	if my_date is None:
		my_date = "N/A"
	return my_date
	conn.close()

# 4.11 Population

def getPopLastYear(country_code, source):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT np_years FROM nations_populations WHERE np_ISO_3166_1_alpha_2 = ? and np_sources = ?",[country_code, source])
	year = None
	my_years = []
	for row in cur:
		try:
			my_year = row[0]
		except:
			my_year = "N/A"
		my_years.append(my_year)
	my_years.sort(reverse = True)
	try:	
		year = my_years[0]
	except:
		year = "N/A"
	if year is None:
		year = "N/A"
	return year
	conn.close()

def getPop(country_code,year,source):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT np_values FROM nations_populations WHERE np_ISO_3166_1_alpha_2 = ? and np_sources = ? and np_years = ?",[country_code, source,year])
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()
	
# 4.12 IDH

def getHDIRank(country_code,year):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nh_ranks FROM nations_hdi WHERE nh_ISO_3166_1_alpha_2 = ? and nh_years = ?",[country_code, year])
	my_value = None
	for row in cur:
		my_value = int()
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

# 4.13 Nom officiel

def getOfficial(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nd_official_name FROM nations_datas WHERE nd_ISO_3166_1_alpha_2 = ?",[country_code])
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()
	
# 4.14 Dirigeants

def getLeaders_1(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nl_names, nl_functions FROM nations_leaders WHERE nl_ISO_3166_1_alpha_2 = ? and nl_ranks = 1",[country_code])
	result = list()	
	my_name = None
	my_function = None
	for row in cur:
		my_name = row[0]
		my_function = row[1]
	if my_name is None:
		my_name = "N/A"
	if my_function is None:
		my_function is "N/A"
	result.append(my_name)
	result.append(my_function)
	return result
	conn.close()

def getLeaders_2(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nl_names, nl_functions FROM nations_leaders WHERE nl_ISO_3166_1_alpha_2 = ? and nl_ranks = 2",[country_code])
	result = list()	
	my_name = None
	my_function = None
	for row in cur:
		my_name = row[0]
		my_function = row[1]
	if my_name is None:
		my_name = "N/A"
	if my_function is None:
		my_function is "N/A"
	result.append(my_name)
	result.append(my_function)
	return result
	conn.close()

def getLeaders_3(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nl_names, nl_functions FROM nations_leaders WHERE nl_ISO_3166_1_alpha_2 = ? and nl_ranks = 3",[country_code])
	result = list()	
	my_name = None
	my_function = None
	for row in cur:
		my_name = row[0]
		my_function = row[1]
	if my_name is None:
		my_name = "N/A"
	if my_function is None:
		my_function is "N/A"
	result.append(my_name)
	result.append(my_function)
	return result
	conn.close()
	
# 4.15 Sortir les 5 meilleurs clients

def getBestClients(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nbc_clients, nbc_values, nbc_years, nbc_sources FROM nations_best_clients WHERE nbc_declarants = ?",[country_code])	
	result = list()
	my_client = None
	my_value = None
	my_year = None
	my_source = None
	for row in cur:
		one_client = list()
		my_client = row[0]
		my_value = row[1]
		my_year = row[2]
		my_source = row[3]	
		if my_client is None:
			my_client = "N/A"
		if my_value is None:
			my_value is "N/A"
		if my_year is None:
			my_year is "N/A"
		if my_source is None:
			my_source is "N/A"
		one_client.append(my_client)
		one_client.append(my_value)
		one_client.append(my_year)
		one_client.append(my_source)
		result.append(one_client)
	return result
	conn.close()

# 4.16 Sortir les 5 meilleurs fournisseurs

def getBestProviders(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nbp_providers, nbp_values, nbp_years, nbp_sources FROM nations_best_providers WHERE nbp_declarants = ?",[country_code])	
	result = list()
	my_provider = None
	my_value = None
	my_year = None
	my_source = None
	for row in cur:
		one_provider = list()
		my_provider = row[0]
		my_value = row[1]
		my_year = row[2]
		my_source = row[3]	
		if my_provider is None:
			my_provider = "N/A"
		if my_value is None:
			my_value is "N/A"
		if my_year is None:
			my_year is "N/A"
		if my_source is None:
			my_source is "N/A"
		one_provider.append(my_provider)
		one_provider.append(my_value)
		one_provider.append(my_year)
		one_provider.append(my_source)
		result.append(one_provider)
	return result
	conn.close()

# 4.17 Nature du régime

def getStateForm(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nd_state_form FROM nations_datas WHERE nd_ISO_3166_1_alpha_2 = ?",[country_code])
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

# 4.18 Capitale

def getCapitale(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT mt_towns, mt_populations, mt_years FROM countries_main_towns WHERE mt_ISO_3166_1_alpha_2 = ? and mt_capital_cities = ?",[country_code, True])
	my_result = list()
	my_name = None
	my_pop = None
	my_year = None	
	for row in cur:
		my_name = row[0]
		my_pop = row[1]
		my_year = row[2]
	if my_name is None:
		my_name = "N/A"
	if my_pop is None:
		my_pop = "N/A"
	if my_year is None:
		my_year = "N/A"
	my_result.append(my_name)
	my_result.append(my_pop)
	my_result.append(my_year)
	return my_result
	conn.close()

# 4.19 Langues

def getLanguages(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nl_labels, nl_values, nl_years FROM nations_languages WHERE nl_ISO_3166_1_alpha_2 = ?",[country_code])
	my_result = list()
	for row in cur:
		my_language = None
		my_pop = None
		my_year = None
		subresult = list()
		my_language = row[0]
		my_pop = row[1]
		my_year = row[2]
		subresult.append(my_language)
		subresult.append(my_pop)
		subresult.append(my_year)
		my_result.append(subresult)
	my_result.sort(key=lambda my_result: my_result[1], reverse=True)	
	return my_result
	conn.close()

def getLanguagesTotal(country_code,year):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nl_values FROM nations_languages WHERE nl_ISO_3166_1_alpha_2 = ? and nl_years = ? and nl_labels= ?",[country_code,year,"Total"])
	my_total = int()	
	for row in cur:
		my_total = row[0]
		if my_total is None:
			my_total = "N/A"
	return my_total
	conn.close()

# 4.20 Langues officielles

def getOfficialLanguages(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nd_off_languages FROM nations_datas WHERE nd_ISO_3166_1_alpha_2 = ?",[country_code])
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()
	
# 4.21 Villes principales

def getTowns(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT mt_towns, mt_populations, mt_types, mt_years FROM countries_main_towns WHERE mt_ISO_3166_1_alpha_2 = ? AND mt_types= ?",[country_code,"City proper"])
	result = list()
	for row in cur:
		my_name = None
		my_pop = None
		my_type = None
		my_year = None		
		subresult = list()	
		my_name = row[0]
		my_pop = row[1]
		my_type = row[2]
		my_year = row[3]
		my_type = "ville".encode('utf-8')
		subresult.append(my_name)
		subresult.append(my_pop)
		subresult.append(my_type)
		subresult.append(my_year)
		result.append(subresult)
	result.sort(key=lambda result: result[1], reverse=True)	
	return result
	conn.close()

def getAgglomerations(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT mt_towns, mt_populations, mt_types, mt_years FROM countries_main_towns WHERE mt_ISO_3166_1_alpha_2 = ? AND mt_types= ?",[country_code,"Urban agglomeration"])
	result = list()
	for row in cur:
		my_name = None
		my_pop = None
		my_type = None
		my_year = None		
		subresult = list()	
		my_name = row[0]
		my_pop = row[1]
		my_type = row[2]
		my_year = row[3]
		my_type = "ville".encode('utf-8')
		subresult.append(my_name)
		subresult.append(my_pop)
		subresult.append(my_type)
		subresult.append(my_year)
		result.append(subresult)
	result.sort(key=lambda result: result[1], reverse=True)	
	return result
	conn.close()

# 4.22 Religions

def getReligions(country_code):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nr_labels, nr_values, nr_years FROM nations_religions WHERE nr_ISO_3166_1_alpha_2 = ?",[country_code])
	my_result = list()
	for row in cur:
		my_religion = None
		my_pop = None
		my_year = None
		subresult = list()
		my_religion = row[0]
		my_pop = row[1]
		my_year = row[2]
		subresult.append(my_religion)
		subresult.append(my_pop)
		subresult.append(my_year)
		my_result.append(subresult)
	my_result.sort(key=lambda my_result: my_result[1], reverse=True)	
	return my_result
	conn.close()

def getReligionsTotal(country_code,year):
	conn = sqlite3.connect("datas/bases/general.db")
	cur = conn.cursor()
	cur.execute("SELECT nr_values FROM nations_religions WHERE nr_ISO_3166_1_alpha_2 = ? and nr_years = ? and nr_labels= ?",[country_code,year,"Total"])
	my_total = int()	
	for row in cur:
		my_total = row[0]
		if my_total is None:
			my_total = "N/A"
	return my_total
	conn.close()
	
# 4.23 Douanes

# Sortir tous les territoires UE correspondants à un pays donné
def getTerritories(country_code):
	my_Territories = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_eu_code FROM countries_names where cn_nation_code = ?", (country_code,))
	for row in cur:
		my_Territory = row[0]
		my_Territories.append(my_Territory)
	# On retire les doublons	
	my_Territories = list(set(my_Territories))
	# On classe par ordre
	my_Territories.sort()
	conn.close()
	return my_Territories
	
# Sortir tous les territoires correspondant à la France
def uneEtIndivisible():
	my_Territories = list()
	conn = sqlite3.connect("datas/bases/nomenclatures.db")
	cur = conn.cursor()
	cur.execute("SELECT cn_eu_code FROM countries_names where cn_nation_code = ?", ("FR",))
	for row in cur:
		my_Territory = row[0]
		my_Territories.append(my_Territory)
	# On retire les doublons	
	my_Territories = list(set(my_Territories))
	# On classe par ordre
	my_Territories.sort()
	conn.close()
	return my_Territories
	
# Sortir toutes les années possibles dans une table de la base Douanes pour un territoire
def getAllYearsinDouanesTerritory(prefixe,table,territory_code):
	conn = sqlite3.connect("datas/bases/douanes.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_years FROM "+table+" WHERE "+prefixe+"_eu_codes = ?",[territory_code])
	my_years = []	
	my_date = None	
	for row in cur:
		my_date = row[0]
		my_years.append(my_date)
	if my_date is None:
		my_date = "N/A"
	my_years.sort()	
	return my_years
	conn.close()

def getDataValueinDouanesTerritory(prefixe, table, territory_code,year):
	conn = sqlite3.connect("datas/bases/douanes.db")
	cur = conn.cursor()
	cur.execute("SELECT "+prefixe+"_values FROM "+table+" WHERE "+prefixe+"_eu_codes = ? AND "+prefixe+"_years = ?",[territory_code, year])
	my_value = None	
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()

# Sortir les importations totales vers la France-territoire depuis un autre territoire pour une année donnée

def getAllImportTerritoryToTerritory(territory,year):
	conn = sqlite3.connect("datas/bases/douanes.db")
	cur = conn.cursor()
	cur.execute("SELECT SUM(nia_values) from nations_import_all where nia_years = ? and nia_eu_codes = ?", (year,territory))
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()
	
# Sortir les exportations totales depuis la France-territoire vers un autre territoire pour une année donnée

def getAllExportTerritoryToTerritory(territory,year):
	conn = sqlite3.connect("datas/bases/douanes.db")
	cur = conn.cursor()
	cur.execute("SELECT SUM(nea_values) from nations_export_all where nea_years = ? and nea_eu_codes = ?", (year,territory))
	my_value = None
	for row in cur:
		my_value = row[0]
	if my_value is None:
		my_value = "N/A"
	return my_value
	conn.close()	

# Sortir toutes les exportations pour un pays et une année	
def getXportsCountry(country_code,year):
	my_result = 0	
	allTerritories = list()
	allTerritories = getTerritories(country_code)
	for territory in allTerritories:	
		conn = sqlite3.connect("datas/bases/douanes.db")
		cur = conn.cursor()
		cur.execute("SELECT SUM(ne_values) from nations_exportations where ne_years = ? and ne_eu_codes = ?", (year,territory))
		my_value = None
		for row in cur:
			my_value = row[0]
		if my_value is None:
			my_value = 0
		else:
			my_value = int(my_value)
		my_result = my_result+my_value	
	return my_result
	conn.close()

# Sortir toutes les importations pour un territoire et une année	
def getImportsCountry(country_code,year):
	my_result = 0	
	allTerritories = list()
	allTerritories = getTerritories(country_code)
	for territory in allTerritories:	
		conn = sqlite3.connect("datas/bases/douanes.db")
		cur = conn.cursor()
		cur.execute("SELECT SUM(ni_values) from nations_importations where ni_years = ? and ni_eu_codes = ?", (year,territory))
		my_value = None
		for row in cur:
			my_value = row[0]
		if my_value is None:
			my_value = 0
		else:
			my_value = int(my_value)
		my_result = my_result+my_value	
	return my_result
	conn.close()

#print deAlpha2versLibelleFR("US")
#print getXportsTerritory("US",2012)
#print getImportsTerritory("US",2012)
#print getXportsCountry("US",2012)
#print getImportsCountry("US",2012)