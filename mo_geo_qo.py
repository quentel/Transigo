#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                             mo_geo_qo.py                                   #
#                                                                            #
#                        Christophe Quentel, 2016                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script importe dans notre base (créée avec sql_nomenclatures.py) la table
# des noms de pays et de capitales rendue publique par le Quai d'Orsay

# 1. IMPORTATION DES MODULES NECESSAIRES

import sqlite3, csv, os
from functions import log, deWikiversISO, listeTerritoiresFRcanoniques, deCanoniqueversISO

log("")
log("================================")
log("Lancement du script mo_geo_qo.py")
log("================================")

# 2. SCRIPT

compteur = 0

# On a un seul fichier dans le dossier, mais son nom varie
path  = "datas/geographie_diplomatique/"
liste_fichiers = os.listdir(path)
# Connectons-nous à la base de données
conn = sqlite3.connect("datas/bases/nomenclatures.db")
# Créons le dictionnaire qui stockera nos données traitées
dicoGeoDiplo = dict()
# Créons la liste qui stockera nos données brutes
listeGeoDiplo = list()


# 2.1 Lisons le fichier et insérons son contenu dans nore liste

for fichier in liste_fichiers:
	
	log("Traitement du fichier %s." % fichier)		
	# On ouvre le fichier	
	path2 = path+fichier
	mon_fichier = open(path2, "rb")
	reader = csv.reader(mon_fichier, delimiter=';')
	
	# Le fichier est mal foutu : 
	# 1. plusieurs couples pays/capitale coexistent sur une même ligne
	# 2. le nom d'un pays ou d'une capitale peut apparaître sur deux lignes différentes.
	#
	# On va stocker les couples pays/capitale dans un dictionnaire
	# On va par ailleurs créer un dictionnaire dans lequel on gardera, pour chaque ligne, la dernière capitale incomplète
	
	row_number = 1
	# On lit notre fichier ligne à ligne
	for row in reader:
		# Voyons combien de couples pays/capitale nous avons sur cette ligne (on ne divise pas par deux, néanmoins)		
		nb_couples = len(row)
		# Le pays apparaît toujours en premier, et la capitale en second		
		index_pays = 0
		index_capitale = 1
		while index_pays < nb_couples:
			pays = row[index_pays]
			capitale = row[index_capitale]
			# Si nous n'avons pas de capitale, mais un pays, c'est que le nom du pays est sur deux lignes, et que nous en sommes à la première			
			if capitale == "" and pays != "":
				marqueur_pays = 1
				marqueur_capitale = 0
			# Si nous n'avons pas de pays, mais une capitale, c'est que le nom de capitale est sur deux lignes, et que nous en sommes à la première
			elif pays == "" and capitale != "":
				marqueur_pays = 0				
				marqueur_capitale = 1
			# Si nous des données pour les deux...
			else:
				marqueur_pays = 0
				marqueur_capitale = 0
			# Collons tout cela dans un tuple
			tupleCouple = row_number,pays,index_pays,marqueur_pays,capitale,index_capitale,marqueur_capitale
			# Et ajoutons le tuple à notre liste			
			listeGeoDiplo.append(tupleCouple)
			# Incrémentons les compteurs
			index_pays = index_pays+2		
			index_capitale = index_capitale+2	
		row_number = row_number+1
	
	mon_fichier.close()

pays_incomplets = list()

for couple in listeGeoDiplo:
	num_de_ligne = couple[0]
	# Pour info, le fichier original est encodé en latin_1, mais est fourni sans documentation.
	# Je n'ai donc pu trouver cet encodage que par essai/erreur...
	nom_pays = unicode(couple[1].decode('latin_1'))
	col_pays = couple[2]
	marqueur_pays = couple[3]
	nom_capitale = unicode(couple[4].decode('latin_1'))
	col_capitale = couple[5]
	marqueur_capitale = couple[6]
	# Si nous avons une capitale et un pays, pas de problème
	if marqueur_pays == 0 and marqueur_capitale == 0:
		dicoGeoDiplo[nom_pays]=nom_capitale
	# Si nous avons un pays, mais pas de capitale, c'est que le nom du pays est sur deux lignes,
	# et que nous en sommes à la première
	if marqueur_pays == 1 and marqueur_capitale == 0:
		for couple2 in listeGeoDiplo:
			num_de_ligne2 = couple2[0]
			nom_pays2 = unicode(couple2[1].decode('latin_1'))
			col_pays2 = couple2[2]
			nom_capitale2 = unicode(couple2[4].decode('latin_1'))
			# Nous cherchons la même colonne sur la ligne suivante
			if (num_de_ligne2 == num_de_ligne+1) and (col_pays2 == col_pays):
				# Retenons le nom du pays incomplet : il nous faudra le supprimer du dictionnaire final
				pays_incomplets.append(nom_pays)				
				pays_incomplets.append(nom_pays2)				
				nom_pays = nom_pays+nom_pays2
				nom_capitale = nom_capitale2
			dicoGeoDiplo[nom_pays]=nom_capitale
	# Si nous avons une capitale, mais pas de pays, c'est que le nom de la capitale est sur deux lignes
	# et que nous en sommes à la seconde
	if marqueur_pays == 0 and marqueur_capitale == 1:
		for couple3 in listeGeoDiplo:
			num_de_ligne3 = couple3[0]
			nom_capitale3 = unicode(couple3[4].decode('latin_1'))
			nom_pays3 = unicode(couple3[1].decode('latin_1'))
			col_capitale3 = couple3[5]
			# Nous cherchons la même colonne sur la ligne précédente
			if (num_de_ligne3 == num_de_ligne-1) and (col_capitale3 == col_capitale):
				nom_capitale = nom_capitale3+nom_capitale
				# Du coup, le nom de mon pays est là				
				nom_pays = nom_pays3				
		# Supprimons l'entrée incomplète				
		del dicoGeoDiplo[nom_pays]
		# Saisissons l'entrée complète dans notre dictionnaire				
		dicoGeoDiplo[nom_pays]=nom_capitale
	
for pays_a_virer in pays_incomplets:	
	del dicoGeoDiplo[pays_a_virer]

# Créons un dico pour stocker notre nom de pays façon QO, face à son nom selon la délégation à la langue française
dicoGeoDiploCanonique = dict()
	
for country, capital in dicoGeoDiplo.iteritems():
	# Si nous trouvons notre pays canonique dans le nom de notre pays Quai d'Orsay, ajoutons le à la liste	
	for pays_lib in listeTerritoiresFRcanoniques():
		# Créons une liste pour enregistrer nos trouvailles
		lunDansLAutre = list()  
		if country.find(pays_lib)>-1:
			lunDansLAutre.append(pays_lib)
		# Si notre liste contient un seul pays, nous l'ajoutons au dictionnaire
		if len(lunDansLAutre) == 1:
			dicoGeoDiploCanonique[pays_lib] = country
		# Si notre liste contient plus d'un pays, nous ajoutons la meilleure réponse au dictionnaire
		elif len(lunDansLAutre) > 1:
			# Classons la du plus petit au plus grand		
			lunDansLAutre = sorted(lunDansLAutre, key=len, reverse=True)
			dicoGeoDiploCanonique[pays_lib] = lunDansLAutre[0]

#list(set(listeGeoDiploCanonique))

print dicoGeoDiploCanonique.items()
		

#			print "Oui, dans "+country+", je trouve bien "+pays_lib
		#else:
		#	print "Zut : "+ pays_lib
	
	# Et on supprime le fichier traité



#	path_to_rm = path+fichier
#	os.remove(path_to_rm)




# On enregistre
conn.commit()
# On ferme la base
conn.close()
# On informe le fichier log
log("Table qo_names initialisée")
log("%s villes et capitales entrées dans notre base" % compteur)