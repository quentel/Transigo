#!/usr/bin/env python
# -*- coding: utf-8 -*-

##############################################################################
#                                                                            #
#                              TRANSIGO                                      #           
# Traitement normalisé et synthèse d'informations géographiquement orientées #
#                                                                            #
#                              dl_visas.py                                   #
#                                                                            #
#                        Christophe Quentel, 2015                            #
#                                                                            #
#                          transigo@quentel.com                              #
#                                                                            #
##############################################################################

# Ce script télécharge les fichiers ZIP mis à disposition par le ministère des Affaires étrangères et du Développement
# international relatif aux visas. Il trouve ces fichiers via le site Opendata du gouvernement français 
# (data.gouv.fr/fr/datasets/donnees-brutes-pour-creation-de-statistiques-sur-les-visas-delivres-par-la-france-de-2006-a-2016/), télécharge cette page pour y chercher les liens pertinents
# télécharge le fichiers .zip qui nous intéresse, ouvre ce conteneur, récupère en son sein les données elles-mêmes, enregistre
# le tout dans un répertoire "visas" et supprime tout le reste de notre disque dur.

# 1. IMPORTATION DES MODULES NECESSAIRES

import os, sys, shutil, urllib2, re, libarchive
from functions import log

log("")
log("===============================")
log("Lancement du script dl_visas.py")
log("===============================")

# 2. SCRIPT

# L'identifiant du jeu de données qui nous intéresse
id_dataset = "582d6c2bc751df3077c0bb7e"

# L'URL qui en découle
URL = "https://www.data.gouv.fr/api/1/datasets/"+id_dataset+"/"
# Création du répertoire temporaire destiné à accueillir nos fichiers avant décompression
dl_dir = "tmp"
# Si le répertoire n'existe pas, créons-le
if not os.path.exists(dl_dir):
	log("Création du répertoire temporaire destiné à accueillir nos fichiers provisoires")
	os.makedirs(dl_dir)

# On ouvre le fichier contenant le menu
response = urllib2.urlopen(URL)
# On stocke son contenu dans la variable "menu"
menu = response.read()
# Nous rechercherons des URL annoncés par le tag... "url" ! Les lignes se terminent par une accolade fermante. Nous récupérons tout ce qui se trouve entre les deux
pattern = re.compile('url(.*?)}')
# Le résultat est stocké dans la liste "results"
results = pattern.findall(menu)
#print results
# Il nous reste à ouvrir notre liste de résultat
for result in results:
	print result
	link = result[4:-1]
	# Trouvons le nom du fichier lui-même, après le dernier slash
	index = link.rfind("/")
	# Nous avons besoin du nom du fichier, pour le réenregistrer
	dl_name = link[(index+1):]
	print dl_name	
	# Si ce fichier se termine par .7z, c'est celui que nous cherchons
	if dl_name.find(".7z") > -1:
		# Nous allons l'enregistrer dans le répertoire tmp, sous son appelation douanière							
		dl_zip = dl_dir+"/"+dl_name
		try:
			with open(dl_zip,'w') as f:
				f.write(urllib2.urlopen(link).read())
				f.close()
			log("Téléchargement du fichier "+dl_zip)
			# Nous décompactons le dossier
			path_unzipped = "decompresse/"									
			# Si le répertoire n'existe pas, on le crée
			if not os.path.exists(path_unzipped):
				os.makedirs(path_unzipped)
			try:			
				print dl_zip				
				libarchive.extract_file(dl_zip)				
				#with libarchive.public.file_reader(dl_zip) as e:
				#	for entry in e:
				#		with open(path_unzipped + str(entry), 'wb') as f:
				#			for block in entry.get_blocks():
				#				f.write(block)
				log("Décompression du fichier "+dl_zip)


#				with zipfile.ZipFile(dl_zip, "r") as z:
#					z.extractall(path_unzipped)
					
				# Ces zip contiennent soit un répertoire, soit un fichier. Vérifions-le
#				try:
#					mon_chemin = path_unzipped
#					contenu = os.listdir(mon_chemin)
#					for fichier in contenu:
#						# Si leur nom contient un .txt, c'est un fichier
#						if fichier.find(".txt") > -1:
#							# On retrouve son chemin
#							original_file = path_unzipped+fichier
#							# On s'apprête à le copier dans notre répertoire définitif, avec son nom
#							copy_name = dl_name.replace("zip","txt")
#							copy_name = str(count)+"-"+copy_name
#							copy_file = "datas/visas/"+copy_name
#							# Si le répertoire n'existe pas, créons-le
#							copy_dir = os.path.dirname(copy_file)
#							if not os.path.exists(copy_dir):
#								os.makedirs(copy_dir)
							# Et on copie le fichier dans le bon répertoire
#							try:
								# On copie le bon fichier dans le bon répertoire													
#								shutil.copyfile(original_file,copy_file)
#								# Et on supprime le fichier original pour éviter les conflits ultérieurs
#								os.remove(original_file)
#							except Exception, e:
#								log("===Echec de la copie du fichier %s" % e)
#								pass
#				except:
#					pass									
			except Exception, e:
				log("=== Erreur lors de la décompression du fichier. Il est possible que le module Unzip ne supporte pas ce format : %s" % e)
				print "Erreur décomp"				
				pass							
		except Exception, e:	
			log("===Echec du téléchargement d'un fichier .7z : %s" % e)
			pass	
	else:
		print "Ce n'est pas le bon fichier"	
																

#if os.path.exists("/tmp"):
#	shutil.rmtree('tmp')
#if os.path.exists("decompresse"):
#	shutil.rmtree('decompresse')
